﻿namespace bgTeam.KitVlgDocs.Delo
{
    public static class SearchVocabParams
    {
        public const string Name = "Name";

        public const string Post = "Post";

        public const string Surname = "Surname";
    }
}

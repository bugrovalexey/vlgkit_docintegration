﻿namespace bgTeam.KitVlgDocs.Delo
{

    public static class SearchTablesParamValues
    {
        public const string IsNull = "IsNull";

        public const string IsNotNull = "IsNotNull";

        public static class Result
        {
            public const string Doc = "Doc";

            public const string Resol = "Resol";

            public const string Project = "Project";
        }

        public static class DocKind
        {
            public const string In = "In";

            public const string Let = "Let";

            public const string Both = "Both";

            public const string Out = "Out";
        }
    }
}

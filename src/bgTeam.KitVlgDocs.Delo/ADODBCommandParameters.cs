﻿namespace bgTeam.KitVlgDocs.Delo
{
    /// <summary>
    /// .
    /// </summary>
    public class ADODBCommandParameters : DynamicObjectWrapper
    {
        public ADODBCommandParameters(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="parameter"></param>
        public void Append(ADODBCommandParameter parameter)
            => Exec((x, p1) => x.Append(p1.Object), parameter);
    }
}

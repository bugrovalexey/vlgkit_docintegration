﻿namespace bgTeam.KitVlgDocs.Delo
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDeloError
    {
        /// <summary>
        /// Код ошибки.
        /// </summary>
        int ErrCode { get; }

        /// <summary>
        /// Описание ошибки.
        /// </summary>
        string ErrText { get; }
    }
}

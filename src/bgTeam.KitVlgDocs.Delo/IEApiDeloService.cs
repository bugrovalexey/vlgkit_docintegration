﻿namespace bgTeam.KitVlgDocs.Delo
{
    using System;

    public interface IEApiDeloService
    {
        /// <summary>
        /// Хранимая процедура создает новую РК. Новая РК может быть создана как связанная с другой РК документа, либо РК проекта документа, при этом часть информации из связанной РК (РКПД) будет перенесена в новую РК.
        /// Чтобы создать РК из другой РК, аргумент aIsnLinkingDoc должен содержать идентификатор связанного документа.
        /// Чтобы создать РК из РКПД, аргумент aIsnLinkingPrj должен содержать идентификатор исходной РКПД.
        /// В обоих случаях аргумент aCopyShablon содержит битовую маску, указывающую, какую информацию из исходной РК (РКПД) нужно перенести в новую РК. Иначе аргумент не используется.
        /// При успешном выполнении, процедура возвращает идентификатор новой РК в аргументе aIsn.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Возвращаемое значение, идентификатор зарегистрированной РК.
        /// </param>
        /// <param name="aDueCard">
        /// Код due картотеки регистрации.
        /// </param>
        /// <param name="aIsnCab">
        /// Идентификатор кабинета регистрации, в соответствие со справочником «Кабинеты». Значение 0 используется для «безкабинетной» регистрации.
        /// </param>
        /// <param name="aDueDocgroup">
        /// Код due группы документов, в соответствие со справочником «Группы документов».
        /// </param>
        /// <param name="aOrderNum">
        /// Порядковый  номер.
        /// Если в шаблоне номера группы документов есть элемент порядковый номер («{2}» или «{@2}»), то значение этого параметра следует вычислять с помощью процедуры reserve_num.
        /// Передача иного значения приведёт к неправильному формированию регистрационного номера!
        /// Если же в шаблоне номера нет этого элемента, то нужно передавать значение 0.
        /// </param>
        /// <param name="aFreeNum">
        /// Регистрационный номер. Если шаблон номера документа равен «{@}» или «{@2}», то в РК подставляется значение aFreeNum, иначе номер формируется автоматически в соответствие с шаблоном номера, указанном в параметрах группы документов, а значение данного аргумента не используется.
        /// </param>
        /// <param name="aDocDate">
        /// Дата регистрации РК.
        /// </param>
        /// <param name="aSecurlevel">
        /// Идентификатор грифа доступа. В соответствие со справочником «Грифы доступа».
        /// </param>
        /// <param name="aConsists">
        /// Состав.
        /// </param>
        /// <param name="aSpecimen">
        /// Номера  экземпляров.
        /// </param>
        /// <param name="aPlanDate">
        /// Плановая  дата исполнения документа.
        /// </param>
        /// <param name="aFactDate">
        /// Фактическая дата исполнения документа.
        /// </param>
        /// <param name="aControlState">
        /// Флаг контрольности.
        /// </param>
        /// <param name="aAnnotat">
        /// Содержание.
        /// </param>
        /// <param name="aNote">
        /// Примечание.
        /// </param>
        /// <param name="aDuePersonWhos">
        /// Коды due должностных лиц, кому адресован входящий документ, в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aIsnDelivery">
        /// Идентификатор вида доставки входящего документа, в соответствие со справочником «Виды доставки».
        /// </param>
        /// <param name="aDueOrganiz">
        /// Код due организации - корреспондента входящего документа, в соответствие со справочником «Список организаций».
        /// </param>
        /// <param name="aIsnContact">
        /// Идентификатор контакта корреспондента входящего документа для входящих документов. Если передано null, то контактом корреспондента будет являться контакт организации.
        /// </param>
        /// <param name="aCorrespNum">
        /// Исходящий  номер входящего документа.
        /// </param>
        /// <param name="aCorrespDate">
        /// Исходящая  дата входящего документа.
        /// </param>
        /// <param name="aCorrespSign">
        /// Лицо, подписавшее  входящий документ.
        /// </param>
        /// <param name="aIsnCitizen">
        /// Идентификатор гражданина – корреспондента письма, в соответствие со справочником «Граждане».
        /// </param>
        /// <param name="aIsCollective">
        /// Признак  коллективного письма.
        /// </param>
        /// <param name="aIsAnonim">
        /// Признак  анонимного письма.
        /// </param>
        /// <param name="aSigns">
        /// Список подписавших исходящий документ, в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aDuePersonExe">
        /// Код due исполнителя исходящего документа, в соответствие со справочником «Подразделения».
        /// Начиная с версии 11.0.3 - список исполнителей.
        /// </param>
        /// <param name="aIsnNomenc">
        /// Идентификатор дела в номенклатуе дел (параметр нужен только для групп документов, в шаблон номерообразования которых, входит индекс дела по номенклатуре), в соответствие со справочником «Номенклатура дел».
        /// </param>
        /// <param name="aNothardcopy">
        /// Флаг "без досылки бумажного экземпляра".
        /// </param>
        /// <param name="aCito">
        /// Флаг "срочно".
        /// </param>
        /// <param name="aIsnLinkingDoc">
        /// Идентификатор связанной РК (идентификатор РК, с которой делается связанная РК). Если не связанная, то null.
        /// </param>
        /// <param name="aIsnLinkingPrj">
        /// Идентификатор регистрируемого РКПД (в случае  регистрации связанной РК из проекта). Если не на основе РКПД, то null.
        /// </param>
        /// <param name="aIsnClLink">
        /// Идентификатор типа связки, в соответствие со справочником «Типы связок».
        /// </param>
        /// <param name="aCopyShablon">
        /// Битовая  маска для копирования реквизитов, обрабатывается при регистрации связанной РК, если указан aIsnLinkingDoc или aIsnLinkingPrj:
        /// 1.Содержание
        /// 2.Рубрики (РК или РКПД)
        /// 3.Доступ
        /// 4.Подписавшие исходящий документ
        /// 5.Исполнитель
        /// 6.Визы
        /// 7.Адресаты
        /// 8.Корреспонденты
        /// 9.Кому
        /// 10.Сопроводительные документы
        /// 11.Доставка
        /// 12.Дата регистрации
        /// 13.Состав
        /// 14.Файлы
        /// 15.Примечание
        /// 16.Соисполнители (РК или РКПД)
        /// 17.Допреквизиты (РК или РКПД)
        /// 18.Связки(РК или РКПД)
        /// (1 – копировать реквизит, 0 – нет, пример: 1110001111001010).
        /// Только для связанного документа и на основе РКПД.
        /// </param>
        /// <param name="aVisas">
        /// Список лиц, завизировавших документ, в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aEDocument">
        /// Флаг "Оригинал в электронном виде". Начиная с версии 11.0.3.
        /// </param>
        /// <param name="aSends">
        /// Заполнение адресатов РК. Если передано значение Null – список будет взят из реквизитов "по умолчанию" данной группы документов, если он определен. Если передано слово "EMPTY" – список останется не заполненным. (с версии 13.1)
        /// </param>
        /// <param name="askipcopy_ref_file_isns">
        /// Список файлов не подлежащих копированию из связанной РК/РКПД при установленном флаге № 14 в параметре aCopyShablon. (с версии 13.1)
        /// </param>
        /// <param name="aIsnLinkTranparent">
        /// Флаг «прозначности» создаваемой связки при регистрации связанной РК. Если передано значение Null – будет взято значение «по умолчанию» из справочника связок. (с версии 13.1)
        /// </param>
        /// <param name="aIsnLinkTranparentPare">
        /// Флаг «прозначности» создаваемой обратной связки при регистрации связанной РК. Если передано значение Null – будет взято значение «по умолчанию» из справочника связок. (с версии 13.1)
        /// </param>
        /// <param name="aTelNum">
        /// Почтовый номер для РК входящих и писем граждан. (с версии 13.2).
        /// </param>
        void AddRC(IDeloHead oHead, ref int? aIsn, string aDueCard, int aIsnCab, string aDueDocgroup, int aOrderNum, string aFreeNum, DateTime aDocDate, int aSecurlevel, string aConsists, string aSpecimen, DateTime? aPlanDate, DateTime? aFactDate, int? aControlState, string aAnnotat, string aNote, string aDuePersonWhos, int? aIsnDelivery, string aDueOrganiz, int? aIsnContact, string aCorrespNum, DateTime? aCorrespDate, string aCorrespSign, int? aIsnCitizen, int? aIsCollective, int? aIsAnonim, string aSigns, string aDuePersonExe, int? aIsnNomenc, int? aNothardcopy, int? aCito, int? aIsnLinkingDoc, int? aIsnLinkingPrj, int? aIsnClLink, string aCopyShablon, string aVisas, int? aEDocument, string aSends, string askipcopy_ref_file_isns, int? aIsnLinkTranparent, int? aIsnLinkTranparentPare, string aTelNum);

        /// <summary>
        /// Хранимая процедура изменяет значения атрибутов РК с указанным Isn. При этом регистрационный номер не генерируется, а подставляется как есть и, если требуется, проверяется на уникальность.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор зарегистрированной РК.
        /// </param>
        /// <param name="aDueCard">
        /// Параметр устарел, не используется.
        /// </param>
        /// <param name="aIsnCab">
        /// Параметр устарел, не используется.
        /// </param>
        /// <param name="aFreeNum">
        /// Регистрационный номер.
        /// </param>
        /// <param name="aDocDate">
        /// Дата регистрации РК.
        /// </param>
        /// <param name="aSecurlevel">
        /// Идентификатор грифа доступа. В соответствие со справочником «Грифы доступа».
        /// </param>
        /// <param name="aConsists">
        /// Состав.
        /// </param>
        /// <param name="aSpecimen">
        /// Номера экземпляров.
        /// </param>
        /// <param name="aPlanDate">
        /// Плановая  дата исполнения документа.
        /// </param>
        /// <param name="aFactDate">
        /// Фактическая дата исполнения документа.
        /// </param>
        /// <param name="aControlState">
        /// Флаг контрольности.
        /// </param>
        /// <param name="aAnnotat">
        /// Содержание.
        /// </param>
        /// <param name="aNote">
        /// Примечание.
        /// </param>
        /// <param name="aDuePersonWho">
        /// Параметр устарел, не используется.
        /// </param>
        /// <param name="aIsnDelivery">
        /// Идентификатор вида доставки входящего документа (обязательно для входящих документов, писем и обращений граждан), в соответствие со справочником «Виды доставки».
        /// </param>
        /// <param name="aIsCollective">
        /// Признак  коллективного письма.
        /// </param>
        /// <param name="aIsAnonim">
        /// Признак  анонимного письма.
        /// </param>
        /// <param name="aDuePersonSign">
        /// Параметр устарел, не используется.
        /// </param>
        /// <param name="aDuePersonExe">
        /// Начиная с версии 8.11 параметр устарел, не используется.
        /// </param>
        /// <param name="aNothardcopy">
        /// Флаг «без досылки бум. Экземпляра».
        /// </param>
        /// <param name="aCito">
        /// Флаг «срочно».
        /// </param>
        /// <param name="aOrderNum">
        /// Порядковый номер.
        /// Начиная с версии 11.0.
        /// </param>
        /// <param name="aEDocument">
        /// Флаг "Оригинал в электронном виде". Начиная с версии 11.0.3.
        /// </param>
        /// <param name="aTelNum">
        /// Почтовый номер для РК входящих и писем граждан. (с версии 13.2).
        /// </param>
        void EditRC(IDeloHead oHead, int aIsn, string aDueCard, int? aIsnCab, string aFreeNum, DateTime? aDocDate, int aSecurlevel, string aConsists, string aSpecimen, DateTime? aPlanDate, DateTime? aFactDate, int? aControlState, string aAnnotat, string aNote, string aDuePersonWho, int? aIsnDelivery, int? aIsCollective, int? aIsAnonim, string aDuePersonSign, string aDuePersonExe, int? aNothardcopy, int? aCito, int? aOrderNum, int? aEDocument, string aTelNum);

        /// <summary>
        /// Процедура выполняет прикрепление файла к объекту БД либо изменение описания и/или содержимого прикрепленного файла.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="action">
        /// Выполняемое действие.
        /// Если параметр равен 2, то после изменения также удаляется содержимое файла из БД, и его содержимое нужно заново скопировать при помощи процедуры add_chunk.
        /// </param>
        /// <param name="aISN_REF_FILE">
        /// При добавлении нового файла – null, возвращаемое значение: идентификатор файла.
        /// При изменении или удалении файла – идентификатор существующего файла.
        /// </param>
        /// <param name="aISN_RC">
        /// Идентификатор объекта, к которому прикреплен файл. Тип объекта определяется параметром akind_doc.
        /// </param>
        /// <param name="aKind_Doc">
        /// Вид объекта.
        /// </param>
        /// <param name="aDescription">
        /// Комментарий.
        /// </param>
        /// <param name="aCategory">
        /// Категория.
        /// </param>
        /// <param name="aSecur">
        /// Идентификатор грифа доступа, в соответствие со справочником «Грифы доступа».
        /// Если aSecur = null при том, что action in (2, 3), то значение берется от файла по параметру aISN_REF_FILE.
        /// Если aSecur = null при том, что action = 1 и aKind_Doc in (1, 2, 3), то значение берется от поля securlevel документа, к которому добавляется файл: aISN_RC = doc.rc.isn_doc.
        /// Для aKind_Doc not in (1, 2, 3)  не используется
        /// </param>
        /// <param name="aLockFlag">
        /// Флаг защиты от редактирования.
        /// Если aLockFlag = null при том что action = 1, то значение берется от параметра пользователя ‘FILE_LOCK’. (Если значение параметра = 1, то устанавливается значение =1, иначе 0).
        /// Если aLockFlag = null при том что action != 1, то устанавливается значение = 0.
        /// </param>
        /// <param name="aFileAccessDues">
        /// Список кодов due должностных лиц и подразделений, которым должен быть доступен для просмотра файл, в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aFilename">
        /// Полный путь к прикрепляемому файлу.
        /// </param>
        /// <param name="aDontDelFlag">
        /// Признак запрета удаления файла.
        /// </param>
        void SaveFileWF(IDeloHead oHead, int action, int? aISN_REF_FILE, int aISN_RC, int aKind_Doc, string aDescription, string aCategory, int? aSecur, int? aLockFlag, string aFileAccessDues, string aFilename, int? aDontDelFlag);

        /// <summary>
        /// Процедура изменяет значение допреквизита.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnOwner">Код владельца, для которого нужно изменить значение реквизита.</param>
        /// <param name="aArName">api_name допреквизита, значение которого нужно изменить.</param>
        /// <param name="aCard">Код due картотеки, в контексте которой выполняется обновление параметров.</param>
        /// <param name="aValue">Новое значение дополнительного реквизита.</param>
        void EditAr(IDeloHead oHead, int aIsnOwner, string aArName, string aCard, string aValue);

        /// <summary>
        /// Процедура резервирует порядковый номер РК.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aDueDocgroup">Код due группы документов. </param>
        /// <param name="aYear">Год в счетчике номерообразования РК.</param>
        /// <param name="aDueCard">Код due картотеки, в контексте которой выполняется обновление параметров.</param>
        /// <param name="aSessionId">Идентификатор сессии.</param>
        /// <param name="aOrderNum">Порядковый номер.</param>
        /// <param name="aFreeNum">Регистрационный номер.</param>
        void ReserveNum(IDeloHead oHead, string aDueDocgroup, int aYear, string aDueCard, string aSessionId, ref int? aOrderNum, ref string aFreeNum);

        /// <summary>
        /// Процедура возвращает порядковый номер РК.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aDueDocgroup">Код due группы документов. </param>
        /// <param name="aYear">Год в счетчике номерообразования РК.</param>
        /// <param name="aFreeNum">Код due картотеки, в контексте которой выполняется обновление параметров.</param>
        /// <param name="aOrderNum">Порядковый номер.</param>
        void ReturnNum(IDeloHead oHead, string aDueDocgroup, int aYear, int aOrderNum, ref string aFreeNum);

        /// <summary>
        /// Добавление рубрик документов.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnDoc">Идентификатор РК.</param>
        /// <param name="codes">Список кодов due, в соответствие со справочником "Рубрикатор".</param>
        /// <param name="acard">Код due текущей картотеки.</param>
        void AddRubric(IDeloHead oHead, int aIsnDoc, string codes, string acard);

        /// <summary>
        /// .
        /// </summary>
        /// <param name="oHead"></param>
        /// <param name="a_isn_ref_file"></param>
        /// <param name="a_eds_body"></param>
        /// <param name="a_isn_sign_kind"></param>
        /// <param name="a_due_person"></param>
        /// <param name="a_flag"></param>
        /// <param name="a_eds_body_blob"></param>
        /// <param name="a_certificate_owner"></param>
        /// <param name="a_signing_date"></param>
        void AddFileEds(IDeloHead oHead, int a_isn_ref_file, string a_eds_body, int? a_isn_sign_kind, string a_due_person, int? a_flag, byte[] a_eds_body_blob, string a_certificate_owner, DateTime? a_signing_date);

        /// <summary>
        /// .
        /// </summary>
        /// <param name="oHead"></param>
        /// <param name="aIsnRefFile"></param>
        /// <param name="aEdsBody"></param>
        /// <param name="a_eds_body_blob"></param>
        /// <param name="a_isn_sign_kind"></param>
        /// <param name="a_certificate_owner"></param>
        /// <param name="a_eds_body_blob"></param>
        /// <param name="a_signing_date"></param>
        /// <param name="a_sign_text"></param>
        void ImportFileEds(IDeloHead oHead, int aIsnRefFile, string aEdsBody, byte[] a_eds_body_blob, int? a_isn_sign_kind, string a_certificate_owner, DateTime? a_signing_date, string a_sign_text);

        /// <summary>
        /// .
        /// </summary>
        /// <param name="oHead"></param>
        /// <param name="a_isn_doc"></param>
        /// <param name="a_isn_ref_send"></param>
        /// <param name="a_card"></param>
        /// <param name="a_cab"></param>
        /// <param name="a_isn_contact"></param>
        /// <param name="a_send_date"></param>
        /// <param name="a_send_person"></param>
        /// <param name="a_isn_delivery"></param>
        /// <param name="a_sending_type"></param>
        /// <param name="a_note"></param>
        /// <param name="a_reg_n"></param>
        /// <param name="a_reg_date"></param>
        /// <param name="a_answer"></param>
        void EditOuterSend(IDeloHead oHead, int a_isn_doc, int a_isn_ref_send, string a_card, int a_cab, int a_isn_contact, DateTime? a_send_date, string a_send_person, int a_isn_delivery, int a_sending_type, string a_note, string a_reg_n, DateTime? a_reg_date, int a_answer);
    }
}
﻿namespace bgTeam.KitVlgDocs.Delo
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDeloSearchVocab : IDeloCriterion
    {
        /// <summary>
        /// Количество выбираемых элементов из справочника. One – выбор одного элемента, осуществляется пользователем. Many – выбор нескольких элементов, осуществляется пользователем. All – автоматический выбор всех элементов справочника, удовлетворяющих остальным условиям отбора. OneLevel - автоматический выбор элементов справочника только первого уровня подчиненности.
        /// </summary>
        string Select { get; set; }

        /// <summary>
        /// Разрешается ли отбирать и показывать  логически удаленные элементы справочника. Yes - выбирать удаленные значения. No - не выбирать удаленные значения.
        /// </summary>
        string Deleted { get; set; }
    }
}

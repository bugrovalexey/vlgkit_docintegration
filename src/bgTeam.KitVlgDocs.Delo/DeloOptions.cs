﻿namespace bgTeam.KitVlgDocs.Delo
{
    using System;

    public class DeloOptions
    {
        public string Host { get; set; }

        public string Owner { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string Settings { get; set; }

        public string Delivery { get; set; }

        public string Securlevel { get; set; }

        public TimeSpan ExportTimeShift { get; set; }

        public string ExportDocgroup { get; set; }

        public string ImportDocgroup { get; set; }

        public string Organiz { get; set; }

        public string Card { get; set; }
    }
}

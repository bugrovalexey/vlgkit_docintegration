﻿namespace bgTeam.KitVlgDocs.Delo
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDeloResultSet : IDeloError
    {

        IDeloCriterion Source { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Delo
{

    public interface IDeloEntity : IDeloError
    {
        /// <summary>
        /// Системный номер записи в справочнике.
        /// </summary>
        int Isn { get; }
    }
}

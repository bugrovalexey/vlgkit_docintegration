﻿namespace bgTeam.KitVlgDocs.Delo
{
    using bgTeam.KitVlgDocs.Delo.Impl;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddDeloEApi(this IServiceCollection services, IConfiguration deloConfiguration)
        {
            services
                .Configure<DeloOptions>(deloConfiguration)
                .AddSingleton<IDeloHead, DeloHead>()
                .AddSingleton<IDeloService, DeloService>()
                .AddSingleton<IEApiDeloService, EApiDeloService>();

            return services;
        }
    }
}

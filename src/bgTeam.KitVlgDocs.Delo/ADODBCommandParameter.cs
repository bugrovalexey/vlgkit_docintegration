﻿namespace bgTeam.KitVlgDocs.Delo
{
    /// <summary>
    /// .
    /// </summary>
    public class ADODBCommandParameter : DynamicObjectWrapper
    {
        public ADODBCommandParameter(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// .
        /// </summary>
        public object Value => Exec(x => x.Value);
    }
}

﻿namespace bgTeam.KitVlgDocs.Delo
{
    using Microsoft.CSharp.RuntimeBinder;
    using System;

    /// <summary>
    /// .
    /// </summary>
    public abstract class DynamicObjectWrapper
    {
        protected dynamic _object;

        protected DynamicObjectWrapper()
        {
        }

        protected DynamicObjectWrapper(dynamic @object)
        {
            _object = @object;
        }

        internal dynamic Object => _object;

        protected virtual void EnsureIsValid() { }

        protected void Exe(Action<dynamic> action)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            EnsureIsValid();

            action.Invoke(_object);
        }

        protected void Exe<U>(Action<dynamic, U> action, U arg)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            EnsureIsValid();

            action.Invoke(_object, arg);
        }

        protected void Exe<U1, U2>(Action<dynamic, U1, U2> action, U1 arg1, U2 arg2)
        {
            if (action == null)
            {
                throw new ArgumentNullException(nameof(action));
            }

            EnsureIsValid();

            action.Invoke(_object, arg1, arg2);
        }

        protected T Exec<T>(Func<dynamic, T> func)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            EnsureIsValid();

            return func.Invoke(_object);
        }

        protected T Exec<T, U>(Func<dynamic, U, T> func, U arg)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            EnsureIsValid();

            return func.Invoke(_object, arg);
        }

        protected T Exec<T, U1, U2>(Func<dynamic, U1, U2, T> func, U1 arg1, U2 arg2)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            EnsureIsValid();

            return func.Invoke(_object, arg1, arg2);
        }

        protected T Exec<T, U1, U2, U3>(Func<dynamic, U1, U2, U3, T> func, U1 arg1, U2 arg2, U3 arg3)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            EnsureIsValid();

            return func.Invoke(_object, arg1, arg2, arg3);
        }

        protected T Exec<T, U1, U2, U3, U4>(Func<dynamic, U1, U2, U3, U4, T> func, U1 arg1, U2 arg2, U3 arg3, U4 arg4)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            EnsureIsValid();

            return func.Invoke(_object, arg1, arg2, arg3, arg4);
        }

        protected bool TryExec<T>(Func<dynamic, T> func, out T result)
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            EnsureIsValid();

            try
            {
                result = func.Invoke(_object);
                return true;
            }
            catch (Exception)
            {

                result = default;
                return false;
            }
        }

        protected T Wrap<T>(Func<dynamic, dynamic> func)
            where T : DynamicObjectWrapper
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            EnsureIsValid();

            var result = func.Invoke(_object);

            return IsNull(result) ? null : (T)Activator.CreateInstance(typeof(T), result);
        }

        protected T Wrap<T, U>(Func<dynamic, U, dynamic> func, U arg)
            where T : DynamicObjectWrapper
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            EnsureIsValid();

            var result = func.Invoke(_object, arg);

            return IsNull(result) ? null : (T)Activator.CreateInstance(typeof(T), result);
        }

        protected T Wrap<T, U1, U2, U3, U4>(Func<dynamic, U1, U2, U3, U4, dynamic> func, U1 arg1, U2 arg2, U3 arg3, U4 arg4)
            where T : DynamicObjectWrapper
        {
            if (func == null)
            {
                throw new ArgumentNullException(nameof(func));
            }

            EnsureIsValid();

            var result = func.Invoke(_object, arg1, arg2, arg3, arg4);

            return IsNull(result) ? null : (T)Activator.CreateInstance(typeof(T), result);
        }

        private bool IsNull(dynamic value)
        {
            return value == null
                || (value is object obj && (Convert.DBNull.Equals(obj) || DBNull.Value.Equals(obj)))
                || IsEmptyObject();

            bool IsEmptyObject()
            {
                try
                {
                    return (bool)(value.ErrCode == -100);
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Delo
{

    public static class SearchVocab
    {
        public const string Rubric = "Rubric";

        public const string Security = "Security";

        public const string DocGroup = "DocGroup";

        public const string CardIndex = "CardIndex";

        public const string Organiz = "Organiz";

        public const string Department = "Department";

        public const string Delivery = "Delivery";
    }
}

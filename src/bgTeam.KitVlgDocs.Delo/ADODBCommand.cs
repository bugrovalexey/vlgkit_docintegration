﻿namespace bgTeam.KitVlgDocs.Delo
{
    /// <summary>
    /// .
    /// </summary>
    public class ADODBCommand : DynamicObjectWrapper
    {
        public ADODBCommand(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// .
        /// </summary>
        public ADODBCommandParameters Parameters => Wrap<ADODBCommandParameters>(x => x.Parameters);

        /// <summary>
        /// .
        /// </summary>
        public string CommandText
        {
            get => Exec(x => (string)x.CommandText);
            set => Exec((x, p1) => x.CommandText = p1, value);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="direction"></param>
        /// <param name="maxLength"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public ADODBCommandParameter CreateParameter(string name, int type, int direction, int maxLength, object value)
            => Wrap<ADODBCommandParameter, int, int, int, object>((x, p1, p2, p3, p4) => x.CreateParameter(name, p1, p2, p3, (dynamic)p4), type, direction, maxLength, value);
    }
}

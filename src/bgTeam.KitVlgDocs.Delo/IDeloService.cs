﻿namespace bgTeam.KitVlgDocs.Delo
{
    using bgTeam.KitVlgDocs.Domain.Dto;
    using System;
    using System.Collections.Generic;

    public interface IDeloService
    {
        IEnumerable<DeloIncomingDocumentDto> LoadDocumentsForImport();

        void SetImportState(int rcIsn, string regNum, DateTime? regDate, DateTime? exporDateTime);

        void SetImportStateNote(int rcIsn, bool isSuccessful);

        DeloDocumentExportStatusDto ExportDocument(DeloOutgoingDocumentDto document);
    }
}

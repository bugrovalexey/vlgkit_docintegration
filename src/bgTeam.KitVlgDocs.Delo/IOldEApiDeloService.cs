﻿namespace bgTeam.KitVlgDocs.Delo
{
    using System;

    public interface IOldEApiDeloService
    {
        /// <summary>
        /// Хранимая процедура создает новую РК. Новая РК может быть создана как связанная с другой РК документа, либо РК проекта документа, при этом часть информации из связанной РК (РКПД) будет перенесена в новую РК.
        /// Чтобы создать РК из другой РК, аргумент aIsnLinkingDoc должен содержать идентификатор связанного документа.
        /// Чтобы создать РК из РКПД, аргумент aIsnLinkingPrj должен содержать идентификатор исходной РКПД.
        /// В обоих случаях аргумент aCopyShablon содержит битовую маску, указывающую, какую информацию из исходной РК (РКПД) нужно перенести в новую РК. Иначе аргумент не используется.
        /// При успешном выполнении, процедура возвращает идентификатор новой РК в аргументе aIsn.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Возвращаемое значение, идентификатор зарегистрированной РК.
        /// </param>
        /// <param name="aDueCard">
        /// Код due картотеки регистрации.
        /// </param>
        /// <param name="aIsnCab">
        /// Идентификатор кабинета регистрации, в соответствие со справочником «Кабинеты». Значение 0 используется для «безкабинетной» регистрации.
        /// </param>
        /// <param name="aDueDocgroup">
        /// Код due группы документов, в соответствие со справочником «Группы документов».
        /// </param>
        /// <param name="aOrderNum">
        /// Порядковый  номер.
        /// Если в шаблоне номера группы документов есть элемент порядковый номер («{2}» или «{@2}»), то значение этого параметра следует вычислять с помощью процедуры reserve_num.
        /// Передача иного значения приведёт к неправильному формированию регистрационного номера!
        /// Если же в шаблоне номера нет этого элемента, то нужно передавать значение 0.
        /// </param>
        /// <param name="aFreeNum">
        /// Регистрационный номер. Если шаблон номера документа равен «{@}» или «{@2}», то в РК подставляется значение aFreeNum, иначе номер формируется автоматически в соответствие с шаблоном номера, указанном в параметрах группы документов, а значение данного аргумента не используется.
        /// </param>
        /// <param name="aDocDate">
        /// Дата регистрации РК.
        /// </param>
        /// <param name="aSecurlevel">
        /// Идентификатор грифа доступа. В соответствие со справочником «Грифы доступа».
        /// </param>
        /// <param name="aConsists">
        /// Состав.
        /// </param>
        /// <param name="aSpecimen">
        /// Номера  экземпляров.
        /// </param>
        /// <param name="aPlanDate">
        /// Плановая  дата исполнения документа.
        /// </param>
        /// <param name="aFactDate">
        /// Фактическая дата исполнения документа.
        /// </param>
        /// <param name="aControlState">
        /// Флаг контрольности.
        /// </param>
        /// <param name="aAnnotat">
        /// Содержание.
        /// </param>
        /// <param name="aNote">
        /// Примечание.
        /// </param>
        /// <param name="aDuePersonWhos">
        /// Коды due должностных лиц, кому адресован входящий документ, в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aIsnDelivery">
        /// Идентификатор вида доставки входящего документа, в соответствие со справочником «Виды доставки».
        /// </param>
        /// <param name="aDueOrganiz">
        /// Код due организации - корреспондента входящего документа, в соответствие со справочником «Список организаций».
        /// </param>
        /// <param name="aIsnContact">
        /// Идентификатор контакта корреспондента входящего документа для входящих документов. Если передано null, то контактом корреспондента будет являться контакт организации.
        /// </param>
        /// <param name="aCorrespNum">
        /// Исходящий  номер входящего документа.
        /// </param>
        /// <param name="aCorrespDate">
        /// Исходящая  дата входящего документа.
        /// </param>
        /// <param name="aCorrespSign">
        /// Лицо, подписавшее  входящий документ.
        /// </param>
        /// <param name="aIsnCitizen">
        /// Идентификатор гражданина – корреспондента письма, в соответствие со справочником «Граждане».
        /// </param>
        /// <param name="aIsCollective">
        /// Признак  коллективного письма.
        /// </param>
        /// <param name="aIsAnonim">
        /// Признак  анонимного письма.
        /// </param>
        /// <param name="aSigns">
        /// Список подписавших исходящий документ, в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aDuePersonExe">
        /// Код due исполнителя исходящего документа, в соответствие со справочником «Подразделения».
        /// Начиная с версии 11.0.3 - список исполнителей.
        /// </param>
        /// <param name="aIsnNomenc">
        /// Идентификатор дела в номенклатуе дел (параметр нужен только для групп документов, в шаблон номерообразования которых, входит индекс дела по номенклатуре), в соответствие со справочником «Номенклатура дел».
        /// </param>
        /// <param name="aNothardcopy">
        /// Флаг "без досылки бумажного экземпляра".
        /// </param>
        /// <param name="aCito">
        /// Флаг "срочно".
        /// </param>
        /// <param name="aIsnLinkingDoc">
        /// Идентификатор связанной РК (идентификатор РК, с которой делается связанная РК). Если не связанная, то null.
        /// </param>
        /// <param name="aIsnLinkingPrj">
        /// Идентификатор регистрируемого РКПД (в случае  регистрации связанной РК из проекта). Если не на основе РКПД, то null.
        /// </param>
        /// <param name="aIsnClLink">
        /// Идентификатор типа связки, в соответствие со справочником «Типы связок».
        /// </param>
        /// <param name="aCopyShablon">
        /// Битовая  маска для копирования реквизитов, обрабатывается при регистрации связанной РК, если указан aIsnLinkingDoc или aIsnLinkingPrj:
        /// 1.Содержание
        /// 2.Рубрики (РК или РКПД)
        /// 3.Доступ
        /// 4.Подписавшие исходящий документ
        /// 5.Исполнитель
        /// 6.Визы
        /// 7.Адресаты
        /// 8.Корреспонденты
        /// 9.Кому
        /// 10.Сопроводительные документы
        /// 11.Доставка
        /// 12.Дата регистрации
        /// 13.Состав
        /// 14.Файлы
        /// 15.Примечание
        /// 16.Соисполнители (РК или РКПД)
        /// 17.Допреквизиты (РК или РКПД)
        /// 18.Связки(РК или РКПД)
        /// (1 – копировать реквизит, 0 – нет, пример: 1110001111001010).
        /// Только для связанного документа и на основе РКПД.
        /// </param>
        /// <param name="aVisas">
        /// Список лиц, завизировавших документ, в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aEDocument">
        /// Флаг "Оригинал в электронном виде". Начиная с версии 11.0.3.
        /// </param>
        /// <param name="aSends">
        /// Заполнение адресатов РК. Если передано значение Null – список будет взят из реквизитов "по умолчанию" данной группы документов, если он определен. Если передано слово "EMPTY" – список останется не заполненным. (с версии 13.1)
        /// </param>
        /// <param name="askipcopy_ref_file_isns">
        /// Список файлов не подлежащих копированию из связанной РК/РКПД при установленном флаге № 14 в параметре aCopyShablon. (с версии 13.1)
        /// </param>
        /// <param name="aIsnLinkTranparent">
        /// Флаг «прозначности» создаваемой связки при регистрации связанной РК. Если передано значение Null – будет взято значение «по умолчанию» из справочника связок. (с версии 13.1)
        /// </param>
        /// <param name="aIsnLinkTranparentPare">
        /// Флаг «прозначности» создаваемой обратной связки при регистрации связанной РК. Если передано значение Null – будет взято значение «по умолчанию» из справочника связок. (с версии 13.1)
        /// </param>
        /// <param name="aTelNum">
        /// Почтовый номер для РК входящих и писем граждан. (с версии 13.2)
        /// </param>
        void add_rc(dynamic oHead, ref int? aIsn, string aDueCard, int aIsnCab, string aDueDocgroup, int aOrderNum, string aFreeNum, DateTime aDocDate, int aSecurlevel, string aConsists, string aSpecimen, DateTime? aPlanDate, DateTime? aFactDate, int? aControlState, string aAnnotat, string aNote, string aDuePersonWhos, int? aIsnDelivery, string aDueOrganiz, int? aIsnContact, string aCorrespNum, DateTime? aCorrespDate, string aCorrespSign, int? aIsnCitizen, int? aIsCollective, int? aIsAnonim, string aSigns, string aDuePersonExe, int? aIsnNomenc, int? aNothardcopy, int? aCito, int? aIsnLinkingDoc, int? aIsnLinkingPrj, int? aIsnClLink, string aCopyShablon, string aVisas, int? aEDocument, string aSends, string askipcopy_ref_file_isns, int? aIsnLinkTranparent, int? aIsnLinkTranparentPare, string aTelNum);
        /// <summary>
        /// Хранимая процедура изменяет значения атрибутов РК с указанным Isn. При этом регистрационный номер не генерируется, а подставляется как есть и, если требуется, проверяется на уникальность.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор зарегистрированной РК.
        /// </param>
        /// <param name="aDueCard">
        /// Параметр устарел, не используется.
        /// </param>
        /// <param name="aIsnCab">
        /// Параметр устарел, не используется.
        /// </param>
        /// <param name="aFreeNum">
        /// Регистрационный номер.
        /// </param>
        /// <param name="aDocDate">
        /// Дата регистрации РК.
        /// </param>
        /// <param name="aSecurlevel">
        /// Идентификатор грифа доступа. В соответствие со справочником «Грифы доступа».
        /// </param>
        /// <param name="aConsists">
        /// Состав.
        /// </param>
        /// <param name="aSpecimen">
        /// Номера экземпляров.
        /// </param>
        /// <param name="aPlanDate">
        /// Плановая  дата исполнения документа.
        /// </param>
        /// <param name="aFactDate">
        /// Фактическая дата исполнения документа.
        /// </param>
        /// <param name="aControlState">
        /// Флаг контрольности.
        /// </param>
        /// <param name="aAnnotat">
        /// Содержание.
        /// </param>
        /// <param name="aNote">
        /// Примечание.
        /// </param>
        /// <param name="aDuePersonWho">
        /// Параметр устарел, не используется.
        /// </param>
        /// <param name="aIsnDelivery">
        /// Идентификатор вида доставки входящего документа (обязательно для входящих документов, писем и обращений граждан), в соответствие со справочником «Виды доставки».
        /// </param>
        /// <param name="aIsCollective">
        /// Признак  коллективного письма.
        /// </param>
        /// <param name="aIsAnonim">
        /// Признак  анонимного письма.
        /// </param>
        /// <param name="aDuePersonSign">
        /// Параметр устарел, не используется.
        /// </param>
        /// <param name="aDuePersonExe">
        /// Начиная с версии 8.11 параметр устарел, не используется.
        /// </param>
        /// <param name="aNothardcopy">
        /// Флаг «без досылки бум. Экземпляра».
        /// </param>
        /// <param name="aCito">
        /// Флаг «срочно».
        /// </param>
        /// <param name="aOrderNum">
        /// Порядковый номер.
        /// Начиная с версии 11.0.
        /// </param>
        /// <param name="aEDocument">
        /// Флаг "Оригинал в электронном виде". Начиная с версии 11.0.3.
        /// </param>
        /// <param name="aTelNum">
        /// Почтовый номер для РК входящих и писем граждан. (с версии 13.2)
        /// </param>
        void edit_rc(dynamic oHead, int aIsn, string aDueCard, int? aIsnCab, string aFreeNum, DateTime? aDocDate, int aSecurlevel, string aConsists, string aSpecimen, DateTime? aPlanDate, DateTime? aFactDate, int? aControlState, string aAnnotat, string aNote, string aDuePersonWho, int? aIsnDelivery, int? aIsCollective, int? aIsAnonim, string aDuePersonSign, string aDuePersonExe, int? aNothardcopy, int? aCito, int? aOrderNum, int? aEDocument, string aTelNum);
        /// <summary>
        /// Хранимая процедура удаляет РК с заданным isn, который передается в аргумент aIsnDoc.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aisndoc">
        /// Идентификатор РК
        /// </param>
        /// <param name="acard">
        /// Код due картотеки регистрации.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="aretnum">
        /// Использовать текущий порядковый номер при регистрации следующей РК.
        /// </param>
        /// <param name="ayear">
        /// Год регистрации РК.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        void del_rc(dynamic oHead, int aisndoc, string acard, int aretnum, int? ayear);
        /// <summary>
        /// Процедура резервирует порядковый номер РК в таблице номерообразования в соответствие с заданной группой документов и годом. Возвращаются 2 значения: aOrderNum – порядковый номер, как целое число, и aFreeNum – порядковый номер, как строка.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aOper">
        /// Тип операции.
        /// </param>
        /// <param name="aDueDocgroup">
        /// Код due группы документов, в соответствие со справочником «Группы документов».
        /// </param>
        /// <param name="aYear">
        /// Год в счетчике номерообразования РК.
        /// </param>
        /// <param name="card_id">
        /// Код due картотеки регистрации.
        /// </param>
        /// <param name="aOrderNum">
        /// Возвращаемое значение: порядковый номер.
        /// </param>
        /// <param name="aFreeNum">
        /// Возвращаемое значение: регистрационный номер (то же, что и aOrderNum).
        /// </param>
        /// <param name="aSessionId">
        /// Идентификатор сессии. Только для внутреннего использования.
        /// </param>
        void reserve_num(dynamic oHead, string aOper, string aDueDocgroup, int aYear, string card_id, ref int? aOrderNum, ref string aFreeNum, string aSessionId);
        /// <summary>
        /// Процедура возвращает порядковый номер РК в таблицу номерообразования, после чего он может быть опять зарезервирован с помощью процедуры reserve_num.
        /// Процудуру нужно использовать с осторожностью, т.к. возврат используемого номера может привести к дублированию номеров РК.
        /// При выполнении процедуры могут возникнуть следующие исключения:
        /// •	Доступ запрещен: пользователь  (имя пользователя)  не является пользователем системы Дело.
        /// •	Не найдена группа документов с кодом = (код due группы документов).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aOper">
        /// Тип операции.
        /// </param>
        /// <param name="aDueDocgroup">
        /// Код due группы документов, в соответствие со справочником «Группы документов».
        /// </param>
        /// <param name="aYear">
        /// Год в счетчике номерообразования РК.
        /// </param>
        /// <param name="aOrderNum">
        /// Возвращаемый порядковый номер.
        /// </param>
        /// <param name="aFreeNum">
        /// Не используется.
        /// </param>
        void return_num(dynamic oHead, string aOper, string aDueDocgroup, int aYear, int aOrderNum, ref string aFreeNum);
        /// <summary>
        /// Процедура выполняет прикрепление файла к объекту БД либо изменение описания и/или содержимого прикрепленного файла.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="action">
        /// Выполняемое действие.
        /// Если параметр равен 2, то после изменения также удаляется содержимое файла из БД, и его содержимое нужно заново скопировать при помощи процедуры add_chunk.
        /// </param>
        /// <param name="aISN_REF_FILE">
        /// При добавлении нового файла – null, возвращаемое значение: идентификатор файла.
        /// При изменении или удалении файла – идентификатор существующего файла.
        /// </param>
        /// <param name="aISN_RC">
        /// Идентификатор объекта, к которому прикреплен файл. Тип объекта определяется параметром akind_doc.
        /// </param>
        /// <param name="aKind_Doc">
        /// Вид объекта.
        /// </param>
        /// <param name="aDescription">
        /// Комментарий.
        /// </param>
        /// <param name="aCategory">
        /// Категория.
        /// </param>
        /// <param name="aSecur">
        /// Идентификатор грифа доступа, в соответствие со справочником «Грифы доступа».
        /// Если aSecur = null при том, что action in (2, 3), то значение берется от файла по параметру aISN_REF_FILE.
        /// Если aSecur = null при том, что action = 1 и aKind_Doc in (1, 2, 3), то значение берется от поля securlevel документа, к которому добавляется файл: aISN_RC = doc.rc.isn_doc.
        /// Для aKind_Doc not in (1, 2, 3)  не используется
        /// </param>
        /// <param name="aLockFlag">
        /// Флаг защиты от редактирования.
        /// Если aLockFlag = null при том что action = 1, то значение берется от параметра пользователя ‘FILE_LOCK’. (Если значение параметра = 1, то устанавливается значение =1, иначе 0).
        /// Если aLockFlag = null при том что action != 1, то устанавливается значение = 0.
        /// </param>
        /// <param name="aFileAccessDues">
        /// Список кодов due должностных лиц и подразделений, которым должен быть доступен для просмотра файл, в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aFilename">
        /// Полный путь к прикрепляемому файлу.
        /// </param>
        /// <param name="aDontDelFlag">
        /// Признак запрета удаления файла.
        /// </param>
        void save_file_wf(dynamic oHead, int action, int? aISN_REF_FILE, int aISN_RC, int aKind_Doc, string aDescription, string aCategory, int? aSecur, int? aLockFlag, string aFileAccessDues, string aFilename, int? aDontDelFlag);
        /// <summary>
        /// Процедура добавляет цифровую подпись к файлу, идентификатор которого указан в аргументе aIsnRefFile. ЭЦП в данном случае передается в ReverseHex кодировке. Отличие от add_file_eds состоит в том, что add_file_eds предназначена для загрузки созданной в системе Дело ЭЦП, и позволяет задать дополнительные атрибуты подписи, связанные с бизнес-логикой системы.
        /// При загрузке ЭЦП в ReverseHex кодировке массив байт реверсируется и представляется в hex строке.
        /// Например, если содержимое массива следующее: 0x01, 0x02, 0x03, 0x11, 0x12, 0x1A, то получится результирующая строка: «1A1211030201».
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnRefFile">
        /// Идентификатор файла к которому загружается ЭЦП.
        /// </param>
        /// <param name="aEdsBody">
        /// ЭЦП в ReverseHex кодировке.
        /// </param>
        /// <param name="a_eds_body_blob">
        /// Содержимое подписи.
        /// </param>
        /// <param name="a_isn_sign_kind">
        /// Идентификатор вида пописи, в соответствие со справочником «Виды подписей».
        /// </param>
        /// <param name="a_certificate_owner">
        /// Владелец сертификата.
        /// </param>
        /// <param name="a_signing_date">
        /// Дата подписи.
        /// </param>
        /// <param name="a_sign_text">
        /// Текст подписи.
        /// </param>
        void import_file_eds(dynamic oHead, int aIsnRefFile, string aEdsBody, string a_eds_body_blob, int? a_isn_sign_kind, string a_certificate_owner, DateTime? a_signing_date, string a_sign_text);
        /// <summary>
        /// Процедура добавляет ЭЦП к файлу, идентификатор которого указан в аргументе a_isn_ref_file.
        /// Флаг a_flag используется при добавлении подписи к файлу, прикрепленному к РКПД.
        /// Тип подписи устанавливается в зависимости от типа визы и категории файла.
        /// a_flag       Тип визы          REF_FILE.Category       Тип подписи
        /// 0            -                 -                       1–«авторская»
        /// 1            -                 Not null                1 – «авторская»
        /// 1            2 –«не согласен»  Null                    2–«согласующая»
        /// 1            1 –«согласен»     Null                    3–«утверждающая»
        /// 1            Not in (1, 2)     Null                    0–«не определена»
        /// Для файлов, не прикрепленных к РКПД устанавливается тип подписи, указанный в аргументе a_isn_sign_kind.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="a_isn_ref_file">
        /// Идентификатор файла к которому загружается ЭЦП.
        /// </param>
        /// <param name="a_eds_body">
        /// Содержимое подписи.
        /// </param>
        /// <param name="a_isn_sign_kind">
        /// Идентификатор вида пописи, в соответствие со справочником «Виды подписей».
        /// </param>
        /// <param name="a_due_person">
        /// Код due подписывающего, в соответствие со списком визирующих и подписывающих лиц РКПД.
        /// </param>
        /// <param name="a_flag">
        /// Флаг
        /// </param>
        /// <param name="a_eds_body_blob">
        /// Содержимое подписи.
        /// </param>
        /// <param name="a_certificate_owner">
        /// Владелец сертификата.
        /// </param>
        /// <param name="a_signing_date">
        /// Дата подписи.
        /// </param>
        void add_file_eds(dynamic oHead, int a_isn_ref_file, string a_eds_body, int? a_isn_sign_kind, string a_due_person, int? a_flag, string a_eds_body_blob, string a_certificate_owner, DateTime? a_signing_date);
        /// <summary>
        /// Процедура удаляет ЭЦП файла.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="a_isn_ref_file_eds">
        /// Идентификатор записи в таблице ЭЦП файлов.
        /// </param>
        void del_file_eds(dynamic oHead, int a_isn_ref_file_eds);
        /// <summary>
        /// Процедура выполняет пересылку заданного документа адресатам, указанным в аргументе codes. Если в настройках параметров пользователя параметр «Записывать в журнал передачи документов» (см. Руководство технолога. т. 2, раздел 13.1.13.) установлен в значение «Да» и пользователь имеет права на запись в ЖПД, то в журнал пересылки документов добавляется информация о пересылке.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnDoc">
        /// Идентификатор пересылаемой РК.
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// </param>
        /// <param name="acab">
        /// Идентификатор текущего кабинета, в соответствие со справочником «Кабинеты».
        /// </param>
        /// <param name="codes">
        /// Список кодов due адресатов, которым пересылается РК, в соответствие со справочником «Подразделения».
        /// </param>
        void add_forward(dynamic oHead, int aIsnDoc, string acard, int acab, string codes);
        /// <summary>
        /// Процедура отменяет пересылку документа.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aisnforward">
        /// Идентификатор удаляемой пересылки.
        /// </param>
        void del_forward(dynamic oHead, int aisnforward);
        /// <summary>
        /// Процедура добавляет записи в журнал передачи документов. Если параметр aIsnNomenc не равен null, то происходит списание документа в дело.
        /// При отметке передачи документа другим должностным лицам, аргумент aIsnNomenc должен быть равен null.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnDoc">
        /// Идентификатор документа.
        /// </param>
        /// <param name="aCard">
        /// Код Due картотеки корреспондента.
        /// </param>
        /// <param name="aCab">
        /// Идентификатор кабинета корреспондента.
        /// </param>
        /// <param name="aCodes">
        /// Список кодов due адресатов в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aIsnNomenc">
        /// Идентификатор дела.
        /// </param>
        /// <param name="aDates">
        /// Дата и время создания записи в журнале.
        /// </param>
        /// <param name="aFlagsOriginal">
        /// Список  “Флаг  копии”. Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр aCodes.
        /// </param>
        /// <param name="aOrigNums">
        /// Список “Номер  оригинала в журнале”. Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр aCodes.
        /// </param>
        /// <param name="aCopyNums">
        /// Список “Номер  копии в журнале”. Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр aCodes.
        /// </param>
        /// <param name="aClearFldr6">
        /// Флаг удаления из папки «В Дело» (1 – да, 0 – нет).
        /// </param>
        /// <param name="aDescructFlag">
        /// Флаг типа записи.
        /// </param>
        void add_journal(dynamic oHead, int aIsnDoc, string aCard, int aCab, string aCodes, int? aIsnNomenc, DateTime? aDates, string aFlagsOriginal, string aOrigNums, string aCopyNums, int aClearFldr6, int aDescructFlag);
        /// <summary>
        /// Процедура добавляет записи о визах в РК исходящих документов (с версии 13.2).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор РК.
        /// </param>
        /// <param name="aDuePersons">
        /// Список кодов due из справочника "Подразделения".
        /// </param>
        /// <param name="aVisaDates">
        /// Список дат визирования.
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aDuePersons.
        /// </param>
        /// <param name="aNotes">
        /// Список "Примечаний" к создаваемым визам.
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aDuePersons.
        /// </param>
        void add_visa(dynamic oHead, int aIsn, string aDuePersons, DateTime[] aVisaDates, string aNotes);
        /// <summary>
        /// Процедура удаляет указанную визу (с версии 13.2).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnRefVisa">
        /// Идентификатор записи о визе РК.
        /// </param>
        void del_visa(dynamic oHead, int aIsnRefVisa);
        /// <summary>
        /// Процедура изменяет информацию о визе с заданным идентификатором (с версии 13.2).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnRefVisa">
        /// Идентификатор записи о визе РК.
        /// </param>
        /// <param name="aVisaDate">
        /// Дата визирования.
        /// </param>
        /// <param name="aNote">
        /// Примечание к визе.
        /// </param>
        void edit_visa(dynamic oHead, int aIsnRefVisa, DateTime? aVisaDate, string aNote);
        /// <summary>
        /// Процедура добавляет записи о подписях в РК исходящих документов (с версии 13.2).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор РК.
        /// </param>
        /// <param name="aDuePersons">
        /// Список кодов due из справочника "Подразделения".
        /// </param>
        /// <param name="aSignDates">
        /// Список дат подписания.
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aDuePersons.
        /// </param>
        void add_sign(dynamic oHead, int aIsn, string aDuePersons, DateTime[] aSignDates);
        /// <summary>
        /// Процедура удаляет указанную подпись (с версии 13.2).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnDocSign">
        /// Идентификатор записи о подписи РК.
        /// </param>
        void del_sign(dynamic oHead, int aIsnDocSign);
        /// <summary>
        /// Процедура изменяет информацию о подписи с заданным идентификатором (с версии 13.2).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnDocSign">
        /// Идентификатор записи о подписи РК.
        /// </param>
        /// <param name="aSignDate">
        /// Дата подписания.
        /// </param>
        void edit_sign(dynamic oHead, int aIsnDocSign, DateTime? aSignDate);
        /// <summary>
        /// Процедура добавляет записи о соисполнителях в РК исходящих документов (с версии 13.2).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор РК.
        /// </param>
        /// <param name="aDuesOrganiz">
        /// Список кодов due из справочника "Организации".
        /// </param>
        /// <param name="aIsnsContact">
        /// Список идентификаторов контакта.
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов организаций – параметр aDuesOrganiz.
        /// </param>
        /// <param name="aSoispNums">
        /// Список исходящих номеров соисполнителей.
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов организаций – параметр aDuesOrganiz.
        /// </param>
        /// <param name="aSoispDates">
        /// Список дат подписания.
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aDuePersons.
        /// </param>
        /// <param name="aSoispPersons">
        /// Список подписей соисполнителей.
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов организаций – параметр aDuesOrganiz.
        /// </param>
        void add_soisp(dynamic oHead, int aIsn, string aDuesOrganiz, string aIsnsContact, string aSoispNums, DateTime[] aSoispDates, string aSoispPersons);
        /// <summary>
        /// Процедура удаляет указанного соисполнителя (с версии 13.2).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnRefSoisp">
        /// Идентификатор записи о соисполнителе РК.
        /// </param>
        void del_soisp(dynamic oHead, int aIsnRefSoisp);
        /// <summary>
        /// Процедура изменяет информацию о соисполнителе с заданным идентификатором (с версии 13.2).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnRefSoisp">
        /// Идентификатор записи о соисполнителе РК.
        /// </param>
        /// <param name="aSoispNum">
        /// Исходящий номер
        /// </param>
        /// <param name="aSoispDate">
        /// Исходящая дата
        /// </param>
        /// <param name="aSoispPerson">
        /// Подписавший
        /// </param>
        void edit_soisp(dynamic oHead, int aIsnRefSoisp, string aSoispNum, DateTime? aSoispDate, string aSoispPerson);
        /// <summary>
        /// Добавляет ссылку на РК, поручение или РКПД в личную папку пользователя. Вид папки должен соответствовать виду добавляемого объекта.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnRefUfolder">
        /// Идентификатор папки.
        /// </param>
        /// <param name="aIsnRefDoc">
        /// Идентификатор добавляемого объекта
        /// </param>
        /// <param name="aKindDoc">
        /// Вид добавляемого объекта.
        /// </param>
        /// <param name="aNote">
        /// Примечание к записи.
        /// </param>
        void add_ref_ufolder(dynamic oHead, int aIsnRefUfolder, int aIsnRefDoc, int? aKindDoc, string aNote);
        /// <summary>
        /// Удаляет ссылку на РК, поручение или РКПД из личной папки пользователя.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnRefUfolder">
        /// Идентификатор папки.
        /// </param>
        /// <param name="aIsnRefDoc">
        /// Идентификатор удаляемого объекта
        /// </param>
        void del_ref_ufolder(dynamic oHead, int aIsnRefUfolder, int aIsnRefDoc);
        /// <summary>
        /// Процедура добавляет адресатов в РК документа. Адресаты могут браться из справочника организаций, граждан или подразделений. Также добавляются соответствующие записи в журнал, если адресаты внутренние (из справочника подразделений).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnDoc">
        /// Идентификатор РК.
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// </param>
        /// <param name="acab">
        /// Идентификатор текущего кабинета, в соответствие со справочником "Кабинеты".
        /// </param>
        /// <param name="aclassif">
        /// Название справочника, из которого добавляются адресаты.
        /// </param>
        /// <param name="codes">
        /// Список кодов адресатов.
        /// </param>
        /// <param name="aFlagsOriginal">
        /// Список флагов оригинал/копия.  Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр codes.
        /// </param>
        /// <param name="aOrigNums">
        /// Список номеров оригиналов, только для внутренних адресатов. Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр codes.
        /// </param>
        /// <param name="aCopyNums">
        /// Список номеров  копий, только для групп документов с нумеруемыми копиями и только для внутренних адресатов
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр codes.
        /// </param>
        /// <param name="aDate">
        /// Дата отправки в формате YYYYMMDD (по умолчанию null). Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр codes.
        /// </param>
        /// <param name="aSend_Person">
        /// Список ФИО “Кому адресован”
        /// (для внутренних - null). Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр codes.
        /// </param>
        /// <param name="aIsn_Delivery">
        /// Список идентификаторов доставки, в соответствие со справочником «Виды доставки».
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр codes.
        /// </param>
        /// <param name="aSendingType">
        /// Тип отправки. Только для версии ЦБ.
        /// </param>
        /// <param name="aIsnsContact">
        /// Список идентификаторов контактов к добавляемым адресатам.
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр codes.
        /// </param>
        /// <param name="aNotes">
        /// Список примечаний. Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр codes.
        /// </param>
        /// <param name="aConsists">
        /// Список значений для реквизита «Состав» для адресатов-организаций и граждан. Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр codes. (с версии 13.2)
        /// </param>
        void add_send(dynamic oHead, int aIsnDoc, string acard, int acab, string aclassif, string codes, string aFlagsOriginal, string aOrigNums, string aCopyNums, DateTime[] aDate, string aSend_Person, string aIsn_Delivery, int? aSendingType, string aIsnsContact, string aNotes, string aConsists);
        /// <summary>
        /// Процедура обновляет информацию о внутреннем адресате для документа с заданным aisn и адресата с идентификатором aisn_ref_send. Изменяется дата отправки, состав документа, флаг «оригинал/копия» и примечание. Также добавляется информация в журнал передачи документов.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnDoc">
        /// Идентификатор документа.
        /// </param>
        /// <param name="aisn_ref_send">
        /// Идентификатор адресата.
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// </param>
        /// <param name="acab">
        /// Идентификатор текущего кабинета, в соответствие со справочником «Кабинеты».
        /// </param>
        /// <param name="aFlagOriginal">
        /// Флаг  оригинал/копия.
        /// </param>
        /// <param name="aOrigNum">
        /// Номер оригинала.
        /// </param>
        /// <param name="aCopyNum">
        /// Номер  копии.
        /// </param>
        /// <param name="aDates">
        /// Дата и время отправки.
        /// </param>
        /// <param name="aNote">
        /// Примечание.
        /// </param>
        void edit_send(dynamic oHead, int aIsnDoc, int aisn_ref_send, string acard, int acab, int aFlagOriginal, string aOrigNum, string aCopyNum, DateTime? aDates, string aNote);
        /// <summary>
        /// Процедура удаляет адресата из РК.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aisnrefsend">
        /// Идентификатор адресата.
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        void del_send(dynamic oHead, int aisnrefsend, string acard);
        /// <summary>
        /// Процедура обновляет информацию о внешнем адресате для документа с заданным aisn и адресата с идентификатором aisn_ref_send.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="a_isn_doc">
        /// Идентификатор РК.
        /// </param>
        /// <param name="a_isn_ref_send">
        /// Идентификатор адресата.
        /// </param>
        /// <param name="a_card">
        /// Код due текущей картотеки.
        /// С версии 11.0 параметр устарел, не используется.
        /// </param>
        /// <param name="a_cab">
        /// Идентификатор текущего кабинета, в соответствие со справочником «Кабиинеты».
        /// С версии 11.0 параметр устарел, не используется.
        /// </param>
        /// <param name="a_isn_contact">
        /// Идентификатор контакта.
        /// </param>
        /// <param name="a_send_date">
        /// Дата отправки.
        /// </param>
        /// <param name="a_send_person">
        /// Кому адресован.
        /// </param>
        /// <param name="a_isn_delivery">
        /// Идентификатор вида доставки в соответствие со справочником «Виды доставки».
        /// </param>
        /// <param name="a_sending_type">
        /// Тип отправки.
        /// </param>
        /// <param name="a_note">
        /// Примечание.
        /// </param>
        /// <param name="a_reg_n">
        /// Регистрационный номер документа у адресата.
        /// </param>
        /// <param name="a_reg_date">
        /// Дата регистрации документа у адресата.
        /// </param>
        /// <param name="a_answer">
        /// Флаг «Требуется ли квитанция о регистрации».
        /// </param>
        void edit_outer_send(dynamic oHead, int a_isn_doc, int a_isn_ref_send, string a_card, int a_cab, int a_isn_contact, DateTime? a_send_date, string a_send_person, int a_isn_delivery, int a_sending_type, string a_note, string a_reg_n, DateTime? a_reg_date, int a_answer);
        /// <summary>
        /// Процедура добавляет рубрики к РК документа.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnDoc">
        /// Идентификатор РК.
        /// </param>
        /// <param name="codes">
        /// Список кодов due, в соответствие со справочником «Рубрикатор».
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        void add_rubric(dynamic oHead, int aIsnDoc, string codes, string acard);
        /// <summary>
        /// Процедура удаляет рубрику с указанным идентификатором.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aisnrefrubric">
        /// Идентификатор рубрики.
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        void del_rubric(dynamic oHead, int aisnrefrubric, string acard);
        /// <summary>
        /// Процедура добавляет связку к РК документа или проекта документа.
        /// В параметры aisndoc и aisn_linkdoc передаются идентификаторы связываемых РК (РКПД). Процедура автоматически определяет их типы. Если производится связка с документом, не зарегистрированным в системе, то aisn_link_doc должен быть равен null, а в alinked_num передается регистрационный номер связанного документа, не зарегистрированного в системе.
        /// При выполнении процедуры создается связка между документами. В разделе «Связки» документа aisndoc появляется прямая связка, а во втором – обратная.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aisndoc">
        /// Идентификатор РК (РКПД).
        /// </param>
        /// <param name="aisn_linkdoc">
        /// Идентификатор связанной РК (РКПД).
        /// </param>
        /// <param name="aisn_link">
        /// Идентификатор типа связки в соответствие со справочником «Типы связок».
        /// </param>
        /// <param name="alinked_num">
        /// Регистрационный номер документа, не зарегистрированного в системе Дело (если aisn_linkdoc = null). Если связка производится с зарегистрированным документом, то null (по умолчанию null).
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="aUrlStr">
        /// Сетевой адрес для связки с не зарегистрированным в системе документом.
        /// </param>
        void add_link(dynamic oHead, int aisndoc, int? aisn_linkdoc, int aisn_link, string alinked_num, string acard, string aUrlStr);
        /// <summary>
        /// Процедура удаляет указанную связку.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aisn_ref_link">
        /// Идентификатор связки.
        /// </param>
        /// <param name="aisn_doc">
        /// Идентификатор РК (РКПД).
        /// </param>
        /// <param name="aisn_linkeddoc">
        /// Идентификатор связанной РК (РКПД).
        /// </param>
        /// <param name="aisn_link">
        /// Идентификатор вида связки в соответствие со справочником «Типы связок».
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        void del_link(dynamic oHead, int aisn_ref_link, int aisn_doc, int aisn_linkeddoc, int aisn_link, string acard);
        /// <summary>
        /// Процедура добавляет корреспондентов к РК входящих документов или писем граждан.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnDoc">
        /// Идентификатор РК.
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="acab">
        /// Идентификатор кабинета, в соответствие со справочником «Кабинеты».
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="aCodes">
        /// Список кодов due из справочника «Организации» (для входящих документов) или «Граждане» (для писем граждан).
        /// </param>
        /// <param name="aCorrespNums">
        /// Список исходящих номеров (по умолчанию null).
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aCodes.
        /// </param>
        /// <param name="aCorrespDates">
        /// Список исходящих дат (по умолчанию null).
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aCodes.
        /// </param>
        /// <param name="aCorrespSigns">
        /// Лицо, подписавшее  входящий документ.
        /// </param>
        /// <param name="aIsnsContact">
        /// Список идентификаторов контакта (по умолчанию null).
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aCodes.
        /// </param>
        /// <param name="aNeedAnswers">
        /// Список признаков необходимости ответа (по умолчанию null). (с версии 13.2)
        /// Только для писем граждан.
        /// Значение «1» означает ответ нужен, «0» - ответ не нужен.
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aCodes.
        /// </param>
        /// <param name="aNotes">
        /// Список "Примечаний" к создаваемым корреспондентам входящих РК. (с версии 13.2)
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aCodes.
        /// </param>
        void add_corresp(dynamic oHead, int aIsnDoc, string acard, int acab, string aCodes, string aCorrespNums, DateTime[] aCorrespDates, string aCorrespSigns, string aIsnsContact, string aNeedAnswers, string aNotes);
        /// <summary>
        /// Процедура изменяет информацию о корреспонденте с заданным идентификатором.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор записи о корреспонденте РК.
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="acab">
        /// Идентификатор кабинета, в соответствие со справочником «Кабинеты».
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="aCode">
        /// Код due из справочника «Организации» (для входящих документов) или «Граждане» (для писем граждан).
        /// </param>
        /// <param name="aisn_contact">
        /// Идентификатор контакта, в соответствие со справочником контактов (CONTACT).
        /// </param>
        /// <param name="aCorrespNum">
        /// Исходящий номер (по умолчанию null).
        /// </param>
        /// <param name="aCorrespDate">
        /// Исходящая дата (по умолчанию null).
        /// </param>
        /// <param name="aCorrespSign">
        /// Кто подписал (по умолчанию null).
        /// </param>
        /// <param name="aNote">
        /// Примечание (по умолчанию null). (с версии 13.2)
        /// </param>
        void edit_corresp(dynamic oHead, int aIsn, string acard, int acab, string aCode, int? aisn_contact, string aCorrespNum, DateTime? aCorrespDate, string aCorrespSign, string aNote);
        /// <summary>
        /// Процедура удаляет корреспондента из РК.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор записи о корреспонденте РК.
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        void del_corresp(dynamic oHead, int aIsn, string acard);
        /// <summary>
        /// Процедура добавляет к РК входящих документов или писем граждан информацию о сопроводительных документах.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnDoc">
        /// Идентификатор документа.
        /// </param>
        /// <param name="acard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="acab">
        /// Идентификатор кабинета, в соответствие со справочником «Кабинеты».
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="aCodes">
        /// Код due из справочника «Организации» (для входящих документов).
        /// </param>
        /// <param name="aCorrespNums">
        /// Список исходящих номеров (по умолчанию null).
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aCodes.
        /// </param>
        /// <param name="aCorrespDates">
        /// Список исходящих дат (по умолчанию null).
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aCodes.
        /// </param>
        /// <param name="aCorrespSigns">
        /// Список «Кто подписал» (по умолчанию null).
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов due организаций – параметр aCodes.
        /// </param>
        /// <param name="aAnnotats">
        /// Список "Аннотация" (по умолчанию null).
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов due организаций – параметр aCodes.
        /// </param>
        /// <param name="aConsists">
        /// Список "Состав" (по умолчанию null).
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов due организаций – параметр aCodes.
        /// </param>
        /// <param name="aNotes">
        /// Список "Примечание" (по умолчанию null).
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов due организаций – параметр aCodes.
        /// </param>
        /// <param name="aIsnsContact">
        /// Список идентификаторов контакта (по умолчанию null).
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов – параметр aCodes.
        /// </param>
        void add_cover_doc(dynamic oHead, int aIsnDoc, string acard, int acab, string aCodes, string aCorrespNums, DateTime[] aCorrespDates, string aCorrespSigns, string aAnnotats, string aConsists, string aNotes, string aIsnsContact);
        /// <summary>
        /// Процедура изменяет информацию об указанном сопроводительном документе.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор записи о сопроводительном документе.
        /// </param>
        /// <param name="aCard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="aCab">
        /// Идентификатор кабинета, в соответствие со справочником «Кабинеты».
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="aCode">
        /// Код due организации, в соответствие со справочником «Организации».
        /// </param>
        /// <param name="aCorrespNum">
        /// Исходящий номер (по умолчанию null).
        /// </param>
        /// <param name="aCorrespDate">
        /// Исходящая дата (по умолчанию null).
        /// </param>
        /// <param name="aCorrespSign">
        /// Кто подписал (по умолчанию null).
        /// </param>
        /// <param name="aAnnotat">
        /// Содержание.
        /// </param>
        /// <param name="aConsist">
        /// Состав.
        /// </param>
        /// <param name="aNote">
        /// Примечание.
        /// </param>
        void edit_cover_doc(dynamic oHead, int aIsn, string aCard, int aCab, string aCode, string aCorrespNum, DateTime? aCorrespDate, string aCorrespSign, string aAnnotat, string aConsist, string aNote);
        /// <summary>
        /// Процедура удаляет указанный сопроводительный документ.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор записи о сопроводительном документе.
        /// </param>
        /// <param name="aCard">
        /// Код due текущей картотеки.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        void del_cover_doc(dynamic oHead, int aIsn, string aCard);
        /// <summary>
        /// Процедура изменяет значение допреквизита.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnOwner">
        /// Код владельца, для которого нужно изменить значение реквизита.
        /// </param>
        /// <param name="aArName">
        /// api_name допреквизита, значение которого нужно изменить.
        /// </param>
        /// <param name="aСard">
        /// Код due картотеки, в контексте которой выполняется обновление параметров.
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="aValue">
        /// Новое значение дополнительного реквизита.
        /// </param>
        void edit_ar(dynamic oHead, int aIsnOwner, string aArName, string aСard, string aValue);
        /// <summary>
        /// Процедура создает новую РКПД. Новая РКПД может быть создана как связанная с РК документа или с поручением, при этом часть информации из связанной РК (и поручения) будет перенесена в новую РКПД.
        /// Чтобы создать РКПД из РК, аргумент aIsnLinkingDoc должен содержать идентификатор связанного документа.
        /// Чтобы создать РКПД из поручения, аргумент aIsnLinkingDoc должен указывать на РК нужного поручения, а аргумент aIsnLinkingRes – на само поручение.
        /// Аргумент acopy_shablon – содержит битовую маску, указывающую, какую информацию из исходной РК и поручения нужно перенести в новую РКПД. Иначе аргумент не используется.
        /// При успешном выполнении, процедура возвращает идентификатор новой РКПД в аргументе aIsn.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Возвращаемое значение, идентификатор зарегистрированной РКПД.
        /// </param>
        /// <param name="aDueDocgroup">
        /// Код due группы документов, в соответствие со справочником «Группы документов».
        /// </param>
        /// <param name="aOrderNum">
        /// Порядковый  номер.
        /// Если в шаблоне номера группы документов есть элемент порядковый номер («{2}» или «{@2}»), то значение этого параметра следует вычислять с помощью процедуры reserve_num.
        /// Передача иного значения приведёт к неправильному формированию регистрационного номера!
        /// Если же в шаблоне номера нет этого элемента, то нужно передавать значение 0.
        /// </param>
        /// <param name="aFreeNum">
        /// Регистрационный номер. Если шаблон номера документа равен «{@}» или «{@2}», то в РК подставляется значение aFreeNum, иначе номер формируется автоматически в соответствие с шаблоном номера, указанном в параметрах группы документов, а значение данного аргумента не используется.
        /// </param>
        /// <param name="aPrjDate">
        /// Дата регистрации РК.
        /// </param>
        /// <param name="aSecurlevel">
        /// Идентификатор грифа доступа. В соответствие со справочником «Грифы доступа».
        /// </param>
        /// <param name="aConsists">
        /// Состав.
        /// </param>
        /// <param name="aPlanDate">
        /// Плановая  дата исполнения документа.
        /// </param>
        /// <param name="aAnnotat">
        /// Содержание.
        /// </param>
        /// <param name="aNote">
        /// Примечание.
        /// </param>
        /// <param name="aDuePersonExe">
        /// Код due исполнителя исходящего документа, в соответствие со справочником «Подразделения».
        /// Начиная с версии 11.0.3 - список исполнителей.
        /// </param>
        /// <param name="aIsnLinkingDoc">
        /// Идентификатор связанной РК (идентификатор РК, с которой делается связанная РК). Если не связанная, то null.
        /// </param>
        /// <param name="aIsnLinkingPrj">
        /// Идентификатор регистрируемого РКПД (в случае  регистрации связанной РК из проекта). Если не на основе РКПД, то null.
        /// </param>
        /// <param name="aIsnClLink">
        /// Идентификатор типа связки, в соответствие со справочником «Типы связок».
        /// </param>
        /// <param name="aCopyShablon">
        /// Битовая  маска для копирования реквизитов, обрабатывается при регистрации связанной РК, если указан aIsnLinkingDoc или aIsnLinkingPrj:
        /// 1.Содержание
        /// 2.Рубрики (РК или РКПД)
        /// 3.Доступ
        /// 4.Подписавшие исходящий документ
        /// 5.Исполнитель
        /// 6.Визы
        /// 7.Адресаты
        /// 8.Корреспонденты
        /// 9.Кому
        /// 10.Сопроводительные документы
        /// 11.Доставка
        /// 12.Дата регистрации
        /// 13.Состав
        /// 14.Файлы
        /// 15.Примечание
        /// 16.Соисполнители (РК или РКПД)
        /// 17.Допреквизиты (РК или РКПД)
        /// 18.Связки(РК или РКПД)
        /// (1 – копировать реквизит, 0 – нет, пример: 1110001111001010).
        /// Только для связанного документа и на основе РКПД.
        /// </param>
        /// <param name="aVisas">
        /// Список лиц, завизировавших документ, в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aEDocument">
        /// Флаг "Оригинал в электронном виде". Начиная с версии 11.0.3.
        /// </param>
        void add_prj(dynamic oHead, ref int? aIsn, string aDueDocgroup, int aOrderNum, string aFreeNum, DateTime? aPrjDate, int aSecurlevel, string aConsists, DateTime? aPlanDate, string aAnnotat, string aNote, string aDuePersonExe, int? aIsnLinkingDoc, int? aIsnLinkingPrj, int? aIsnClLink, string aCopyShablon, string aVisas, int? aEDocument);
        /// <summary>
        /// Процедура удаляет РКПД с указанным идентификатором «пакета». Удаляются все версии РКПД.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnBatch">
        /// Идентификатор "пакета" - идентификатор, общий для всех версий удаляемой РКПД.
        /// </param>
        /// <param name="aRetNumber">
        /// Флаг возврата номера в пул.
        /// </param>
        /// <param name="aIsnPrj">
        /// Идентификатор версии РКПД.
        /// </param>
        void del_prj(dynamic oHead, int aIsnBatch, int aRetNumber, int? aIsnPrj);
        /// <summary>
        /// Хранимая процедура изменяет значения в РКПД с указанным Isn.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор редактируемой РКПД.
        /// </param>
        /// <param name="aPrjDate">
        /// Дата  регистрации проекта документа.
        /// </param>
        /// <param name="aSecurlevel">
        /// Идентификатор грифа доступа. В соответствие со справочником «Грифы доступа».
        /// </param>
        /// <param name="aConsists">
        /// Состав.
        /// </param>
        /// <param name="aPlanDate">
        /// Плановая  дата исполнения проекта.
        /// </param>
        /// <param name="aAnnotat">
        /// Содержание.
        /// </param>
        /// <param name="aNote">
        /// Примечание (необязательное).
        /// </param>
        /// <param name="aDuePersonExe">
        /// Исполнитель  проекта, в соответствие со справочником «Подразделения».
        /// </param>
        /// <param name="aFreeNum">
        /// Номер проекта.
        /// </param>
        /// <param name="aOrderNum">
        /// Порядковый номер.
        /// </param>
        /// <param name="aEDocument">
        /// Флаг "Оригинал в электронном виде".
        /// </param>
        void edit_prj(dynamic oHead, int aIsn, DateTime? aPrjDate, int aSecurlevel, string aConsists, DateTime? aPlanDate, string aAnnotat, string aNote, string aDuePersonExe, string aFreeNum, int? aOrderNum, int? aEDocument);
        /// <summary>
        /// Процедура создает новую версию РКПД из указанной старой версии. При этом новая версия отмечается как текущая, а с предпоследней - этот признак снимается. Копируются файлы (только файлы исполнителя проекта).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Возвращаемое значение: идентификатор новой версии РКПД.
        /// </param>
        /// <param name="aIsnOldVersion">
        /// Идентификатор версии РКПД из которой будет сделана новая версия.
        /// </param>
        /// <param name="aCopySecondaryVisa">
        /// Флаг копирования в новую версию записей "вторичного визирования".
        /// </param>
        void add_prj_version(dynamic oHead, ref int? aIsn, int aIsnOldVersion, int aCopySecondaryVisa);
        /// <summary>
        /// Процедура отправляет РКПД с указанным идентификатором на регистрацию.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор отправляемой на регистрацию РКПД.
        /// </param>
        void send_prj_reg(dynamic oHead, int aIsn);
        /// <summary>
        /// Процедура резервирует порядковый номер РКПД в таблице номерообразования в соответствие с заданной группой документов и годом. Возвращаются 2 значения: aOrderNum – порядковый номер, как целое число, и aFreeNum – порядковый номер, как строка.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aOper">
        /// Тип операции.
        /// </param>
        /// <param name="aDueDocgroup">
        /// Код due группы документов, в соответствие со справочником «Группы документов».
        /// </param>
        /// <param name="aYear">
        /// Год в счетчике номерообразования РК.
        /// </param>
        /// <param name="aOrderNum">
        /// Возвращаемое значение: порядковый номер.
        /// </param>
        /// <param name="aFreeNum">
        /// Возвращаемое значение: регистрационный номер (то же, что и aOrderNum).
        /// </param>
        /// <param name="aSessionId">
        /// Идентификатор сессии. Только для внутреннего использования.
        /// </param>
        void reserve_prj_num(dynamic oHead, string aOper, string aDueDocgroup, int aYear, ref int? aOrderNum, ref string aFreeNum, string aSessionId);
        /// <summary>
        /// Процедура возвращает порядковый номер РК проекта документа в таблицу номерообразования, после чего он может быть опять зарезервирован с помощью процедуры reserve_prj_num.
        /// Процудуру нужно использовать с осторожностью, т.к. возврат используемого номера может привести к дублированию номеров РК.
        /// При выполнении процедуры могут возникнуть следующие исключения:
        /// •	Доступ запрещен: пользователь  (имя пользователя)  не является пользователем системы Дело.
        /// •	Не найдена группа документов с кодом = (код due группы документов).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aOper">
        /// Тип операции.
        /// </param>
        /// <param name="aDueDocgroup">
        /// Код due группы документов, в соответствие со справочником «Группы документов».
        /// </param>
        /// <param name="aYear">
        /// Год в счетчике номерообразования РК.
        /// </param>
        /// <param name="aOrderNum">
        /// Возвращаемый порядковый номер.
        /// </param>
        /// <param name="aFreeNum">
        /// Не используется.
        /// </param>
        void return_prj_num(dynamic oHead, string aOper, string aDueDocgroup, int aYear, int aOrderNum, ref string aFreeNum);
        /// <summary>
        /// Процедура добавляет рубрики с указанными в аргументе codes кодами due.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aisnprj">
        /// Идентификатор РКПД.
        /// </param>
        /// <param name="codes">
        /// Список код due рубрик, в соответствие со справочником «Рубрикатор».
        /// </param>
        void add_prj_rubric(dynamic oHead, int aisnprj, string codes);
        /// <summary>
        /// Процедура удаляет указанную рубрику из РКПД.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="a_isn_prj_ref_rubric">
        /// Идентификатор  рубрики РКПД.
        /// </param>
        void del_prj_rubric(dynamic oHead, int a_isn_prj_ref_rubric);
        /// <summary>
        /// Процедура добавляет адресатов в РК документа. Адресаты могут браться из справочника организаций, граждан или подразделений. Также добавляются соответствующие записи в журнал, если адресаты внутренние (из справочника подразделений).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор проекта документа.
        /// </param>
        /// <param name="aclassif">
        /// Список идентификаторов адресатов.
        /// </param>
        /// <param name="aCodes">
        /// Список кодов адресатов.
        /// </param>
        /// <param name="aIsnsContact">
        /// Список идентификаторов контактов к добавляемым адресатам.
        /// Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр codes.
        /// </param>
        /// <param name="aSendPersons">
        /// Для  внешних адресатов – список кому адресовано (по умолчанию null). Количество элементов в списке должно соответствовать количеству элементов в списке кодов адресатов – параметр aCodes.
        /// </param>
        /// <param name="aSendingType">
        /// Тип отправки. Только для версии ЦБ.
        /// </param>
        void add_prj_send(dynamic oHead, int aIsn, string aclassif, string aCodes, string aIsnsContact, string aSendPersons, int? aSendingType);
        /// <summary>
        /// Процедура удаляет указанного адресата РКПД.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор записи адресата РКПД.
        /// </param>
        void del_prj_send(dynamic oHead, int aIsn);
        /// <summary>
        /// Процедура добавляет должностных лиц, указанных в as_rep_isns, как подписывающих либо визирующих проект документа. А также осуществляет отправку на визирование (подписание), если указан срок ответа.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnPrj">
        /// Идентификатор текущей версии РКПД.
        /// </param>
        /// <param name="aKind">
        /// Вид  добавляемых записей.
        /// </param>
        /// <param name="an_isn_prj_visa_sign">
        /// Идентификатор «родительской» записи при добавлении визы.
        /// </param>
        /// <param name="an_term">
        /// Срок ответа.
        /// </param>
        /// <param name="an_term_flag">
        /// Единицы  измерения срока.
        /// </param>
        /// <param name="an_parallel">
        /// Признак  «параллельной» отправки на визирование (подписание) должностным лицам.
        /// </param>
        /// <param name="as_rep_isns">
        /// Список  идентификаторов должностных лиц, в соответствие со справочником «Подразделения».
        /// </param>
        void add_prjvisasign(dynamic oHead, int aIsnPrj, int aKind, int? an_isn_prj_visa_sign, int? an_term, int an_term_flag, int an_parallel, string as_rep_isns);
        /// <summary>
        /// Процедура осуществляет отправку на визирование (подписание) РКПД. Отправка осуществляется для тех виз и подписей, которые были добавлены, но не отправлены.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnPrj">
        /// Идентификатор текущей версии РКПД.
        /// </param>
        /// <param name="an_term">
        /// Срок ответа.
        /// </param>
        /// <param name="an_term_flag">
        /// Единицы  измерения срока.
        /// </param>
        /// <param name="an_parallel">
        /// Признак  «параллельной» отправки на визирование (подписание) должностным лицам.
        /// </param>
        /// <param name="as_rep_isns">
        /// Список  идентификаторов должностных лиц, в соответствие со справочником «Подразделения».
        /// </param>
        void send_prjvisasign(dynamic oHead, int aIsnPrj, int? an_term, int an_term_flag, int an_parallel, string as_rep_isns);
        /// <summary>
        /// Процедура отменяет отправку на визирование (подпись).
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор записи о подписи (визе) РКПД.
        /// </param>
        void recall_prjvs(dynamic oHead, int aIsn);
        /// <summary>
        /// Процедура удаляет указанную визу или подпись РКПД. При установленном флаге aCascade, каскадно удаляются записи вторичного визирования.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsn">
        /// Идентификатор записи подписи (визы) РКПД.
        /// </param>
        /// <param name="aСascade">
        /// Флаг каскадного удаления.
        /// </param>
        void del_prjvs(dynamic oHead, int aIsn, int aСascade);
        /// <summary>
        /// Процедура записывает в БД информацию о визировании или подписании проекта, а также, если в свойствах проекта установлен флаг «автоматическая регистрация», проект отправляется на регистрацию.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="an_isn_prj_visa">
        /// Идентификатор записи о подписи или визе.
        /// </param>
        /// <param name="ad_rep_date">
        /// Дата  подписи.
        /// </param>
        /// <param name="an_sign_visa_type">
        /// Вид  подписи или идентификатор вида визы.
        /// </param>
        /// <param name="as_rep_text">
        /// Текст  визы.
        /// </param>
        /// <param name="an_reserve_folder_flag">
        /// Флаг «Оставить ссылку в папке кабинета».
        /// </param>
        void set_prj_visa_sign(dynamic oHead, int an_isn_prj_visa, DateTime ad_rep_date, int an_sign_visa_type, string as_rep_text, int an_reserve_folder_flag);
        /// <summary>
        /// Процедура изменяет значение допреквизита.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnPrj">
        /// Идентификатор  РКПД.
        /// </param>
        /// <param name="aArName">
        /// api_name допреквизита, значение которого нужно изменить.
        /// </param>
        /// <param name="aValue">
        /// Новое значение дополнительного реквизита.
        /// </param>
        void edit_ar_prj(dynamic oHead, int aIsnPrj, string aArName, string aValue);
        /// <summary>
        /// Процедура создает поручение. В зависимости от параметров можно создавать поручение-резолюцию, поручение-пункт и подчиненную резолюцию.
        /// Для создания подчиненной резолюции в аргумент an_isn_parent_resolution нужно передавать идентификатор родительского поручения.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="an_isn_resolution">
        /// Возвращаемое значение, идентификатор зарегистрированного поручения.
        /// </param>
        /// <param name="an_kind_resolution">
        /// Признак вида поручения.
        /// </param>
        /// <param name="an_isn_ref_doc">
        /// Идентификатор РК документа, к которому добавляется поручение.
        /// </param>
        /// <param name="an_isn_parent_resolution">
        /// Идентификатор родительской резолюции.
        /// </param>
        /// <param name="an_isn_author">
        /// Идентификатор должностного лица - автора резолюции, в соответствие со справочником "Подразделения".
        /// </param>
        /// <param name="as_item_number">
        /// Номер пункта.
        /// </param>
        /// <param name="as_resolution_text">
        /// Текст поручения.
        /// </param>
        /// <param name="an_isn_category">
        /// Идентификатор категории поручения, в соответствие со справочником "Категории поручений".
        /// </param>
        /// <param name="an_conf">
        /// Флаг конфиденциальности резолюции.
        /// </param>
        /// <param name="ad_resolution_date">
        /// Дата поручения.
        /// </param>
        /// <param name="ad_send_date">
        /// Дата рассылки (не используется).
        /// </param>
        /// <param name="an_notify_author">
        /// Флаг рассылки в кабинет.
        /// </param>
        /// <param name="ad_plan_date">
        /// Плановая дата.
        /// </param>
        /// <param name="ad_interim_date">
        /// Промежуточная дата.
        /// </param>
        /// <param name="ad_fact_date">
        /// Фактическая дата.
        /// </param>
        /// <param name="as_due_controller">
        /// Код due контролера поручения, в соответствие со справочником "Подразделения".
        /// </param>
        /// <param name="an_control_state">
        /// Флаг контрольности.
        /// </param>
        /// <param name="as_summary">
        /// Ход исполнения поручения.
        /// </param>
        /// <param name="an_cascade_control">
        /// Флаг  снятия с контроля "каскадно".
        /// </param>
        /// <param name="an_control_duty">
        /// Флаг "Резолюция отвечает за контрольность РК".
        /// </param>
        /// <param name="as_resume">
        /// Основание для снятия с контроля.
        /// </param>
        /// <param name="an_left_resolution">
        /// Флаг "Рассылать резолюцию".
        /// </param>
        /// <param name="an_cycle">
        /// Флаг цикла. Осуществлять ли рассылку в кабинет исполнителю, если его кабинет тот же, что и кабинет автора.
        /// </param>
        /// <param name="as_note">
        /// Примечание.
        /// </param>
        /// <param name="as_rep_isns">
        /// Список идентификаторов исполнителей, в соответствие со справочником "Подразделения".
        /// </param>
        /// <param name="as_rep_responsible_isns">
        /// Список идентификаторов ответственных исполнителей, в соответствие со справочником "Подразделения".
        /// </param>
        /// <param name="an_cab">
        /// Идентификатор кабинета, в соответствие со справочником "Кабинеты".
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        /// <param name="an_is_project">
        /// Признак для создания проекта резолюции
        /// С версии 11.0 (Параметр не доступен из процессов без установки дополнительного обновления.)
        /// </param>
        void add_resolution(dynamic oHead, ref int? an_isn_resolution, int an_kind_resolution, int an_isn_ref_doc, int? an_isn_parent_resolution, string an_isn_author, string as_item_number, string as_resolution_text, int? an_isn_category, int an_conf, DateTime? ad_resolution_date, DateTime? ad_send_date, int an_notify_author, DateTime? ad_plan_date, DateTime? ad_interim_date, DateTime? ad_fact_date, string as_due_controller, int? an_control_state, string as_summary, int an_cascade_control, int an_control_duty, string as_resume, int an_left_resolution, int an_cycle, string as_note, string as_rep_isns, string as_rep_responsible_isns, int? an_cab, int an_is_project);
        /// <summary>
        /// Хранимая процедура изменяет данные поручения.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="a_isn_resolution">
        /// Идентификатор поручения.
        /// </param>
        /// <param name="a_plan_date">
        /// Плановая дата.
        /// </param>
        /// <param name="a_interim_date">
        /// Промежуточная дата.
        /// </param>
        /// <param name="a_send_date">
        /// Дата рассылки.
        /// </param>
        /// <param name="a_summary">
        /// Ход исполнения поручения.
        /// </param>
        /// <param name="a_resume">
        /// Основание для снятия с контроля.
        /// </param>
        /// <param name="a_resolution_text">
        /// Текст поручения.
        /// </param>
        /// <param name="a_isn_status_exec">
        /// Идентификатор состояния исполнения, в соответствие со справочником «Статус исполнения (поручение)».
        /// </param>
        /// <param name="a_control_state">
        /// Флаг контрольности.
        /// </param>
        /// <param name="a_note">
        /// Примечание.
        /// </param>
        /// <param name="a_control_duty">
        /// Флаг "Резолюция отвечает за контрольность РК".
        /// </param>
        /// <param name="a_due_controller">
        /// Код due контролера поручения, в соответствие со справочником "Подразделения".
        /// </param>
        /// <param name="a_due_author">
        /// Идентификатор должностного лица - автора резолюции, в соответствие со справочником "Подразделения".
        /// </param>
        /// <param name="a_isn_category">
        /// Идентификатор категории поручения, в соответствие со справочником "Категории поручений".
        /// </param>
        /// <param name="a_rep_isns">
        /// Список идентификаторов исполнителей, в соответствие со справочником "Подразделения".
        /// </param>
        /// <param name="a_rep_responsible_isns">
        /// Список идентификаторов ответственных исполнителей, в соответствие со справочником "Подразделения".
        /// </param>
        void edit_resolution(dynamic oHead, int a_isn_resolution, DateTime? a_fact_date, DateTime? a_plan_date, DateTime? a_interim_date, DateTime? a_send_date, string a_summary, string a_resume, string a_resolution_text, int? a_isn_status_exec, int? a_control_state, string a_note, int a_control_duty, string a_due_controller, string a_due_author, int? a_isn_category, string a_rep_isns, string a_rep_responsible_isns);
        /// <summary>
        /// Процедура удаляет поручение. Если установлен флаг каскадного удаления, то удаляются вложенные резолюции.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="an_isn_resolution">
        /// Идентификатор  удаляемого поручения.
        /// </param>
        /// <param name="a_delete_cascade">
        /// Флаг каскадного удаления.
        /// </param>
        void del_resolution(dynamic oHead, int an_isn_resolution, int a_delete_cascade);
        /// <summary>
        /// Выполняет возврат проекта резолюции, путем удаления записи о проекте резолюции из 5 папки кабинета автора проекта резолюции.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="an_isn_resolution">
        /// Идентификатор проекта резолюции.
        /// </param>
        void return_res_proj(dynamic oHead, int an_isn_resolution);
        /// <summary>
        /// Принимает на контроль поручения, список которых указан в as_isns_resolution. Причем могут быть приняты только пункты.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="as_isns_resolution">
        /// Идентификатор  удаляемого поручения.
        /// </param>
        /// <param name="an_cab">
        /// Идентификатор кабинета, в соответствие со справочником «Кабинеты».
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        void accept_control(dynamic oHead, string as_isns_resolution, int? an_cab);
        /// <summary>
        /// Процедура записывает отчет о выполнении поручения в БД.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="an_isn_reply">
        /// Идентификатор исполнителя отчета.
        /// </param>
        /// <param name="as_reply_text">
        /// Текст отчета об исполнении.
        /// </param>
        /// <param name="ad_reply_date">
        /// Дата  исполнения поручения в формате YYYYMMDD (null, если нет даты).
        /// </param>
        /// <param name="an_status_reply">
        /// Идентификатор состояния исполнения отчета, в соответствие со справочником «Состояние исполнения (исполнитель)» (если без статуса, то -1).
        /// </param>
        /// <param name="an_cab">
        /// Идентификатор кабинета, в соответствие со справочником «Кабинеты».
        /// С версии 8.10 параметр устарел, не используется.
        /// </param>
        void set_reply(dynamic oHead, int an_isn_reply, string as_reply_text, DateTime? ad_reply_date, int an_status_reply, int? an_cab);
        /// <summary>
        /// Документ переносится в папку "На исполнении" того же кабинета, и, в случае когда элемент связан с исполнителем, проставляется дата приема к исполнению.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="an_isn_folder_item">
        /// Идентификатор ссылки на документ из папки «Поступившие».
        /// </param>
        void accept_execute(dynamic oHead, int an_isn_folder_item);
        /// <summary>
        /// Документ переносится в папку "На исполнении" того же кабинета, и, в случае когда элемент связан с исполнителем, проставляется дата приема к исполнению.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="an_isn_folder_item">
        /// Идентификатор  элемента папки.
        /// </param>
        /// <param name="an_kind_doc">
        /// Вид  документа.
        /// </param>
        void mark_fi_as_read(dynamic oHead, int an_isn_folder_item, int an_kind_doc);
        /// <summary>
        /// Документ переносится в папку "На исполнении" того же кабинета, и, в случае когда элемент связан с исполнителем, проставляется дата приема к исполнению.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="an_isn_folder_item">
        /// Идентификатор  элемента папки.
        /// </param>
        void delete_fi(dynamic oHead, int an_isn_folder_item);
        /// <summary>
        /// Обновляет значение указанного параметра. В случае, когда такой записи не существует, добавляет ее. Если установлен флаг af_default, то значение параметра устанавливается по умолчанию для всех пользователей.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="as_parm_name">
        /// Имя  параметра.
        /// </param>
        /// <param name="as_parm_value">
        /// Значение параметра.
        /// </param>
        /// <param name="af_default">
        /// Флаг «по умолчанию для всех пользователей».
        /// </param>
        void set_user_parm(dynamic oHead, string as_parm_name, string as_parm_value, int? af_default);
        /// <summary>
        /// В зависимости от значения параметра aOper процедура выполняет следующие функции:
        /// •	'G' - поиск организации по наименованию. Возвращаются идентификатор и код due найденной записи в параметрах aIsn и aDue соответственно.
        /// •	'I' - добавить запись в справочник. Не выполняется если был выполнен успешный поиск.
        /// •	'F' - добавить техническую запись в таблицу DEPARTMENT. Может использоваться в комбинации 'IF' или отдельно.
        /// •	'U' - изменить запись.
        /// •	'UA' - изменить поля только если поле не заполнено. Используется при заполнении 	реквизитов из e-mail.
        /// •	'D' - удалить запись (не реализовано).
        /// •	'L' - найдена логически удаленная запись (только выход).
        /// Операция 'G' - может комбинироваться с любой из операций изменения справочника. Например, 'GI' – найти организацию, а если не найдена, то добавить.
        /// Операции 'I', 'U' и 'D' взаимоисключающие.
        /// После выполнения процедуры, в aOper возвращаются коды выполненных операций.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aOper">
        /// Операция. На входе – требуемая операция, на выходе – выполненная операция.
        /// </param>
        /// <param name="aIsn">
        /// Возвращаемый параметр – идентификатор записи . Для операций 'I' и 'G' - должны быть 0.
        /// </param>
        /// <param name="aDue">
        /// Возвращаемый параметр  due записи. Для операций 'I' и 'G' – должен быть пробел.
        /// </param>
        /// <param name="aHighNode">
        /// Идентификатор или код due родительской записи для операции 'I'.
        /// </param>
        /// <param name="aClassifName">
        /// Наименование.
        /// </param>
        /// <param name="aZipCode">
        /// Почтовый индекс.
        /// </param>
        /// <param name="aCity">
        /// Город.
        /// </param>
        /// <param name="aAddress">
        /// Адрес организации
        /// </param>
        /// <param name="aFullname">
        /// Информационный атрибут новой организации (имя).
        /// </param>
        /// <param name="aOkpo">
        /// Информационный атрибут новой организации (ОКПО).
        /// </param>
        /// <param name="aInn">
        /// Информационный атрибут новой организации (ИНН).
        /// </param>
        /// <param name="aDue_region">
        /// Информационный атрибут новой организации (Код due региона, в соответствие со справочником «Регионы»).
        /// </param>
        /// <param name="aEmail">
        /// Электронный адрес.
        /// </param>
        /// <param name="aMailForAll">
        /// Признак использования E_MAIL для всех представителей.
        /// </param>
        /// <param name="aIsnAddrCategory">
        /// Категория адресата.
        /// </param>
        /// <param name="aNote">
        /// Примечание.
        /// </param>
        /// <param name="aOkonh">
        /// Код по ОКОНХ. (с версии 13.2)
        /// </param>
        /// <param name="aLawAdress">
        /// Юридический адрес организации. (с версии 13.2)
        /// </param>
        /// <param name="aSertificat">
        /// Сертификат. (с версии 13.2)
        /// </param>
        /// <param name="aCode">
        /// Код организации. (с версии 13.2)
        /// </param>
        void save_organiz_web(dynamic oHead, string aOper, ref int? aIsn, ref string aDue, string aHighNode, string aClassifName, string aZipCode, string aCity, string aAddress, string aFullname, string aOkpo, string aInn, string aDue_region, string aEmail, int aMailForAll, int aIsnAddrCategory, string aNote, string aOkonh, string aLawAdress, string aSertificat, string aCode);
        /// <summary>
        /// В зависимости от значения параметра a_oper процедура выполняет следующие функции:
        /// •	'I' - добавить запись.
        /// При добавлении заполнять только поля a_oper и isn_bank_recv.
        /// •	'U' - изменить запись.
        /// •	'D' – удалить запись.
        /// При удалении заполнять только a_isn_bank_recv.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="a_oper">
        /// Операция.
        /// </param>
        /// <param name="a_isn_bank_recv">
        /// Идентификатор записи реквизита. Для операции создания выходной параметр, для удаления и изменения входной параметр.
        /// </param>
        /// <param name="a_isn_organiz">
        /// Идентификатор организации.
        /// </param>
        /// <param name="a_classif_name">
        /// Наименование.
        /// </param>
        /// <param name="a_bank_name">
        /// Банк.
        /// </param>
        /// <param name="a_acount">
        /// Счет.
        /// </param>
        /// <param name="a_subacount">
        /// Корсчет.
        /// </param>
        /// <param name="a_bik">
        /// БИК.
        /// </param>
        /// <param name="a_city">
        /// Город.
        /// </param>
        /// <param name="a_note">
        /// Примечание.
        /// </param>
        void save_organiz_bank_recvisit(dynamic oHead, string a_oper, ref int? a_isn_bank_recv, int a_isn_organiz, string a_classif_name, string a_bank_name, string a_acount, string a_subacount, string a_bik, string a_city, string a_note);
        /// <summary>
        /// Если соответствующей записи для контакта нет в таблице DEPARTMENT процедура сначала создает ее, затем возвращает значение поля DUE для этой записи.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="a_isn_contact">
        /// Идентификатор контакта.
        /// </param>
        /// <param name="a_due">
        /// Возвращаемый параметр кода due department.
        /// </param>
        void get_tech_due(dynamic oHead, int a_isn_contact, ref string a_due);
        /// <summary>
        /// В зависимости от значения параметра aOper процедура выполняет следующие функции:
        /// •	'G' - поиск гражданина по 4 полям (aSurname, aZipcode, aCity, aAddress). Возвращаются идентификатор и код due найденной записи в параметрах aIsn и aDue соответственно.
        /// •	'I' - добавить запись в справочник. Не выполняется если был выполнен успешный поиск.
        /// •	'U' - изменить запись.
        /// •	'UA' - изменить поля только если поле не заполнено. Используется при заполнении 	реквизитов из e-mail.
        /// •	'D' - удалить запись (не реализовано).
        /// •	'L' - найдена логически удаленная запись (только выход).
        /// Операция 'G' - может комбинироваться с любой из операций изменения справочника. Например, 'GI' – найти организацию, а если не найдена, то добавить.
        /// Операции 'I', 'U' и 'D' взаимоисключающие.
        /// После выполнения процедуры, в aOper возвращаются коды выполненных операций.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aOper">
        /// Операция. На входе  - требуемая операция, на выходе  - выполненная операция.
        /// </param>
        /// <param name="aIsn">
        /// Возвращаемый параметр – идентификатор записи . Для операций 'I' и 'G' - должны быть 0.
        /// </param>
        /// <param name="aSurname">
        /// ФИО.
        /// </param>
        /// <param name="aZipCode">
        /// Почтовый индекс.
        /// </param>
        /// <param name="aCity">
        /// Город.
        /// </param>
        /// <param name="aAddress">
        /// Адрес гражданина.
        /// </param>
        /// <param name="aDue_region">
        /// Информационный атрибут новой организации (Код due региона, в соответствие со справочником «Регионы»).
        /// </param>
        /// <param name="aIsn_Addr_Category">
        /// Категория адресата.
        /// </param>
        /// <param name="aCitstatusCl_Due">
        /// Код due статуса, в соответствие со справочником «Статус заявителя».
        /// </param>
        /// <param name="aEmail">
        /// Адрес электронной почты гражданина.
        /// </param>
        /// <param name="aPhone">
        /// Номер телефона гражданина.
        /// </param>
        /// <param name="aNote">
        /// Примечание. С версии 12.2
        /// </param>
        /// <param name="aInn">
        /// ИНН гражданина. С версии 13.2
        /// </param>
        /// <param name="aSnils">
        /// СНИЛС гражданина. С версии 13.2.
        /// </param>
        /// <param name="aSex">
        /// Пол. С версии 13.2.
        /// </param>
        /// <param name="aSeries">
        /// Серия паспорта. С версии 13.2.
        /// </param>
        /// <param name="aNPasport">
        /// Номер паспорта. С версии 13.2.
        /// </param>
        /// <param name="aGiven">
        /// Кем и когда выдан паспорт. С версии 13.2.
        /// </param>
        void save_citizen_web(dynamic oHead, string aOper, ref int? aIsn, string aSurname, string aZipCode, string aCity, string aAddress, string aDue_region, int? aIsn_Addr_Category, string aCitstatusCl_Due, string aEmail, string aPhone, string aNote, string aInn, string aSnils, int aSex, string aSeries, string aNPasport, string aGiven);
        /// <summary>
        /// Процедура освобождает зарезервированный номер в таблице номерообразования, который был занят при помощи reserve_prj_num.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aduedocgroup">
        /// Код due который подавали в reserve_num.
        /// </param>
        /// <param name="ayear">
        /// Значение года.
        /// </param>
        /// <param name="aordernum">
        /// Порядковый номер, полученный с помощью reserve_num.
        /// </param>
        void release_prj_num_web(dynamic oHead, string aduedocgroup, int ayear, int aordernum);
        /// <summary>
        /// Процедура освобождает зарезервированный номер в таблице номерообразования, который был занят при помощи reserve_num.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aduedocgroup">
        /// Код due который подавали в reserve_num.
        /// </param>
        /// <param name="ayear">
        /// Значение года.
        /// </param>
        /// <param name="aordernum">
        /// Порядковый номер, полученный с помощью reserve_num.
        /// </param>
        void release_num_web(dynamic oHead, string aduedocgroup, int ayear, int aordernum);
        /// <summary>
        /// Процедура добавляет запись в протокол просмотра или изменения объекта в зависимости от идентификатора операции протоколирования.
        /// Аргументы aTable_Id, aOper_Id, aSuboper_Id, aOper_Describe вместе составляют уникальный ключ операции протоколирования.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aTable_Id">
        /// Вида объекта протокола
        /// </param>
        /// <param name="aOper_Id">
        /// Вид операция протокола
        /// </param>
        /// <param name="aSuboper_Id">
        /// Подвид операции протокола
        /// </param>
        /// <param name="aOper_Describe">
        /// Уточнение операции протокола
        /// </param>
        /// <param name="aRef_Isn">
        /// Идентификатор объекта
        /// </param>
        /// <param name="aOper_Comment">
        /// Комментарий
        /// </param>
        void write_prot(dynamic oHead, string aTable_Id, string aOper_Id, string aSuboper_Id, string aOper_Describe, int aRef_Isn, string aOper_Comment);
        /// <summary>
        /// Процедура добавляет (создает) новый экземпляр процесса по заданной конфигурационной ассоциации.
        /// Правила заполнения параметра aObjectId см. в описании процедуры add_evnt_queue_item.
        /// Если параметр aParams не null, то его значение должно быть правильным xml документом.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aIsnProcessConfig">
        /// Идентификатор записи о конфигурации типа процесса
        /// </param>
        /// <param name="aMessage">
        /// Сообщение – комментарий поясняющий причину создания процесса
        /// </param>
        /// <param name="aObjectId">
        /// Идентификатор объекта
        /// </param>
        /// <param name="aParams">
        /// Параметры запуска процесса
        /// </param>
        /// <param name="aIsnInstance">
        /// Возвращаемое значение, идентификатор записи экземпляра.
        /// </param>
        void add_wf_instance(dynamic oHead, int aIsnProcessConfig, string aMessage, string aObjectId, string aParams, ref int? aIsnInstance);
        /// <summary>
        /// Процедура добавляет новое событие в очередь. Событие относится к объекту системы, ссылка на который указывается параметрами aObjectName и aObjectId. Значение параметра aObjectId формируется по следющим правилам:
        /// •	Если идентификатор объекта это число (например DOC_RC.ISN_DOC) – то в параметре aObjectId должна передаваться строка ISN#(ISN_DOC).
        /// •	Eсли идентификатор это код DUE – то в параметре aObjectId передается значение кода.
        /// Если объект к которому относится сообщение это РК, РКПД или поручение, то в параметре aDueDocgroup должен передаваться код DUE соответствующей группы документов. Для остальных типов объектов этот параметр не заполняется.
        /// В параметре aFlags может передаваться произвольная информация, но желательно передавать в нем список строковых идентификаторов, разделенных символом «,» (запятая): (flag1),(flag2),…,(flagN)
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="aKindEvent">
        /// Тип события
        /// </param>
        /// <param name="aObjectName">
        /// Тип объекта
        /// </param>
        /// <param name="aObjectId">
        /// Идентификатор объекта
        /// </param>
        /// <param name="aDueDocgroup">
        /// Код due группы документов
        /// </param>
        /// <param name="aFlags">
        /// Дополнительные флаги события
        /// </param>
        void add_evnt_queue_item(dynamic oHead, int aKindEvent, string aObjectName, string aObjectId, string aDueDocgroup, string aFlags);
        /// <summary>
        /// Процедура создает напоминание исполнителю поручения.
        /// Для вызова данной процедуры пользователь должен быть допущенн к 3-й папке кабинета, и поручение должно находиться в 3-й папке кабинета.
        /// </summary>
        /// <param name="oHead">Головной объект системы, поддерживающий интерфейс IHead.</param>
        /// <param name="a_isn_reply">
        /// Идентификатор исполнителя отчета.
        /// </param>
        /// <param name="a_reminder_text">
        /// Текст напоминания.
        /// </param>
        void add_reminder(dynamic oHead, int a_isn_reply, string a_reminder_text);
    }
}
﻿namespace bgTeam.KitVlgDocs.Delo
{

    public static class SearchTablesParams
    {
        public const string Result = "Result";

        public const string DocKind = "DocKind";

        public static class Rc
        {
            public const string DocDate = "Rc.DocDate";

            public const string DocGroup = "Rc.DocGroup";

            public const string Delivery = "Rc.Delivery";
        }

        public static class Addr
        {
            public const string AddrCode = "Addr.AddrCode";

            public const string SendType = "Addr.SendType";
         }

        public static class AddRc
        {
            public const string DataExport = "AddRc.Data_export";

            public const string TimeExport = "AddRc.Time_export";
        }
    }
}

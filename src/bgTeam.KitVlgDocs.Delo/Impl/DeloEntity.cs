﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class DeloEntity : DeloError
    {
        protected DeloEntity(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Системный номер записи в справочнике.
        /// </summary>
        public int Isn
            => Exec(x => (int)x.Isn);
    }
}

﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    ///  .
    /// </summary>
    public class DeloSigns : DeloEntity
    {
        public DeloSigns(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// ДЛ, подписавшее документ.
        /// </summary>
        public DeloDepartment WhoSign
            => Wrap<DeloDepartment>(x => x.Who_Sign);
    }
}

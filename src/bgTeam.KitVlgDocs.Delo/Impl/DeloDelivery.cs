﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    ///  .
    /// </summary>
    public class DeloDelivery : DeloDictionaryEntity
    {
        public DeloDelivery(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Наименование вида доставки (отправки) документа.
        /// </summary>
        public string Name
            => Exec(x => x.Name as string);
    }
}

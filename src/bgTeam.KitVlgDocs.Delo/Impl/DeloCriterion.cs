﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    /// 
    /// </summary>
    public class DeloCriterion : DeloError
    {
        protected DeloCriterion()
            : base()
        {
        }

        protected DeloCriterion(dynamic @object)
            : base((object)@object)
        {
        }
    }
}

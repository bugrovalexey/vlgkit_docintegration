﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using Microsoft.Extensions.Options;

    /// <inheritdoc/>
    public class DeloService : IDeloService
    {
        private static readonly Regex _fioRegex = new Regex(@"([A-Za-zА-Яа-я]+)\s*([A-Za-zА-Яа-я]?)\.\s*([A-Za-zА-Яа-я]?)\.", RegexOptions.Compiled);

        private readonly object _guard = new object();

        private readonly IDeloHead _head;
        private readonly IAppLogger _logger;
        private readonly IEApiDeloService _eApi;
        private readonly IOptions<DeloOptions> _options;

        private int _delivery;
        private int _securlevel;
        private string _dueCard;
        private string _dueDocgroupExp;
        private string _dueDocgroupImp;
        private string _dueOrganiz;
        private bool _wasCached;

        public DeloService(IAppLogger logger, IDeloHead head, IEApiDeloService eApi, IOptions<DeloOptions> options)
        {
            _logger = logger;
            _eApi = eApi;
            _head = head;
            _options = options;
        }

        /// <inheritdoc/>
        public DeloDocumentExportStatusDto ExportDocument(DeloOutgoingDocumentDto document)
        {
            EnsureInitialized();

            var now = DateTime.Now;

            int? rcAIsn = 0;
            int? rcOrderNum = 0;
            string rcFreeNum = null;
            string duePersonWhos = null;

            try
            {
                _eApi.ReserveNum(
                    _head,
                    aDueDocgroup: _dueDocgroupExp,
                    aYear: now.Year,
                    aDueCard: _dueCard,
                    aSessionId: null,
                    ref rcOrderNum,
                    ref rcFreeNum);

                _logger.Info($"ReserveNum: {rcOrderNum}; {rcFreeNum};");

                if (document.Addresse != null)
                {
                    var vocabulary = _head.GetCriterion(SearchCriterion.Vocabulary) as DeloSearchVocab;

                    vocabulary.Vocabulary = SearchVocab.Department;
                    vocabulary.SetSearchValue(SearchVocabParams.Name, document.Addresse.Name);
                    vocabulary.SetSearchValue(SearchVocabParams.Post, document.Addresse.Position);

                    var resultset = _head.GetResultSet();

                    resultset.Source = vocabulary;

                    var count = resultset.Fill();

                    if (count > 0)
                    {
                        duePersonWhos = (resultset.GetItem(0) as DeloDepartment).Dcode;
                    }
                    else
                    {
                        _logger.Warning($"Couldn't find addressee {document.Addresse.Name}.");
                    }
                }

                var signer = !string.IsNullOrEmpty(document.Signer?.Name) && _fioRegex.Match(document.Signer.Name) is Match m01 && m01.Success
                    ? $"{m01.Groups[1]} {m01.Groups[2]}.{m01.Groups[3]}."
                    : document.Signer?.Name;

                _eApi.AddRC(
                    _head,
                    ref rcAIsn,
                    aDueCard: _dueCard,
                    aIsnCab: 0,
                    aDueDocgroup: _dueDocgroupExp,
                    aOrderNum: rcOrderNum ?? 0,
                    aFreeNum: rcFreeNum,
                    aDocDate: now,
                    aSecurlevel: _securlevel,
                    aConsists: document.IncludeList,
                    aSpecimen: null,
                    aPlanDate: document.ControlDate,
                    aFactDate: null,
                    aControlState: null,
                    aAnnotat: document.Annotation,
                    aNote: null,
                    aDuePersonWhos: duePersonWhos,
                    aIsnDelivery: _delivery,
                    aDueOrganiz: _dueOrganiz,
                    aIsnContact: null,
                    aCorrespNum: document.RegistrationNumber,
                    aCorrespDate: document.RegistrationDate,
                    aCorrespSign: signer,
                    aIsnCitizen: null,
                    aIsCollective: null,
                    aIsAnonim: null,
                    aSigns: null,
                    aDuePersonExe: null,
                    aIsnNomenc: null,
                    aNothardcopy: null,
                    aCito: null,
                    aIsnLinkingDoc: null,
                    aIsnLinkingPrj: null,
                    aIsnClLink: null,
                    aCopyShablon: null,
                    aVisas: null,
                    aEDocument: null,
                    aSends: null,
                    askipcopy_ref_file_isns: null,
                    aIsnLinkTranparent: null,
                    aIsnLinkTranparentPare: null,
                    aTelNum: null);

                if (document.Subjects?.Length > 0 && rcAIsn != null)
                {
                    var rubrics = new List<string>();
                    var vocabulary = _head.GetCriterion(SearchCriterion.Vocabulary) as DeloSearchVocab;

                    vocabulary.Vocabulary = SearchVocab.Rubric;
                    var resultset = _head.GetResultSet();

                    resultset.Source = vocabulary;

                    for (int i = 0; i < document.Subjects.Length; i++)
                    {
                        vocabulary.SetSearchValue(SearchVocabParams.Name, document.Subjects[i]);

                        var count = resultset.Fill();

                        if (count > 0)
                        {
                            rubrics.Add((resultset.GetItem(0) as DeloRubric).Dcode);
                        }
                        else
                        {
                            _logger.Warning($"Couldn't find rubric {document.Subjects[i]}.");
                        }
                    }

                    _eApi.AddRubric(_head, rcAIsn.Value, string.Join("|", rubrics), null);
                }
            }
            catch (Exception)
            {
                if ((rcAIsn == null || rcAIsn == 0) && rcOrderNum > 0)
                {
                    _eApi.ReturnNum(_head, _dueDocgroupExp, now.Year, rcOrderNum.Value, ref rcFreeNum);
                }

                throw;
            }

            if (rcAIsn != null)
            {
                var directory = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString()));

                foreach (var (item, signatures) in document.Files)
                {
                    if (item.Data != null)
                    {
                        var name = Path.Combine(directory.FullName, item.FileName);

                        using (var stream = File.Create(name))
                        {
                            stream.Write(item.Data, 0, item.Data.Length);
                        }

                        _eApi.SaveFileWF(
                            _head,
                            action: 1,
                            aISN_REF_FILE: null,
                            rcAIsn.Value,
                            aKind_Doc: 1,
                            aDescription: item.FileName,
                            aCategory: null,
                            aSecur: null,
                            aLockFlag: null,
                            aFileAccessDues: null,
                            aFilename: name,
                            aDontDelFlag: null);
                    }
                    else if (!string.IsNullOrEmpty(item.Text))
                    {
                        var name = Path.Combine(directory.FullName, item.FileName);

                        File.WriteAllText(name, item.Text);

                        _eApi.SaveFileWF(
                            _head,
                            action: 1,
                            aISN_REF_FILE: null,
                            rcAIsn.Value,
                            aKind_Doc: 1,
                            aDescription: item.FileName,
                            aCategory: null,
                            aSecur: null,
                            aLockFlag: null,
                            aFileAccessDues: null,
                            aFilename: name,
                            aDontDelFlag: null);
                    }
                    else
                    {
                        _logger.Warning($"Null data for file {item.FileName} in document {document.Id}.");
                    }
                }

                directory.Delete(true);

                try
                {
                    if (_head.GetRow("RcIn", rcAIsn.Value) is DeloRcIn rc)
                    {
                        _logger.Info($"Rc files count {rc.FilesCnt}.");

                        for (int i = 0; i < rc.FilesCnt; i++)
                        {
                            var file = rc.GetFiles(i);

                            var fileDto = document.Files.FirstOrDefault(x => string.Equals(x.File.FileName, file.Descript, StringComparison.OrdinalIgnoreCase));

                            if (fileDto?.Signatures?.Length > 0)
                            {
                                foreach (var signature in fileDto.Signatures)
                                {
                                    _logger.Info($"Added eds to {file.Name}");

                                    _eApi.ImportFileEds(
                                        _head,
                                        aIsnRefFile: file.Isn,
                                        aEdsBody: null,
                                        a_eds_body_blob: signature,
                                        a_isn_sign_kind: null,
                                        a_certificate_owner: null,
                                        a_signing_date: null,
                                        a_sign_text: null);
                                }
                            }
                            else if (fileDto == null)
                            {
                                _logger.Info($"Couldn't find file {file.Descript}.");
                            }
                        }
                    }
                    else
                    {
                        _logger.Warning($"RcIn {rcAIsn} not found.");
                    }
                }
                catch (Exception exp)
                {
                    _logger.Error("Exception while adding eds.");
                    _logger.Error(exp);
                }
            }

            return new DeloDocumentExportStatusDto
            {
                Id = document.Id,
                Ins = rcAIsn,
            };
        }

        /// <inheritdoc/>
        public IEnumerable<DeloIncomingDocumentDto> LoadDocumentsForImport()
        {
            EnsureInitialized();
            try
            {
                var result = new List<DeloIncomingDocumentDto>();

                var reultSet = _head.GetResultSet();
                var criterion = _head.GetCriterion(SearchCriterion.Table) as DeloSearchTables;

                criterion.SetParameters(SearchTablesParams.Result, SearchTablesParamValues.Result.Doc);
                criterion.SetParameters(SearchTablesParams.DocKind, SearchTablesParamValues.DocKind.Out);
                criterion.SetParameters(SearchTablesParams.Addr.SendType, _delivery);
                criterion.SetParameters(SearchTablesParams.Rc.DocGroup, _dueDocgroupImp);
                criterion.SetParameters(SearchTablesParams.AddRc.DataExport, SearchTablesParamValues.IsNull);

                reultSet.Source = criterion;

                var count = reultSet.Fill();

                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (reultSet.GetItem(i) is DeloRcOut item)
                        {
                            var iResult = new DeloIncomingDocumentDto
                            {
                                Isn = item.Isn,
                                Annotation = item.Contents,
                                IncludeList = item.Consist,
                                RegistrationDate = item.DocDate,
                                RegistrationNumber = item.RegNum,
                            };

                            {
                                var addrs = new List<DeloAddresseeDto>();

                                try
                                {
                                    for (int i1 = 0; i1 < item.AddrCnt; i1++)
                                    {
                                        if (item.GetAddr(i1) is DeloAddr addr)
                                        {
                                            switch (addr.Addressee)
                                            {
                                                case DeloOrganiz addressee:
                                                    addrs.Add(new DeloAddresseeDto
                                                    {
                                                        Name = addressee.Name,
                                                        Surname = addr.Person,
                                                        FullName = addressee.FullName,
                                                    });
                                                    break;
                                                case DeloCitizen addressee:
                                                    addrs.Add(new DeloAddresseeDto
                                                    {
                                                        Name = addressee.Name,
                                                        Surname = addr.Person,
                                                    });
                                                    break;
                                                case DeloDepartment addressee:
                                                    addrs.Add(new DeloAddresseeDto
                                                    {
                                                        Surname = addr.Person,
                                                        Department = addressee.Name,
                                                    });
                                                    break;
                                            }
                                        }
                                    }
                                }
                                catch (Exception exp)
                                {
                                    _logger.Error(exp);
                                }

                                iResult.Addressees = addrs.ToArray();
                            }

                            {
                                var files = new List<DeloFileDto>();
                                var signs = new List<FileDto>();

                                var directory = Directory.CreateDirectory(
                                    Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString()));

                                try
                                {
                                    for (int i1 = 0; i1 < item.FilesCnt; i1++)
                                    {
                                        var fileDto = new DeloFileDto();

                                        if (item.GetFiles(i1) is DeloFiles file && file.Contents is DeloContents contents)
                                        {
                                            contents.Prepare(directory.FullName);

                                            try
                                            {
                                                _logger.Info($"Load document {item.Isn} file {file.Name}.");

                                                fileDto.Content = new FileDto
                                                {
                                                    FileName = file.Name,
                                                    Data = File.ReadAllBytes(Path.Combine(directory.FullName, contents.Name)),
                                                };
                                            }
                                            finally
                                            {
                                                contents.Unprepare();
                                            }

                                            for (int i2 = 0; i2 < file.EDSCnt; i2++)
                                            {
                                                var eds = file.GetEDS(i2);

                                                var edsPath = Path.Combine(
                                                    directory.FullName,
                                                    $"{Path.GetFileNameWithoutExtension(contents.Name)}_{i2:00}_eds.p7s");

                                                eds.SaveToFile(edsPath);

                                                signs.Add(new FileDto
                                                {
                                                    FileName = Path.GetFileName(edsPath),
                                                    Data = File.ReadAllBytes(edsPath),
                                                });
                                            }

                                            fileDto.Signatures = signs.ToArray();

                                            files.Add(fileDto);

                                            signs.Clear();
                                        }
                                    }
                                }
                                finally
                                {
                                    directory.Delete(true);
                                }

                                iResult.Files = files.ToArray();
                            }

                            {
                                var rubrics = new List<string>();

                                try
                                {
                                    for (int i1 = 0; i1 < item.RubricCnt; i1++)
                                    {
                                        var rubric = item.GetRubric(i1);
                                        rubrics.Add(rubric.Name);
                                    }
                                }
                                catch (Exception exp)
                                {
                                    _logger.Error(exp);
                                }

                                iResult.Subjects = rubrics.ToArray();
                            }

                            {
                                var signs = new List<string>();

                                try
                                {
                                    for (int i1 = 0; i1 < item.PersonSignsCnt; i1++)
                                    {
                                        var sign = item.GetPersonSigns(i1);
                                        signs.Add(sign.WhoSign.Name);
                                    }
                                }
                                catch (Exception exp)
                                {
                                    _logger.Error(exp);
                                }

                                iResult.Signers = signs.ToArray();
                            }

                            result.Add(iResult);
                        }
                    }
                }
                else if (count == 0)
                {
                    _logger.Info("No documents for import.");
                }
                else
                {
                    throw new InvalidOperationException($"RcOut fill error {count}; {reultSet.ErrCode}:{reultSet.ErrText}.");
                }

                return result.ToArray();
            }
            finally
            {
                _head.Close();
            }
        }

        /// <inheritdoc/>
        public void SetImportState(int rcIsn, string regNum, DateTime? regDate, DateTime? exporDateTime)
        {
            EnsureInitialized();

            try
            {
                var date = exporDateTime?.ToString("yyyyMMdd");
                var time = exporDateTime?.ToString("HHmmss");

                _eApi.EditAr(_head, rcIsn, "Data_export", string.Empty, date);
                _eApi.EditAr(_head, rcIsn, "Time_export", string.Empty, time);

                var document = _head.GetRow("RcOut", rcIsn) as DeloRcOut;

                if (document == null)
                {
                    throw new InvalidOperationException($"Document {rcIsn} not found.");
                }

                try
                {
                    if (document?.AddrCnt > 0)
                    {
                        var addr = document.GetAddr(0);

                        _eApi.EditOuterSend(
                            _head,
                            rcIsn,
                            a_isn_ref_send: addr.Isn,
                            a_card: null,
                            a_cab: 0,
                            a_isn_contact: addr.Contact,
                            a_send_date: addr.SendDate,
                            a_send_person: addr.Person,
                            a_isn_delivery: addr.Delivery?.Isn ?? _delivery,
                            a_sending_type: 0,
                            a_note: addr.Person,
                            a_reg_n: regNum,
                            a_reg_date: regDate,
                            a_answer: addr.Answer);
                    }
                }
                catch (Exception exp)
                {
                    _logger.Error("Exception while setting registration number and date.");
                    _logger.Error(exp);
                    throw;
                }

                SetImportStateNoteShared(document, rcIsn, true);
            }
            finally
            {
                _head.Close();
            }
        }

        /// <inheritdoc/>
        public void SetImportStateNote(int rcIsn, bool isSuccessful)
        {
            EnsureInitialized();

            try
            {
                var document = _head.GetRow("RcOut", rcIsn) as DeloRcOut;

                if (document == null)
                {
                    throw new InvalidOperationException($"Document {rcIsn} not found.");
                }

                SetImportStateNoteShared(document, rcIsn, isSuccessful);
            }
            finally
            {
                _head.Close();
            }
        }

        private void EnsureInitialized()
        {
            if (!_head.Active)
            {
                lock (_guard)
                {
                    if (!_head.Active)
                    {
                        if (!string.IsNullOrWhiteSpace(_options.Value.Settings) && _options.Value.Settings.Length > 0)
                        {
                            _head.LoadSettings(_options.Value.Settings);
                        }
                        _logger.Info($"Connecting to Delo EApi: Host {_options.Value.Host}, Owner {_options.Value.Owner}, Login {_options.Value.Login}");

                        if (!_head.OpenWithParamsEx(_options.Value.Host, _options.Value.Owner, _options.Value.Login, _options.Value.Password))
                        {
                            throw new InvalidOperationException($"Couldn`t connect to EApi.");
                        }

                        _logger.Info("Delo EApi connected successfully.");

                        _logger.Info($"User { _head.UserInfo.Name}, ISN {_head.UserInfo.Isn}; Server time is {_head.ServerToday}.");
                    }
                }
            }

            if (!_wasCached)
            {
                lock (_guard)
                {
                    if (!_wasCached)
                    {
                        var vocabulary = _head.GetCriterion(SearchCriterion.Vocabulary) as DeloSearchVocab;

                        vocabulary.Vocabulary = SearchVocab.Security;
                        vocabulary.SetSearchValue(SearchVocabParams.Name, _options.Value.Securlevel);

                        var resultset = _head.GetResultSet();

                        resultset.Source = vocabulary;

                        var count = resultset.Fill();

                        if (count > 0)
                        {
                            _securlevel = resultset.GetItem(0).Isn;
                        }
                        else
                        {
                            throw new InvalidOperationException($"{SearchVocab.Security} fill error {count}.");
                        }

                        vocabulary.Vocabulary = SearchVocab.Delivery;
                        vocabulary.SetSearchValue(SearchVocabParams.Name, _options.Value.Delivery);

                        count = resultset.Fill();

                        if (count > 0)
                        {
                            _delivery = resultset.GetItem(0).Isn;
                        }
                        else
                        {
                            throw new InvalidOperationException($"{SearchVocab.Delivery} fill error {count}.");
                        }

                        vocabulary.Vocabulary = SearchVocab.DocGroup;
                        vocabulary.SetSearchValue(SearchVocabParams.Name, _options.Value.ExportDocgroup);

                        count = resultset.Fill();

                        if (count > 0)
                        {
                            _dueDocgroupExp = (resultset.GetItem(0) as DeloDocGroup).Dcode;
                        }
                        else
                        {
                            throw new InvalidOperationException($"{SearchVocab.DocGroup} fill error {count}.");
                        }

                        vocabulary.Vocabulary = SearchVocab.DocGroup;
                        vocabulary.SetSearchValue(SearchVocabParams.Name, _options.Value.ImportDocgroup);

                        count = resultset.Fill();

                        if (count > 0)
                        {
                            _dueDocgroupImp = (resultset.GetItem(0) as DeloDocGroup).Dcode;
                        }
                        else
                        {
                            throw new InvalidOperationException($"{SearchVocab.DocGroup} fill error {count}.");
                        }

                        vocabulary.Vocabulary = SearchVocab.CardIndex;
                        vocabulary.SetSearchValue(SearchVocabParams.Name, _options.Value.Card);

                        count = resultset.Fill();

                        if (count > 0)
                        {
                            _dueCard = (resultset.GetItem(0) as DeloCardIndex).Dcode;
                        }
                        else
                        {
                            throw new InvalidOperationException($"{SearchVocab.CardIndex} fill error {count}.");
                        }

                        vocabulary.Vocabulary = SearchVocab.Organiz;
                        vocabulary.SetSearchValue(SearchVocabParams.Name, _options.Value.Organiz);

                        count = resultset.Fill();

                        if (count > 0)
                        {
                            for (int i = 0; i < count; i++)
                            {
                                var organiz = resultset.GetItem(i) as DeloOrganiz;

                                if (organiz.IsNode || organiz.Deleted)
                                {
                                    continue;
                                }

                                _dueOrganiz = organiz.Dcode;
                                break;
                            }
                        }
                        else
                        {
                            throw new InvalidOperationException($"{SearchVocab.Organiz} fill error {count}.");
                        }

                        _wasCached = true;
                    }
                }
            }

            _logger.Info($"Parameters: SecurLevel {_securlevel};Delivery {_delivery};DueDocGroupExp {_dueDocgroupExp};DueDocGroupImp {_dueDocgroupImp};DueCard {_dueCard};DueOrganiz {_dueOrganiz}");
        }

        private void SetImportStateNoteShared(DeloRcOut document, int rcIsn, bool isSuccessful)
        {
            try
            {
                _eApi.EditRC(
                    _head,
                    rcIsn,
                    null,
                    null,
                    document.RegNum,
                    document.DocDate,
                    document.Security.Isn,
                    document.Consist,
                    document.Specimen,
                    document.PlanDate,
                    document.FactDate,
                    document.IsControl,
                    document.Contents,
                    GenerateNote(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    document.OrderNum,
                    null,
                    null);

                string GenerateNote()
                {
                    var state = isSuccessful ? "выгружен" : "не выгружен";

                    if (string.IsNullOrWhiteSpace(document.Note) || document.Note.Length == 0)
                    {
                        return $"Статус экспорта: {state}";
                    }
                    else if (document.Note.Contains(";Статус экспорта:"))
                    {
                        var splited = document.Note.Split(
                            new[] { ";Статус экспорта:" },
                            StringSplitOptions.RemoveEmptyEntries);

                        return $"{splited[0]};Статус экспорта: {state}";
                    }

                    return $"{document.Note};Статус экспорта: {state}";
                }
            }
            catch (Exception exp)
            {
                _logger.Error("Exception while setting registration state to Note.");
                _logger.Error(exp);
                throw;
            }
        }
    }
}

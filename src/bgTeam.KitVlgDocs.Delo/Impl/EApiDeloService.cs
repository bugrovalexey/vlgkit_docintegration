﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;
    using System.IO;
    using System.Text;

    public class EApiDeloService : IEApiDeloService
    {
        private readonly IAppLogger _logger;

        public EApiDeloService(IAppLogger logger)
        {
            _logger = logger;
        }

        /// <inheritdoc/>
        public void AddRC(
            IDeloHead oHead, ref int? aIsn, string aDueCard, int aIsnCab, string aDueDocgroup, int aOrderNum,
            string aFreeNum, DateTime aDocDate, int aSecurlevel, string aConsists, string aSpecimen, DateTime? aPlanDate,
            DateTime? aFactDate, int? aControlState, string aAnnotat, string aNote, string aDuePersonWhos,
            int? aIsnDelivery, string aDueOrganiz, int? aIsnContact, string aCorrespNum, DateTime? aCorrespDate,
            string aCorrespSign, int? aIsnCitizen, int? aIsCollective, int? aIsAnonim, string aSigns,
            string aDuePersonExe, int? aIsnNomenc, int? aNothardcopy, int? aCito, int? aIsnLinkingDoc,
            int? aIsnLinkingPrj, int? aIsnClLink, string aCopyShablon, string aVisas, int? aEDocument, string aSends,
            string askipcopy_ref_file_isns, int? aIsnLinkTranparent, int? aIsnLinkTranparentPare, string aTelNum)
        {
            const string maskDate = "yyyyMMdd";

            var proc = oHead.GetProc("add_rc");

            var aIsnParam = proc.CreateParameter("aIsn", 3, 3, 0, aIsn);

            proc.Parameters.Append(aIsnParam);
            proc.Parameters.Append(proc.CreateParameter("aDueCard", 200, 1, 48, aDueCard));
            proc.Parameters.Append(proc.CreateParameter("aIsnCab", 3, 1, 0, aIsnCab));
            proc.Parameters.Append(proc.CreateParameter("aDueDocgroup", 200, 1, 48, aDueDocgroup));
            proc.Parameters.Append(proc.CreateParameter("aOrderNum", 3, 1, 0, aOrderNum));
            proc.Parameters.Append(proc.CreateParameter("aFreeNum", 200, 1, 64, check_null(aFreeNum)));
            proc.Parameters.Append(proc.CreateParameter("aDocDate", 200, 1, 20, check_null(aDocDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aSecurlevel", 3, 1, 0, aSecurlevel));
            proc.Parameters.Append(proc.CreateParameter("aConsists", 200, 1, 255, check_null(aConsists)));
            proc.Parameters.Append(proc.CreateParameter("aSpecimen", 200, 1, 64, check_null(aSpecimen)));
            proc.Parameters.Append(proc.CreateParameter("aPlanDate", 200, 1, 20, check_null(aPlanDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aFactDate", 200, 1, 20, check_null(aFactDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aControlState", 3, 1, 0, check_null(aControlState)));
            proc.Parameters.Append(proc.CreateParameter("aAnnotat", 200, 1, 2000, check_null(aAnnotat)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 2000, check_null(aNote)));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonWhos", 200, 1, 8000, check_null(aDuePersonWhos)));
            proc.Parameters.Append(proc.CreateParameter("aIsnDelivery", 3, 1, 0, check_null(aIsnDelivery)));
            proc.Parameters.Append(proc.CreateParameter("aDueOrganiz", 200, 1, 48, check_null(aDueOrganiz)));
            proc.Parameters.Append(proc.CreateParameter("aIsnContact", 3, 1, 0, check_null(aIsnContact)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespNum", 200, 1, 64, check_null(aCorrespNum)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespDate", 200, 1, 20, check_null(aCorrespDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespSign", 200, 1, 255, check_null(aCorrespSign)));
            proc.Parameters.Append(proc.CreateParameter("aIsnCitizen", 3, 1, 0, check_null(aIsnCitizen)));
            proc.Parameters.Append(proc.CreateParameter("aIsCollective", 3, 1, 0, check_null(aIsCollective)));
            proc.Parameters.Append(proc.CreateParameter("aIsAnonim", 3, 1, 0, check_null(aIsAnonim)));
            proc.Parameters.Append(proc.CreateParameter("aSigns", 200, 1, 8000, check_null(aSigns)));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonExe", 200, 1, 8000, check_null(aDuePersonExe)));
            proc.Parameters.Append(proc.CreateParameter("aIsnNomenc", 3, 1, 0, check_null(aIsnNomenc)));
            proc.Parameters.Append(proc.CreateParameter("aNothardcopy", 3, 1, 0, check_null(aNothardcopy)));
            proc.Parameters.Append(proc.CreateParameter("aCito", 3, 1, 0, check_null(aCito)));
            proc.Parameters.Append(proc.CreateParameter("aIsnLinkingDoc", 3, 1, 0, check_null(aIsnLinkingDoc)));
            proc.Parameters.Append(proc.CreateParameter("aIsnLinkingPrj", 3, 1, 0, check_null(aIsnLinkingPrj)));
            proc.Parameters.Append(proc.CreateParameter("aIsnClLink", 3, 1, 0, check_null(aIsnClLink)));
            proc.Parameters.Append(proc.CreateParameter("aCopyShablon", 200, 1, 20, check_null(aCopyShablon)));
            proc.Parameters.Append(proc.CreateParameter("aVisas", 200, 1, 8000, aVisas));
            proc.Parameters.Append(proc.CreateParameter("aEDocument", 3, 1, 0, aEDocument));
            proc.Parameters.Append(proc.CreateParameter("aSends", 200, 1, 8000, aSends));
            proc.Parameters.Append(proc.CreateParameter("askipcopy_ref_file_isns", 200, 1, 8000, askipcopy_ref_file_isns));
            proc.Parameters.Append(proc.CreateParameter("aIsnLinkTranparent", 3, 1, 0, aIsnLinkTranparent));
            proc.Parameters.Append(proc.CreateParameter("aIsnLinkTranparentPare", 3, 1, 0, aIsnLinkTranparent));
            proc.Parameters.Append(proc.CreateParameter("aTelNum", 200, 1, 64, aTelNum));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                throw new DeloException(oHead.ErrCode, oHead.ErrText);
            }
            else
            {
                aIsn = (int?)aIsnParam.Value;
            }
        }

        /// <inheritdoc/>
        public void EditRC(
            IDeloHead oHead, int aIsn, string aDueCard, int? aIsnCab, string aFreeNum, DateTime? aDocDate,
            int aSecurlevel, string aConsists, string aSpecimen, DateTime? aPlanDate, DateTime? aFactDate,
            int? aControlState, string aAnnotat, string aNote, string aDuePersonWho, int? aIsnDelivery,
            int? aIsCollective, int? aIsAnonim, string aDuePersonSign, string aDuePersonExe, int? aNothardcopy,
            int? aCito, int? aOrderNum, int? aEDocument, string aTelNum)
        {
            var proc = oHead.GetProc("edit_rc");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("aDueCard", 200, 1, 48, string.Empty));
            proc.Parameters.Append(proc.CreateParameter("aIsnCab", 3, 1, 0, Convert.DBNull));
            proc.Parameters.Append(proc.CreateParameter("aFreeNum", 200, 1, 64, check_null(aFreeNum)));
            proc.Parameters.Append(proc.CreateParameter("aDocDate", 200, 1, 20, check_null(aDocDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aSecurlevel", 3, 1, 0, aSecurlevel));
            proc.Parameters.Append(proc.CreateParameter("aConsists", 200, 1, 255, check_null(aConsists)));
            proc.Parameters.Append(proc.CreateParameter("aSpecimen", 200, 1, 64, check_null(aSpecimen)));
            proc.Parameters.Append(proc.CreateParameter("aPlanDate", 200, 1, 20, check_null(aPlanDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aFactDate", 200, 1, 20, check_null(aFactDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aControlState", 3, 1, 0, check_null(aControlState)));
            proc.Parameters.Append(proc.CreateParameter("aAnnotat", 200, 1, 2000, check_null(aAnnotat)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 2000, check_null(aNote)));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonWho", 200, 1, 48, string.Empty));
            proc.Parameters.Append(proc.CreateParameter("aIsnDelivery", 3, 1, 0, check_null(aIsnDelivery)));
            proc.Parameters.Append(proc.CreateParameter("aIsCollective", 3, 1, 0, check_null(aIsCollective)));
            proc.Parameters.Append(proc.CreateParameter("aIsAnonim", 3, 1, 0, check_null(aIsAnonim)));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonSign", 200, 1, 48, string.Empty));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonExe", 200, 1, 48, string.Empty));
            proc.Parameters.Append(proc.CreateParameter("aNothardcopy", 3, 1, 0, check_null(aNothardcopy)));
            proc.Parameters.Append(proc.CreateParameter("aCito", 3, 1, 0, check_null(aCito)));
            proc.Parameters.Append(proc.CreateParameter("aOrderNum", 3, 1, 0, check_null(aOrderNum)));
            proc.Parameters.Append(proc.CreateParameter("aEDocument", 3, 1, 0, check_null(aEDocument)));
            proc.Parameters.Append(proc.CreateParameter("aTelNum", 200, 1, 64, aTelNum));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                throw new DeloException(oHead.ErrCode, oHead.ErrText);
            }
        }

        /// <inheritdoc/>
        public void SaveFileWF(
            IDeloHead oHead, int action, int? aISN_REF_FILE, int aISN_RC, int aKind_Doc, string aDescription,
            string aCategory, int? aSecur, int? aLockFlag, string aFileAccessDues, string aFilename, int? aDontDelFlag)
        {

            var proc = oHead.GetProc("save_file_wf");

            proc.Parameters.Append(proc.CreateParameter("action", 3, 1, 0, action));
            proc.Parameters.Append(proc.CreateParameter("aISN_REF_FILE", 3, 1, 0, check_null(aISN_REF_FILE)));
            proc.Parameters.Append(proc.CreateParameter("aISN_RC", 3, 1, 0, check_null(aISN_RC)));
            proc.Parameters.Append(proc.CreateParameter("aKind_Doc", 3, 1, 0, check_null(aKind_Doc)));
            proc.Parameters.Append(proc.CreateParameter("aDescription", 200, 1, 255, check_null(aDescription)));
            proc.Parameters.Append(proc.CreateParameter("aCategory", 200, 1, 255, check_null(aCategory)));
            proc.Parameters.Append(proc.CreateParameter("aSecur", 3, 1, 0, check_null(aSecur)));
            proc.Parameters.Append(proc.CreateParameter("aLockFlag", 3, 1, 0, check_null(aLockFlag)));
            proc.Parameters.Append(proc.CreateParameter("aFileAccessDues", 200, 1, 255, check_null(aFileAccessDues)));
            proc.Parameters.Append(proc.CreateParameter("aFileName", 200, 1, 255, check_null(aFilename)));
            proc.Parameters.Append(proc.CreateParameter("aDontDelFlag", 3, 1, 0, check_null(aDontDelFlag)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                throw new DeloException(oHead.ErrCode, oHead.ErrText);
            }
        }

        /// <inheritdoc/>
        public void EditAr(IDeloHead oHead, int aIsnOwner, string aArName, string aCard, string aValue)
        {
            var proc = oHead.GetProc("edit_ar");

            proc.Parameters.Append(proc.CreateParameter("aIsnOwner", 3, 1, 0, aIsnOwner));
            proc.Parameters.Append(proc.CreateParameter("aArName", 200, 1, 24, aArName));
            proc.Parameters.Append(proc.CreateParameter("aCard", 200, 1, 48, check_null(aCard)));
            proc.Parameters.Append(proc.CreateParameter("aValue", 200, 1, 2000, check_null(aValue)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                throw new DeloException(oHead.ErrCode, oHead.ErrText);
            }
        }

        /// <inheritdoc/>
        public void AddRubric(IDeloHead oHead, int aIsnDoc, string codes, string acard)
        {
            var proc = oHead.GetProc("add_rubric");

            proc.Parameters.Append(proc.CreateParameter("aIsnDoc", 3, 1, 0, aIsnDoc));
            proc.Parameters.Append(proc.CreateParameter("codes", 200, 1, 2000, codes));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, string.Empty)); //Устарел

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                throw new DeloException(oHead.ErrCode, oHead.ErrText);
            }
        }

        private static object check_null<T>(T prop)
        {
            return check_null(prop, string.Empty);
        }

        private static object check_null<T>(T prop, string date_mask)
        {
            switch (prop)
            {
                case int intProp:
                    return intProp;
                case string strProp:
                    return strProp;
                case DateTime _ when string.IsNullOrEmpty(date_mask):
                    return string.Empty;
                case DateTime dtProp when !string.IsNullOrEmpty(date_mask):
                    return dtProp.ToString(date_mask);
                case DateTime[] dtProps when !string.IsNullOrEmpty(date_mask):
                    var outStrDate = new StringBuilder();

                    for (int i = 0; i < dtProps.Length; i++)
                    {
                        if (i != dtProps.Length - 1)
                        {
                            if (dtProps[i] != DateTime.MinValue)
                            {
                                outStrDate
                                    .Append(dtProps[i].ToString(date_mask))
                                    .Append('|');
                            }
                            else
                            {
                                outStrDate.Append(Convert.DBNull)
                                    .Append('|');
                            }
                        }
                        else if (dtProps[i] != DateTime.MinValue)
                        {
                            outStrDate.Append(dtProps[i].ToString(date_mask));
                        }
                    }

                    return outStrDate.ToString();
                default:
                    return Convert.DBNull;
            }
        }

        /// <inheritdoc/>
        public void ReserveNum(IDeloHead oHead, string aDueDocgroup, int aYear, string aDueCard, string aSessionId, ref int? aOrderNum, ref string aFreeNum)
        {
            var proc = oHead.GetProc("reserve_num");

            proc.Parameters.Append(proc.CreateParameter("aOper", 200, 1, 2, "N"));
            proc.Parameters.Append(proc.CreateParameter("aDueDocgroup", 200, 1, 48, aDueDocgroup));
            proc.Parameters.Append(proc.CreateParameter("aYear", 3, 1, 0, aYear));
            proc.Parameters.Append(proc.CreateParameter("card_id", 200, 1, 48, aDueCard));
            var aOrderNumParam = proc.CreateParameter("aOrderNum", 3, 3, 0, check_null(aOrderNum));
            proc.Parameters.Append(aOrderNumParam);
            var aFreeNumParam = proc.CreateParameter("aFreeNum", 200, 3, 64, check_null(aFreeNum));
            proc.Parameters.Append(aFreeNumParam);
            proc.Parameters.Append(proc.CreateParameter("aSessionId", 200, 1, 255, check_null(aSessionId)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aOrderNum = null;
                aFreeNum = null;
                throw new DeloException(oHead.ErrCode, oHead.ErrText);
            }
            else
            {
                aOrderNum = (int?)aOrderNumParam.Value;
                aFreeNum = (string)aFreeNumParam.Value;
            }
        }

        /// <inheritdoc/>
        public void ReturnNum(IDeloHead oHead, string aDueDocgroup, int aYear, int aOrderNum, ref string aFreeNum)
        {
            var proc = oHead.GetProc("return_num");

            var aFreeNumParam = proc.CreateParameter("aFreeNum", 200, 1, 64, aFreeNum);

            proc.Parameters.Append(proc.CreateParameter("aOper", 200, 1, 2, "R"));
            proc.Parameters.Append(proc.CreateParameter("aDueDocgroup", 200, 1, 48, aDueDocgroup));
            proc.Parameters.Append(proc.CreateParameter("aYear", 3, 1, 0, aYear));
            proc.Parameters.Append(proc.CreateParameter("aOrderNum", 200, 1, 64, aOrderNum));
            proc.Parameters.Append(aFreeNumParam);

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aFreeNum = null;
                throw new DeloException(oHead.ErrCode, oHead.ErrText);
            }
            else
            {
                aFreeNum = (string)aFreeNumParam.Value;
            }
        }

        /// <inheritdoc/>
        public void AddFileEds(IDeloHead oHead, int a_isn_ref_file, string a_eds_body, int? a_isn_sign_kind, string a_due_person, int? a_flag, byte[] a_eds_body_blob, string a_certificate_owner, DateTime? a_signing_date)
        {
            var proc = oHead.GetProc("add_file_eds");
            string maskDate = "yyyyMMdd HH:mm:ss";

            proc.Parameters.Append(proc.CreateParameter("a_isn_ref_file", 3, 1, 0, a_isn_ref_file));
            proc.Parameters.Append(proc.CreateParameter("a_eds_body", 200, 1, 2000, check_null(a_eds_body)));
            proc.Parameters.Append(proc.CreateParameter("a_isn_sign_kind", 3, 1, 0, check_null(a_isn_sign_kind)));
            proc.Parameters.Append(proc.CreateParameter("a_due_person", 200, 1, 48, a_due_person));
            proc.Parameters.Append(proc.CreateParameter("a_flag", 3, 1, 0, a_flag));
            proc.Parameters.Append(proc.CreateParameter("a_eds_body_blob", 128, 1, 32000, a_eds_body_blob));
            proc.Parameters.Append(proc.CreateParameter("a_certificate_owner", 200, 1, 255, a_certificate_owner));
            proc.Parameters.Append(proc.CreateParameter("a_signing_date", 200, 1, 20, check_null(a_signing_date, maskDate)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                throw new DeloException(oHead.ErrCode, oHead.ErrText);
            }
        }

        /// <inheritdoc/>
        public void ImportFileEds(IDeloHead oHead, int aIsnRefFile, string aEdsBody, byte[] a_eds_body_blob, int? a_isn_sign_kind, string a_certificate_owner, DateTime? a_signing_date, string a_sign_text)
        {
            var proc = oHead.GetProc("import_file_eds");
            const string maskDate = "yyyyMMdd HH:mm:ss";

            proc.Parameters.Append(proc.CreateParameter("aIsnRefFile", 3, 1, 0, aIsnRefFile));
            proc.Parameters.Append(proc.CreateParameter("aEdsBody", 200, 1, 2000, check_null(aEdsBody)));
            proc.Parameters.Append(proc.CreateParameter("a_eds_body_blob", 128, 1, 32000, a_eds_body_blob));
            proc.Parameters.Append(proc.CreateParameter("a_isn_sign_kind", 3, 1, 0, check_null(a_isn_sign_kind)));
            proc.Parameters.Append(proc.CreateParameter("a_certificate_owner", 200, 1, 255, a_certificate_owner));
            proc.Parameters.Append(proc.CreateParameter("a_signing_date", 200, 1, 20, check_null(a_signing_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("a_sign_text", 200, 1, 255, a_sign_text));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                throw new DeloException(oHead.ErrCode, oHead.ErrText);
            }
        }

        /// <inheritdoc/>
        public void EditOuterSend(IDeloHead oHead, int a_isn_doc, int a_isn_ref_send, string a_card, int a_cab, int a_isn_contact, DateTime? a_send_date, string a_send_person, int a_isn_delivery, int a_sending_type, string a_note, string a_reg_n, DateTime? a_reg_date, int a_answer)
        {
            var proc = oHead.GetProc("edit_outer_send");
            const string maskDate = "yyyyMMdd HH:mm:ss";

            proc.Parameters.Append(proc.CreateParameter("a_isn_doc", 3, 1, 0, a_isn_doc));
            proc.Parameters.Append(proc.CreateParameter("a_isn_ref_send", 3, 1, 0, a_isn_ref_send));
            proc.Parameters.Append(proc.CreateParameter("a_card", 200, 1, 48, string.Empty)); //Устарел
            proc.Parameters.Append(proc.CreateParameter("a_cab", 3, 1, 0, Convert.DBNull)); //Устарел
            proc.Parameters.Append(proc.CreateParameter("a_isn_contact", 3, 1, 0, a_isn_contact));
            proc.Parameters.Append(proc.CreateParameter("a_send_date", 200, 1, 20, check_null(a_send_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("a_send_person", 200, 1, 64, check_null(a_send_person)));
            proc.Parameters.Append(proc.CreateParameter("a_isn_delivery", 3, 1, 0, a_isn_delivery));
            proc.Parameters.Append(proc.CreateParameter("a_sending_type", 3, 1, 0, a_sending_type));
            proc.Parameters.Append(proc.CreateParameter("a_note", 200, 1, 2000, check_null(a_note)));
            proc.Parameters.Append(proc.CreateParameter("a_reg_n", 200, 1, 64, a_reg_n));
            proc.Parameters.Append(proc.CreateParameter("a_reg_date", 200, 1, 20, check_null(a_reg_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("a_answer", 3, 1, 0, a_answer));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                throw new DeloException(oHead.ErrCode, oHead.ErrText);
            }
        }
    }
}

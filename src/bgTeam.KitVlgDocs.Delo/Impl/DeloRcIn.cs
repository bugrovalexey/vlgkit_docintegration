﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;

    /// <summary>
    /// .
    /// </summary>
    public class DeloRcIn : DeloDoc
    {
        public DeloRcIn(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Вид РК: "Входящий" от организации(в данной версии не поддерживается).
        /// </summary>
        public string DocKind
        {
            get => Exec(x => (string)x.DocKind);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloDepartment"/>.
        /// </summary>
        public DeloDepartment Addressee
        {
            get => Wrap<DeloDepartment>(x => x.Addressee);
        }

        /// <summary>
        /// Число Количество адресатов документа в организации (реквизит "Кому").
        /// </summary>
        public int AddressesCnt
        {
            get => Exec(x => (int)x.AddressesCnt);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloDepartment"/> I-го адресата документа(сортировка осуществляется по свойству .OrderNum).
        /// </summary>
        public DeloDepartment GetAddresses(int index) => Wrap<DeloDepartment, int>((x, p1) => x.Addresses[p1], index);

        /// <summary>
        /// Ссылка на объект <see cref="DeloDelivery"/>.
        /// </summary>
        public DeloDelivery Delivery
        {
            get => Wrap<DeloDelivery>(x => x.Delivery);
        }

        /// <summary>
        /// Номер телеграммы(Почтовый №).
        /// </summary>
        public string Telegram
        {
            get => Exec(x => (string)x.Telegram);
        }

        /// <summary>
        /// Комментарий РК.
        /// </summary>
        public string Note
        {
            get => Exec(x => (string)x.Note);
        }

        /// <summary>
        /// Количество тематических кодов РК.
        /// </summary>
        public int RubricCnt
        {
            get => Exec(x => (int)x.RubricCnt);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloRubric"/>  I-го тематического кода РК (сортировка осуществляется по свойству .OrderNum).
        /// </summary>
        public DeloRubric GetRubric(int index) => Wrap<DeloRubric, int>((x, p1) => x.Rubric[p1], index);

        /// <summary>
        /// .
        /// </summary>
        public int AddrCnt
        {
            get => Exec(x => (int)x.AddrCnt);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloAddr"/>.
        /// </summary>
        public DeloAddr GetAddr(int index) => Wrap<DeloAddr, int>((x, p1) => x.Addr[p1], index);
    }
}

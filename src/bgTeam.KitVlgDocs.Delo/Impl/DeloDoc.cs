﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;

    /// <summary>
    /// .
    /// </summary>
    public class DeloDoc : DeloEntity
    {
        public DeloDoc(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloDocGroup"/>.
        /// </summary>
        public DeloDocGroup Docgroup
        {
            get => Wrap<DeloDocGroup>(x => x.Docgroup);
        }

        /// <summary>
        /// Регистрационный номер документа(РК) (уникальный номер РК в номерообразующей группе в течении календарного года).
        /// </summary>
        public string RegNum
        {
            get => Exec(x => x.RegNum as string);
        }

        /// <summary>
        /// Порядковый номер документа в номерообразующей группе за текущий год(как правило, составляющая регистрационного номера).
        /// </summary>
        public int OrderNum
        {
            get => Exec(x => (int)x.OrderNum);
        }

        /// <summary>
        /// Экземпляры документа(перечень зарегистрированных номеров экземпляров документа.
        /// </summary>
        public string Specimen
        {
            get => Exec(x => (string)x.Specimen);
        }

        /// <summary>
        /// Контрольность РК.
        /// </summary>
        public int? IsControl
        {
            get => Exec(x => x.IsControl as int?);
        }

        /// <summary>
        /// Дата регистрации документа.
        /// </summary>
        public DateTime DocDate
        {
            get => Exec(x => (DateTime)x.DocDate);
        }

        /// <summary>
        /// Плановая дата исполнения документа.
        /// </summary>
        public DateTime? PlanDate
        {
            get => Exec(x => x.PlanDate as DateTime?);
        }

        /// <summary>
        /// Фактическая дата исполнения документа.
        /// </summary>
        public DateTime? FactDate
        {
            get => Exec(x => x.FactDate as DateTime?);
        }

        /// <summary>
        /// Состав документа.
        /// </summary>
        public string Consist
        {
            get => Exec(x => x.Consist as string);
        }

        /// <summary>
        /// Краткое содержание документа.
        /// </summary>
        public string Contents
        {
            get => Exec(x => x.Contents as string);
        }

        /// <summary>
        /// Комментарий РК.
        /// </summary>
        public string Note
        {
            get => Exec(x => x.Note as string);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloCardIndex"/>.
        /// </summary>
        public DeloCardIndex CardReg
        {
            get => Wrap<DeloCardIndex>(x => x.CardReg);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloCabinet"/>.
        /// </summary>
        public DeloCabinet CabReg
        {
            get => Wrap<DeloCabinet>(x => x.CabReg);
        }

        /// <summary>
        /// Признак модели персонифицированного доступа к данной РК.
        /// </summary>
        public bool AccessMode
        {
            get => Exec(x => (bool)x.AccessMode);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloSecurity"/>.
        /// </summary>
        public DeloSecurity Security
        {
            get => Wrap<DeloSecurity>(x => x.Security);
        }

        /// <summary>
        /// Количество прикрепленных к РК файлов(без учета файлов – отчетов исполнителей поручений и прав доступа пользователя).
        /// </summary>
        public int FilesCnt
        {
            get => Exec(x => (int)x.FilesCnt);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloFiles"/> I-й подписи документа (сортировка осуществляется по свойству .OrderNum).
        /// </summary>
        public DeloFiles GetFiles(int index) => Wrap<DeloFiles, int>((x, p1) => x.Files[p1], index);
    }
}

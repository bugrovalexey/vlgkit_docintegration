﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    /// <summary>
    ///  Тип "CardIndex" Элемент справочника "Картотеки".
    /// </summary>
    public class DeloCardIndex : DeloDictionaryEntity
    {
        public DeloCardIndex(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Наименование картотеки.
        /// </summary>
        public string Name => Exec(x => (string)x.Name);

        /// <summary>
        /// Уникальный код картотекообразующего подразделения/должностного лица.
        /// </summary>
        public string Dcode => Exec(x => (string)x.Dcode);
    }
}

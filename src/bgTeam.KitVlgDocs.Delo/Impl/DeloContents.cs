﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    ///  .
    /// </summary>
    public class DeloContents : DeloError
    {
        public DeloContents(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Индикатор готовности содержания прикрепленного файла к чтению.
        /// </summary>
        public bool Prepared
            => Exec(x => (bool)x.Prepared);

        /// <summary>
        /// Имя временного файла в который будет выгружен содержание.
        /// </summary>
        public string Name
            => Exec(x => (string)x.Name);

        /// <summary>
        /// Подготавливает временный файл с содержанием.
        /// </summary>
        public void Prepare(string path)
            => Exe(x => x.Prepare(path));

        /// <summary>
        /// Открывает временный файл.
        /// </summary>
        public void Open()
            => Exe(x => x.Open());

        /// <summary>
        /// Удаляет временный файл.
        /// </summary>
        public void Unprepare()
            => Exe(x => x.Unprepare());
    }
}

﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    ///  Тип "Rubric" Элемент справочника "Рубрики".
    /// </summary>
    public class DeloRubric : DeloDictionaryEntity
    {
        public DeloRubric(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Наименование тематической рубрики.
        /// </summary>
        public string Name => Exec(x => (string)x.Name);

        /// <summary>
        /// Уникальный код тематической рубрики, определяющий ее местоположение в иерархии.
        /// </summary>
        public string Dcode => Exec(x => (string)x.Dcode);
    }
}

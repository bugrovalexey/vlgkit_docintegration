﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    ///  .
    /// </summary>
    public class DeloOrganiz : DeloDictionaryEntity
    {
        public DeloOrganiz(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Уникальный код организации.
        /// </summary>
        public string Dcode => Exec(x => (string)x.Dcode);

        /// <summary>
        /// Признак вершины.
        /// </summary>
        public bool IsNode => Exec(x => (bool)x.IsNode);

        /// <summary>
        /// Признак логического удаления элемента.
        /// </summary>
        public bool Deleted => Exec(x => (bool)x.Deleted);

        /// <summary>
        /// Наименование организации.
        /// </summary>
        public string Name => Exec(x => (string)x.Name);

        /// <summary>
        /// Полное наименование организации.
        /// </summary>
        public string FullName => Exec(x => (string)x.FullName);

        /// <summary>
        /// Количество контактов .
        /// </summary>
        public int NewContCnt => Exec(x => (int)x.New_ContCnt);

        /// <summary>
        /// Полное наименование организации.
        /// </summary>
        public string NewContact => Exec(x => (string)x.FullName);
    }
}

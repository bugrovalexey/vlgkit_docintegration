﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;

    /// <summary>
    ///  .
    /// </summary>
    public class DeloFiles : DeloEntity
    {
        public DeloFiles(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Вид РК или РКПД, к которой относится запись.
        /// </summary>
        public string RcKind
        {
            get => Exec(x => (string)x.RcKind);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloDoc"/>.
        /// </summary>
        public DeloDoc Rc
            => WrapRc(RcKind, Exec(x => x.Rc));

        /// <summary>
        /// Наименование прикрепленного файла.
        /// </summary>
        public string Name
            => Exec(x => x.Name as string);

        /// <summary>
        /// Описание прикрепленного файла.
        /// </summary>
        public string Descript
            => Exec(x => x.Descript as string);

        /// <summary>
        /// Содержание прикрепленного файла.
        /// Содержит не пустой объект, если файл доступен пользователю для просмотра.
        /// Иначе это свойство ссылается на пустой объект.
        /// </summary>
        public DeloContents Contents
            => Wrap<DeloContents>(x => x.Contents);

        /// <summary>
        /// Количество ЭЦП файла.
        /// </summary>
        public int EDSCnt
        {
            get => Exec(x => (int)x.EDSCnt);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloEDS"/> содержащий I-ю ЭЦП файла.
        /// </summary>
        public DeloEDS GetEDS(int index) => Wrap<DeloEDS, int>((x, p1) => x.EDS[p1], index);

        private DeloDoc WrapRc(string type, dynamic @obj)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            switch (type.ToLowerInvariant())
            {
                case "rcin":
                    return new DeloRcIn(@obj);
                case "rcout":
                    return new DeloRcOut(@obj);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    /// <summary>
    /// .
    /// </summary>
    public class DeloContacts : DeloEntity
    {
        public DeloContacts(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// ФИО контакта.
        /// </summary>
        public string Surname => Exec(x => x.Surname as string);

        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string Phone => Exec(x => x.Phone as string);

        /// <summary>
        /// Подразделение.
        /// </summary>
        public string Department => Exec(x => x.Department as string);

        /// <summary>
        /// Ссылка на объект <see cref="DeloOrganiz"/>.
        /// </summary>
        public DeloOrganiz Organiz => Wrap<DeloOrganiz>(x => x.Organiz);
    }
}

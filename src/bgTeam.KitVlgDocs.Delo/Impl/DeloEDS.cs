﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;

    public class DeloEDS : DeloEntity
    {
        public DeloEDS(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Идентификатор сертификата на котором получена подпись(в данной версии не поддерживатся).
        /// </summary>
        public string CertificateId
            => Exec(x => x.CertificateId as string);

        /// <summary>
        /// Информация о лице, подписавшем файл.
        /// </summary>
        public string SignText
            => Exec(x => x.SignText as string);

        /// <summary>
        /// Дата подписания файла.
        /// </summary>
        public DateTime? SignDate
            => Exec(x => x.SignDate as DateTime?);

        /// <summary>
        /// Сохранить подпись в файл.
        /// </summary>
        /// <param name="fileName">Имя файла.</param>
        public void SaveToFile(string fileName)
            => Exe((x, p1) => x.SaveToFile(p1), fileName);
    }
}

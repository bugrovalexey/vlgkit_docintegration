﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    /// 
    /// </summary>
    public class DeloSearchVocab : DeloCriterion
    {
        public DeloSearchVocab(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Количество выбираемых элементов из справочника. One – выбор одного элемента, осуществляется пользователем. Many – выбор нескольких элементов, осуществляется пользователем. All – автоматический выбор всех элементов справочника, удовлетворяющих остальным условиям отбора. OneLevel - автоматический выбор элементов справочника только первого уровня подчиненности.
        /// </summary>
        public string Select
        {
            get => Exec(x => (string)x.Select);
            set => Exec((x, p1) => x.Select = p1, value);
        }

        /// <summary>
        /// Разрешается ли отбирать и показывать  логически удаленные элементы справочника. Yes - выбирать удаленные значения. No - не выбирать удаленные значения.
        /// </summary>
        public string Deleted
        {
            get => Exec(x => (string)x.Deleted);
            set => Exec((x, p1) => x.Deleted = p1, value);
        }

        /// <summary>
        /// Имя справочника, с которым необходимо работать (Обязательно заполняемое свойство).
        /// </summary>
        public string Vocabulary
        {
            get => Exec(x => (string)x.Vocabulary);
            set => Exec((x, p1) => x.Vocabulary = p1, value);
        }

        /// <summary>
        /// .
        /// </summary>
        public string GetSearchValue(string key)
            => Exec((x, p1) => (string)x.SearchValue[p1], key);

        /// <summary>
        /// .
        /// </summary>
        public void SetSearchValue(string key, string value)
            => Exe((x, p1, p2) => x.SearchValue[p1] = p2, key, value);
    }
}

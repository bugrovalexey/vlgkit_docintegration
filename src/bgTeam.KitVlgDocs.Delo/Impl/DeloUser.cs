﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    /// <summary>
    /// Тип "User" Элемент справочника "Пользователи".
    /// </summary>
    public class DeloUser : DeloDictionaryEntity
    {
        public DeloUser(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Имя пользователя (сортировка по фамилиям).
        /// </summary>
        public string Name => Exec(x => (string)x.Name);
    }
}

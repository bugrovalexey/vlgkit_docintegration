﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class DeloDictionaryEntity : DeloEntity
    {
        protected DeloDictionaryEntity(dynamic @object)
            : base((object)@object)
        {
        }
    }
}

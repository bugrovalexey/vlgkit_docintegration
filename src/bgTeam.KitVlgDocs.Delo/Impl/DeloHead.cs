﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    internal class DeloHead : DeloError, IDeloHead
    {
        private readonly object _guard = new object();

        private Type _type;

        public DeloHead()
            : base()
        {
        }

        /// <inheritdoc/>
        public bool Active
            => Exec(x => (bool)x.Active);

        /// <inheritdoc/>
        public DeloUser UserInfo
            => Wrap<DeloUser>(x => x.UserInfo);

        /// <inheritdoc/>
        public DateTime ServerToday
            => Exec(x => (DateTime)x.ServerToday);
        /// <inheritdoc/>
        public bool Open()
            => Exec(x => (bool)x.Open());

        /// <inheritdoc/>
        public void Close()
            => Exe(x => x.Close());

        /// <inheritdoc/>
        public bool OpenWithParams(string username, string password)
            => Exec((x, p1, p2) => (bool)x.OpenWithParams(p1, p2), username, password);

        /// <inheritdoc/>
        public bool OpenWithParamsEx(string host, string owner, string username, string password)
            => Exec((x, p1, p2, p3, p4) => (bool)x.OpenWithParamsEx(p1, p2, p3, p4), host, owner, username, password);

        /// <inheritdoc/>
        public void LoadSettings(string filePath)
            => Exe((x, p1) => x.LoadSettings(p1), filePath);

        /// <inheritdoc/>
        public ADODBCommand GetProc(string name)
            => Wrap<ADODBCommand, string>((x, p1) => x.GetProc(p1), name);

        /// <inheritdoc/>
        public void ExecuteProc(ADODBCommand command)
            => Exec((x, p1) => x.ExecuteProc(command.Object), command);

        /// <inheritdoc/>
        public DeloCriterion GetCriterion(string type)
        {
            switch (type.ToLowerInvariant())
            {
                case "table":
                    return Wrap<DeloSearchTables, string>((x, p1) => x.GetCriterion(p1), type);
                case "vocabulary":
                    return Wrap<DeloSearchVocab, string>((x, p1) => x.GetCriterion(p1), type);
                default: throw new InvalidOperationException();
            }
        }

        /// <inheritdoc/>
        public DeloResultSet GetResultSet()
            => Wrap<DeloResultSet>(x => x.GetResultSet());

        public DeloEntity GetRow(string type, int isn)
            => WrapItem(type, Exec((x, p1, p2) => x.GetRow(p1, p2), type, isn));

        public DeloEntity GetRow(string type, string dcode)
            => WrapItem(type, Exec((x, p1, p2) => x.GetRow(p1, p2), type, dcode));

        protected override void EnsureIsValid()
        {
            if (_type == null || _object == null || !_object.Active)
            {
                lock (_guard)
                {
                    if (_type == null)
                    {
                        _type = Type.GetTypeFromProgID("Eapi.Head")
                            ?? throw new InvalidOperationException($"Couldn`t find 'Eapi.Head' COM api component.");
                    }

                    if (_object == null || !(bool)_object.Active)
                    {
                        _object = Activator.CreateInstance(_type)
                            ?? throw new InvalidOperationException($"Couldn`t create 'Eapi.Head' COM api component.");
                    }
                }
            }
        }

        private DeloEntity WrapItem(string type, dynamic @obj)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            switch (type.ToLowerInvariant())
            {
                case "organiz":
                    return new DeloOrganiz(@obj);
                case "department":
                    return new DeloDepartment(@obj);
                case "security":
                    return new DeloSecurity(@obj);
                case "docgroup":
                    return new DeloDocGroup(@obj);
                case "delivery":
                    return new DeloDelivery(@obj);
                case "cabinet":
                    return new DeloCabinet(@obj);
                case "cardindex":
                    return new DeloCardIndex(@obj);
                case "rubric":
                    return new DeloRubric(@obj);
                case "rcin":
                    return new DeloRcIn(@obj);
                case "rcout":
                    return new DeloRcOut(@obj);
                case "user":
                    return new DeloUser(@obj);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    ///  .
    /// </summary>
    public class DeloDepartment : DeloDictionaryEntity
    {
        public DeloDepartment(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Уникальный код подразделения/должностного лица.
        /// </summary>
        public string Dcode => Exec(x => (string)x.Dcode);

        /// <summary>
        /// Наименование подразделения/ДЛ.
        /// </summary>
        public string Name => Exec(x => (string)x.Name);
    }
}

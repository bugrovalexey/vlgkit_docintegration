﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    /// Тип "Security" Элемент справочника "Грифы доступа".
    /// </summary>
    public class DeloSecurity : DeloDictionaryEntity
    {
        public DeloSecurity(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Наименование грифа доступа.
        /// </summary>
        public string Name => Exec(x => (string)x.Name);
    }
}

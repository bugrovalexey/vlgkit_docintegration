﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    /// <summary>
    /// .
    /// </summary>
    public class DeloRcOut : DeloDoc
    {
        public DeloRcOut(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Первое ДЛ, подписавшее документ.
        /// Ссылка на объект <see cref="DeloDepartment"/>.
        /// </summary>
        public DeloDepartment PersonSign
        {
            get => Wrap<DeloDepartment>(x => x.PersonSign);
        }

        /// <summary>
        /// Количество ДЛ, подписавших документ.
        /// </summary>
        public int PersonSignsCnt
        {
            get => Exec(x => (int)x.PersonSignsCnt);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloSigns"/> I-й подписи документа (сортировка осуществляется по свойству .OrderNum).
        /// </summary>
        public DeloSigns GetPersonSigns(int index) => Wrap<DeloSigns, int>((x, p1) => x.PersonSigns[p1], index);

        /// <summary>
        /// Количество адресатов РК.
        /// </summary>
        public int AddrCnt
        {
            get => Exec(x => (int)x.AddrCnt);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloAddr"/> содержащий I-го адресата РК (сортировка осуществляется по свойству .OrderNum).
        /// </summary>
        public DeloAddr GetAddr(int index) => Wrap<DeloAddr, int>((x, p1) => x.Addr[p1], index);

        /// <summary>
        /// Количество тематических кодов РК .
        /// </summary>
        public int RubricCnt
        {
            get => Exec(x => (int)x.RubricCnt);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloRubric"/>  I-го тематического кода РК (сортировка осуществляется по свойству .OrderNum).
        /// </summary>
        public DeloRubric GetRubric(int index) => Wrap<DeloRubric, int>((x, p1) => x.Rubric[p1], index);
    }
}

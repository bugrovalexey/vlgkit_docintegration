﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;

    /// <summary>
    /// .
    /// </summary>
    public class DeloSignKind : DeloEntity
    {
        public DeloSignKind(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Наименование вида подписи(системный).
        /// </summary>
        public string Name
            => Exec(x => x.Name as string);

        /// <summary>
        /// Текст вида подписи(пользовательский).
        /// </summary>
        public string SignText
            => Exec(x => x.SignText as string);

        /// <summary>
        /// Признак логического удаления элемента.
        /// </summary>
        public bool Deleted
            => Exec(x => (bool)x.Deleted);

        /// <summary>
        /// Вес элемента в справочнике.
        /// </summary>
        public int Weight
            => Exec(x => (int)x.Weight);
    }
}

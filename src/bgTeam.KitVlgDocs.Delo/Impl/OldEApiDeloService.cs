﻿

namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;
    using System.IO;

    public class OldEApiDeloService : IOldEApiDeloService
    {
        public void add_rc(dynamic oHead, ref int? aIsn, string aDueCard, int aIsnCab, string aDueDocgroup, int aOrderNum, string aFreeNum, DateTime aDocDate, int aSecurlevel, string aConsists, string aSpecimen, DateTime? aPlanDate, DateTime? aFactDate, int? aControlState, string aAnnotat, string aNote, string aDuePersonWhos, int? aIsnDelivery, string aDueOrganiz, int? aIsnContact, string aCorrespNum, DateTime? aCorrespDate, string aCorrespSign, int? aIsnCitizen, int? aIsCollective, int? aIsAnonim, string aSigns, string aDuePersonExe, int? aIsnNomenc, int? aNothardcopy, int? aCito, int? aIsnLinkingDoc, int? aIsnLinkingPrj, int? aIsnClLink,  string aCopyShablon,  string aVisas,  int? aEDocument, string aSends, string askipcopy_ref_file_isns, int? aIsnLinkTranparent, int? aIsnLinkTranparentPare, string aTelNum)
        {
            dynamic proc = oHead.GetProc("add_rc");
            string maskDate = "yyyyMMdd";

            dynamic aIsnParam = proc.CreateParameter("aIsn", 3, 3, 0, (object)aIsn);
            proc.Parameters.Append(aIsnParam);
            proc.Parameters.Append(proc.CreateParameter("aDueCard", 200, 1, 48, aDueCard));
            proc.Parameters.Append(proc.CreateParameter("aIsnCab", 3, 1, 0, aIsnCab));
            proc.Parameters.Append(proc.CreateParameter("aDueDocgroup", 200, 1, 48, aDueDocgroup));
            proc.Parameters.Append(proc.CreateParameter("aOrderNum", 3, 1, 0, aOrderNum));
            proc.Parameters.Append(proc.CreateParameter("aFreeNum", 200, 1, 64, check_null(aFreeNum)));
            proc.Parameters.Append(proc.CreateParameter("aDocDate", 200, 1, 20, check_null(aDocDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aSecurlevel", 3, 1, 0, aSecurlevel));
            proc.Parameters.Append(proc.CreateParameter("aConsists", 200, 1, 255, check_null(aConsists)));
            proc.Parameters.Append(proc.CreateParameter("aSpecimen", 200, 1, 64, aSpecimen));
            proc.Parameters.Append(proc.CreateParameter("aPlanDate", 200, 1, 20, check_null(aPlanDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aFactDate", 200, 1, 20, check_null(aFactDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aControlState", 3, 1, 0, (object)check_null(aControlState)));
            proc.Parameters.Append(proc.CreateParameter("aAnnotat", 200, 1, 2000, check_null(aAnnotat)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 2000, check_null(aNote)));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonWhos", 200, 1, 8000, check_null(aDuePersonWhos)));
            proc.Parameters.Append(proc.CreateParameter("aIsnDelivery", 3, 1, 0, (object)check_null(aIsnDelivery)));
            proc.Parameters.Append(proc.CreateParameter("aDueOrganiz", 200, 1, 48, check_null(aDueOrganiz)));
            proc.Parameters.Append(proc.CreateParameter("aIsnContact", 3, 1, 0, (object)check_null(aIsnContact)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespNum", 200, 1, 64, check_null(aCorrespNum)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespDate", 200, 1, 20, check_null(aCorrespDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespSign", 200, 1, 255, check_null(aCorrespSign)));
            proc.Parameters.Append(proc.CreateParameter("aIsnCitizen", 3, 1, 0, (object)check_null(aIsnCitizen)));
            proc.Parameters.Append(proc.CreateParameter("aIsCollective", 3, 1, 0, (object)check_null(aIsCollective)));
            proc.Parameters.Append(proc.CreateParameter("aIsAnonim", 3, 1, 0, (object)check_null(aIsAnonim)));
            proc.Parameters.Append(proc.CreateParameter("aSigns", 200, 1, 8000, check_null(aSigns)));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonExe", 200, 1, 8000, check_null(aDuePersonExe)));
            proc.Parameters.Append(proc.CreateParameter("aIsnNomenc", 3, 1, 0, (object)check_null(aIsnNomenc)));
            proc.Parameters.Append(proc.CreateParameter("aNothardcopy", 3, 1, 0, (object)check_null(aNothardcopy)));
            proc.Parameters.Append(proc.CreateParameter("aCito", 3, 1, 0, (object)check_null(aCito)));
            proc.Parameters.Append(proc.CreateParameter("aIsnLinkingDoc", 3, 1, 0, (object)check_null(aIsnLinkingDoc)));
            proc.Parameters.Append(proc.CreateParameter("aIsnLinkingPrj", 3, 1, 0, (object)check_null(aIsnLinkingPrj)));
            proc.Parameters.Append(proc.CreateParameter("aIsnClLink", 3, 1, 0, (object)check_null(aIsnClLink)));
            proc.Parameters.Append(proc.CreateParameter("aCopyShablon", 200, 1, 20, check_null(aCopyShablon)));
            proc.Parameters.Append(proc.CreateParameter("aVisas", 200, 1, 8000, aVisas));
            proc.Parameters.Append(proc.CreateParameter("aEDocument", 3, 1, 0, (object)aEDocument));
            proc.Parameters.Append(proc.CreateParameter("aSends", 200, 1, 8000, aSends));
            proc.Parameters.Append(proc.CreateParameter("askipcopy_ref_file_isns", 200, 1, 8000, askipcopy_ref_file_isns));
            proc.Parameters.Append(proc.CreateParameter("aIsnLinkTranparent", 3, 1, 0, (object)aIsnLinkTranparent));
            proc.Parameters.Append(proc.CreateParameter("aIsnLinkTranparentPare", 3, 1, 0, (object)aIsnLinkTranparent));
            proc.Parameters.Append(proc.CreateParameter("aTelNum", 200, 1, 64, aTelNum));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aIsn = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                aIsn = (int?)aIsnParam.Value;
            }
        }

        public void edit_rc(dynamic oHead, int aIsn, string aDueCard, int? aIsnCab, string aFreeNum, DateTime? aDocDate, int aSecurlevel, string aConsists, string aSpecimen, DateTime? aPlanDate, DateTime? aFactDate, int? aControlState, string aAnnotat, string aNote, string aDuePersonWho, int? aIsnDelivery, int? aIsCollective, int? aIsAnonim, string aDuePersonSign, string aDuePersonExe, int? aNothardcopy, int? aCito, int? aOrderNum, int? aEDocument, string aTelNum)
        {
            dynamic proc = oHead.GetProc("edit_rc");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("aDueCard", 200, 1, 48, string.Empty)); //устарел
            proc.Parameters.Append(proc.CreateParameter("aIsnCab", 3, 1, 0, (object)Convert.DBNull)); //устарел
            proc.Parameters.Append(proc.CreateParameter("aFreeNum", 200, 1, 64, check_null(aFreeNum)));
            proc.Parameters.Append(proc.CreateParameter("aDocDate", 200, 1, 20, check_null(aDocDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aSecurlevel", 3, 1, 0, aSecurlevel));
            proc.Parameters.Append(proc.CreateParameter("aConsists", 200, 1, 255, check_null(aConsists)));
            proc.Parameters.Append(proc.CreateParameter("aSpecimen", 200, 1, 64, check_null(aSpecimen)));
            proc.Parameters.Append(proc.CreateParameter("aPlanDate", 200, 1, 20, check_null(aPlanDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aFactDate", 200, 1, 20, check_null(aFactDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aControlState", 3, 1, 0, (object)check_null(aControlState)));
            proc.Parameters.Append(proc.CreateParameter("aAnnotat", 200, 1, 2000, check_null(aAnnotat)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 2000, check_null(aNote)));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonWho", 200, 1, 48, string.Empty)); //Устарел
            proc.Parameters.Append(proc.CreateParameter("aIsnDelivery", 3, 1, 0, (object)check_null(aIsnDelivery)));
            proc.Parameters.Append(proc.CreateParameter("aIsCollective", 3, 1, 0, (object)check_null(aIsCollective)));
            proc.Parameters.Append(proc.CreateParameter("aIsAnonim", 3, 1, 0, (object)check_null(aIsAnonim)));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonSign", 200, 1, 48, string.Empty)); //Устарел
            proc.Parameters.Append(proc.CreateParameter("aDuePersonExe", 200, 1, 48, string.Empty)); //Устарел
            proc.Parameters.Append(proc.CreateParameter("aNothardcopy", 3, 1, 0, check_null(aNothardcopy)));
            proc.Parameters.Append(proc.CreateParameter("aCito", 3, 1, 0, (object)check_null(aCito)));
            proc.Parameters.Append(proc.CreateParameter("aOrderNum", 3, 1, 0, (object)check_null(aOrderNum)));
            proc.Parameters.Append(proc.CreateParameter("aEDocument", 3, 1, 0, (object)check_null(aEDocument)));
            proc.Parameters.Append(proc.CreateParameter("aTelNum", 200, 1, 64, aTelNum));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_rc(dynamic oHead, int aisndoc, string acard, int aretnum, int? ayear)
        {
            dynamic proc = oHead.GetProc("del_rc");


            proc.Parameters.Append(proc.CreateParameter("aisndoc", 3, 1, 0, aisndoc));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, string.Empty)); //Устарел
            proc.Parameters.Append(proc.CreateParameter("aretnum", 3, 1, 0, aretnum));
            proc.Parameters.Append(proc.CreateParameter("ayear", 3, 1, 0, Convert.DBNull)); //Устарел

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void reserve_num(dynamic oHead, string aOper, string aDueDocgroup, int aYear, string card_id,  ref int? aOrderNum,  ref string aFreeNum, string aSessionId)
        {
            dynamic proc = oHead.GetProc("reserve_num");

            proc.Parameters.Append(proc.CreateParameter("aOper", 200, 1, 2, aOper));
            proc.Parameters.Append(proc.CreateParameter("aDueDocgroup", 200, 1, 48, aDueDocgroup));
            proc.Parameters.Append(proc.CreateParameter("aYear", 3, 1, 0, aYear));
            proc.Parameters.Append(proc.CreateParameter("card_id", 200, 1, 48, card_id));
            dynamic aOrderNumParam = proc.CreateParameter("aOrderNum", 3, 3, 0, (object)check_null(aOrderNum));
            proc.Parameters.Append(aOrderNumParam);
            dynamic aFreeNumParam = proc.CreateParameter("aFreeNum", 200, 3, 64, check_null(aFreeNum));
            proc.Parameters.Append(aFreeNumParam);
            proc.Parameters.Append(proc.CreateParameter("aSessionId", 200, 1, 255, aSessionId));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aOrderNum = null;
                aFreeNum = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                aOrderNum = (int?)aOrderNumParam.Value;
                aFreeNum = (string)aFreeNumParam.Value;
            }
        }

        public void return_num(dynamic oHead, string aOper, string aDueDocgroup, int aYear, int aOrderNum, ref string aFreeNum)
        {
            dynamic proc = oHead.GetProc("return_num");

            dynamic aFreeNumParam = proc.CreateParameter("aFreeNum", 200, 1, 64, (object)aFreeNum);

            proc.Parameters.Append(proc.CreateParameter("aOper", 200, 1, 2, aOper));
            proc.Parameters.Append(proc.CreateParameter("aDueDocgroup", 200, 1, 48, aDueDocgroup));
            proc.Parameters.Append(proc.CreateParameter("aYear", 3, 1, 0, aYear));
            proc.Parameters.Append(proc.CreateParameter("aOrderNum", 200, 1, 64, aOrderNum));
            proc.Parameters.Append(aFreeNumParam);

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aFreeNum = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                aFreeNum = (string)aFreeNumParam.Value;
            }
        }

        public void add_forward(dynamic oHead, int aIsnDoc, string acard, int acab, string codes)
        {
            dynamic proc = oHead.GetProc("add_forward");

            proc.Parameters.Append(proc.CreateParameter("aIsnDoc", 3, 1, 0, aIsnDoc));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, acard));
            proc.Parameters.Append(proc.CreateParameter("acab", 3, 1, 0, acab));
            proc.Parameters.Append(proc.CreateParameter("codes", 200, 1, 2000, codes));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_forward(dynamic oHead, int aisnforward)
        {
            dynamic proc = oHead.GetProc("del_forward");

            proc.Parameters.Append(proc.CreateParameter("aisnforward", 3, 1, 0, aisnforward));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_journal(dynamic oHead, int aIsnDoc, string aCard, int aCab, string aCodes, int? aIsnNomenc, DateTime? aDates, string aFlagsOriginal, string aOrigNums, string aCopyNums, int aClearFldr6, int aDescructFlag)
        {
            dynamic proc = oHead.GetProc("add_journal");
            string maskDate = "yyyyMMdd HH:mm:ss";

            proc.Parameters.Append(proc.CreateParameter("aIsnDoc", 3, 1, 0, aIsnDoc));
            proc.Parameters.Append(proc.CreateParameter("aCard", 200, 1, 48, aCard));
            proc.Parameters.Append(proc.CreateParameter("aCab", 3, 1, 0, aCab));
            proc.Parameters.Append(proc.CreateParameter("aCodes", 200, 1, 2000, aCodes));
            proc.Parameters.Append(proc.CreateParameter("aIsnNomenc", 3, 1, 0, (object)check_null(aIsnNomenc)));
            proc.Parameters.Append(proc.CreateParameter("aDates", 200, 1, 20, check_null(aDates, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aFlagsOriginal", 200, 1, 2000, check_null(aFlagsOriginal)));
            proc.Parameters.Append(proc.CreateParameter("aOrigNums", 200, 1, 2000, check_null(aOrigNums)));
            proc.Parameters.Append(proc.CreateParameter("aCopyNums", 200, 1, 2000, check_null(aCopyNums)));
            proc.Parameters.Append(proc.CreateParameter("aClearFldr6", 3, 1, 0, aClearFldr6));
            proc.Parameters.Append(proc.CreateParameter("aDescructFlag", 3, 1, 0, aDescructFlag));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_visa(dynamic oHead, int aIsn, string aDuePersons, DateTime[] aVisaDates, string aNotes)
        {
            dynamic proc = oHead.GetProc("add_visa");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("aDuePersons", 200, 1, 2000, check_null(aDuePersons)));
            proc.Parameters.Append(proc.CreateParameter("aVisaDates", 200, 1, 2000, check_null(aVisaDates, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aNotes", 200, 1, 2000, check_null(aNotes)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_visa(dynamic oHead, int aIsnRefVisa)
        {
            dynamic proc = oHead.GetProc("del_visa");

            proc.Parameters.Append(proc.CreateParameter("aIsnRefVisa", 3, 1, 0, aIsnRefVisa));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_visa(dynamic oHead, int aIsnRefVisa, DateTime? aVisaDate, string aNote)
        {
            dynamic proc = oHead.GetProc("edit_visa");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsnRefVisa", 3, 1, 0, aIsnRefVisa));
            proc.Parameters.Append(proc.CreateParameter("aVisaDate", 200, 1, 20, check_null(aVisaDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 2000, aNote));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_sign(dynamic oHead, int aIsn, string aDuePersons, DateTime[] aSignDates)
        {
            dynamic proc = oHead.GetProc("add_sign");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("aDuePersons", 200, 1, 2000, check_null(aDuePersons)));
            proc.Parameters.Append(proc.CreateParameter("aSignDates", 200, 1, 2000, check_null(aSignDates, maskDate)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_sign(dynamic oHead, int aIsnDocSign)
        {
            dynamic proc = oHead.GetProc("del_sign");

            proc.Parameters.Append(proc.CreateParameter("aIsnDocSign", 3, 1, 0, aIsnDocSign));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_sign(dynamic oHead, int aIsnDocSign, DateTime? aSignDate)
        {
            dynamic proc = oHead.GetProc("edit_sign");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsnDocSign", 3, 1, 0, aIsnDocSign));
            proc.Parameters.Append(proc.CreateParameter("aSignDate", 200, 1, 20, check_null(aSignDate, maskDate)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_soisp(dynamic oHead, int aIsn, string aDuesOrganiz, string aIsnsContact, string aSoispNums, DateTime[] aSoispDates, string aSoispPersons)
        {
            dynamic proc = oHead.GetProc("add_soisp");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("aDuesOrganiz", 200, 1, 8000, check_null(aDuesOrganiz)));
            proc.Parameters.Append(proc.CreateParameter("aIsnsContact", 200, 1, 8000, check_null(aIsnsContact)));
            proc.Parameters.Append(proc.CreateParameter("aSoispNums", 200, 1, 8000, check_null(aSoispNums)));
            proc.Parameters.Append(proc.CreateParameter("aSoispDates", 200, 1, 8000, check_null(aSoispDates, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aSoispPersons", 200, 1, 8000, check_null(aSoispPersons)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_soisp(dynamic oHead, int aIsnRefSoisp)
        {
            dynamic proc = oHead.GetProc("del_soisp");

            proc.Parameters.Append(proc.CreateParameter("aIsnRefSoisp", 3, 1, 0, aIsnRefSoisp));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_soisp(dynamic oHead, int aIsnRefSoisp, string aSoispNum, DateTime? aSoispDate, string aSoispPerson)
        {
            dynamic proc = oHead.GetProc("edit_soisp");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsnRefSoisp", 3, 1, 0, aIsnRefSoisp));
            proc.Parameters.Append(proc.CreateParameter("aSoispNum", 200, 1, 64, aSoispNum));
            proc.Parameters.Append(proc.CreateParameter("aSoispDate", 200, 1, 20, check_null(aSoispDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aSoispPerson", 200, 1, 2000, aSoispPerson));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_ref_ufolder(dynamic oHead,int aIsnRefUfolder, int aIsnRefDoc, int? aKindDoc, string aNote)
        {
            dynamic proc = oHead.GetProc("add_ref_ufolder");

            proc.Parameters.Append(proc.CreateParameter("aIsnRefUfolder", 3, 1, 0, aIsnRefUfolder));
            proc.Parameters.Append(proc.CreateParameter("aIsnRefDoc", 3, 1, 0, aIsnRefDoc));
            proc.Parameters.Append(proc.CreateParameter("aKindDoc", 3, 1, 0, check_null(aKindDoc)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 255, aNote));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_ref_ufolder(dynamic oHead, int aIsnRefUfolder, int aIsnRefDoc)
        {
            dynamic proc = oHead.GetProc("del_ref_ufolder");

            proc.Parameters.Append(proc.CreateParameter("aIsnRefUfolder", 3, 1, 0, aIsnRefUfolder));
            proc.Parameters.Append(proc.CreateParameter("aIsnRefDoc", 3, 1, 0, aIsnRefDoc));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_send(dynamic oHead, int aIsnDoc, string acard, int acab, string aclassif, string codes, string aFlagsOriginal, string aOrigNums, string aCopyNums, DateTime[] aDate, string aSend_Person, string aIsn_Delivery, int? aSendingType, string aIsnsContact, string aNotes, string aConsists)
        {
            dynamic proc = oHead.GetProc("add_send");
            string maskDate = "yyyyMMdd HH:mm:ss";

            proc.Parameters.Append(proc.CreateParameter("aIsnDoc", 3, 1, 0, aIsnDoc));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, acard));
            proc.Parameters.Append(proc.CreateParameter("acab", 3, 1, 0, acab));
            proc.Parameters.Append(proc.CreateParameter("aclassif", 200, 1, 20, aclassif));
            proc.Parameters.Append(proc.CreateParameter("codes", 200, 1, 2000, codes));
            proc.Parameters.Append(proc.CreateParameter("aFlagsOriginal", 200, 1, 2000, check_null(aFlagsOriginal)));
            proc.Parameters.Append(proc.CreateParameter("aOrigNums", 200, 1, 2000, check_null(aOrigNums)));
            proc.Parameters.Append(proc.CreateParameter("aCopyNums", 200, 1, 2000, check_null(aCopyNums)));
            proc.Parameters.Append(proc.CreateParameter("aDate", 200, 1, 2000, check_null(aDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aSend_Person", 200, 1, 2000, check_null(aSend_Person)));
            proc.Parameters.Append(proc.CreateParameter("aIsn_Delivery", 200, 1, 2000, check_null(aIsn_Delivery)));
            proc.Parameters.Append(proc.CreateParameter("aSendingType", 3, 1, 0, (object)check_null(aSendingType)));
            proc.Parameters.Append(proc.CreateParameter("aIsnsContact", 200, 1, 2000, check_null(aIsnsContact)));
            proc.Parameters.Append(proc.CreateParameter("aNotes", 200, 1, 8000, check_null(aNotes)));
            proc.Parameters.Append(proc.CreateParameter("aConsists", 200, 1, 8000, check_null(aConsists)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_send(dynamic oHead, int aIsnDoc, int aisn_ref_send, string acard, int acab, int aFlagOriginal, string aOrigNum, string aCopyNum, DateTime? aDates, string aNote)
        {
            dynamic proc = oHead.GetProc("edit_send");
            string maskDate = "yyyyMMdd hh:mm:ss";

            proc.Parameters.Append(proc.CreateParameter("aIsnDoc", 3, 1, 0, aIsnDoc));
            proc.Parameters.Append(proc.CreateParameter("aisn_ref_send", 3, 1, 0, aisn_ref_send));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, acard));
            proc.Parameters.Append(proc.CreateParameter("acab", 3, 1, 0, acab));
            proc.Parameters.Append(proc.CreateParameter("aFlagOriginal", 3, 1, 0, aFlagOriginal));
            proc.Parameters.Append(proc.CreateParameter("aOrigNum", 200, 1, 255, check_null(aOrigNum)));
            proc.Parameters.Append(proc.CreateParameter("aCopyNum", 200, 1, 255, check_null(aCopyNum)));
            proc.Parameters.Append(proc.CreateParameter("aDates", 200, 1, 20, check_null(aDates, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 2000, check_null(aNote)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_send(dynamic oHead, int aisnrefsend, string acard)
        {
            dynamic proc = oHead.GetProc("del_send");

            proc.Parameters.Append(proc.CreateParameter("aisnrefsend", 3, 1, 0, aisnrefsend));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, string.Empty)); //устарел

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_outer_send(dynamic oHead, int a_isn_doc, int a_isn_ref_send, string a_card, int a_cab, int a_isn_contact, DateTime? a_send_date, string a_send_person, int a_isn_delivery, int a_sending_type, string a_note, string a_reg_n, DateTime? a_reg_date, int a_answer)
        {
            dynamic proc = oHead.GetProc("edit_outer_send");
            string maskDate = "yyyyMMdd HH:mm:ss";

            proc.Parameters.Append(proc.CreateParameter("a_isn_doc", 3, 1, 0, a_isn_doc));
            proc.Parameters.Append(proc.CreateParameter("a_isn_ref_send", 3, 1, 0, a_isn_ref_send));
            proc.Parameters.Append(proc.CreateParameter("a_card", 200, 1, 48, string.Empty)); //Устарел
            proc.Parameters.Append(proc.CreateParameter("a_cab", 3, 1, 0, Convert.DBNull)); //Устарел
            proc.Parameters.Append(proc.CreateParameter("a_isn_contact", 3, 1, 0, a_isn_contact));
            proc.Parameters.Append(proc.CreateParameter("a_send_date", 200, 1, 20, check_null(a_send_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("a_send_person", 200, 1, 64, check_null(a_send_person)));
            proc.Parameters.Append(proc.CreateParameter("a_isn_delivery", 3, 1, 0, a_isn_delivery));
            proc.Parameters.Append(proc.CreateParameter("a_sending_type", 3, 1, 0, a_sending_type));
            proc.Parameters.Append(proc.CreateParameter("a_note", 200, 1, 2000, check_null(a_note)));
            proc.Parameters.Append(proc.CreateParameter("a_reg_n", 200, 1, 64, a_reg_n));
            proc.Parameters.Append(proc.CreateParameter("a_reg_date", 200, 1, 20, check_null(a_reg_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("a_answer", 3, 1, 0, a_answer));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_rubric(dynamic oHead, int aIsnDoc, string codes, string acard)
        {
            dynamic proc = oHead.GetProc("add_rubric");

            proc.Parameters.Append(proc.CreateParameter("aIsnDoc", 3, 1, 0, aIsnDoc));
            proc.Parameters.Append(proc.CreateParameter("codes", 200, 1, 2000, codes));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, string.Empty)); //Устарел

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_rubric(dynamic oHead, int aisnrefrubric, string acard)
        {
            dynamic proc = oHead.GetProc("del_rubric");

            proc.Parameters.Append(proc.CreateParameter("aisnrefrubric", 3, 1, 0, aisnrefrubric));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, string.Empty)); //Устарел

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_link(dynamic oHead, int aisndoc, int? aisn_linkdoc, int aisn_link, string alinked_num, string acard, string aUrlStr)
        {

            dynamic proc = oHead.GetProc("add_link");

            proc.Parameters.Append(proc.CreateParameter("aisndoc", 3, 1, 0, aisndoc));
            proc.Parameters.Append(proc.CreateParameter("aisn_linkdoc", 3, 1, 0, (object)check_null(aisn_linkdoc)));
            proc.Parameters.Append(proc.CreateParameter("aisn_link", 3, 1, 0, aisn_link));
            proc.Parameters.Append(proc.CreateParameter("alinked_num", 200, 1, 255, check_null(alinked_num)));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, string.Empty));
            proc.Parameters.Append(proc.CreateParameter("aUrlStr", 200, 1, 255, check_null(aUrlStr)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_link(dynamic oHead, int aisn_ref_link, int aisn_doc, int aisn_linkeddoc, int aisn_link, string acard)
        {
            dynamic proc = oHead.GetProc("del_link");

            proc.Parameters.Append(proc.CreateParameter("aisn_ref_link", 3, 1, 0, aisn_ref_link));
            proc.Parameters.Append(proc.CreateParameter("aisn_doc", 3, 1, 0, aisn_doc));
            proc.Parameters.Append(proc.CreateParameter("aisn_linkeddoc", 3, 1, 0, check_null(aisn_linkeddoc)));
            proc.Parameters.Append(proc.CreateParameter("aisn_link", 3, 1, 0, aisn_link));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, string.Empty)); //Устарел

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_resolution(dynamic oHead, ref int? an_isn_resolution, int an_kind_resolution, int an_isn_ref_doc, int? an_isn_parent_resolution, string an_isn_author, string as_item_number, string as_resolution_text, int? an_isn_category, int an_conf, DateTime? ad_resolution_date, DateTime? ad_send_date, int an_notify_author, DateTime? ad_plan_date, DateTime? ad_interim_date, DateTime? ad_fact_date, string as_due_controller, int? an_control_state, string as_summary, int an_cascade_control, int an_control_duty, string as_resume, int an_left_resolution, int an_cycle, string as_note, string as_rep_isns, string as_rep_responsible_isns, int? an_cab, int a_is_project)
        {
            dynamic proc = oHead.GetProc("add_resolution");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("an_kind_resolution", 3, 1, 0, an_kind_resolution));
            proc.Parameters.Append(proc.CreateParameter("an_isn_ref_doc", 3, 1, 0, an_isn_ref_doc));
            proc.Parameters.Append(proc.CreateParameter("an_isn_parent_resolution", 3, 1, 0, check_null(an_isn_parent_resolution)));
            proc.Parameters.Append(proc.CreateParameter("an_isn_author", 200, 1, 48, an_isn_author));
            proc.Parameters.Append(proc.CreateParameter("as_item_number", 200, 1, 64, check_null(as_item_number)));
            proc.Parameters.Append(proc.CreateParameter("as_resolution_text", 200, 1, 2000, check_null(as_resolution_text)));
            proc.Parameters.Append(proc.CreateParameter("an_isn_category", 3, 1, 0, check_null(an_isn_category)));
            proc.Parameters.Append(proc.CreateParameter("an_conf", 3, 1, 0, an_conf));
            proc.Parameters.Append(proc.CreateParameter("ad_resolution_date", 200, 1, 20, check_null(ad_resolution_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("ad_send_date", 200, 1, 20, check_null(ad_send_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("an_notify_author", 3, 1, 0, an_notify_author));
            proc.Parameters.Append(proc.CreateParameter("ad_plan_date", 200, 1, 20, check_null(ad_plan_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("ad_interim_date", 200, 1, 20, check_null(ad_interim_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("ad_fact_date", 200, 1, 20, check_null(ad_fact_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("as_due_controller", 200, 1, 48, check_null(as_due_controller)));
            proc.Parameters.Append(proc.CreateParameter("an_control_state", 3, 1, 0, check_null(an_control_state)));
            proc.Parameters.Append(proc.CreateParameter("as_summary", 200, 1, 2000, check_null(as_summary)));
            proc.Parameters.Append(proc.CreateParameter("an_cascade_control", 3, 1, 0, an_cascade_control));
            proc.Parameters.Append(proc.CreateParameter("an_control_duty", 3, 1, 0, an_control_duty));
            proc.Parameters.Append(proc.CreateParameter("as_resume", 200, 1, 2000, check_null(as_resume)));
            proc.Parameters.Append(proc.CreateParameter("an_left_resolution", 3, 1, 0, an_left_resolution));
            proc.Parameters.Append(proc.CreateParameter("an_cycle", 3, 1, 0, an_cycle));
            proc.Parameters.Append(proc.CreateParameter("as_note", 200, 1, 255, check_null(as_note)));
            proc.Parameters.Append(proc.CreateParameter("as_rep_isns", 200, 1, 2000, as_rep_isns));
            proc.Parameters.Append(proc.CreateParameter("as_rep_responsible_isns", 200, 1, 2000, as_rep_responsible_isns));
            proc.Parameters.Append(proc.CreateParameter("an_cab", 3, 1, 0, Convert.DBNull));
            dynamic anIsnParam = proc.CreateParameter("an_isn_resolution", 3, 3, 0, (object)an_isn_resolution);
            proc.Parameters.Append(anIsnParam);
            proc.Parameters.Append(proc.CreateParameter("an_is_project", 3, 1, 0, a_is_project));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                an_isn_resolution = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                an_isn_resolution = (int?)anIsnParam.Value;
            }
        }

        public void del_resolution(dynamic oHead, int an_isn_resolution, int a_delete_cascade)
        {
            dynamic proc = oHead.GetProc("del_resolution");

            proc.Parameters.Append(proc.CreateParameter("an_isn_resolution", 3, 1, 0, an_isn_resolution));
            proc.Parameters.Append(proc.CreateParameter("a_delete_cascade", 3, 1, 0, a_delete_cascade));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_resolution(dynamic oHead, int a_isn_resolution, DateTime? a_fact_date, DateTime? a_plan_date, DateTime? a_interim_date, DateTime? a_send_date, string a_summary, string a_resume, string a_resolution_text, int? a_isn_status_exec, int? a_control_state, string a_note, int a_control_duty, string a_due_controller, string a_due_author, int? a_isn_category, string a_rep_isns, string a_rep_responsible_isns)
        {
            dynamic proc = oHead.GetProc("edit_resolution");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("a_isn_resolution", 3, 1, 0, a_isn_resolution));
            proc.Parameters.Append(proc.CreateParameter("a_fact_date", 200, 1, 20, check_null(a_fact_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("a_plan_date", 200, 1, 20, check_null(a_plan_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("a_interim_date", 200, 1, 20, check_null(a_interim_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("a_send_date", 200, 1, 20, check_null(a_send_date, "yyyyMMdd HH:mm:ss")));
            proc.Parameters.Append(proc.CreateParameter("a_summary", 200, 1, 2000, check_null(a_summary)));
            proc.Parameters.Append(proc.CreateParameter("a_resume", 200, 1, 2000, check_null(a_resume)));
            proc.Parameters.Append(proc.CreateParameter("a_resolution_text", 200, 1, 2000, check_null(a_resolution_text)));
            proc.Parameters.Append(proc.CreateParameter("a_isn_status_exec", 3, 1, 0, check_null(a_isn_status_exec)));
            proc.Parameters.Append(proc.CreateParameter("a_control_state", 3, 1, 0, check_null(a_control_state)));
            proc.Parameters.Append(proc.CreateParameter("a_note", 200, 1, 255, check_null(a_note)));
            proc.Parameters.Append(proc.CreateParameter("a_control_duty", 3, 1, 0, a_control_duty));
            proc.Parameters.Append(proc.CreateParameter("a_due_controller", 200, 1, 48, check_null(a_due_controller)));
            proc.Parameters.Append(proc.CreateParameter("a_due_author", 200, 1, 48, a_due_author));
            proc.Parameters.Append(proc.CreateParameter("a_isn_category", 3, 1, 0, check_null(a_isn_category)));
            proc.Parameters.Append(proc.CreateParameter("a_rep_isns", 200, 1, 2000, a_rep_isns));
            proc.Parameters.Append(proc.CreateParameter("a_rep_responsible_isns", 200, 1, 2000, a_rep_responsible_isns));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void return_res_proj(dynamic oHead, int an_isn_resolution)
        {
            dynamic proc = oHead.GetProc("Return_res_proj");

            proc.Parameters.Append(proc.CreateParameter("an_isn_resolution", 3, 1, 0, an_isn_resolution));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void accept_control(dynamic oHead, string as_isns_resolution, int? an_cab)
        {
            dynamic proc = oHead.GetProc("accept_control");

            proc.Parameters.Append(proc.CreateParameter("as_isns_resolution", 200, 1, 2000, as_isns_resolution));
            proc.Parameters.Append(proc.CreateParameter("an_cab", 3, 1, 0, Convert.DBNull)); //Устарел
            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void set_reply(dynamic oHead, int an_isn_reply, string as_reply_text, DateTime? ad_reply_date, int an_status_reply, int? an_cab)
        {
            dynamic proc = oHead.GetProc("set_reply");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("an_isn_reply", 3, 1, 0, an_isn_reply));
            proc.Parameters.Append(proc.CreateParameter("as_reply_text", 200, 1, 2000, as_reply_text));
            proc.Parameters.Append(proc.CreateParameter("ad_reply_date", 200, 1, 20, check_null(ad_reply_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("an_status_reply", 3, 1, 0, an_status_reply));
            proc.Parameters.Append(proc.CreateParameter("an_cab", 3, 1, 0, Convert.DBNull)); //Устарел

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void accept_execute(dynamic oHead, int an_isn_folder_item)
        {
            dynamic proc = oHead.GetProc("accept_execute");

            proc.Parameters.Append(proc.CreateParameter("an_isn_folder_item", 3, 1, 0, an_isn_folder_item));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void mark_fi_as_read(dynamic oHead, int an_isn_folder_item, int an_kind_doc)
        {
            dynamic proc = oHead.GetProc("mark_fi_as_read");

            proc.Parameters.Append(proc.CreateParameter("an_isn_folder_item", 3, 1, 0, an_isn_folder_item));
            proc.Parameters.Append(proc.CreateParameter("an_kind_doc", 3, 1, 0, an_kind_doc));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void delete_fi(dynamic oHead, int an_isn_folder_item)
        {
            dynamic proc = oHead.GetProc("delete_fi");

            proc.Parameters.Append(proc.CreateParameter("an_isn_folder_item", 3, 1, 0, an_isn_folder_item));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void set_user_parm(dynamic oHead, string as_parm_name, string as_parm_value, int? af_default)
        {
            dynamic proc = oHead.GetProc("set_user_parm");

            proc.Parameters.Append(proc.CreateParameter("as_parm_name", 200, 1, 64, as_parm_name));
            proc.Parameters.Append(proc.CreateParameter("as_parm_value", 200, 1, 2000, as_parm_value));
            proc.Parameters.Append(proc.CreateParameter("af_default", 3, 1, 0, check_null(af_default)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void upd_pasw(dynamic oHead, string new_pass)
        {
            dynamic proc = oHead.GetProc("upd_pasw");

            proc.Parameters.Append(proc.CreateParameter("new_pass", 200, 1, 30, new_pass));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void save_organiz_web(dynamic oHead, string aOper, ref int? aIsn, ref string aDue, string aHighNode, string aClassifName, string aZipCode, string aCity, string aAddress, string aFullname, string aOkpo, string aInn, string aDue_region, string aEmail, int aMailForAll, int aIsnAddrCategory, string aNote, string aOkonh, string aLawAdress, string aSertificat, string aCode)
        {
            dynamic proc = oHead.GetProc("save_organiz_web");

            proc.Parameters.Append(proc.CreateParameter("aOper", 200, 1, 3, aOper));
            dynamic aIsnParam = proc.CreateParameter("aIsn", 3, 3, 0, check_null(aIsn));
            proc.Parameters.Append(aIsnParam);
            dynamic aDueParam = proc.CreateParameter("aDue", 200, 3, 48, check_null(aDue));
            proc.Parameters.Append(aDueParam);
            proc.Parameters.Append(proc.CreateParameter("aHighNode", 200, 1, 48, check_null(aHighNode)));
            proc.Parameters.Append(proc.CreateParameter("aClassifName", 200, 1, 255, check_null(aClassifName)));
            proc.Parameters.Append(proc.CreateParameter("aZipCode", 200, 1, 12, check_null(aZipCode)));
            proc.Parameters.Append(proc.CreateParameter("aCity", 200, 1, 255, check_null(aCity)));
            proc.Parameters.Append(proc.CreateParameter("aAddress", 200, 1, 255, check_null(aAddress)));
            proc.Parameters.Append(proc.CreateParameter("aFullname", 200, 1, 255, check_null(aFullname)));
            proc.Parameters.Append(proc.CreateParameter("aOkpo", 200, 1, 8, check_null(aOkpo)));
            proc.Parameters.Append(proc.CreateParameter("aInn", 200, 1, 64, check_null(aInn)));
            proc.Parameters.Append(proc.CreateParameter("aDue_region", 200, 1, 48, check_null(aDue_region)));
            proc.Parameters.Append(proc.CreateParameter("aEmail", 200, 1, 64, check_null(aEmail)));
            proc.Parameters.Append(proc.CreateParameter("aMailForAll", 3, 1, 0, aMailForAll));
            proc.Parameters.Append(proc.CreateParameter("aIsnAddrCategory", 3, 1, 0, check_null(aIsnAddrCategory)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 255, check_null(aNote)));
            proc.Parameters.Append(proc.CreateParameter("aOkonh", 200, 1, 16, check_null(aOkonh)));
            proc.Parameters.Append(proc.CreateParameter("aLawAdress", 200, 1, 255, check_null(aLawAdress)));
            proc.Parameters.Append(proc.CreateParameter("aSertificat", 200, 1, 255, check_null(aSertificat)));
            proc.Parameters.Append(proc.CreateParameter("aCode", 200, 1, 4, check_null(aCode)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aIsn = null;
                aDue = string.Empty;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                aIsn = (int?)aIsnParam.Value;
                aDue = aDueParam.Value;
            }
        }

        public void save_organiz_bank_recvisit(dynamic oHead, string a_oper, ref int? a_isn_bank_recv, int a_isn_organiz, string a_classif_name, string a_bank_name, string a_acount, string a_subacount, string a_bik, string a_city, string a_note)
        {
            dynamic proc = oHead.GetProc("save_organiz_bank_recvisit");

            proc.Parameters.Append(proc.CreateParameter("a_oper", 200, 1, 1, a_oper));
            dynamic aIsnParam = proc.CreateParameter("aIsn", 3, 3, 0, (object)check_null(a_isn_bank_recv));
            proc.Parameters.Append(aIsnParam);
            proc.Parameters.Append(proc.CreateParameter("a_isn_organiz", 3, 1, 0, a_isn_organiz));
            proc.Parameters.Append(proc.CreateParameter("aa_classif_name", 200, 1, 64, a_classif_name));
            proc.Parameters.Append(proc.CreateParameter("a_bank_name", 200, 1, 64, a_bank_name));
            proc.Parameters.Append(proc.CreateParameter("a_acount", 200, 1, 24, a_acount));
            proc.Parameters.Append(proc.CreateParameter("a_subacount", 200, 1, 24, a_subacount));
            proc.Parameters.Append(proc.CreateParameter("a_bik", 200, 1, 9, a_bik));
            proc.Parameters.Append(proc.CreateParameter("aCity", 200, 1, 64, check_null(a_city)));
            proc.Parameters.Append(proc.CreateParameter("a_note", 200, 1, 255, a_note));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                a_isn_bank_recv = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                a_isn_bank_recv = (int?)aIsnParam.Value;
            }
        }

        public void get_tech_due(dynamic oHead, int a_isn_contact, ref string a_due)
        {
            dynamic proc = oHead.GetProc("get_tech_due");

            proc.Parameters.Append(proc.CreateParameter("a_isn_contact", 3, 1, 0, a_isn_contact));
            dynamic a_dueParam = proc.CreateParameter("a_due", 200, 3, 48, (object)check_null(a_due));
            proc.Parameters.Append(a_dueParam);
            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                a_due = string.Empty;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                a_due = (string)a_dueParam.Value;
            }
        }

        public void save_citizen_web(dynamic oHead, string aOper, ref int? aIsn, string aSurname, string aZipcode, string aCity, string aAddress, string aDue_region, int? aIsn_Addr_Category, string aCitstatusCl_Due, string aPhone, string aEmail, string aNote, string aInn, string aSnils, int aSex, string aSeries, string aNPasport, string aGiven)
        {
            dynamic proc = oHead.GetProc("save_citizen_web");

            proc.Parameters.Append(proc.CreateParameter("aOper", 200, 1, 3, aOper));
            dynamic aIsnParam = proc.CreateParameter("aIsn", 3, 3, 0, (object)check_null(aIsn));
            proc.Parameters.Append(aIsnParam);
            proc.Parameters.Append(proc.CreateParameter("aSurname", 200, 1, 64, check_null(aSurname)));
            proc.Parameters.Append(proc.CreateParameter("aZipcode", 200, 1, 12, check_null(aZipcode)));
            proc.Parameters.Append(proc.CreateParameter("aCity", 200, 1, 255, check_null(aCity)));
            proc.Parameters.Append(proc.CreateParameter("aAddress", 200, 1, 255, check_null(aAddress)));
            proc.Parameters.Append(proc.CreateParameter("aDue_region", 200, 1, 48, check_null(aDue_region)));
            proc.Parameters.Append(proc.CreateParameter("aIsn_Addr_Category", 3, 1, 0, check_null(aIsn_Addr_Category)));
            proc.Parameters.Append(proc.CreateParameter("aCitstatusCl_Due", 200, 1, 48, check_null(aCitstatusCl_Due)));
            proc.Parameters.Append(proc.CreateParameter("aEmail", 200, 1, 64, check_null(aEmail)));
            proc.Parameters.Append(proc.CreateParameter("aPhone", 200, 1, 64, check_null(aPhone)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 255, check_null(aNote)));
            proc.Parameters.Append(proc.CreateParameter("aInn", 200, 1, 64, check_null(aInn)));
            proc.Parameters.Append(proc.CreateParameter("aSnils", 200, 1, 14, check_null(aSnils)));
            proc.Parameters.Append(proc.CreateParameter("aSex", 3, 1, 0, check_null(aSex)));
            proc.Parameters.Append(proc.CreateParameter("aSeries", 200, 1, 64, check_null(aSeries)));
            proc.Parameters.Append(proc.CreateParameter("aNPasport", 200, 1, 64, check_null(aNPasport)));
            proc.Parameters.Append(proc.CreateParameter("aGiven", 200, 1, 255, check_null(aGiven)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aIsn = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                aOper = "";
                aIsn = (int?)aIsnParam.Value;
            }
        }

        public void release_num_web(dynamic oHead, string aduedocgroup, int ayear, int aordernum)
        {
            dynamic proc = oHead.GetProc("release_num_web");

            proc.Parameters.Append(proc.CreateParameter("aduedocgroup", 200, 1, 48, check_null(aduedocgroup)));
            proc.Parameters.Append(proc.CreateParameter("ayear", 3, 1, 0, check_null(ayear)));
            proc.Parameters.Append(proc.CreateParameter("aordernum", 3, 1, 0, check_null(aordernum)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void release_prj_num_web(dynamic oHead, string aduedocgroup, int ayear, int aordernum)
        {
            dynamic proc = oHead.GetProc("release_prj_num_web");

            proc.Parameters.Append(proc.CreateParameter("aduedocgroup", 200, 1, 48, check_null(aduedocgroup)));
            proc.Parameters.Append(proc.CreateParameter("ayear", 3, 1, 0, check_null(ayear)));
            proc.Parameters.Append(proc.CreateParameter("aordernum", 3, 1, 0, check_null(aordernum)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void write_prot(dynamic oHead, string aTable_Id, string aOper_Id, string aSuboper_Id, string aOper_Describe, int aRef_Isn, string aOper_Comment)
        {
            dynamic proc = oHead.GetProc("write_prot");

            proc.Parameters.Append(proc.CreateParameter("aTable_Id", 200, 1, 1, aTable_Id));
            proc.Parameters.Append(proc.CreateParameter("aOper_Id", 200, 1, 1, aOper_Id));
            proc.Parameters.Append(proc.CreateParameter("aSuboper_Id", 200, 1, 1, aSuboper_Id));
            proc.Parameters.Append(proc.CreateParameter("aOper_Describe", 200, 1, 3, aOper_Describe));
            proc.Parameters.Append(proc.CreateParameter("aRef_Isn", 3, 1, 0, aRef_Isn));
            proc.Parameters.Append(proc.CreateParameter("aOper_Comment", 200, 1, 255, check_null(aOper_Comment)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_evnt_queue_item(dynamic oHead, int aKindEvent, string aObjectName, string aObjectId, string aDueDocgroup, string aFlags)
        {
            dynamic proc = oHead.GetProc("add_evnt_queue_item");

            proc.Parameters.Append(proc.CreateParameter("aKindEvent", 3, 1, 0, aKindEvent));
            proc.Parameters.Append(proc.CreateParameter("aObjectName", 200, 1, 48, aObjectName));
            proc.Parameters.Append(proc.CreateParameter("aObjectId", 200, 1, 255, check_null(aObjectId)));
            proc.Parameters.Append(proc.CreateParameter("aDueDocgroup", 200, 1, 48, check_null(aDueDocgroup)));
            proc.Parameters.Append(proc.CreateParameter("aFlags", 200, 1, 2000, check_null(aFlags)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_wf_instance(dynamic oHead, int aIsnProcessConfig, string aMessage, string aObjectID, string aParams, ref int? aIsnInstance)
        {
            dynamic proc = oHead.GetProc("add_wf_instance");

            proc.Parameters.Append(proc.CreateParameter("aIsnProcessConfig", 3, 1, 0, aIsnProcessConfig));
            proc.Parameters.Append(proc.CreateParameter("aMessage", 200, 1, 255, aMessage));
            proc.Parameters.Append(proc.CreateParameter("aObjectID", 200, 1, 255, aObjectID));
            proc.Parameters.Append(proc.CreateParameter("aParams", 200, 1, 2000, check_null(aParams)));
            dynamic aIsnInstanceParam = proc.CreateParameter("aIsnInstance", 3, 3, 0, (object)check_null(aIsnInstance));
            proc.Parameters.Append(aIsnInstanceParam);
            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aIsnInstance = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                aIsnInstance = (int?)aIsnInstanceParam.Value;
            }
        }

        public void add_prj(dynamic oHead, ref int? aIsn, string aDueDocGroup, int aOrderNum, string aFreeNum, DateTime? aPrjDate, int aSecurlevel, string aConsists, DateTime? aPlanDate, string aAnnotat, string aNote, string aDuePersonExe, int? aIsnLinkingDoc, int? aIsnLinkingPrj, int? aIsnClLink, string aCopyShablon, string aVisas, int? aEDocument)
        {
            dynamic proc = oHead.GetProc("add_prj");
            string maskDate = "yyyyMMdd";


            dynamic aIsnParam = proc.CreateParameter("aIsn", 3, 3, 0, (object)aIsn);
            proc.Parameters.Append(aIsnParam);
            proc.Parameters.Append(proc.CreateParameter("aDueDocGroup", 200, 1, 48, aDueDocGroup));
            proc.Parameters.Append(proc.CreateParameter("aOrderNum", 3, 1, 0, aOrderNum));
            proc.Parameters.Append(proc.CreateParameter("aFreeNum", 200, 1, 64, check_null(aFreeNum)));
            proc.Parameters.Append(proc.CreateParameter("aPrjDate", 200, 1, 20, aPrjDate.HasValue ? aPrjDate.Value.ToString(maskDate) : DateTime.Now.ToString(maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aSecurlevel", 3, 1, 0, aSecurlevel == 0 ? 1 : aSecurlevel));
            proc.Parameters.Append(proc.CreateParameter("aConsists", 200, 1, 255, check_null(aConsists)));
            proc.Parameters.Append(proc.CreateParameter("aPlanDate", 200, 1, 20, check_null(aPlanDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aAnnotat", 200, 1, 2000, check_null(aAnnotat)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 2000, check_null(aNote)));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonExe", 200, 1, 8000, check_null(aDuePersonExe)));
            proc.Parameters.Append(proc.CreateParameter("aIsnLinkingDoc", 3, 1, 0, (object)check_null(aIsnLinkingDoc)));
            proc.Parameters.Append(proc.CreateParameter("aIsnLinkingRes", 3, 1, 0, (object)check_null(aIsnLinkingPrj)));
            proc.Parameters.Append(proc.CreateParameter("aIsnClLink", 3, 1, 0, (object)check_null(aIsnClLink)));
            proc.Parameters.Append(proc.CreateParameter("aCopyShablon", 200, 1, 20, check_null(aCopyShablon)));
            proc.Parameters.Append(proc.CreateParameter("aVisas", 200, 1, 64, check_null(aCopyShablon)));
            proc.Parameters.Append(proc.CreateParameter("aEDocument", 3, 1, 0, (object)aEDocument));
            
            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aIsn = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                aIsn = (int?)aIsnParam.Value;
            }
        }

        public void del_prj(dynamic oHead, int aIsnBatch, int aRetNumber, int? aIsnPrj)
        {
            dynamic proc = oHead.GetProc("del_prj");

            proc.Parameters.Append(proc.CreateParameter("aIsnBatch", 3, 1, 0, aIsnBatch));
            proc.Parameters.Append(proc.CreateParameter("aRetNumber", 3, 1, 0, aRetNumber));
            proc.Parameters.Append(proc.CreateParameter("aIsnPrj", 3, 1, 0, check_null(aIsnPrj)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_prj(dynamic oHead, int aIsn, DateTime? aPrjDate, int aSecurlevel, string aConsists, DateTime? aPlanDate, string aAnnotat, string aNote, string aDuePersonExe, string aFreeNum, int? aOrderNum, int? aEDocument)
        {
            dynamic proc = oHead.GetProc("edit_prj");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("aPrjDate", 200, 1, 20, check_null(aPrjDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aSeurlevel", 3, 1, 0, aSecurlevel));
            proc.Parameters.Append(proc.CreateParameter("aConsists", 200, 1, 255, check_null(aConsists)));
            proc.Parameters.Append(proc.CreateParameter("aPlanDate", 200, 1, 20, check_null(aPlanDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aAnnotat", 200, 1, 2000, check_null(aAnnotat)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 2000, check_null(aNote)));
            proc.Parameters.Append(proc.CreateParameter("aDuePersonExe", 200, 1, 48, aDuePersonExe));
            proc.Parameters.Append(proc.CreateParameter("aFreeNum", 200, 1, 64, aFreeNum));
            proc.Parameters.Append(proc.CreateParameter("aOrderNum", 3, 1, 0, check_null(aOrderNum)));
            proc.Parameters.Append(proc.CreateParameter("aEDocument", 3, 1, 0, check_null(aEDocument)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_prj_version(dynamic oHead, ref int? aIsn, int aIsnOldVersion, int aCopySecondaryVisa)
        {
            dynamic proc = oHead.GetProc("add_prj_version");

            dynamic aIsnParam = proc.CreateParameter("aIsn", 3, 3, 0, (object)check_null(aIsn));
            proc.Parameters.Append(aIsnParam);
            proc.Parameters.Append(proc.CreateParameter("aIsnOldVersion", 3, 1, 0, aIsnOldVersion));
            proc.Parameters.Append(proc.CreateParameter("aCopySecondaryVisa", 3, 1, 0, aCopySecondaryVisa));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aIsn = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                aIsn = (int?)aIsnParam.Value;
            }
        }

        public void send_prj_reg(dynamic oHead, int aIsn)
        {
            dynamic proc = oHead.GetProc("send_prj_reg");

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void reserve_prj_num(dynamic oHead, string aOper, string aDueDocgroup, int aYear, ref int? aOrderNum, ref string aFreeNum, string aSessionId)
        {
            dynamic proc = oHead.GetProc("reserve_prj_num");

            proc.Parameters.Append(proc.CreateParameter("aOper", 200, 1, 2, aOper));
            proc.Parameters.Append(proc.CreateParameter("aDueDocgroup", 200, 1, 48, aDueDocgroup));
            proc.Parameters.Append(proc.CreateParameter("aYear", 3, 1, 0, aYear));
            dynamic aOrderNumParam = proc.CreateParameter("aOrderNum", 3, 3, 0, (object)check_null(aOrderNum));
            proc.Parameters.Append(aOrderNumParam);
            dynamic aFreeNumParam = proc.CreateParameter("aFreeNum", 200, 3, 64, check_null(aFreeNum));
            proc.Parameters.Append(aFreeNumParam);
            proc.Parameters.Append(proc.CreateParameter("aSessionId", 200, 1, 255, aSessionId));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aOrderNum = null;
                aFreeNum = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                aOrderNum = (int?)aOrderNumParam.Value;
                aFreeNum = (string)aFreeNumParam.Value;
            }
        }

        public void return_prj_num(dynamic oHead, string aOper, string aDueDocgroup, int aYear, int aOrderNum, ref string aFreeNum)
        {
            dynamic proc = oHead.GetProc("return_prj_num");

            proc.Parameters.Append(proc.CreateParameter("aOper", 200, 1, 2, aOper));
            proc.Parameters.Append(proc.CreateParameter("aDueDocgroup", 200, 1, 48, aDueDocgroup));
            proc.Parameters.Append(proc.CreateParameter("aYear", 3, 1, 0, aYear));
            proc.Parameters.Append(proc.CreateParameter("aOrderNum", 3, 1, 0, (object)aOrderNum));
            dynamic aFreeNumParam = proc.CreateParameter("aFreeNum", 200, 1, 64, check_null(aFreeNum));
            proc.Parameters.Append(aFreeNumParam);
            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
            {
                aFreeNum = null;
                throw new Exception(oHead.ErrText);
            }
            else
            {
                aFreeNum = (string)aFreeNumParam.Value;
            }
        }

        public void save_file_wf(dynamic oHead, int action, int? aISN_REF_FILE, int aISN_RC, int aKind_Doc, string aDescription, string aCategory, int? aSecur, int? aLockFlag, string aFileAccessDues, string aFileName, int? aDontDelFlag)
        {

            dynamic proc = oHead.GetProc("save_file_wf");
            proc.Parameters.Append(proc.CreateParameter("Action", 3, 1, 0, 1));
            proc.Parameters.Append(proc.CreateParameter("aISN_REF_FILE", 3, 1, 0, 0));
            proc.Parameters.Append(proc.CreateParameter("aISN_RC", 3, 1, 0, 4324));
            proc.Parameters.Append(proc.CreateParameter("aKind_Doc", 3, 1, 0, 3));
            proc.Parameters.Append(proc.CreateParameter("aDescription", 200, 1, 255, "test.txt"));
            proc.Parameters.Append(proc.CreateParameter("aCategory", 200, 1, 64, System.DBNull.Value));
            proc.Parameters.Append(proc.CreateParameter("aSecur", 3, 1, 0, 0));
            proc.Parameters.Append(proc.CreateParameter("aLockFlag", 3, 1, 0, 0));
            proc.Parameters.Append(proc.CreateParameter("aFileAccessDues", 200, 1, 8000, System.DBNull.Value));
            proc.Parameters.Append(proc.CreateParameter("aDontDelFlag", 3, 1, 0, 0));

            proc.Parameters.Append(proc.CreateParameter("aIs_hidden", 3, 1, 0, 0));
            proc.Parameters.Append(proc.CreateParameter("aApply_eds", 3, 1, 0, 0));
            proc.Parameters.Append(proc.CreateParameter("aSend_enabled", 3, 1, 0, 0));
            proc.Parameters.Append(proc.CreateParameter("aSaveSourceFile", 3, 1, 0, 1));

            proc.Parameters.Append(proc.CreateParameter("aFilename", 200, 1, 2000, @"C:\Test.txt"));
            oHead.ExecuteProc(proc);


            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);

            /*	List<string> files;
				files = new List<string>();

				dynamic doc = oHead.GetRow("RcOut", 4482);

				foreach (var file in doc.Files)
				{
					string newEosFile = file.Descript;

					if (!(file.Contents is DBNull))
					{
						string pathToDownload = @"C:\";

						if (file.Contents.ErrCode == 0)
						{
							file.Contents.Prepare(pathToDownload);
							if (file.Contents.ErrCode == 0)
							{
								files.Add(newEosFile);
								MessageBox.Show("Good");
							}
							else if (file.Contents.ErrCode != 0)
							{
								MessageBox.Show("Poor");
							}
						}
					}
				}

			/*	if (!(file.Contents is DBNull))
				{
					dynamic contents = file.Contents;

					var pathToDownload = @"c:\TEmp\";

					if (contents.ErrCode == 0)
					{
						contents.Prepare(pathToDownload);
						if (contents.ErrCode == 0)
						{

							MessageBox.Show("Гуд");
						}
						else if (contents.ErrCode != 0)
						{
							MessageBox.Show("Плохо");
						}
					}
				}
				*/


            //dynamic proc = oHead.GetProc("SAVE_FILE_WF");

            //proc.Parameters.Append(proc.CreateParameter("action", 3, 1, 0, action));
            //proc.Parameters.Append(proc.CreateParameter("aISN_REF_FILE", 3, 1, 0, check_null(aISN_REF_FILE)));
            //proc.Parameters.Append(proc.CreateParameter("aISN_RC", 3, 1, 0, check_null(aISN_RC)));
            //proc.Parameters.Append(proc.CreateParameter("aKind_Doc", 3, 1, 0, check_null(aKind_Doc)));
            //proc.Parameters.Append(proc.CreateParameter("aDescription", 200, 1, 255, check_null(aDescription)));
            //proc.Parameters.Append(proc.CreateParameter("aCategory", 200, 1, 255, check_null(aCategory)));
            //proc.Parameters.Append(proc.CreateParameter("aSecur", 3, 1, 0, check_null(aSecur)));
            //proc.Parameters.Append(proc.CreateParameter("aLockFlag", 3, 1, 0, check_null(aLockFlag)));
            //proc.Parameters.Append(proc.CreateParameter("aFileAccessDues", 200, 1, 255, check_null(aFileAccessDues)));
            //proc.Parameters.Append(proc.CreateParameter("aFileName", 200, 1, 255, check_null(aFileName)));
            //proc.Parameters.Append(proc.CreateParameter("aDontDelFlag", 3, 1, 0, check_null(aDontDelFlag)));

            //oHead.ExecuteProc(proc);

            //if (oHead.ErrCode < 0)
            //	throw new Exception(oHead.ErrText);
        }

        public void import_file_eds(dynamic oHead, int aIsnRefFile, string aEdsBody, string a_eds_body_blob, int? a_isn_sign_kind, string a_certificate_owner, DateTime? a_signing_date, string a_sign_text)
        {
            dynamic proc = oHead.GetProc("import_file_eds");
            string maskDate = "yyyyMMdd HH:mm:ss";

            proc.Parameters.Append(proc.CreateParameter("aIsnRefFile", 3, 1, 0, aIsnRefFile));
            proc.Parameters.Append(proc.CreateParameter("aEdsBody", 200, 1, 2000, check_null(aEdsBody)));
            proc.Parameters.Append(proc.CreateParameter("a_eds_body_blob", 128, 1, 32000, GetFile(a_eds_body_blob)));
            proc.Parameters.Append(proc.CreateParameter("a_isn_sign_kind", 3, 1, 0, check_null(a_isn_sign_kind)));
            proc.Parameters.Append(proc.CreateParameter("a_certificate_owner", 200, 1, 255, a_certificate_owner));
            proc.Parameters.Append(proc.CreateParameter("a_signing_date", 200, 1, 20, check_null(a_signing_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("a_sign_text", 200, 1, 255, a_sign_text));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_file_eds(dynamic oHead, int a_isn_ref_file, string a_eds_body, int? a_isn_sign_kind, string a_due_person, int? a_flag, string a_eds_body_blob, string a_certificate_owner, DateTime? a_signing_date)
        {
            dynamic proc = oHead.GetProc("add_file_eds");
            string maskDate = "yyyyMMdd HH:mm:ss";

            proc.Parameters.Append(proc.CreateParameter("a_isn_ref_file", 3, 1, 0, a_isn_ref_file));
            proc.Parameters.Append(proc.CreateParameter("a_eds_body", 200, 1, 2000, check_null(a_eds_body)));
            proc.Parameters.Append(proc.CreateParameter("a_isn_sign_kind", 3, 1, 0, check_null(a_isn_sign_kind)));
            proc.Parameters.Append(proc.CreateParameter("a_due_person", 200, 1, 48, a_due_person));
            proc.Parameters.Append(proc.CreateParameter("a_flag", 3, 1, 0, a_flag));
            proc.Parameters.Append(proc.CreateParameter("a_eds_body_blob", 128, 1, 32000, GetFile(a_eds_body_blob)));
            proc.Parameters.Append(proc.CreateParameter("a_certificate_owner", 200, 1, 255, a_certificate_owner));
            proc.Parameters.Append(proc.CreateParameter("a_signing_date", 200, 1, 20, check_null(a_signing_date, maskDate)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_file_eds(dynamic oHead, int a_isn_ref_file_eds)
        {
            dynamic proc = oHead.GetProc("del_file_eds");

            proc.Parameters.Append(proc.CreateParameter("a_isn_ref_file_eds", 3, 1, 0, a_isn_ref_file_eds));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_corresp(dynamic oHead, int aIsnDoc, string acard, int acab, string aCodes, string aCorrespNums, DateTime[] aCorrespDates, string aCorrespSigns, string aIsnsContact, string aNeedAnswers, string aNotes)
        {
            dynamic proc = oHead.GetProc("add_corresp");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsnDoc", 3, 1, 0, aIsnDoc));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, check_null(acard)));
            proc.Parameters.Append(proc.CreateParameter("acab", 3, 1, 0, acab));
            proc.Parameters.Append(proc.CreateParameter("aCodes", 200, 1, 8000, aCodes));
            proc.Parameters.Append(proc.CreateParameter("aCorrespNums", 200, 1, 8000, aCorrespNums));
            proc.Parameters.Append(proc.CreateParameter("aCorrespDates", 200, 1, 8000, check_null(aCorrespDates, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespSigns", 200, 1, 8000, check_null(aCorrespSigns)));
            proc.Parameters.Append(proc.CreateParameter("aIsnsContact", 200, 1, 8000, check_null(aIsnsContact)));
            proc.Parameters.Append(proc.CreateParameter("aNeedAnswers", 200, 1, 8000, check_null(aNeedAnswers)));
            proc.Parameters.Append(proc.CreateParameter("aNotes", 200, 1, 8000, check_null(aNotes)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);

        }

        public void del_corresp(dynamic oHead, int aIsn, string acard)
        {
            dynamic proc = oHead.GetProc("del_corresp");

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, check_null(acard)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_corresp(dynamic oHead, int aIsn, string acard, int acab, string aCode, int? aisn_contact, string aCorrespNum, DateTime? aCorrespDate, string aCorrespSign, string aNote)
        {
            dynamic proc = oHead.GetProc("edit_corresp");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("acard", 200, 1, 48, check_null(acard)));
            proc.Parameters.Append(proc.CreateParameter("acab", 3, 1, 0, acab));
            proc.Parameters.Append(proc.CreateParameter("aCode", 200, 1, 8000, check_null(aCode)));
            proc.Parameters.Append(proc.CreateParameter("aisn_contact", 200, 1, 8000, check_null(aisn_contact)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespNum", 200, 1, 8000, aCorrespNum));
            proc.Parameters.Append(proc.CreateParameter("aCorrespDate", 200, 1, 8000, check_null(aCorrespDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespSign", 200, 1, 8000, check_null(aCorrespSign)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 2000, check_null(aNote)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_cover_doc(dynamic oHead, int aIsnDoc, string aCard, int aCab, string aCodes, string aCorrespNums, DateTime[] aCorrespDates, string aCorrespSigns, string aAnnotats, string aConsists, string aNotes, string aIsnsContact)
        {
            dynamic proc = oHead.GetProc("add_cover_doc");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsnDoc", 3, 1, 0, aIsnDoc));
            proc.Parameters.Append(proc.CreateParameter("aCard", 200, 1, 48, check_null(aCard)));
            proc.Parameters.Append(proc.CreateParameter("aCab", 3, 1, 0, check_null(aCab)));
            proc.Parameters.Append(proc.CreateParameter("aCodes", 200, 1, 8000, aCodes));
            proc.Parameters.Append(proc.CreateParameter("aCorrespNums", 200, 1, 8000, aCorrespNums));
            proc.Parameters.Append(proc.CreateParameter("aCorrespDates", 200, 1, 20, check_null(aCorrespDates, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespSigns", 200, 1, 8000, aCorrespSigns));
            proc.Parameters.Append(proc.CreateParameter("aAnnotats", 200, 1, 2000, check_null(aAnnotats)));
            proc.Parameters.Append(proc.CreateParameter("aConsists", 200, 1, 255, check_null(aConsists)));
            proc.Parameters.Append(proc.CreateParameter("aNotes", 200, 1, 8000, check_null(aNotes)));
            proc.Parameters.Append(proc.CreateParameter("aIsnsContact", 200, 1, 8000, aIsnsContact));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_cover_doc(dynamic oHead, int aIsn, string aCard, int aCab, string aCode, string aCorrespNum, DateTime? aCorrespDate, string aCorrespSign, string aAnnotat, string aConsist, string aNote)
        {
            dynamic proc = oHead.GetProc("edit_cover_doc");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("aCard", 200, 1, 48, check_null(aCard)));
            proc.Parameters.Append(proc.CreateParameter("aCab", 3, 1, 0, check_null(aCab)));
            proc.Parameters.Append(proc.CreateParameter("aCode", 200, 1, 8000, aCode));
            proc.Parameters.Append(proc.CreateParameter("aCorrespNum", 200, 1, 8000, aCorrespNum));
            proc.Parameters.Append(proc.CreateParameter("aCorrespDate", 200, 1, 20, check_null(aCorrespDate, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("aCorrespSign", 200, 1, 8000, aCorrespSign));
            proc.Parameters.Append(proc.CreateParameter("aAnnotat", 200, 1, 2000, check_null(aAnnotat)));
            proc.Parameters.Append(proc.CreateParameter("aConsist", 200, 1, 255, check_null(aConsist)));
            proc.Parameters.Append(proc.CreateParameter("aNote", 200, 1, 8000, check_null(aNote)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_cover_doc(dynamic oHead, int aIsn, string aCard)
        {
            dynamic proc = oHead.GetProc("del_cover_doc");

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("aCard", 200, 1, 48, check_null(aCard)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_ar(dynamic oHead, int aIsnOwner, string aArName, string aCard, string aValue)
        {
            dynamic proc = oHead.GetProc("edit_ar");

            proc.Parameters.Append(proc.CreateParameter("aIsnOwner", 3, 1, 0, aIsnOwner));
            proc.Parameters.Append(proc.CreateParameter("aArName", 200, 1, 2000, aArName));
            proc.Parameters.Append(proc.CreateParameter("aCard", 200, 1, 48, check_null(aCard)));
            proc.Parameters.Append(proc.CreateParameter("aValue", 200, 1, 48, check_null(aValue)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_prjvisasign(dynamic oHead, int aIsnPrj, int aKind, int? an_isn_prj_visa_sign, int? an_term, int an_term_flag, int an_parallel, string as_rep_isns)
        {
            dynamic proc = oHead.GetProc("add_prjvisasign");

            proc.Parameters.Append(proc.CreateParameter("aIsnPrj", 3, 1, 0, aIsnPrj));
            proc.Parameters.Append(proc.CreateParameter("aKind", 3, 1, 0, aKind));
            proc.Parameters.Append(proc.CreateParameter("an_isn_prj_visa_sign", 3, 1, 0, check_null(an_isn_prj_visa_sign)));
            proc.Parameters.Append(proc.CreateParameter("an_term", 3, 1, 0, check_null(an_term_flag)));
            proc.Parameters.Append(proc.CreateParameter("an_term_flag", 3, 1, 0, check_null(an_term_flag)));
            proc.Parameters.Append(proc.CreateParameter("an_parallel", 3, 1, 0, an_parallel));
            proc.Parameters.Append(proc.CreateParameter("as_rep_isns", 200, 1, 2000, as_rep_isns));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void send_prjvisasign(dynamic oHead, int aIsnPrj, int? an_term, int an_term_flag, int an_parallel, string as_rep_isns)
        {
            dynamic proc = oHead.GetProc("send_prjvisasign");

            proc.Parameters.Append(proc.CreateParameter("aIsnPrj", 3, 1, 0, aIsnPrj));
            proc.Parameters.Append(proc.CreateParameter("an_term", 3, 1, 0, check_null(an_term_flag)));
            proc.Parameters.Append(proc.CreateParameter("an_term_flag", 3, 1, 0, check_null(an_term_flag)));
            proc.Parameters.Append(proc.CreateParameter("an_parallel", 3, 1, 0, an_parallel));
            proc.Parameters.Append(proc.CreateParameter("as_rep_isns", 200, 1, 2000, as_rep_isns));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void recall_prjvs(dynamic oHead, int aIsn)
        {
            dynamic proc = oHead.GetProc("recall_prjvs");

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_prjvs(dynamic oHead, int aIsn, int aCascade)
        {
            dynamic proc = oHead.GetProc("del_prjvs");

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("aCascade", 3, 1, 0, aCascade));
            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void set_prj_visa_sign(dynamic oHead, int an_isn_prj_visa, DateTime ad_rep_date, int an_sign_visa_type, string as_rep_text, int an_reserve_fold)
        {
            dynamic proc = oHead.GetProc("set_prj_visa_sign");
            string maskDate = "yyyyMMdd";

            proc.Parameters.Append(proc.CreateParameter("an_isn_prj_visa", 3, 1, 0, an_isn_prj_visa));
            proc.Parameters.Append(proc.CreateParameter("ad_rep_date", 200, 1, 20, check_null(ad_rep_date, maskDate)));
            proc.Parameters.Append(proc.CreateParameter("an_sign_visa_type", 3, 1, 0, an_sign_visa_type));
            proc.Parameters.Append(proc.CreateParameter("ad_rep_date", 200, 1, 2000, check_null(as_rep_text)));
            proc.Parameters.Append(proc.CreateParameter("an_reserve_fold", 3, 1, 0, an_reserve_fold));
            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_prj_rubric(dynamic oHead, int aisnprj, string codes)
        {
            dynamic proc = oHead.GetProc("add_prj_rubric");

            proc.Parameters.Append(proc.CreateParameter("aisnprj", 3, 1, 0, aisnprj));
            proc.Parameters.Append(proc.CreateParameter("codes", 200, 1, 2000, codes));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_reminder(dynamic oHead, int a_isn_reply, string a_reminder_text)
        {
            dynamic proc = oHead.GetProc("add_reminder");
            proc.Parameters.Append(proc.CreateParameter("a_isn_reply", 3, 1, 0, a_isn_reply));
            proc.Parameters.Append(proc.CreateParameter("a_reminder_text", 200, 1, 2000, a_reminder_text));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_prj_rubric(dynamic oHead, int a_isn_prj_ref_rubric)
        {
            dynamic proc = oHead.GetProc("del_prj_rubric");
            proc.Parameters.Append(proc.CreateParameter("a_isn_prj_ref_rubric", 3, 1, 0, a_isn_prj_ref_rubric));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void add_prj_send(dynamic oHead, int aIsn, string aClassif, string aCodes, string aIsnsContact, string aSendPersons, int? aSendingType)
        {
            dynamic proc = oHead.GetProc("add_prj_send");

            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));
            proc.Parameters.Append(proc.CreateParameter("aClassif", 200, 1, 20, aClassif));
            proc.Parameters.Append(proc.CreateParameter("aCodes", 200, 1, 2000, aCodes));
            proc.Parameters.Append(proc.CreateParameter("aIsnsContact", 200, 1, 2000, check_null(aIsnsContact)));
            proc.Parameters.Append(proc.CreateParameter("aSendPersons", 200, 1, 2000, check_null(aSendPersons)));
            proc.Parameters.Append(proc.CreateParameter("aSendingType", 3, 1, 0, (object)check_null(aSendingType)));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void del_prj_send(dynamic oHead, int aIsn)
        {
            dynamic proc = oHead.GetProc("del_prj_send");
            proc.Parameters.Append(proc.CreateParameter("aIsn", 3, 1, 0, aIsn));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        public void edit_ar_prj(dynamic oHead, int aIsnPrj, string aArName, string aValue)
        {
            dynamic proc = oHead.GetProc("edit_ar_prj");

            proc.Parameters.Append(proc.CreateParameter("aIsnPrj", 3, 1, 0, aIsnPrj));
            proc.Parameters.Append(proc.CreateParameter("aArName", 200, 1, 2000, aArName));
            proc.Parameters.Append(proc.CreateParameter("aValue", 200, 1, 48, aValue));

            oHead.ExecuteProc(proc);

            if (oHead.ErrCode < 0)
                throw new Exception(oHead.ErrText);
        }

        static dynamic check_null(dynamic prop, string date_mask)
        {
            dynamic result = Convert.DBNull;

            if (prop != null)
            {
                string typeName = prop.GetType().ToString();
                switch (typeName)
                {
                    case ("System.String"):
                        {
                            string strProp = prop;
                            result = string.IsNullOrEmpty(strProp) ? string.Empty : strProp;
                            break;
                        }
                    case ("System.DateTime"):
                        {
                            DateTime? propDate = prop;
                            if (!string.IsNullOrEmpty(date_mask))
                                result = propDate.HasValue ? propDate.Value.ToString(date_mask) : string.Empty;
                            else result = string.Empty;
                            break;
                        }
                    case ("System.DateTime[]"):
                        {
                            DateTime[] propDate = prop;
                            string outStrDate = "";
                            if (!string.IsNullOrEmpty(date_mask))
                            {
                                for (int i = 0; i < propDate.Length; i++)
                                {
                                    if (i != propDate.Length - 1)
                                    {
                                        if (propDate[i] != DateTime.MinValue)
                                            outStrDate += propDate[i].ToString(date_mask) + "|";
                                        else
                                            outStrDate += Convert.DBNull + "|";
                                    }
                                    else if (propDate[i] != DateTime.MinValue)
                                        outStrDate += propDate[i].ToString(date_mask);
                                }
                                result = outStrDate;
                                break;
                            }
                            result = string.Empty;
                            break;
                        }
                    case ("System.Int32"):
                        {
                            int? propInt = prop;
                            result = propInt.HasValue ? propInt.Value : Convert.DBNull;
                            break;
                        }
                    default:
                        {
                            result = Convert.DBNull;
                            break;
                        }
                }
            }
            return result;
        }

        static dynamic check_null(dynamic prop)
        {
            return check_null(prop, string.Empty);
        }

        static byte[] GetFile(string filePath)
        {
            if (filePath != null)
            {
                using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    var result = new byte[fs.Length];
                    fs.Read(result, 0, result.Length);
                    return result;
                }
            }
            return null;
        }
    }
}
﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class DeloError : DynamicObjectWrapper, IDeloError
    {
        protected DeloError()
            : base()
        {
        }

        protected DeloError(dynamic @object)
            : base((object)@object)
        {
        }

        /// <inheritdoc/>
        public int ErrCode
            => Exec(x => (int)x.ErrCode);

        /// <inheritdoc/>
        public string ErrText
            => Exec(x => (string)x.ErrText);
    }
}

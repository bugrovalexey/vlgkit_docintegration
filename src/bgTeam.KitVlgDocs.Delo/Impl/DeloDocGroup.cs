﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    /// Тип "DocGroup" Элемент справочника "Группы документов.
    /// </summary>
    public class DeloDocGroup : DeloDictionaryEntity
    {
        public DeloDocGroup(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Наименование группы документов.
        /// </summary>
        public string Name => Exec(x => (string)x.Name);

        /// <summary>
        /// Уникальный код группы документов, определяющий ее местоположение в иерархии.
        /// </summary>
        public string Dcode => Exec(x => (string)x.Dcode);
    }
}

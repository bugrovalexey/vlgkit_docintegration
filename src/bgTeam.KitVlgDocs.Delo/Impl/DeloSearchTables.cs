﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    /// <summary>
    /// .
    /// </summary>
    public class DeloSearchTables : DeloCriterion
    {
        public DeloSearchTables(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// .
        /// </summary>
        public string Params
        {
            get => Exec(x => (string)x.Params);
            set => Exec((x, p1) => x.Params = p1, value);
        }

        /// <summary>
        /// .
        /// </summary>
        public void SetParameters(string paramName, object paramValue)
            => Exe((x, p1, p2) => x.SetParameters(p1, p2), paramName, paramValue);

        /// <summary>
        /// .
        /// </summary>
        public object GetParameters(string paramName)
            => Exec((x, p1) => (object)x.GetParameters(p1), paramName);
    }
}

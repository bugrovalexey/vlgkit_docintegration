﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{

    /// <summary>
    ///  .
    /// </summary>
    public class DeloCitizen : DeloDictionaryEntity
    {
        public DeloCitizen(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Фамилия И.О. гражданина.
        /// </summary>
        public string Name => Exec(x => (string)x.Name);
    }
}

﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;

    /// <summary>
    /// .
    /// </summary>
    public class DeloAddr : DeloEntity
    {
        public DeloAddr(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Вид РК или РКПД, к которой относится запись.
        /// </summary>
        public string RcKind
        {
            get => Exec(x => (string)x.RcKind);
        }

        /// <summary>
        /// Ссылка на объект <see cref="DeloDoc"/>.
        /// </summary>
        public DeloDoc Rc
            => WrapRc(RcKind, Exec(x => x.Rc));

        /// <summary>
        /// Вид адресата документа.
        /// </summary>
        public string KindAddr
            => Exec(x => (string)x.KindAddr);

        /// <summary>
        /// .
        /// </summary>
        public string Note
            => Exec(x => (string)x.Note);

        /// <summary>
        /// .
        /// </summary>
        public int Answer
            => Exec(x => (int)x.Answer);

        /// <summary>
        /// .
        /// </summary>
        public DeloDelivery Delivery
            => Wrap<DeloDelivery>(x => x.Delivery);

        /// <summary>
        /// .
        /// </summary>
        public DateTime? SendDate
            => Exec(x => x.SendDate as DateTime?);

        /// <summary>
        /// .
        /// </summary>
        public int Contact
            => Exec(x => (int)x.Contact);

        /// <summary>
        /// Кому адресован документ.
        /// </summary>
        public string Person
            => Exec(x => x.Person as string);

        /// <summary>
        /// Адресат документа.
        /// </summary>
        public DeloEntity Addressee
            => WrapAddressee(KindAddr, Exec(x => x.Addressee));

        private DeloDoc WrapRc(string type, dynamic @object)
        {
            if (string.IsNullOrEmpty(type))
            {
                throw new ArgumentNullException(nameof(type));
            }

            switch (type.ToLowerInvariant())
            {
                case "rcin":
                    return new DeloRcIn(@object);
                case "rcout":
                    return new DeloRcOut(@object);
                default:
                    throw new NotImplementedException();
            }
        }

        private DeloEntity WrapAddressee(string type, dynamic @object)
        {
            if (string.IsNullOrEmpty(type))
            {
                throw new ArgumentNullException(nameof(type));
            }

            switch (type.ToLowerInvariant())
            {
                case "citizen":
                    return new DeloCitizen(@object);
                case "organiz":
                    return new DeloOrganiz(@object);
                case "department":
                    return new DeloDepartment(@object);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}

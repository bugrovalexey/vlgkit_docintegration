﻿namespace bgTeam.KitVlgDocs.Delo.Impl
{
    using System;

    /// <summary>
    /// .
    /// </summary>
    public class DeloResultSet : DeloError
    {
        public DeloResultSet(dynamic @object)
            : base((object)@object)
        {
        }

        /// <summary>
        /// Ссылка на объект, задающий критерий отбора из справочников или др. объектов САДД.
        /// </summary>
        public DeloCriterion Source
        {
            get => Wrap<DeloCriterion>(x => x.Source);
            set => Exec((x, p1) => x.Source = p1.Object, value);
        }

        /// <summary>
        /// Осуществляет выбор элементов в соответствии со свойством Source; Возвращает число: неотрицательное - количество отобранных элементов. В случае ошибки  возвращает отрицательное число – код ошибки.
        /// </summary>
        /// <returns></returns>
        public int Fill() => Exec(x => (int)x.Fill());

        public DeloEntity GetItem(int index) => WrapItem(GetObjectType(index), Exec((x, p1) => x.Item[p1], index));

        public string GetObjectType(int index) => Exec((x, p1) => (string)x.ObjectType[p1], index);

        private DeloEntity WrapItem(string type, dynamic @obj)
        {
            if (type is null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            switch (type.ToLowerInvariant())
            {
                case "organiz":
                    return new DeloOrganiz(@obj);
                case "department":
                    return new DeloDepartment(@obj);
                case "security":
                    return new DeloSecurity(@obj);
                case "docgroup":
                    return new DeloDocGroup(@obj);
                case "delivery":
                    return new DeloDelivery(@obj);
                case "cabinet":
                    return new DeloCabinet(@obj);
                case "cardindex":
                    return new DeloCardIndex(@obj);
                case "rubric":
                    return new DeloRubric(@obj);
                case "rcin":
                    return new DeloRcIn(@obj);
                case "rcout":
                    return new DeloRcOut(@obj);
                case "user":
                    return new DeloUser(@obj);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}

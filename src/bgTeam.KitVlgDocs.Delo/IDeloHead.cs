﻿namespace bgTeam.KitVlgDocs.Delo
{
    using bgTeam.KitVlgDocs.Delo.Impl;
    using System;

    /// <summary>
    /// 
    /// </summary>
    public interface IDeloHead : IDeloError
    {
        /// <summary>
        /// Состояние связи с БД.
        /// </summary>
        bool Active { get; }

        /// <summary>
        /// Объект с информацией о пользователе, который логирован в системе.
        /// </summary>
        DeloUser UserInfo { get; }

        /// <summary>
        /// Дата и время сервера БД на момент обращения к свойству. 
        /// </summary>
        DateTime ServerToday { get; }

        /// <summary>
        /// Осуществляет связь с БД, позволяет пользователю логироваться в системе.
        /// </summary>
        /// <returns>
        /// True, в случае успешного выполнения операции, False – в противном случае.
        /// </returns>
        bool Open();

        /// <summary>
        /// Является альтернативой методу <see cref="Open"/> и позволяет подключаться к БД без диалога с пользователем.
        /// </summary>
        /// <param name="username">Имя пользователя.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>
        /// True, в случае успешного выполнения операции, False – в противном случае.
        /// </returns>
        bool OpenWithParams(string username, string password);

        /// <summary>
        /// Расширенная версия <see cref="OpenWithParams"/>, позволяющая подключаться к серверу/базе, отличных от указанных в Office.Ini.
        /// </summary>
        /// <param name="host">Имя сервера.</param>
        /// <param name="owner">Имя владельца.</param>
        /// <param name="username">Имя пользователя.</param>
        /// <param name="password">Пароль.</param>
        /// <returns>
        /// True, в случае успешного выполнения операции, False – в противном случае.
        /// </returns>
        bool OpenWithParamsEx(string host, string owner, string username, string password);

        /// <summary>
        /// Загрузка настроек подключения из файла.
        /// </summary>
        /// <param name="filePath">Путь к файлу настроек.</param>
        void LoadSettings(string filePath);

        /// <summary>
        /// Возвращает объект <see cref="ADODBCommand"/> который приложение должно использовать для заполнения параметров вызова хранимых процедур.
        /// </summary>
        /// <param name="name">Имя хранимой процедуры.</param> 
        /// Объект для заполнения параметров вызова хранимых процедур.
        /// <returns>
        /// </returns>
        ADODBCommand GetProc(string name);

        /// <summary>
        /// Выполнение хранимой процедуры
        /// В качестве параметра должен использоваться объект <see cref="ADODBCommand"/> полученный с помощью метода <see cref="GetProc(string)"/>.
        /// </summary>
        /// <param name="command">Команда.</param>
        void ExecuteProc(ADODBCommand command);

        /// <summary>
        /// Создает новый объект типа <see cref="DeloSearchVocab"/> или типа <see cref="DeloSearchTables"/> в зависимости от значения <paramref name="type"/>, и возвращает ссылку на него.
        /// </summary>
        /// <param name="type">Тип поиска (Vocabulary, Table).</param>
        /// <returns></returns>
        DeloCriterion GetCriterion(string type);

        /// <summary>
        /// Создает новый объект типа <see cref="DeloResultSet"/> и возвращает ссылку на него.
        /// </summary>
        /// <returns></returns>
        DeloResultSet GetResultSet();

        void Close();

        DeloEntity GetRow(string type, int isn);

        DeloEntity GetRow(string type, string dcode);
    }
}

﻿namespace bgTeam.KitVlgDocs.Delo
{
    /// <summary>
    /// Тип "User" Элемент справочника "Пользователи".
    /// </summary>
    public interface IDeloUser : IDeloDictionaryEntity
    {

        /// <summary>
        /// Имя пользователя (сортировка по фамилиям).
        /// </summary>
        string Name { get; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Delo
{
    using System;

    /// <summary>
    /// Ошибка системы Дело.
    /// </summary>
    public class DeloException : Exception
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DeloException"/> class.
        /// </summary>
        /// <param name="errCode"></param>
        /// <param name="errText"></param>
        public DeloException(int errCode, string errText)
            : base($"({errCode}) {errText}")
        {
            ErrCode = errCode;
            ErrText = errText;
        }

        /// <summary>
        /// Код ошибки.
        /// </summary>
        public int ErrCode { get; }

        /// <summary>
        /// Описание ошибки.
        /// </summary>
        public string ErrText { get; }
    }
}

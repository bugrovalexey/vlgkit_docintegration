﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography.Pkcs;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SignTest
{
    public partial class FormMain : Form
    {
        private static readonly Regex _signerRegex = new Regex(@"([A-Za-zА-Яа-я]+)\s*([A-Za-zА-Яа-я]?)\.\s*([A-Za-zА-Яа-я]?)\.\s?-?\s?([A-Za-zА-Яа-я\s]*)?", RegexOptions.Compiled);

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            RefreshState();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            if (SelectFile("Выбор подписанного файла", "Все файлы (*.*)|*.*", out var fileName))
            {
                Log($"Выбран подписанный файл '{fileName}'");
                txtFile.Text = fileName;
                RefreshState();
            }
        }

        private void btnSelectSignature_Click(object sender, EventArgs e)
        {
            if (SelectFile("Выбор файла подписи", "Файлы подписи PKCS-7 (*.p7s)|*.p7s", out var fileName))
            {
                Log($"Выбран файл подписи '{fileName}'");
                txtSignature.Text = fileName;
                RefreshState();
            }
        }

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            txtLog.Clear();
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            Log("Проверка подписи...");
            btnCheck.Enabled = false;
            Log($"Подпись файла: {(CheckSignatureEx() ? "действительна" : "не действительна")}");
            btnCheck.Enabled = true;
        }

        private bool CheckSignature()
        {
            try
            {
                var file = File.ReadAllBytes(txtFile.Text);
                var signature = File.ReadAllBytes(txtSignature.Text);

                var signedCms = new SignedCms(new ContentInfo(file), true);

                try
                {
                    signedCms.Decode(signature);
                }
                catch
                {
                    var text = Encoding.ASCII.GetString(signature).Replace("\r", string.Empty).Replace("\n", string.Empty);
                    var data = Convert.FromBase64String(text);
                    signedCms.Decode(data);
                }

                signedCms.CheckSignature(true);

                return true;
            }
            catch (Exception exp)
            {
                Log($"Ошибка при проверке подписи файла: {exp.GetType().Name}");
                Log($"Сообщение: {exp.Message.Trim()}"); 
                Log($"Трассировка стека:");
                Log(exp.StackTrace);
                return false;
            }
        }

        private bool CheckSignatureEx()
        {
            try
            {
                var file = File.ReadAllBytes(txtFile.Text);
                var signers = new[] { "Лихачев В.В. - Глава Волгограда" };
                var signature = File.ReadAllBytes(txtSignature.Text);

                var signedCms = new SignedCms(new ContentInfo(file), true);

                try
                {
                    signedCms.Decode(signature);
                }
                catch
                {
                    var text = Encoding.ASCII.GetString(signature).Replace("\r", string.Empty).Replace("\n", string.Empty);
                    var data = Convert.FromBase64String(text);
                    signedCms.Decode(data);
                }

                var certNow = DateTime.Now.AddSeconds(5);

                var signersInfos = signedCms.SignerInfos.OfType<SignerInfo>().ToArray();

                if (!VerifySignature(signedCms, false) && VerifySignature(signedCms, true))
                {
                    Log($"Неудалось проверить подпись документа стандартным методом.");

                    using (var chain = X509Chain.Create())
                    {
                        chain.ChainPolicy = new X509ChainPolicy
                        {
                            RevocationFlag = X509RevocationFlag.EndCertificateOnly,
                            RevocationMode = X509RevocationMode.NoCheck,
                            VerificationFlags =
                                X509VerificationFlags.IgnoreRootRevocationUnknown |
                                X509VerificationFlags.IgnoreCertificateAuthorityRevocationUnknown |
                                X509VerificationFlags.IgnoreCtlSignerRevocationUnknown |
                                X509VerificationFlags.IgnoreEndRevocationUnknown,
                        };

                        foreach (var signerInfo in signersInfos)
                        {
                            if (!chain.Build(signerInfo.Certificate))
                            {
                                Log($"Неудалось построить цепочку сертификатов для проверики подписи документа.");
                                return false;
                            }

                            if (!chain.ChainStatus.All(x => x.Status == X509ChainStatusFlags.NoError))
                            {
                                var info = string.Join(", ", chain.ChainStatus.Select(x => Tuple.Create(x.Status, x.StatusInformation)));

                                Log($"Проверка цепочки сертификатов вернула ошибку {info}.");
                                return false;
                            }

                            Log($"Цепочка сертификатов для {signerInfo.Certificate.Thumbprint} верна.");

                            chain.Reset();
                        }
                    }
                }

                if (!VerifySigners(signersInfos, signers))
                {
                    return false;
                }

                return true;
            }
            catch (Exception exp)
            {
                Log($"Ошибка при проверке подписи файла: {exp.GetType().Name}");
                Log($"Сообщение: {exp.Message.Trim()}");
                Log($"Трассировка стека:");
                Log(exp.StackTrace);
                return false;
            }

            bool VerifySigners(SignerInfo[] signersInfos, string[] signers)
            {
                if (signers == null || signers.Length > signersInfos.Length)
                {
                    Log($"Неудалось проверить соответствие сертификата и лица подписавшего документ.");
                    return false;
                }

                foreach (var item in signers)
                {
                    if (_signerRegex.Match(item) is Match match && match.Success)
                    {
                        var signer_regex01 = $@"CN={match.Groups[1].Value}\s*{match.Groups[2].Value}[а-яА-Я]*\s*{match.Groups[3].Value}[а-яА-Я]*";
                        var signer_regex02 = match.Groups[4].Success ? $@"T={match.Groups[4].Value}" : string.Empty;

                        if (!signersInfos.Any(
                            x => Regex.IsMatch(x.Certificate.Subject, signer_regex01) &&
                                (string.IsNullOrEmpty(signer_regex02) || Regex.IsMatch(x.Certificate.Subject, signer_regex02))))
                        {
                            Log($"Неудалось найти сертификат для {item}.");
                            return false;
                        }
                    }
                    else if (!signersInfos.Any(x => x.Certificate.Subject.Contains(item)))
                    {
                        Log($"Неудалось найти сертификат для {item}.");
                        return false;
                    }

                    Log($"Сертификат для {item} найден.");
                }

                return true;
            }

            bool VerifySignature(SignedCms cms, bool verifySignatureOnly)
            {
                try
                {
                    cms.CheckSignature(verifySignatureOnly);
                    return true;
                }
                catch (Exception exp)
                {
                    Log($"Ошибка при проверке подписи файла: {exp.GetType().Name}");
                    Log($"Сообщение: {exp.Message.Trim()}");
                    Log($"Трассировка стека:");
                    Log(exp.StackTrace);
                    return false;
                }
            }
        }

        private void RefreshState()
        {
            btnCheck.Enabled = 
                !string.IsNullOrWhiteSpace(txtFile.Text) && txtFile.Text?.Length > 0 &&
                !string.IsNullOrWhiteSpace(txtSignature.Text) && txtSignature.Text?.Length > 0 &&
                File.Exists(txtFile.Text) && File.Exists(txtSignature.Text);
        }

        private void Log(string message)
        {
            txtLog.AppendText(message);
            txtLog.AppendText(Environment.NewLine);
        }

        private bool SelectFile(string title, string filter, out string fileName)
        {
            dlgOpenFile.Title = title;
            dlgOpenFile.Filter = filter;
            dlgOpenFile.FileName = string.Empty;

            if (dlgOpenFile.ShowDialog() == DialogResult.OK)
            {
                fileName = dlgOpenFile.FileName;
                return true;
            }

            fileName = null;
            return false;
        }
    }
}

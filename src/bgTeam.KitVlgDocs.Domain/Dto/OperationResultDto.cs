﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    public class OperationResultDto
    {
        public bool Success { get; set; }

        public string Result { get; set; }

        public static implicit operator OperationResultDto((bool, string) value)
        {
            return new OperationResultDto
            {
                Success = value.Item1,
                Result = value.Item2,
            };
        }

        public void Deconstruct(out bool success, out string result)
        {
            success = Success;
            result = Result;
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    public class DataOutgoingDocumentDto
    {
        public long Id { get; set; }

        public byte[] Document { get; set; }

        public OutgoingDocumentSignerDto[] Signers { get; set; }
    }
}

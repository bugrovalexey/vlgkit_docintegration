﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    using System;

    public class DeloOutgoingDocumentContactDto
    {
        public string Name { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }

        public string Position { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    using bgTeam.KitVlgDocs.Domain.Xml;

    public class IncomingDocumentDto
    {
        public FileDto[] Files { get; set; }

        public Container Passport { get; set; }

        public FileDto Document { get; set; }

        public FileDto PassportXml { get; set; }

        public FileDto DocumentXml { get; set; }

        public string Kind { get; set; }

        public string HeaderGuid { get; set; }

        public string SenderGuid { get; set; }

        public string SenderName { get; set; }

        public string[] MedoAddressees { get; set; }

        public string SignatureStatus { get; set; }

        public FileDto[] SignatureP7s { get; set; }
    }
}

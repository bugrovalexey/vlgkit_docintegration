﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    public class DeloOutgoingDocumentFileDto
    {
        public FileDto File { get; set; }

        public byte[][] Signatures { get; set; }

        public void Deconstruct(out FileDto file, out byte[][] signatures)
        {
            file = File;
            signatures = Signatures;
        }
    }
}

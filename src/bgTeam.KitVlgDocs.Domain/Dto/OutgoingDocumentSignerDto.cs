﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    public class OutgoingDocumentSignerDto
    {
        public long ContactId { get; set; }

        public string Id { get; set; }

        public string Store { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    using bgTeam.KitVlgDocs.Domain.Xml;

    public class FullOutgoingDocumentDto
    {
        public long Id { get; set; }

        public OutgoingDocumentSignerDto[] Signers { get; set; }

        public string[] Receivers { get; set; }

        public StampsDataDto Stamps { get; set; }

        public long IdentityEx { get; set; }

        public string AuthorGuid { get; set; }

        public Container Container { get; set; }

        public FileDto[] Files { get; set; }
    }
}

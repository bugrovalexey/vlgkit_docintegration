﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    public class OutgoinDocumentDto
    {
        public long Id { get; set; }

        public byte[] Signature { get; set; }
    }
}

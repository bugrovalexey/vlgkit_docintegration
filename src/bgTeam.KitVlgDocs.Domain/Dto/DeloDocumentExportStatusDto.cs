﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    public class DeloDocumentExportStatusDto
    {
        public long Id { get; set; }

        public int? Ins { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    using System;

    public class DeloOutgoingDocumentDto
    {
        public long Id { get; set; }

        public string Annotation { get; set; }

        public string[] Subjects { get; set; }

        public DateTime RegistrationDate { get; set; }

        public string RegistrationNumber { get; set; }

        public string IncludeList { get; set; }

        public DateTime? ControlDate { get; set; }

        public DeloOutgoingDocumentFileDto[] Files { get; set; }

        public DeloOutgoingDocumentContactDto Signer { get; set; }

        public DeloOutgoingDocumentContactDto Addresse { get; set; }
    }
}

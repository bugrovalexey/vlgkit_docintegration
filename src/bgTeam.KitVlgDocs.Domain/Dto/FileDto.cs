﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    public class FileDto
    {
        public string FileName { get; set; }

        public byte[] Data { get; set; }

        public string Text { get; set; }
    }
}

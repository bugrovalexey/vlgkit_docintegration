﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    using System.Collections.Generic;
    using bgTeam.KitVlgDocs.Domain.Xml;

    public class MedoContainerDto
    {
        public Container Passport { get; set; }

        public FileDto PassportXml { get; set; }

        public FileDto Document { get; set; }

        public IEnumerable<FileDto> Signatures { get; set; }

        public IEnumerable<FileDto> Files { get; set; }
    }
}

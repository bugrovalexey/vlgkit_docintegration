﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    using System;

    public class DeloIncomingDocumentDto
    {
        public int Isn { get; set; }

        public DeloFileDto[] Files { get; set; }

        public DateTime RegistrationDate { get; set; }

        public string RegistrationNumber { get; set; }

        public string IncludeList { get; set; }

        public string Annotation { get; set; }

        public string[] Signers { get; set; }

        public string[] Subjects { get; set; }

        public DeloAddresseeDto[] Addressees { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Domain.Xml.Iedms;

    public class MedoDocumentDto
    {
        public string Directory { get; set; }

        public string[] Addressies { get; set; }

        public MedoContainerDto Container { get; set; }

        public Communication Communication { get; set; }

        public IDocumentStorage Storage { get; set; }
    }
}

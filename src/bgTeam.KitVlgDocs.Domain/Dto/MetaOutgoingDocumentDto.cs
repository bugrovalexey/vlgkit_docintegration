﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    public class MetaOutgoingDocumentDto
    {
        public long Id { get; set; }

        public OutgoingDocumentSignerDto[] Signers { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    public class StampsDataDto
    {
        public int RegistrationX { get; set; }

        public int RegistrationY { get; set; }

        public int SignatureX { get; set; }

        public int SignatureY { get; set; }

        public int SignaturePage { get; set; }
    }
}

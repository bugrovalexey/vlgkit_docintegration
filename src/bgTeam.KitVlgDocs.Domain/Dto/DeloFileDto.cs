﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    using System;

    public class DeloFileDto
    {
        public FileDto Content { get; set; }

        public FileDto[] Signatures { get; set; }
    }
}

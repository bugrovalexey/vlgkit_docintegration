﻿namespace bgTeam.KitVlgDocs.Domain.Dto
{
    using System;

    public class DeloAddresseeDto
    {
        public string Name { get; set; }

        public string FullName { get; set; }

        public string Phone { get; set; }

        public string Surname { get; set; }

        public string Department { get; set; }
    }
}

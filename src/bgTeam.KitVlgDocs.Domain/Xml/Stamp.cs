﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("stamp", Namespace = "http://minsvyaz.ru/container")]
    public class Stamp
    {
        [XmlElement("position", Namespace = "http://minsvyaz.ru/container")]
        public Position Position { get; set; }

        [XmlAttribute("localName", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString", Form = XmlSchemaForm.Qualified)]
        public string LocalName { get; set; }
    }
}

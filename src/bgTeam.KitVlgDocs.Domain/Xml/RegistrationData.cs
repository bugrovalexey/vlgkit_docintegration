﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System;
    using System.Xml.Serialization;

    [XmlType("registrationData", Namespace = "http://minsvyaz.ru/container")]
    [XmlInclude(typeof(Registration))]
    public class RegistrationData
    {
        [XmlElement("number", Namespace = "http://minsvyaz.ru/container", DataType = "token")]
        public string Number { get; set; }

        [XmlElement("date", Namespace = "http://minsvyaz.ru/container", DataType = "date")]
        public DateTime Date { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("qualifiedValue", Namespace = "http://minsvyaz.ru/container")]
    public class QualifiedValue
    {
        [XmlText(DataType = "normalizedString")]
        public string Value { get; set; }

        [XmlAttribute("id", Namespace = "http://minsvyaz.ru/container", DataType = "token", Form = XmlSchemaForm.Qualified)]
        public string Id { get; set; }
    }
}

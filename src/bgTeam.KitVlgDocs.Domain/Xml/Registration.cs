﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Serialization;

    [XmlType("registration", Namespace = "http://minsvyaz.ru/container")]
    public class Registration : RegistrationData
    {
        [XmlElement("registrationStamp", Namespace = "http://minsvyaz.ru/container")]
        public Stamp RegistrationStamp { get; set; }
    }
}

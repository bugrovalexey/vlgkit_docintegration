﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Collections.Generic;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("linkedDocument", Namespace = "http://minsvyaz.ru/container")]
    public class LinkedDocument
    {
        public LinkedDocument()
        {
            Signer = new List<Employee>();
        }

        [XmlElement("organization", Namespace = "http://minsvyaz.ru/container")]
        public Organization Organization { get; set; }

        [XmlElement("department", Namespace = "http://minsvyaz.ru/container")]
        public QualifiedValue Department { get; set; }

        [XmlElement("registration", Namespace = "http://minsvyaz.ru/container")]
        public RegistrationData Registration { get; set; }

        [XmlElement("signer", Namespace = "http://minsvyaz.ru/container")]
        public List<Employee> Signer { get; }

        [XmlIgnore]
        public bool SignerSpecified => Signer.Count != 0;

        [XmlAttribute("uid", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString", Form = XmlSchemaForm.Qualified)]
        public string Uid { get; set; }
    }
}

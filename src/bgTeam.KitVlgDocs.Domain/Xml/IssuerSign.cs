﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Serialization;

    [XmlType("IssuerSign", Namespace = "http://minsvyaz.ru/container", AnonymousType = true)]
    public class IssuerSign
    {
        [XmlElement("person", Namespace = "http://minsvyaz.ru/container")]
        public Signer Person { get; set; }

        [XmlElement("documentSignature", Namespace = "http://minsvyaz.ru/container")]
        public SignatureInfo DocumentSignature { get; set; }
    }
}

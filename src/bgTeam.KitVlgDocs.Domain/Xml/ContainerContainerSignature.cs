﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("ContainerContainerSignature", Namespace = "http://minsvyaz.ru/container", AnonymousType = true)]
    public class ContainerContainerSignature
    {
        [XmlAttribute("localName", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString", Form = XmlSchemaForm.Qualified)]
        public string LocalName { get; set; }
    }
}

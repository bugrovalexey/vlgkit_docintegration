﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Collections.Generic;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("ContainerAttachmentsAttachment", Namespace = "http://minsvyaz.ru/container", AnonymousType = true)]
    public class ContainerAttachmentsAttachment
    {
        public ContainerAttachmentsAttachment()
        {
            Signature = new List<ContainerAttachmentsAttachmentSignature>();
        }

        [XmlElement("order", Namespace = "http://minsvyaz.ru/container", DataType = "nonNegativeInteger")]
        public string Order { get; set; }

        [XmlElement("description", Namespace = "http://minsvyaz.ru/container", DataType = "string")]
        public string Description { get; set; }

        [XmlElement("signature", Namespace = "http://minsvyaz.ru/container")]
        public List<ContainerAttachmentsAttachmentSignature> Signature { get; }

        [XmlIgnore]
        public bool SignatureSpecified => Signature.Count != 0;

        [XmlAttribute("localName", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString", Form = XmlSchemaForm.Qualified)]
        public string LocalName { get; set; }
    }
}

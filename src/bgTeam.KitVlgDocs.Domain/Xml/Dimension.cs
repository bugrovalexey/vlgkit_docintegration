﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Serialization;

    [XmlType("dimension", Namespace = "http://minsvyaz.ru/container")]
    public class Dimension
    {
        [XmlElement("w", Namespace = "http://minsvyaz.ru/container", DataType = "positiveInteger")]
        public string W { get; set; }

        [XmlElement("h", Namespace = "http://minsvyaz.ru/container", DataType = "positiveInteger")]
        public string H { get; set; }
    }
}

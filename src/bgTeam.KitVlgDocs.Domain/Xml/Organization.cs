﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("organization", Namespace = "http://minsvyaz.ru/container")]
    public class Organization
    {
        [XmlElement("title", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString")]
        public string Title { get; set; }

        [XmlElement("address", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString")]
        public string Address { get; set; }

        [XmlElement("phone", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString")]
        public string Phone { get; set; }

        [XmlElement("email", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString")]
        public string Email { get; set; }

        [XmlElement("website", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString")]
        public string Website { get; set; }

        [XmlAttribute("id", Namespace = "http://minsvyaz.ru/container", DataType = "token", Form = XmlSchemaForm.Qualified)]
        public string Id { get; set; }
    }
}

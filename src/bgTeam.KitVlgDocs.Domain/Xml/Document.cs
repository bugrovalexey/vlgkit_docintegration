﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("document", Namespace = "http://minsvyaz.ru/container")]
    public class Document
    {
        [XmlElement("pagesQuantity", Namespace = "http://minsvyaz.ru/container", DataType = "positiveInteger")]
        public string PagesQuantity { get; set; }

        [XmlElement("enclosurePagesQuantity", Namespace = "http://minsvyaz.ru/container", DataType = "nonNegativeInteger")]
        public string EnclosurePagesQuantity { get; set; }

        [XmlElement("description", Namespace = "http://minsvyaz.ru/container", DataType = "string")]
        public string Description { get; set; }

        [XmlAttribute("localName", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString", Form = XmlSchemaForm.Qualified)]
        public string LocalName { get; set; }
    }
}

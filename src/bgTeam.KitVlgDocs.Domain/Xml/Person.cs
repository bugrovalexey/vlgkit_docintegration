﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("person", Namespace = "http://minsvyaz.ru/container")]
    [XmlInclude(typeof(Employee))]
    [XmlInclude(typeof(Executor))]
    [XmlInclude(typeof(Signer))]
    public class Person
    {
        [XmlElement("post", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString")]
        public string Post { get; set; }

        [XmlElement("name", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString")]
        public string Name { get; set; }

        [XmlElement("phone", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString")]
        public string Phone { get; set; }

        [XmlElement("email", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString")]
        public string Email { get; set; }

        [XmlAttribute("id", Namespace = "http://minsvyaz.ru/container", DataType = "token", Form = XmlSchemaForm.Qualified)]
        public string Id { get; set; }
    }
}

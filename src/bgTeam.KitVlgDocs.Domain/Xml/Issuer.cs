﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlType("issuer", Namespace = "http://minsvyaz.ru/container")]
    public class Issuer
    {
        public Issuer()
        {
            Sign = new List<IssuerSign>();
        }

        [XmlElement("organization", Namespace = "http://minsvyaz.ru/container")]
        public Organization Organization { get; set; }

        [XmlElement("department", Namespace = "http://minsvyaz.ru/container")]
        public QualifiedValue Department { get; set; }

        [XmlElement("registration", Namespace = "http://minsvyaz.ru/container")]
        public Registration Registration { get; set; }

        [XmlElement("sign", Namespace = "http://minsvyaz.ru/container")]
        public List<IssuerSign> Sign { get; }

        [XmlElement("executor", Namespace = "http://minsvyaz.ru/container")]
        public Executor Executor { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Serialization;

    [XmlType("position", Namespace = "http://minsvyaz.ru/container")]
    public class Position
    {
        [XmlElement("page", Namespace = "http://minsvyaz.ru/container", DataType = "positiveInteger")]
        public string Page { get; set; }

        [XmlElement("topLeft", Namespace = "http://minsvyaz.ru/container")]
        public Coordinate TopLeft { get; set; }

        [XmlElement("dimension", Namespace = "http://minsvyaz.ru/container")]
        public Dimension Dimension { get; set; }
    }
}

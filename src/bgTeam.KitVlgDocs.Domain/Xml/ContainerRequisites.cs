﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlType("ContainerRequisites", Namespace = "http://minsvyaz.ru/container", AnonymousType = true)]
    public class ContainerRequisites
    {
        public ContainerRequisites()
        {
            Links = new List<LinkedDocument>();
        }

        [XmlElement("documentKind", Namespace = "http://minsvyaz.ru/container")]
        public QualifiedValue DocumentKind { get; set; }

        [XmlElement("documentPlace", Namespace = "http://minsvyaz.ru/container")]
        public QualifiedValue DocumentPlace { get; set; }

        [XmlElement("classification", Namespace = "http://minsvyaz.ru/container")]
        public QualifiedValue Classification { get; set; }

        [XmlElement("annotation", Namespace = "http://minsvyaz.ru/container", DataType = "string")]
        public string Annotation { get; set; }

        [XmlArray("links", Namespace = "http://minsvyaz.ru/container")]
        [XmlArrayItem("link", Namespace = "http://minsvyaz.ru/container")]
        public List<LinkedDocument> Links { get; }

        [XmlIgnore]
        public bool LinksSpecified => Links.Count != 0;
    }
}

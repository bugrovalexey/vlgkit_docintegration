﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Serialization;

    [XmlType("coordinate", Namespace = "http://minsvyaz.ru/container")]
    public class Coordinate
    {
        [XmlElement("x", Namespace = "http://minsvyaz.ru/container", DataType = "short")]
        public short X { get; set; }

        [XmlElement("y", Namespace = "http://minsvyaz.ru/container", DataType = "short")]
        public short Y { get; set; }
    }
}

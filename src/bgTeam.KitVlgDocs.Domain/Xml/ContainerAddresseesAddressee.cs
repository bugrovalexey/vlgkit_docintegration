﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlType("ContainerAddresseesAddressee", Namespace = "http://minsvyaz.ru/container", AnonymousType = true)]
    public class ContainerAddresseesAddressee
    {
        public ContainerAddresseesAddressee()
        {
            Person = new List<Person>();
        }

        [XmlElement("organization", Namespace = "http://minsvyaz.ru/container")]
        public Organization Organization { get; set; }

        [XmlElement("department", Namespace = "http://minsvyaz.ru/container")]
        public QualifiedValue Department { get; set; }

        [XmlElement("person", Namespace = "http://minsvyaz.ru/container")]
        public List<Person> Person { get; }

        [XmlIgnore]
        public bool PersonSpecified => Person.Count != 0;
    }
}

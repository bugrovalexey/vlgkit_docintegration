﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.ComponentModel;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("signatureInfo", Namespace = "http://minsvyaz.ru/container")]
    public class SignatureInfo
    {
        [XmlElement("signatureStamp", Namespace = "http://minsvyaz.ru/container")]
        public Stamp SignatureStamp { get; set; }

        [XmlAttribute("localName", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString", Form = XmlSchemaForm.Qualified)]
        public string LocalName { get; set; }

        [DefaultValue(SignatureType.Утверждающая)]
        [XmlAttribute("type", Namespace = "http://minsvyaz.ru/container", Form = XmlSchemaForm.Qualified)]
        public SignatureType Type { get; set; } = SignatureType.Утверждающая;
    }
}

﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Collections.Generic;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("container", Namespace = "http://minsvyaz.ru/container", AnonymousType = true)]
    [XmlRoot("container", Namespace = "http://minsvyaz.ru/container")]
    public class Container
    {
        public Container()
        {
            Authors = new List<Issuer>();
            Addressees = new List<ContainerAddresseesAddressee>();
            Attachments = new List<ContainerAttachmentsAttachment>();
        }

        [XmlElement("requisites", Namespace = "http://minsvyaz.ru/container")]
        public ContainerRequisites Requisites { get; set; }

        [XmlArray("authors", Namespace = "http://minsvyaz.ru/container")]
        [XmlArrayItem("author", Namespace = "http://minsvyaz.ru/container")]
        public List<Issuer> Authors { get; }

        [XmlArray("addressees", Namespace = "http://minsvyaz.ru/container")]
        [XmlArrayItem("addressee", Namespace = "http://minsvyaz.ru/container")]
        public List<ContainerAddresseesAddressee> Addressees { get; }

        [XmlElement("document", Namespace = "http://minsvyaz.ru/container")]
        public Document Document { get; set; }

        [XmlArray("attachments", Namespace = "http://minsvyaz.ru/container")]
        [XmlArrayItem("attachment", Namespace = "http://minsvyaz.ru/container")]
        public List<ContainerAttachmentsAttachment> Attachments { get; }

        [XmlIgnore]
        public bool AttachmentsSpecified => Attachments.Count != 0;

        [XmlElement("containerSignature", Namespace = "http://minsvyaz.ru/container")]
        public ContainerContainerSignature ContainerSignature { get; set; }

        [XmlAttribute("uid", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString", Form = XmlSchemaForm.Qualified)]
        public string Uid { get; set; }

        [XmlAttribute("version", Namespace = "http://minsvyaz.ru/container", DataType = "token", Form = XmlSchemaForm.Qualified)]
        public string Version { get; set; }
    }
}

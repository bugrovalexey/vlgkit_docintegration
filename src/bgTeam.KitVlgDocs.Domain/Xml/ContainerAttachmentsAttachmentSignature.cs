﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [XmlType("ContainerAttachmentsAttachmentSignature", Namespace = "http://minsvyaz.ru/container", AnonymousType = true)]
    public class ContainerAttachmentsAttachmentSignature
    {
        [XmlAttribute("localName", Namespace = "http://minsvyaz.ru/container", DataType = "normalizedString", Form = XmlSchemaForm.Qualified)]
        public string LocalName { get; set; }
    }
}

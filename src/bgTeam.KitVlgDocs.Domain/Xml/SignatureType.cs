﻿namespace bgTeam.KitVlgDocs.Domain.Xml
{
    using System.Xml.Serialization;

    [XmlType("type", Namespace = "http://minsvyaz.ru/container")]
    public enum SignatureType
    {
        Визирующая,
        Утверждающая,
    }
}

﻿namespace bgTeam.KItVlgDocs.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.DataAccess;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using bgTeam.KitVlgDocs.Story;
    using Microsoft.AspNetCore.Mvc;

    [Route("/api/[controller]/[action]")]
    [ApiController]
    public class DeloController : Controller
    {
        private readonly IStoryBuilder _storyBuilder;
        private readonly IMedoDocumentLoader _documentLoader;

        public DeloController(
            IStoryBuilder storyBuilder,
            IMedoDocumentLoader documentLoader)
        {
            _storyBuilder = storyBuilder;
            _documentLoader = documentLoader;
        }

        [HttpPost]
        public Task<OperationResultDto> ImportDocument([FromBody]DeloIncomingDocumentDto @params)
        {
            return _storyBuilder.Build(@params)
                .ReturnAsync<OperationResultDto>();
        }

        [HttpPost]
        public Task<OperationResultDto> ExportDocument([FromBody]OutgoinDocumentDto @params)
        {
            return _storyBuilder.Build(new ServerExportDeloDocumentsStoryContext
            {
                Document = @params,
            }).ReturnAsync<OperationResultDto>();
        }

        [HttpGet]
        public Task<IEnumerable<DeloOutgoingDocumentDto>> GetOutgoingDocuments()
        {
            return _documentLoader.LoadDeloDocuments(null);
        }

        [HttpGet]
        public Task<IEnumerable<MetaOutgoingDocumentDto>> GetOutgoingDocumentsMeta()
        {
            return _documentLoader.LoadDeloMetaDocuments(null);
        }

        [HttpPost]
        public Task<IEnumerable<DataOutgoingDocumentDto>> GetOutgoingDocumentsData([FromBody]long[] @params)
        {
            return _documentLoader.LoadDeloDataDocuments(@params);
        }

        [HttpPost]
        public Task SetExportState([FromBody]DeloDocumentExportStatusDto @params)
        {
            return _storyBuilder.Build(@params)
                .ReturnAsync<ValueTuple>();
        }
    }
}

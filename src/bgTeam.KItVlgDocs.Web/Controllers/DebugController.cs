﻿namespace bgTeam.KItVlgDocs.Web.Controllers
{
    using System.ComponentModel.DataAnnotations;
    using System.IO;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Story.Common;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;

    [Route("/api/[controller]/[action]")]
    [ApiController]
    public class DebugController : Controller
    {
        private readonly IStoryBuilder _storyBuilder;

        public DebugController(
            IStoryBuilder storyBuilder)
        {
            _storyBuilder = storyBuilder;
        }

        public class DebugStampDto
        {
            [Required]
            public IFormFile File { get; set; }

            public StampData SignatureStamp { get; set; }

            public StampData RegistrationStamp { get; set; }
        }

        [HttpPost]
        public async Task<ActionResult> StampDocument([FromForm]DebugStampDto @params)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            byte[] bytes;

            using (var stream = @params.File.OpenReadStream())
            {
                using (var memory = new MemoryStream())
                {
                    await stream.CopyToAsync(memory);

                    bytes = memory.ToArray();
                }
            }

            return File(
                await _storyBuilder.Build(
                    new StampDocumentStoryContext
                    {
                        Document = bytes,
                        SignatureStamp = @params.SignatureStamp,
                        RegistrationStamp = @params.RegistrationStamp,
                    }).ReturnAsync<byte[]>(),
                @params.File.ContentType,
                @params.File.Name);
        }
    }
}

﻿namespace bgTeam.KItVlgDocs.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.DataAccess;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using bgTeam.KitVlgDocs.Story;
    using Microsoft.AspNetCore.Mvc;

    [Route("/api/[controller]/[action]")]
    [ApiController]
    public class MedoController : Controller
    {
        private readonly IStoryBuilder _storyBuilder;
        private readonly IMedoDocumentLoader _documentLoader;

        public MedoController(
            IStoryBuilder storyBuilder,
            IMedoDocumentLoader documentLoader)
        {
            _storyBuilder = storyBuilder;
            _documentLoader = documentLoader;
        }

        [HttpGet]
        public Task<IEnumerable<FullOutgoingDocumentDto>> GetOutgoingDocuments()
        {
            return _documentLoader.LoadFullDocuments(null);
        }

        [HttpGet]
        public Task<IEnumerable<MetaOutgoingDocumentDto>> GetOutgoingDocumentsMeta()
        {
            return _documentLoader.LoadMetaDocuments(null);
        }

        [HttpPost]
        public Task<IEnumerable<DataOutgoingDocumentDto>> GetOutgoingDocumentsData([FromBody]long[] @params)
        {
            return _documentLoader.LoadDataDocuments(@params);
        }

        [HttpGet]
        public Task SetExportState(long id, bool visibleForExport)
        {
            return _storyBuilder.Build(new ServerSetMedoDocumentExportStateStoryContext
            {
                DocumentId = id,
                VisibleForExport = visibleForExport,
            }).ReturnAsync<ValueTuple>();
        }

        [HttpPost]
        public Task<OperationResultDto> ExportDocument([FromBody]OutgoinDocumentDto @params)
        {
            return _storyBuilder.Build(@params)
                .ReturnAsync<OperationResultDto>();
        }

        [HttpPost]
        public Task<OperationResultDto> ImportDocument([FromBody]IncomingDocumentDto @params)
        {
            return _storyBuilder.Build(@params)
                .ReturnAsync<OperationResultDto>();
        }
    }
}

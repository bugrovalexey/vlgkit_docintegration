﻿namespace bgTeam.KItVlgDocs.Web.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    public class HomeController : Controller
    {
        /// <summary>
        /// Метод по-умолчанию.
        /// </summary>
        /// <returns>Имя сервиса и версия платформы.</returns>
        [HttpGet]
        public string Index()
        {
            return "bgTeam.KitVlgDocs.Web api v1";
        }
    }
}

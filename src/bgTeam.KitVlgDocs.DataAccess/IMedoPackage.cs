﻿namespace bgTeam.KitVlgDocs.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.Domain.Dto;

    public interface IMedoPackage
    {
        Task<string> GenerateDocumentNote(IncomingDocumentDto document);

        Task<bool> CheckMedoDocumentUid(string uid);

        Task<bool> CheckDeloDocumentIsn(int isn);

        Task<int> CreateIdentityEx();

        Task<(int? Id, string Name)> GetContact((int? Id, string Name, string Code) value);

        Task<int?> GetFolder(int contact_id, string name);

        Task<int?> GetContact(string name, string code = null, int? bclass_indicator = null);

        Task<int> CreateFile(FileDto file);

        Task<int?> GetEmployee(string first_name);

        Task<int?> GetFolderByDocumentKind(int contact_id, string kind);

        Task CreateMedoDocument(int document_id, FileDto xml);

        Task<int> CounterNextValue(string name, int maxTries = 10);

        Task<int> GetOrCreateMedoAddress(string name, string medo_guid, string medo_address);

        Task<int> FindEnumId(string pkg, string eclass, string val);

        Task<int?> FindEnumByName(string pkg, string eclass, string name);

        Task<int> FindReferenceItemId(string pkg, string eclass, string val);

        Task<int> FindBClassId(string pkg, string bclass, bool nofail = false);

        Task<int> FindEClassId(string pkg, string eclass, bool nofail = false);

        Task AddDocumentToFolder(int documentId, int folderId, DateTime? createDate = null);

        Task<long> CreateIdentityList(long owner_id, long eclass_id, string identity_code);

        Task<int> CreateMedoRoleLink(int parsed_contact_id, int parsed_role_id, int? dfa_role_id);

        Task<int> CreateDocStateHistory(int document_id, int state_id, int editor_id, DateTime change_time, string responsible);

        Task<int> CreateAddressee(int document_id, int contact_id, int role_id, DateTime? out_date = null, string out_registration = null);

        Task<int> CreateMedoContact(int document_id, int? contact_id = null, string region = null, string organization = null, string employee = null, string department = null, string position = null, string note = null, DateTime? correctdt = null, DateTime? out_date = null, string out_registration = null);

        Task<int> CreateDocument(int typeId, int eclassId, int? authorId, string outRegistration, DateTime? outDate, string inRegistration, DateTime? inDate, int contactId, IEnumerable<int> recipients_list, IEnumerable<int> addressees_list, IEnumerable<int> correspondents_list, IEnumerable<int> owners_list, string subj, int stateId, IEnumerable<FileDto> files_list, DateTime createDate, int? editorId, string docket, int? unitId);

        Task CreateClassifiers(int document_id, IEnumerable<long> ids);

        Task<int?> GetFolder(int? address_id);

        Task<(int? ContactId, int? AddressId, string Name)> GetDeloContact(string name, int base_contact, int bclass_indicator, int eclass_id = default);

        Task<(int contactId, string firstName, string middleName, string lastName, string position, string keyIdentifier)> GetSignerContact(int contact_id);
    }
}

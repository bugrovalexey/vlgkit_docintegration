﻿namespace bgTeam.KitVlgDocs.DataAccess.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using Microsoft.Extensions.Options;
    using Oracle.ManagedDataAccess.Client;
    using Oracle.ManagedDataAccess.Types;

    public class MedoPackage : IMedoPackage
    {
        private static readonly Regex _fioRegex = new Regex(@"([A-Za-zА-Яа-я]+)\s*([A-Za-zА-Яа-я]?)\.\s*([A-Za-zА-Яа-я]?)\.", RegexOptions.Compiled);
        private readonly IRepository _repository;
        private readonly ICrudService _crudService;
        private readonly IOptions<WebAppOptions> _options;

        public MedoPackage(
            IRepository repository,
            ICrudService crudService,
            IOptions<WebAppOptions> options)
        {
            _repository = repository;
            _crudService = crudService;
            _options = options;
        }

        public async Task<string> GenerateDocumentNote(IncomingDocumentDto document)
        {
            var author = document.Passport.Authors.First();

            var builder = new StringBuilder(2048)
                .AppendFormat("Вид документа: {0}", document.Passport.Requisites.DocumentKind.Value).AppendLine()
                .AppendFormat("Отправитель МЭДО: {0}", author.Organization.Title).AppendLine()
                .AppendFormat("Подпись документа: {0}", document.SignatureStatus).AppendLine()
                .AppendLine("Данные о лицах, подписавших документ: ");

            var apparatus = await GetContact(_options.Value.ContactsMapping["Apparatus"]);

            builder = document.Passport.Authors.Aggregate(builder, (x, y) =>
            {
                foreach (var item in y.Sign)
                {
                    x.AppendFormat("{0} Дата подписания: {1:yyyy.MM.dd}", item.Person.Name, y.Registration.Date).AppendLine();
                }

                return x;
            }).AppendLine("Корреспонденты: ");

            builder = document.Passport.Authors.Aggregate(builder, (x, y) => x.AppendFormat("{0}, {1}", y.Organization.Title, y.Organization.Address).AppendLine())
                .AppendLine("Адресаты: ")
                .AppendLine(apparatus.Name);

            return builder.ToString();
        }

        public async Task<bool> CheckMedoDocumentUid(string uid)
        {
            var medo_header_guid = await FindEClassId("Medo.References", "HeaderGUID");
            var medo_external_guid = await FindEClassId("Medo.References", "ExternalGUID");

            return await _repository.GetAsync<int>(
                @"SELECT CASE WHEN EXISTS (
SELECT IDENTITYLIST_ID FROM CBO_IDENTITYLIST l INNER JOIN BO_PROTOTYPE p using (PROTOTYPE_ID) WHERE p.ECLASS_ID in (:medo_header_guid, :medo_external_guid) and l.IDENTITY_CODE = :uuid
) THEN 1 ELSE 0 END FROM DUAL", new
                {
                    uuid = uid,
                    medo_header_guid,
                    medo_external_guid,
                }) > 0;
        }

        public async Task<bool> CheckDeloDocumentIsn(int isn)
        {
            var external_id = await FindEClassId("Common", "ExternalSystemIdentity");

            return await _repository.GetAsync<int>(
                @"SELECT CASE WHEN EXISTS (
SELECT IDENTITYLIST_ID FROM CBO_IDENTITYLIST l INNER JOIN BO_PROTOTYPE p using (PROTOTYPE_ID) WHERE p.ECLASS_ID = :external_id and l.IDENTITY_CODE = :isn
) THEN 1 ELSE 0 END FROM DUAL", new
                {
                    isn = isn.ToString(),
                    external_id,
                }) > 0;
        }

        public async Task<int> CreateIdentityEx()
        {
            var @params = new OracleDynamicParameters();

            @params.Add(":id", dbType: OracleDbType.Int64, direction: ParameterDirection.InputOutput);

            await _crudService.ExecuteAsync("INSERT INTO CBO_IDENTITYEX (IDENTITYEX_ID) VALUES (CBO_IDENTITYEX_SEQ.NEXTVAL) returning IDENTITYEX_ID into :id", @params);

            return (int)@params.Get<OracleDecimal>("id");
        }

        public Task<int?> GetFolder(int contact_id, string name)
        {
            return _repository.GetAsync<int?>(
                @"
SELECT fldr.FOLDER_ID
FROM DFA_FOLDER fldr
INNER JOIN CNT_ADDRESS adrs
ON adrs.ADDRESS_ID = fldr.ADDRESS_ID
INNER JOIN CNT_CONTACT cont
ON cont.CONTACT_ID  = adrs.CONTACT_ID
WHERE fldr.NAME     like '%' || :name || '%'
AND cont.CONTACT_ID = :contact_id", new
                {
                    name,
                    contact_id,
                });
        }

        public async Task<(int? Id, string Name)> GetContact((int? Id, string Name, string Code) value)
        {
            if (value.Id != null)
            {
                return (value.Id, await _repository.GetAsync<string>("SELECT NAME FROM CNT_CONTACT WHERE CONTACT_ID = :id", new
                {
                    id = value.Id,
                }));
            }
            else if (!string.IsNullOrEmpty(value.Name) && string.IsNullOrEmpty(value.Code))
            {
                return (await _repository.GetAsync<int?>("SELECT CONTACT_ID FROM CNT_CONTACT WHERE NAME = :name", new
                {
                    name = value.Name,
                }), value.Name);
            }
            else if (!string.IsNullOrEmpty(value.Name) && !string.IsNullOrEmpty(value.Code))
            {
                return (await _repository.GetAsync<int?>("SELECT CONTACT_ID FROM CNT_CONTACT WHERE NAME = :name AND CODE = :code", new
                {
                    name = value.Name,
                    code = value.Code,
                }), value.Name);
            }
            else
            {
                throw new InvalidOperationException("Должны быть заданны имя или id контакта.");
            }
        }

        public Task<int?> GetFolder(int? address_id)
        {
            return _repository.GetAsync<int?>(
                @"
SELECT fldr.FOLDER_ID
FROM DFA_FOLDER fldr
WHERE fldr.ADDRESS_ID = :address_id", new
                {
                    address_id,
                });
        }

        public Task<(int? ContactId, int? AddressId, string Name)> GetDeloContact(string name, int base_contact, int bclass_indicator, int eclass_id = default)
        {
            if (string.IsNullOrEmpty(name))
            {
                return default;
            }

            if (_fioRegex.Match(name) is Match match && match.Success)
            {
                var contact_name = $@"^{match.Groups[1].Value}\s*{match.Groups[2].Value}\.\s*{match.Groups[3].Value}\.";

                if (eclass_id > 0)
                {
                    return _repository.GetAsync<(int?, int?, string)>(
 @"
SELECT cnt.CONTACT_ID,
  person_folder.ADDRESS_ID,
  cnt.NAME
FROM CNT_EMPLOYEE emp,
  CNT_CONTACT cnt,
  V_CNT_LEGAL_PERSON_LINK lp_link,
  CNT_LEGAL_PERSON lp,
  cnt_contact lp_cnt,
  cnt_address person_folder
JOIN BO_PROTOTYPE p2
ON p2.PROTOTYPE_ID                 = person_folder.PROTOTYPE_ID
AND p2.ECLASS_ID                   = :eclass_id
WHERE cnt.IS_ASSIGNABLE            = '1'
AND emp.CONTACT_ID                 = cnt.CONTACT_ID
AND lp_link.CONTACT_ID             = cnt.CONTACT_ID
AND lp.CONTACT_ID                  = lp_link.LEGAL_PERSON_ID
AND lp.CONTACT_ID                  = lp_cnt.CONTACT_ID
AND lp_link.LEGAL_PERSON_ID        = :base_contact
AND person_folder.BCLASS_INDICATOR = :bclass_indicator
AND REGEXP_LIKE (cnt.NAME, :contact_name)
AND person_folder.CONTACT_ID = emp.CONTACT_ID", new
 {
     contact_name,
     base_contact,
     bclass_indicator,
     eclass_id,
 });
                }
                else
                {
                    return _repository.GetAsync<(int?, int?, string)>(
 @"
SELECT cnt.CONTACT_ID,
  null AS ADDRESS_ID,
  cnt.NAME
FROM CNT_EMPLOYEE emp,
  CNT_CONTACT cnt,
  V_CNT_LEGAL_PERSON_LINK lp_link,
  CNT_LEGAL_PERSON lp,
  cnt_contact lp_cnt
WHERE cnt.IS_ASSIGNABLE            = '1'
AND emp.CONTACT_ID                 = cnt.CONTACT_ID
AND lp_link.CONTACT_ID             = cnt.CONTACT_ID
AND lp.CONTACT_ID                  = lp_link.LEGAL_PERSON_ID
AND lp.CONTACT_ID                  = lp_cnt.CONTACT_ID
AND lp_link.LEGAL_PERSON_ID        = :base_contact
AND REGEXP_LIKE (cnt.NAME, :contact_name)", new
 {
     contact_name,
     base_contact,
 });
                }
            }

            return _repository.GetAsync<(int?, int?, string)>(
                @"
SELECT q.CONTACT_ID,
  q.ADDRESS_ID,
  q.NAME
FROM
  (SELECT c.CONTACT_ID,
    a.ADDRESS_ID,
    c.NAME
  FROM CNT_CONTACT c
  JOIN CNT_EMPLOYEE e
  ON E.CONTACT_ID     = C.CONTACT_ID
  AND C.IS_ASSIGNABLE = 1
  JOIN CNT_ADDRESS a
  ON A.CONTACT_ID          = C.CONTACT_ID
  WHERE A.BCLASS_INDICATOR = :bclass_indicator
  AND A.ADDRESS            = 'Папка входящей корреспонденции'
  ) q
WHERE q.NAME LIKE '%'
  || :name
  || '%'", new
            {
                name,
                bclass_indicator,
            });
        }

        public Task<(int contactId, string firstName, string middleName, string lastName, string position, string keyIdentifier)> GetSignerContact(int contact_id)
        {
            return _repository.GetAsync<(int, string, string, string, string, string)>(GenerateSql(), new
            {
                contact_id,
            });

            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .AppendLine(@"
SELECT cnt.CONTACT_ID,
  emp.FIRST_NAME,
  emp.MIDDLE_NAME,
  emp.LAST_NAME,
  pos.NAME AS POSITION,
  crt.KEY_IDENTIFIER
FROM CNT_CONTACT cnt
INNER JOIN CNT_EMPLOYEE emp
ON cnt.CONTACT_ID = emp.CONTACT_ID
LEFT JOIN CBO_REFERENCE pos
ON emp.POSITION_ID = pos.REFERENCE_ID
LEFT JOIN CBO_CERTIFICATE crt
ON cnt.CERTIFICATE_ID = crt.CERTIFICATE_ID
WHERE cnt.CONTACT_ID  = :contact_id");

                return builder.Replace(Environment.NewLine, "\n").ToString();
            }
        }

        public Task<int?> GetContact(string name, string code = null, int? bclass_indicator = null)
        {
            return _repository.GetAsync<int?>("SELECT CONTACT_ID FROM CNT_CONTACT WHERE NAME = :name AND (:code IS NULL OR CODE = :code) AND (:bclass_indicator IS NULL OR BCLASS_INDICATOR = :bclass_indicator)", new
            {
                name,
                code,
                bclass_indicator,
            });
        }

        public Task<int?> GetEmployee(string first_name)
        {
            return _repository.GetAsync<int?>("SELECT CONTACT_ID FROM CNT_EMPLOYEE WHERE FIRST_NAME = :first_name", new
            {
                first_name,
            });
        }

        public Task<int?> GetFolderByDocumentKind(int contact_id, string kind)
        {
            if (_options.Value.FoldersMapping.TryGetValue(kind, out var name))
            {
                return GetFolder(contact_id, name);
            }

            throw new InvalidOperationException($"Папка не сконфигурирована для типа ({kind}) документа.");
        }

        public async Task<int> CreateFile(FileDto file)
        {
            var @params = new OracleDynamicParameters(new
            {
                x_blob_ext = Path.GetExtension(file.FileName).TrimStart('.'),
            });

            @params.Add($":x_blob", file.Data, OracleDbType.Blob);
            @params.Add(name: ":id", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            await _crudService.ExecuteAsync(
                "INSERT INTO BO_LOBSTORAGE (LOBSTORAGE_ID, X_BLOB, X_BLOB_EXT) " +
                "VALUES (BO_LOBSTORAGE_SEQ.NEXTVAL, :x_blob, :x_blob_ext) returning LOBSTORAGE_ID into :id",
                @params);

            return (int)@params.Get<OracleDecimal>("id");
        }

        public async Task CreateMedoDocument(int document_id, FileDto xml)
        {
            var xml_id = await CreateFile(xml);

            await _crudService.ExecuteAsync("INSERT INTO MEDO_DOCUMENT (BPDOCUMENT_ID, MEDO_XML_ID) VALUES (:document_id, :xml_id)", new
            {
                document_id,
                xml_id,
            });
        }

        public async Task<int> CounterNextValue(string name, int maxTries = 10)
        {
            while (maxTries > 0)
            {
                var result = await GetCounter();

                if (result.min_hole != 0)
                {
                    throw new NotSupportedException();
                }

                if (result.check_used_values != 0)
                {
                    throw new NotSupportedException();
                }

                var value = result.value + 1;

                if (await UpdateCounter(result.counter_id, result.obj_version, value) > 0)
                {
                    return value;
                }

                maxTries--;
            }

            throw new InvalidOperationException($"Ошибка при получении значения счетчика {name}.");

            Task<int> UpdateCounter(int counter_id, int obj_version, int value)
            {
                return _crudService.ExecuteAsync("UPDATE CBO_COUNTER SET VALUE = :value, OBJ_VERSION = :obj_version_new WHERE COUNTER_ID = :counter_id AND OBJ_VERSION = :obj_version_old", new
                {
                    counter_id,
                    obj_version_old = obj_version,
                    obj_version_new = obj_version + 1,
                    value,
                });
            }

            Task<(int counter_id, int check_used_values, int obj_version, int min_hole, string name, int value, int prototype_id)> GetCounter()
            {
                return _repository.GetAsync<(int counter_id, int check_used_values, int obj_version, int min_hole, string name, int value, int prototype_id)>("SELECT COUNTER_ID as Item1, CHECK_USED_VALUES as Item2, OBJ_VERSION as Item3, MIN_HOLE as Item4, NAME as Item5, VALUE as Item6, PROTOTYPE_ID as Item7 FROM CBO_COUNTER WHERE NAME = :name", new
                {
                    name,
                });
            }
        }

        public async Task<int> GetOrCreateMedoAddress(string name, string medo_guid, string medo_address)
        {
            var bclass_indicator = await FindBClassId("Contacts", "LegalPerson");

            return await GetMedoContact() ?? await Insert();

            async Task<int?> GetMedoContact()
            {
                var medo_address_bclass = await FindBClassId("Medo.Contacts", "Address");

                return await _repository.GetAsync<int?>(GenerateSql(), new
                {
                    medo_address,
                    medo_address_bclass,
                }) ?? await _repository.GetAsync<int?>(GenerateSql1(), new
                {
                    name,
                    medo_guid,
                    medo_address,
                }) ?? await _repository.GetAsync<int?>(GenerateSql2(), new
                {
                    name,
                    medo_address_bclass,
                });

                string GenerateSql()
                {
                    var builder = new StringBuilder()
                        .AppendLine(
@"SELECT a.CONTACT_ID
FROM CNT_ADDRESS a
WHERE a.BCLASS_INDICATOR = :medo_address_bclass AND a.ADDRESS = :medo_address");
                    return builder.Replace(Environment.NewLine, "\n").ToString();
                }

                string GenerateSql1()
                {
                    var builder = new StringBuilder()
                        .AppendLine(
@"SELECT ca.CONTACT_ID
FROM MEDO_ADDRESS ma
INNER JOIN CNT_ADDRESS ca
ON ca.ADDRESS_ID = ma.ADDRESS_ID
WHERE (lower(ma.SZI_NAME) = lower(:name) OR ma.GUID = :medo_guid OR ma.MEDOADDRESS = :medo_address)");
                    return builder.Replace(Environment.NewLine, "\n").ToString();
                }

                string GenerateSql2()
                {
                    var builder = new StringBuilder()
                        .AppendLine(
@"SELECT a.CONTACT_ID
FROM CNT_ADDRESS a
INNER JOIN CNT_CONTACT c
ON c.CONTACT_ID          = a.CONTACT_ID
WHERE a.BCLASS_INDICATOR = :medo_address_bclass
AND lower(c.NAME)               = lower(:name)");
                    return builder.Replace(Environment.NewLine, "\n").ToString();
                }
            }

            async Task<int> Insert()
            {
                var medo_address_bclass = await FindBClassId("Medo.Contacts", "Address");
                var medo_address_category_eclass = await FindEClassId("Medo.Contacts", "AddressCategory");
                var medo_kind_one = await FindEnumId("Medo.References", "MedoKind", "1");

                var @params = new OracleDynamicParameters(new
                {
                    name,
                    medo_guid,
                    medo_address,
                    medo_kind_one,
                    bclass_indicator,
                    medo_address_bclass,
                    medo_address_category_eclass,
                });

                @params.Add(name: ":id", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

                await _crudService.ExecuteAsync(GenerateSql(), @params);

                return (int)@params.Get<OracleDecimal>("id");

                string GenerateSql()
                {
                    var builder = new StringBuilder()
                        .AppendLine(
@"declare
  adres_id number;
  proto_id number;
begin 
  insert into cnt_contact(contact_id, name, bclass_indicator, obj_version)
    values(cnt_contact_seq.nextval, :name, :bclass_indicator, 1) returning contact_id into :id;

  insert into cnt_orgunit(contact_id) 
    values (:id);

  insert into cnt_legal_person(contact_id) 
    values (:id);

  insert into bo_prototype(prototype_id, eclass_id)
    values(bo_prototype_seq.nextval, :medo_address_category_eclass) returning prototype_id into proto_id;

  insert into cnt_address(address_id, address, contact_id, prototype_id, bclass_indicator)
    values(cnt_address_seq.nextval, :medo_address, :id, proto_id, :medo_address_bclass) returning address_id into adres_id;

  insert into medo_address(address_id, szi_name, guid, medokind_id, medoaddress, send_notifications)
    values(adres_id, :name, :medo_guid, :medo_kind_one, :medo_address, 0);
end;");
                    return builder.Replace(Environment.NewLine, "\n").ToString();
                }
            }
        }

        public Task<int> FindEnumId(string pkg, string eclass, string val)
        {
            return _repository.GetAsync<int>("select coretools.findEnumID(:val, :pkg, :eclass) from dual", new
            {
                val,
                pkg,
                eclass,
            });
        }

        public Task<int?> FindEnumByName(string val, string pkg, string eclass)
        {
            return _repository.GetAsync<int?>(
                @"
SELECT en.REFERENCE_ID
FROM CBO_ENUMERATION en,
  BO_ECLASS ec,
  BO_PACKAGE pkg
WHERE pkg.FULLNAME = :pkg
AND ec.NAME        = :eclass
AND en.NAME        = :val
AND pkg.PACKAGE_ID = ec.PACKAGE_ID
AND ec.ECLASS_ID   = en.ECLASS_ID
AND ROWNUM         = 1", new
                {
                    val,
                    pkg,
                    eclass,
                });
        }

        public Task<int> FindReferenceItemId(string pkg, string eclass, string val)
        {
            return _repository.GetAsync<int>("select coretools.findReferenceItemID(:val, :pkg, :eclass) from dual", new
            {
                val,
                pkg,
                eclass,
            });
        }

        public async Task<int> FindBClassId(string pkg, string bclass, bool nofail = false)
        {
            var @params = new OracleDynamicParameters(new
            {
                pkg,
                bclass,
                nofail = nofail ? 1 : 0,
            });
            @params.Add(":result", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            await _crudService.ExecuteAsync("begin :result := coretools.findBClassID(:pkg, :bclass, :nofail = 1); end;", @params);

            return (int)@params.Get<OracleDecimal>("result");
        }

        public async Task<int> FindEClassId(string pkg, string eclass, bool nofail = false)
        {
            var @params = new OracleDynamicParameters(new
            {
                pkg,
                eclass,
                nofail = nofail ? 1 : 0,
            });
            @params.Add(":result", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            await _crudService.ExecuteAsync("begin :result := coretools.findEClassID(:pkg, :eclass, :nofail = 1); end;", @params);

            return (int)@params.Get<OracleDecimal>("result");
        }

        public async Task AddDocumentToFolder(int documentId, int folderId, DateTime? createDate = null)
        {
            var bcDocumentRef = await FindBClassId("Docflow.Documents", "DocumentRef");
            var defDocumentRefTypeId = await FindReferenceItemId("Docflow.References", "DocumentRefType", "General");

            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .AppendLine(
@"declare
  msgid     number;
begin
  insert into dfa_folder_message (message_id, folder_id, send_dt, is_readfl, bclass_indicator)
    values(dfa_folder_message_seq.nextval, :folderId, :createDate, 'T', :bcDocumentRef) returning message_id into msgid;
  insert into dfa_document_ref(message_id, document_id, ref_type_id)
    values(msgid, :documentId, :defDocumentRefTypeId);
END;");
                return builder.Replace(Environment.NewLine, "\n").ToString();
            }

            await _crudService.ExecuteAsync(GenerateSql(), new
            {
                folderId,
                documentId,
                bcDocumentRef,
                defDocumentRefTypeId,
                createDate = createDate ?? DateTime.Now,
            });
        }

        public async Task<long> CreateIdentityList(long owner_id, long eclass_id, string identity_code)
        {
            var @params = new OracleDynamicParameters(new
            {
                owner_id,
                eclass_id,
                identity_code,
            });

            @params.Add(name: ":id", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            await _crudService.ExecuteAsync(GenerateSql(), @params);

            return (long)@params.Get<OracleDecimal>("id");

            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .AppendLine(
@"declare
  proto_id number;
begin
  insert into bo_prototype (prototype_id, eclass_id)
    values(bo_prototype_seq.nextval, :eclass_id) returning prototype_id into proto_id;
  insert into cbo_identitylist(identitylist_id, owner_id, prototype_id, identity_code)
    values(cbo_identitylist_seq.nextval, :owner_id, proto_id, :identity_code) returning identitylist_id into :id;
END;");
                return builder.Replace(Environment.NewLine, "\n").ToString();
            }
        }

        public async Task<int> CreateMedoRoleLink(int parsed_contact_id, int parsed_role_id, int? dfa_role_id)
        {
            var @params = new OracleDynamicParameters(new
            {
                dfa_role_id,
                parsed_role_id,
                parsed_contact_id,
            });

            @params.Add(name: ":id", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            await _crudService.ExecuteAsync(
                "INSERT INTO MEDO_PARSED_ROLELINK (PARSED_ROLELINK_ID, PARSED_ROLE_ID, DFA_ROLE_ID, PARSED_CONTACT_ID) " +
                "VALUES (MEDO_PARSED_ROLELINK_SEQ.NEXTVAL, :parsed_role_id, :dfa_role_id, :parsed_contact_id) returning PARSED_ROLELINK_ID into :id", @params);

            return (int)@params.Get<OracleDecimal>("id");
        }

        public async Task<int> CreateDocStateHistory(int document_id, int state_id, int editor_id, DateTime change_time, string responsible)
        {
            var @params = new OracleDynamicParameters(new
            {
                state_id,
                editor_id,
                document_id,
                change_time,
                responsible,
            });

            @params.Add(name: ":id", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            await _crudService.ExecuteAsync(
                "INSERT INTO DOC_STATE_HISTORY (DOC_STATE_HISTORY_ID, OWNER_ID, STATE_ID, EDITOR_ID, CHANGE_TIME, RESPONSIBLE) " +
                "VALUES (DOC_STATE_HISTORY_SEQ.NEXTVAL, :document_id, :state_id, :editor_id, :change_time, :responsible) returning DOC_STATE_HISTORY_ID into :id", @params);

            return (int)@params.Get<OracleDecimal>("id");
        }

        public async Task<int> CreateAddressee(int document_id, int contact_id, int role_id, DateTime? out_date = null, string out_registration = null)
        {
            var @params = new OracleDynamicParameters(new
            {
                document_id,
                contact_id,
                role_id,
                out_date,
                out_registration,
            });

            @params.Add(name: ":id", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            await _crudService.ExecuteAsync(
                "INSERT INTO DFA_ADDRESSEE (ADDRESSEE_ID, DOCUMENT_ID, CONTACT_ID, ROLE_ID, OUT_DATE, OUT_REGISTRATION) " +
                "VALUES (DFA_ADDRESSEE_SEQ.NEXTVAL, :document_id, :contact_id, :role_id, :out_date, :out_registration) returning ADDRESSEE_ID into :id",
                @params);

            return (int)@params.Get<OracleDecimal>("id");
        }

        public async Task<int> CreateMedoContact(int document_id, int? contact_id = null, string region = null, string organization = null, string employee = null, string department = null, string position = null, string note = null, DateTime? correctdt = null, DateTime? out_date = null, string out_registration = null)
        {
            var @params = new OracleDynamicParameters(new
            {
                document_id,
                contact_id,
                region,
                organization,
                employee,
                department,
                position,
                note,
                correctdt,
                out_date,
                out_registration,
            });

            @params.Add(name: ":id", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            await _crudService.ExecuteAsync(
                "INSERT INTO MEDO_PARSED_CONTACT (PARSED_CONTACT_ID, CONTACT_ID, BPDOCUMENT_ID, REGION, ORGANIZATION, EMPLOYEE, DEPARTMENT, POSITION, NOTE, CORRECTDT, OUT_DATE, OUT_REGISTRATION) " +
                "VALUES (MEDO_PARSED_CONTACT_SEQ.NEXTVAL, :contact_id, :document_id, :region, :organization, :employee, :department, :position, :note, :correctdt, :out_date, :out_registration) returning PARSED_CONTACT_ID into :id", @params);

            return (int)@params.Get<OracleDecimal>("id");
        }

        public async Task<int> CreateDocument(int typeId, int eclassId, int? authorId, string outRegistration, DateTime? outDate, string inRegistration, DateTime? inDate, int contactId, IEnumerable<int> recipients_list, IEnumerable<int> addressees_list, IEnumerable<int> correspondents_list, IEnumerable<int> owners_list, string subj, int stateId, IEnumerable<FileDto> files_list, DateTime createDate, int? editorId, string docket, int? unitId)
        {
            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .AppendLine(@"DECLARE
  p_recipients_list     CoreTools.IDS_ARRAY;
  p_addressees_list     CoreTools.IDS_ARRAY;
  p_correspondents_list CoreTools.IDS_ARRAY;
  p_owners_list         CoreTools.IDS_ARRAY;
  p_files_list          DocflowTools.FILES_ARRAY;
  FUNCTION CreateFile (
    filename string,
    data blob,
    text clob) RETURN DocflowTools.FILE_RECORD IS
  ret DocflowTools.FILE_RECORD;
  BEGIN
    ret.filename := filename;
    ret.data := data;
    ret.text := text;
    RETURN ret;
  END CreateFile;
BEGIN");
                {
                    var index = 0;
                    if (recipients_list != null)
                    {
                        foreach (var item in recipients_list)
                        {
                            builder.AppendLine($"  p_recipients_list( {index++} ) := {item};");
                        }
                    }
                }

                {
                    var index = 0;
                    if (addressees_list != null)
                    {
                        foreach (var item in addressees_list)
                        {
                            builder.AppendLine($"  p_addressees_list( {index++} ) := {item};");
                        }
                    }
                }

                {
                    var index = 0;
                    if (correspondents_list != null)
                    {
                        foreach (var item in correspondents_list)
                        {
                            builder.AppendLine($"  p_correspondents_list( {index++} ) := {item};");
                        }
                    }
                }

                {
                    var index = 0;
                    if (owners_list != null)
                    {
                        foreach (var item in owners_list)
                        {
                            builder.AppendLine($"  p_owners_list( {index++} ) := {item};");
                        }
                    }
                }

                {
                    var index = 0;
                    if (files_list != null)
                    {
                        foreach (var item in files_list)
                        {
                            builder.AppendLine($"  p_files_list( {index} ) := CreateFile(:fname_{index}, {(item.Data != null ? $":fdata_{index}" : "NULL")}, {(item.Text != null ? $":ftext_{index}" : "NULL")});");
                            index++;
                        }
                    }
                }

                builder.Append(@"
  DocflowTools.CreateOrUpdateDocument(:id, :typeId, :eclassId, :authorId, :outRegistration, :outDate, :inRegistration, :inDate, :contactId, p_recipients_list, p_addressees_list, p_correspondents_list, p_owners_list, :subj, :stateId, p_files_list, :createDate, :editorId, :docket, :unitId);
END;");
                return builder.Replace(Environment.NewLine, "\n").ToString();
            }

            var @params = new OracleDynamicParameters(new
            {
                typeId,
                eclassId,
                authorId,
                outRegistration,
                outDate,
                inRegistration,
                inDate,
                contactId,
                subj,
                stateId,
                createDate,
                editorId,
                unitId,
            });
            @params.Add(":id", dbType: OracleDbType.Int32, direction: ParameterDirection.InputOutput);
            @params.Add(":docket", docket, OracleDbType.Clob);
            {
                var index = 0;
                foreach (var item in files_list ?? Enumerable.Empty<FileDto>())
                {
                    @params.Add($":fname_{index}", item.FileName);
                    if (item.Data != null)
                    {
                        @params.Add($":fdata_{index}", item.Data, OracleDbType.Blob);
                    }

                    if (item.Text != null)
                    {
                        @params.Add($":ftext_{index}", item.Text, OracleDbType.Clob);
                    }

                    index++;
                }
            }

            await _crudService.ExecuteAsync(GenerateSql(), @params);

            return (int)@params.Get<OracleDecimal>("id");
        }

        public async Task CreateClassifiers(int document_id, IEnumerable<long> ids)
        {
            await _crudService.ExecuteAsync(GenerateSql(), new
            {
                document_id,
            });

            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .AppendLine(
@"declare
  list_id number;
begin 
  insert into cbo_enumlist(enumlist_id)
    values(cbo_enumlist_seq.nextval) returning enumlist_id into list_id;");

                foreach (var item in ids)
                {
                    builder.AppendLine($@"
  insert into cbo_enumlistitem(enumlistitem_id, value_id, owner_id)
    values(cbo_enumlistitem_seq.nextval, {item}, list_id);");
                }

                builder.AppendLine(@"
  update bo_bpdocument doc set doc.classifier_id = list_id where doc.bpdocument_id = :document_id;
end;");
                return builder.Replace(Environment.NewLine, "\n").ToString();
            }
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.DataAccess.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using bgTeam.KitVlgDocs.Domain.Xml;
    using Microsoft.Extensions.Options;

    public class MedoDocumentLoader : IMedoDocumentLoader
    {
        private struct OutgoingMetaDocumentRecordDto
        {
            public long bpdocument_id;

            public string signers_list;
        }

        private struct OutgoingDataDocumentRecordDto
        {
            public long bpdocument_id;

            public string signers_list;

            public string files_list;
        }

        private struct OutgoingFullDocumentRecordDto
        {
            public int bpdocument_id;
            public int eclass_id;
            public int contact_id;
            public int author_id;
            public string include_list;
            public DateTime in_date;
            public string in_registration;
            public int identityex_id;
            public int? docket_id;
            public string name;
            public string identity_list;
            public string addressee_list;
            public string signers_list;
            public string coordinates_list;
            public string files_list;
            public string type_name;
        }

        private struct OutgoingDeloDocumentRecordDto
        {
            public int bpdocument_id;
            public int contact_id;
            public int author_id;
            public string include_list;
            public DateTime in_date;
            public DateTime? control_date;
            public string in_registration;
            public int identityex_id;
            public int classifier_id;
            public int? docket_id;
            public string name;
            public string identity_list;
            public string addressee_list;
            public string signers_list;
            public string coordinates_list;
            public string files_list;
            public string type_name;
            public string subjects;
        }

        private readonly IAppLogger _logger;
        private readonly IMedoPackage _package;
        private readonly IRepository _repository;
        private readonly IOptions<WebAppOptions> _options;

        private long _type_id;
        private long _close_id;
        private long _external_id;
        private long _solution_id;
        private long _function_id;
        private long _medo_address_id;
        private long _delivery_kind_id;
        private long _delivery_kind_delo_id;
        private long _medo_external_guid_id;
        private long _dfa_role_recipient_id;
        private long _dfa_role_addressee_id;
        private long _dfa_role_correspondent_id;
        private long _medo_out_docs_id;
        private long[] _state_ids;
        private readonly Dictionary<int, string> _eclassMapping
            = new Dictionary<int, string>();

        private bool _isCached;

        public MedoDocumentLoader(
            IAppLogger logger,
            IMedoPackage package,
            IRepository repository,
            IOptions<WebAppOptions> options)
        {
            _logger = logger;
            _package = package;
            _repository = repository;
            _options = options;
        }

        public async Task<IEnumerable<DeloOutgoingDocumentDto>> LoadDeloDocuments(IEnumerable<long> ids)
        {
            await EnsureChached();

            var contact = await _package.GetContact(_options.Value.ContactsMapping["DeloExport"]);

            if (contact.Id == null)
            {
                throw new InvalidOperationException($"Не найден контакт {contact} необходимий для экспорта в систему 'Дело'.");
            }

            var result = await _repository.GetAllAsync<OutgoingDeloDocumentRecordDto>(GenerateSql(), new
            {
                close_id = _close_id,
                external_id = _external_id,
                solution_id = _solution_id,
                function_id = _function_id,
                delo_contact_id = contact.Id,
                dfa_role_recipient_id = _dfa_role_recipient_id,
                delivery_kind_delo_id = _delivery_kind_delo_id,
                task_description = _options.Value.SignTaskFilter,
            });

            var files = await LoadFiles(result.Where(x => x.files_list != null)
                .SelectMany(x => x.files_list.Split(';')));

            var dockets = await LoadFiles(result.Where(x => x.docket_id != null)
                .Select(x => $"{x.bpdocument_id}:{x.docket_id}:Docket"));

            var signers = LoadSigners(result.Where(x => x.signers_list != null)
                .SelectMany(x => x.signers_list.Split(';')));

            var addressee = LoadAddressee(result.Where(x => x.addressee_list != null)
                .SelectMany(x => x.addressee_list.Split(';')));

            var subjects = LoadSubjects(result.Where(x => x.subjects != null)
                .SelectMany(x => x.subjects.Split(';')));

            return result.Select(x =>
            {
                var signer = signers.TryGetValue(x.bpdocument_id, out var sig) ? sig : Array.Empty<(long, string, string)>();
                var addressees = addressee.TryGetValue(x.bpdocument_id, out var addr) ? addr : Array.Empty<(long, long, string)>();

                var item = new DeloOutgoingDocumentDto
                {
                    Id = x.bpdocument_id,
                    IncludeList = x.include_list,
                    ControlDate = x.control_date,
                    Files = (files.TryGetValue(x.bpdocument_id, out var file) ? file : Array.Empty<FileDto>()).Select(z => new DeloOutgoingDocumentFileDto
                    {
                        File = z,
                    }).ToArray(),
                    Annotation = dockets.TryGetValue(x.bpdocument_id, out var docket) ? docket[0].Text : null,
                    RegistrationDate = x.in_date,
                    RegistrationNumber = x.in_registration,
                    Subjects = subjects.TryGetValue(x.classifier_id, out var subject) ? subject : Array.Empty<string>(),

                    Signer = signer.Length > 0 ? FromSigner(signer.First().Item3) : null,
                    Addresse = addressees.Length > 0 ? FromAddressee(addressees.FirstOrDefault(z => z.Item1 == _dfa_role_addressee_id).Item3) : null,
                };

                return item;
            });

            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .Append(@"
SELECT dfa_doc.BPDOCUMENT_ID,
  dfa_doc.CONTACT_ID,
  dfa_doc.AUTHOR_ID,
  dfa_doc.CONTROL_DATE,
  dfa_doc.INCLUDE_LIST,
  bo_bpdoc.IN_DATE,
  bo_bpdoc.IN_REGISTRATION,
  bo_bpdoc.IDENTITYEX_ID,
  bo_bpdoc.NAME,
  bo_bpdoc.DOCKET_ID,
  bo_bpdoc.CLASSIFIER_ID,
  cbo_ref.NAME AS TYPE_NAME,
  (SELECT rtrim(xmlagg(xmlelement(E, p1.OWNER_ID
    || ':'
    || p2.ECLASS_ID
    || ':'
    || p1.IDENTITY_CODE, ';').extract('//text()')
  ORDER BY p2.ECLASS_ID).GetClobVal(), ';')
  FROM CBO_IDENTITYLIST p1
  INNER JOIN BO_PROTOTYPE p2
  ON p2.PROTOTYPE_ID = p1.PROTOTYPE_ID
  WHERE p1.OWNER_ID  = bo_bpdoc.IDENTITYEX_ID
  ) AS identity_list,
  (SELECT rtrim(xmlagg(xmlelement(E, p1.DOCUMENT_ID
    || ':'
    || p1.ROLE_ID
    || ':'
    || p1.CONTACT_ID
    || ':'
    || p2.NAME 
    || '|'
    || emp.LAST_NAME
    || '|'
    || emp.FIRST_NAME
    || '|'
    || emp.MIDDLE_NAME
    || '|'
    || pos.NAME, ';').extract('//text()')
  ORDER BY p1.ROLE_ID).GetClobVal(), ';')
  FROM DFA_ADDRESSEE p1
  INNER JOIN CNT_CONTACT p2
  ON p2.CONTACT_ID     = p1.CONTACT_ID
  INNER JOIN CNT_EMPLOYEE emp
  ON emp.CONTACT_ID = p1.CONTACT_ID
  INNER JOIN CBO_REFERENCE pos
  ON pos.REFERENCE_ID = emp.POSITION_ID
  WHERE p1.DOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  ) AS addressee_list,
  (SELECT rtrim(xmlagg(xmlelement(E, p1.BPDOCUMENT_ID
    || ':'
    || p3.LOBSTORAGE_ID
    || ':'
    || p3.NAME, ';').extract('//text()')
  ORDER BY p1.NORD).GetClobVal(), ';')
  FROM CBO_FILE_LINK p1
  INNER JOIN CBO_FILE p2
  ON p2.FILE_ID = p1.FILE_ID
  INNER JOIN CBO_FILEVERSION p3
  ON p3.FILEVERSION_ID   = p2.CURRENTVERSION_ID
  WHERE p1.BPDOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  AND p1.DELETEDT       IS NULL
  ) AS files_list,
  (SELECT rtrim(xmlagg(xmlelement(E, s.PERFORMED_DOCUMENT_ID
      || ':'
      || s.EDITOR_ID
      || ':'
      || s.KEY_IDENTIFIER
      || ':'
      || s.STORE
      || '|'
      || s.CntName 
      || '|'
      || s.CntLName
      || '|'
      || s.CntFName
      || '|'
      || s.CntMName
      || '|'
      || s.PosName, ';').extract('//text()')
  ORDER BY s.EDITOR_ID).GetClobVal(), ';')
  FROM
      ( SELECT ds.EDITOR_ID,
      p1.PERFORMED_DOCUMENT_ID,
      cert.KEY_IDENTIFIER,
      cert.STORE,
      cnt.NAME AS CntName,
      emp.LAST_NAME AS CntLName,
      emp.FIRST_NAME AS CntFName,
      emp.MIDDLE_NAME AS CntMName,
      pos.NAME AS PosName
      FROM WFA_TASK p1
      INNER JOIN DOC_STATE_HISTORY ds
      ON ds.OWNER_ID = p1.BPDOCUMENT_ID
      INNER JOIN CNT_CONTACT cnt
      ON ds.EDITOR_ID = cnt.CONTACT_ID
      INNER JOIN CBO_CERTIFICATE cert
      ON cert.CERTIFICATE_ID = cnt.CERTIFICATE_ID
      INNER JOIN CNT_EMPLOYEE emp
      ON emp.CONTACT_ID = cnt.CONTACT_ID
      INNER JOIN CBO_REFERENCE pos
      ON pos.REFERENCE_ID = emp.POSITION_ID
      WHERE ds.BPFUNCTION_ID = :function_id
      AND (p1.SOLUTION_ID     = :solution_id OR p1.SOLUTION_ID IS NULL)
      AND p1.STATE_ID        = :close_id
      AND p1.DESCRIPTION like '%' || :task_description || '%'
      ORDER BY ds.CHANGE_TIME DESC
      ) s
  WHERE s.PERFORMED_DOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID AND rownum = 1
  ) AS signers_list,
  (SELECT rtrim(xmlagg(xmlelement(E, p1.OWNER_ID
    || ':'
    || p2.NAME, ';').extract('//text()')
  ORDER BY p1.ENUMLISTITEM_ID).GetClobVal(), ';')
  FROM CBO_ENUMLISTITEM p1
  INNER JOIN CBO_ENUMERATION p2
  ON p2.REFERENCE_ID = p1.VALUE_ID
  WHERE p1.OWNER_ID = bo_bpdoc.CLASSIFIER_ID
  ) AS subjects
FROM DFA_DOCUMENT dfa_doc
INNER JOIN BO_BPDOCUMENT bo_bpdoc
ON bo_bpdoc.BPDOCUMENT_ID = dfa_doc.BPDOCUMENT_ID
INNER JOIN CBO_REFERENCE cbo_ref
ON cbo_ref.REFERENCE_ID        = dfa_doc.DOCUMENTTYPE_ID
WHERE bo_bpdoc.IDENTITYEX_ID IS NOT NULL
AND dfa_doc.DELIVERY_KIND_ID   = :delivery_kind_delo_id
AND EXISTS
  (SELECT 1
  FROM DFA_ADDRESSEE p1
  WHERE p1.DOCUMENT_ID = dfa_doc.BPDOCUMENT_ID
  AND p1.ROLE_ID       = :dfa_role_recipient_id
  AND p1.CONTACT_ID    = :delo_contact_id
  )");

                if (_state_ids.Length > 0)
                {
                    builder.Append($@"
AND dfa_doc.STATE_ID in ({string.Join(", ", _state_ids)})");
                }

                if (ids == null || !ids.Any())
                {
                    builder.Append(@"
AND NOT EXISTS
  (SELECT 1
  FROM CBO_IDENTITYLIST p1
  INNER JOIN BO_PROTOTYPE p2
  ON p2.PROTOTYPE_ID = p1.PROTOTYPE_ID
  WHERE p1.OWNER_ID  = bo_bpdoc.IDENTITYEX_ID
  AND p2.ECLASS_ID   = :external_id
  )");
                }
                else
                {
                    builder.Append($@"
AND dfa_doc.BPDOCUMENT_ID IN ({string.Join(", ", ids)})");
                }

                return builder.Replace(Environment.NewLine, "\n").ToString().Trim();
            }

            DeloOutgoingDocumentContactDto FromSigner(string value)
            {
                var values = value?.Split('|') ?? Array.Empty<string>();

                return values.Length == 6
                    ? new DeloOutgoingDocumentContactDto
                    {
                        Name = values[1],
                        LastName = values[2],
                        FirstName = values[3],
                        MiddleName = values[4],
                        Position = values[5],
                    }
                    : null;
            }

            DeloOutgoingDocumentContactDto FromAddressee(string value)
            {
                var values = value?.Split('|') ?? Array.Empty<string>();

                return values.Length == 5
                    ? new DeloOutgoingDocumentContactDto
                    {
                        Name = values[0],
                        LastName = values[1],
                        FirstName = values[2],
                        MiddleName = values[3],
                        Position = values[4],
                    }
                    : null;
            }
        }

        public async Task<IEnumerable<MetaOutgoingDocumentDto>> LoadDeloMetaDocuments(IEnumerable<long> ids)
        {
            await EnsureChached();

            var contact = await _package.GetContact(_options.Value.ContactsMapping["DeloExport"]);

            if (contact.Id == null)
            {
                throw new InvalidOperationException($"Не найден контакт {contact} необходимий для экспорта в систему 'Дело'.");
            }

            var result = await _repository.GetAllAsync<OutgoingMetaDocumentRecordDto>(GenerateSql(), new
            {
                close_id = _close_id,
                external_id = _external_id,
                solution_id = _solution_id,
                function_id = _function_id,
                delo_contact_id = contact.Id,
                dfa_role_recipient_id = _dfa_role_recipient_id,
                delivery_kind_delo_id = _delivery_kind_delo_id,
                task_description = _options.Value.SignTaskFilter,
            });

            var signers = LoadSigners(result.Where(x => x.signers_list != null)
                .SelectMany(x => x.signers_list.Split(';')));

            return result.Select(x => new MetaOutgoingDocumentDto
            {
                Id = x.bpdocument_id,
                Signers = signers.TryGetValue(x.bpdocument_id, out var value)
                    ? value.Select(z => new OutgoingDocumentSignerDto
                    {
                        Id = z.key_identifier,
                        ContactId = z.contact_id,
                        Store = z.store,
                    }).ToArray()
                    : null,
            }).Where(x => x.Signers != null && x.Signers.Length > 0);

            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .Append(@"
SELECT dfa_doc.BPDOCUMENT_ID,
  (SELECT rtrim(xmlagg(xmlelement(E, s.PERFORMED_DOCUMENT_ID
    || ':'
    || s.EDITOR_ID
    || ':'
    || s.KEY_IDENTIFIER
    || ':'
    || s.STORE, ';').extract('//text()')
  ORDER BY s.EDITOR_ID).GetClobVal(), ';')
  FROM
    (SELECT ds.EDITOR_ID,
      p1.PERFORMED_DOCUMENT_ID,
      cert.KEY_IDENTIFIER,
      cert.STORE
    FROM WFA_TASK p1
    INNER JOIN DOC_STATE_HISTORY ds
    ON ds.OWNER_ID = p1.BPDOCUMENT_ID
    INNER JOIN CNT_CONTACT cnt
    ON ds.EDITOR_ID = cnt.CONTACT_ID
    INNER JOIN CBO_CERTIFICATE cert
    ON cert.CERTIFICATE_ID = cnt.CERTIFICATE_ID
    WHERE ds.BPFUNCTION_ID = :function_id
    AND (p1.SOLUTION_ID    = :solution_id
    OR p1.SOLUTION_ID     IS NULL)
    AND p1.STATE_ID        = :close_id
    AND p1.DESCRIPTION LIKE '%'
      || :task_description
      || '%'
    ORDER BY ds.CHANGE_TIME DESC
    ) s
  WHERE s.PERFORMED_DOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  AND rownum                    = 1
  ) AS signers_list
FROM DFA_DOCUMENT dfa_doc
INNER JOIN BO_BPDOCUMENT bo_bpdoc
ON bo_bpdoc.BPDOCUMENT_ID = dfa_doc.BPDOCUMENT_ID
INNER JOIN CBO_REFERENCE cbo_ref
ON cbo_ref.REFERENCE_ID        = dfa_doc.DOCUMENTTYPE_ID
WHERE bo_bpdoc.IDENTITYEX_ID IS NOT NULL
AND dfa_doc.DELIVERY_KIND_ID   = :delivery_kind_delo_id
AND EXISTS
  (SELECT 1
  FROM DFA_ADDRESSEE p1
  WHERE p1.DOCUMENT_ID = dfa_doc.BPDOCUMENT_ID
  AND p1.ROLE_ID       = :dfa_role_recipient_id
  AND p1.CONTACT_ID    = :delo_contact_id
  )");

                if (_state_ids.Length > 0)
                {
                    builder.Append($@"
AND dfa_doc.STATE_ID in ({string.Join(", ", _state_ids)})");
                }

                if (ids == null || !ids.Any())
                {
                    builder.Append(@"
AND NOT EXISTS
  (SELECT 1
  FROM CBO_IDENTITYLIST p1
  INNER JOIN BO_PROTOTYPE p2
  ON p2.PROTOTYPE_ID = p1.PROTOTYPE_ID
  WHERE p1.OWNER_ID  = bo_bpdoc.IDENTITYEX_ID
  AND p2.ECLASS_ID   = :external_id
  )");
                }
                else
                {
                    builder.Append($@"
AND dfa_doc.BPDOCUMENT_ID IN ({string.Join(", ", ids)})");
                }

                return builder.Replace(Environment.NewLine, "\n").ToString().Trim();
            }
        }

        public async Task<IEnumerable<DataOutgoingDocumentDto>> LoadDeloDataDocuments(IEnumerable<long> ids)
        {
            await EnsureChached();

            var contact = await _package.GetContact(_options.Value.ContactsMapping["DeloExport"]);

            if (contact.Id == null)
            {
                throw new InvalidOperationException($"Не найден контакт {contact} необходимий для экспорта в систему 'Дело'.");
            }

            var result = await _repository.GetAllAsync<OutgoingDataDocumentRecordDto>(GenerateSql(), new
            {
                close_id = _close_id,
                external_id = _external_id,
                solution_id = _solution_id,
                function_id = _function_id,
                delo_contact_id = contact.Id,
                dfa_role_recipient_id = _dfa_role_recipient_id,
                delivery_kind_delo_id = _delivery_kind_delo_id,
                task_description = _options.Value.SignTaskFilter,
            });

            var files = await LoadFiles(result.Where(x => x.files_list != null)
                .SelectMany(x => x.files_list.Split(';')));

            var signers = LoadSigners(result.Where(x => x.signers_list != null)
                .SelectMany(x => x.signers_list.Split(';')));

            return result.Select(x => new DataOutgoingDocumentDto
            {
                Id = x.bpdocument_id,
                Signers = signers.TryGetValue(x.bpdocument_id, out var value)
                    ? value.Select(z => new OutgoingDocumentSignerDto
                    {
                        Id = z.key_identifier,
                        ContactId = z.contact_id,
                        Store = z.store,
                    }).ToArray()
                    : null,
                Document = (files.TryGetValue(x.bpdocument_id, out var file) ? file.FirstOrDefault() : null)?.Data,
            })
            .Where(x => x.Signers != null && x.Signers.Length > 0)
            .Where(x => x.Document != null && x.Document.Length > 0);

            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .Append($@"
SELECT dfa_doc.BPDOCUMENT_ID,
  (SELECT rtrim(xmlagg(xmlelement(E, p1.BPDOCUMENT_ID
    || ':'
    || p3.LOBSTORAGE_ID
    || ':'
    || p3.NAME, ';').extract('//text()')
  ORDER BY p1.NORD).GetClobVal(), ';')
  FROM CBO_FILE_LINK p1
  INNER JOIN CBO_FILE p2
  ON p2.FILE_ID = p1.FILE_ID
  INNER JOIN CBO_FILEVERSION p3
  ON p3.FILEVERSION_ID   = p2.CURRENTVERSION_ID
  WHERE p1.BPDOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  AND p1.DELETEDT       IS NULL
  ) AS files_list,
  (SELECT rtrim(xmlagg(xmlelement(E, s.PERFORMED_DOCUMENT_ID
    || ':'
    || s.EDITOR_ID
    || ':'
    || s.KEY_IDENTIFIER
    || ':'
    || s.STORE, ';').extract('//text()')
  ORDER BY s.EDITOR_ID).GetClobVal(), ';')
  FROM
    (SELECT ds.EDITOR_ID,
      p1.PERFORMED_DOCUMENT_ID,
      cert.KEY_IDENTIFIER,
      cert.STORE
    FROM WFA_TASK p1
    INNER JOIN DOC_STATE_HISTORY ds
    ON ds.OWNER_ID = p1.BPDOCUMENT_ID
    INNER JOIN CNT_CONTACT cnt
    ON ds.EDITOR_ID = cnt.CONTACT_ID
    INNER JOIN CBO_CERTIFICATE cert
    ON cert.CERTIFICATE_ID = cnt.CERTIFICATE_ID
    WHERE ds.BPFUNCTION_ID = :function_id
    AND (p1.SOLUTION_ID    = :solution_id
    OR p1.SOLUTION_ID     IS NULL)
    AND p1.STATE_ID        = :close_id
    AND p1.DESCRIPTION LIKE '%'
      || :task_description
      || '%'
    ORDER BY ds.CHANGE_TIME DESC
    ) s
  WHERE s.PERFORMED_DOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  AND rownum                    = 1
  ) AS signers_list
FROM DFA_DOCUMENT dfa_doc
INNER JOIN BO_BPDOCUMENT bo_bpdoc
ON bo_bpdoc.BPDOCUMENT_ID = dfa_doc.BPDOCUMENT_ID
INNER JOIN CBO_REFERENCE cbo_ref
ON cbo_ref.REFERENCE_ID        = dfa_doc.DOCUMENTTYPE_ID
WHERE bo_bpdoc.IDENTITYEX_ID IS NOT NULL
AND dfa_doc.DELIVERY_KIND_ID   = :delivery_kind_delo_id
AND EXISTS
  (SELECT 1
  FROM DFA_ADDRESSEE p1
  WHERE p1.DOCUMENT_ID = dfa_doc.BPDOCUMENT_ID
  AND p1.ROLE_ID       = :dfa_role_recipient_id
  AND p1.CONTACT_ID    = :delo_contact_id
  )");

                if (_state_ids.Length > 0)
                {
                    builder.Append($@"
AND dfa_doc.STATE_ID in ({string.Join(", ", _state_ids)})");
                }

                if (ids == null || !ids.Any())
                {
                    builder.Append(@"
AND NOT EXISTS
  (SELECT 1
  FROM CBO_IDENTITYLIST p1
  INNER JOIN BO_PROTOTYPE p2
  ON p2.PROTOTYPE_ID = p1.PROTOTYPE_ID
  WHERE p1.OWNER_ID  = bo_bpdoc.IDENTITYEX_ID
  AND p2.ECLASS_ID   = :external_id
  )");
                }
                else
                {
                    builder.Append($@"
AND dfa_doc.BPDOCUMENT_ID IN ({string.Join(", ", ids)})");
                }

                return builder.Replace(Environment.NewLine, "\n").ToString().Trim();
            }
        }

        public async Task<IEnumerable<MetaOutgoingDocumentDto>> LoadMetaDocuments(IEnumerable<long> ids)
        {
            await EnsureChached();

            var result = await _repository.GetAllAsync<OutgoingMetaDocumentRecordDto>(GenerateSql(), new
            {
                type_id = _type_id,
                close_id = _close_id,
                solution_id = _solution_id,
                delivery_kind_id = _delivery_kind_id,
                medo_external_guid_id = _medo_external_guid_id,
                function_id = _function_id,
                task_description = _options.Value.SignTaskFilter,
                outgoing_medo_id = _medo_out_docs_id,
            });

            var signers = LoadSigners(result.Where(x => x.signers_list != null)
                .SelectMany(x => x.signers_list.Split(';')));

            return result.Select(x => new MetaOutgoingDocumentDto
            {
                Id = x.bpdocument_id,
                Signers = signers.TryGetValue(x.bpdocument_id, out var value)
                    ? value.Select(z => new OutgoingDocumentSignerDto
                    {
                        Id = z.key_identifier,
                        ContactId = z.contact_id,
                        Store = z.store,
                    }).ToArray()
                    : null,
            }).Where(x => x.Signers != null && x.Signers.Length > 0);

            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .Append(@"
SELECT dfa_doc.BPDOCUMENT_ID,
  (SELECT rtrim(xmlagg(xmlelement(E, s.PERFORMED_DOCUMENT_ID
    || ':'
    || s.EDITOR_ID
    || ':'
    || s.KEY_IDENTIFIER
    || ':'
    || s.STORE, ';').extract('//text()')
  ORDER BY s.EDITOR_ID).GetClobVal(), ';')
  FROM
    (SELECT ds.EDITOR_ID,
      p1.PERFORMED_DOCUMENT_ID,
      cert.KEY_IDENTIFIER,
      cert.STORE
    FROM WFA_TASK p1
    INNER JOIN DOC_STATE_HISTORY ds
    ON ds.OWNER_ID = p1.BPDOCUMENT_ID
    INNER JOIN CNT_CONTACT cnt
    ON ds.EDITOR_ID = cnt.CONTACT_ID
    INNER JOIN CBO_CERTIFICATE cert
    ON cert.CERTIFICATE_ID = cnt.CERTIFICATE_ID
    WHERE ds.BPFUNCTION_ID = :function_id
    AND (p1.SOLUTION_ID    = :solution_id
    OR p1.SOLUTION_ID     IS NULL)
    AND p1.STATE_ID        = :close_id
    AND p1.DESCRIPTION LIKE '%'
      || :task_description
      || '%'
    ORDER BY ds.CHANGE_TIME DESC
    ) s
  WHERE s.PERFORMED_DOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  AND rownum                    = 1
  ) AS signers_list
FROM DFA_DOCUMENT dfa_doc
INNER JOIN BO_BPDOCUMENT bo_bpdoc
ON bo_bpdoc.BPDOCUMENT_ID = dfa_doc.BPDOCUMENT_ID
INNER JOIN CBO_REFERENCE cbo_ref
ON cbo_ref.REFERENCE_ID = dfa_doc.DOCUMENTTYPE_ID
INNER JOIN BO_PROTOTYPE bp_prot
ON bp_prot.PROTOTYPE_ID = bo_bpdoc.PROTOTYPE_ID
INNER JOIN BO_ECLASS bo_eclass
ON bo_eclass.ECLASS_ID        = bp_prot.ECLASS_ID
WHERE bo_bpdoc.IDENTITYEX_ID IS NOT NULL
AND bo_eclass.PARENT_ID       = :outgoing_medo_id
AND dfa_doc.DOCUMENTTYPE_ID   = :type_id
AND dfa_doc.DELIVERY_KIND_ID  = :delivery_kind_id");

                if (_state_ids.Length > 0)
                {
                    builder.Append($@"
AND dfa_doc.STATE_ID in ({string.Join(", ", _state_ids)})");
                }

                if (ids == null || !ids.Any())
                {
                    builder.Append(@"
AND NOT EXISTS
  (SELECT 1
  FROM CBO_IDENTITYLIST p1
  INNER JOIN BO_PROTOTYPE p2
  ON p2.PROTOTYPE_ID = p1.PROTOTYPE_ID
  WHERE p1.OWNER_ID  = bo_bpdoc.IDENTITYEX_ID
  AND p2.ECLASS_ID   = :medo_external_guid_id
  )");
                }
                else
                {
                    builder.Append($@"
AND dfa_doc.BPDOCUMENT_ID IN ({string.Join(", ", ids)})");
                }

                return builder.Replace(Environment.NewLine, "\n").ToString().Trim();
            }
        }

        public async Task<IEnumerable<DataOutgoingDocumentDto>> LoadDataDocuments(IEnumerable<long> ids)
        {
            await EnsureChached();

            var result = await _repository.GetAllAsync<OutgoingDataDocumentRecordDto>(GenerateSql(), new
            {
                type_id = _type_id,
                close_id = _close_id,
                solution_id = _solution_id,
                delivery_kind_id = _delivery_kind_id,
                medo_external_guid_id = _medo_external_guid_id,
                function_id = _function_id,
                task_description = _options.Value.SignTaskFilter,
                outgoing_medo_id = _medo_out_docs_id,
            });

            var files = await LoadFiles(result.Where(x => x.files_list != null)
                .SelectMany(x => x.files_list.Split(';')));

            var signers = LoadSigners(result.Where(x => x.signers_list != null)
                .SelectMany(x => x.signers_list.Split(';')));

            return result.Select(x => new DataOutgoingDocumentDto
            {
                Id = x.bpdocument_id,
                Signers = signers.TryGetValue(x.bpdocument_id, out var value)
                    ? value.Select(z => new OutgoingDocumentSignerDto
                    {
                        Id = z.key_identifier,
                        ContactId = z.contact_id,
                        Store = z.store,
                    }).ToArray()
                    : null,
                Document = (files.TryGetValue(x.bpdocument_id, out var file) ? file.FirstOrDefault() : null)?.Data,
            })
            .Where(x => x.Signers != null && x.Signers.Length > 0)
            .Where(x => x.Document != null && x.Document.Length > 0);

            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .Append($@"
SELECT dfa_doc.BPDOCUMENT_ID,
  (SELECT rtrim(xmlagg(xmlelement(E, p1.BPDOCUMENT_ID
    || ':'
    || p3.LOBSTORAGE_ID
    || ':'
    || p3.NAME, ';').extract('//text()')
  ORDER BY p1.NORD).GetClobVal(), ';')
  FROM CBO_FILE_LINK p1
  INNER JOIN CBO_FILE p2
  ON p2.FILE_ID = p1.FILE_ID
  INNER JOIN CBO_FILEVERSION p3
  ON p3.FILEVERSION_ID   = p2.CURRENTVERSION_ID
  WHERE p1.BPDOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  AND p1.DELETEDT       IS NULL
  ) AS files_list,
  (SELECT rtrim(xmlagg(xmlelement(E, s.PERFORMED_DOCUMENT_ID
    || ':'
    || s.EDITOR_ID
    || ':'
    || s.KEY_IDENTIFIER
    || ':'
    || s.STORE, ';').extract('//text()')
  ORDER BY s.EDITOR_ID).GetClobVal(), ';')
  FROM
    (SELECT ds.EDITOR_ID,
      p1.PERFORMED_DOCUMENT_ID,
      cert.KEY_IDENTIFIER,
      cert.STORE
    FROM WFA_TASK p1
    INNER JOIN DOC_STATE_HISTORY ds
    ON ds.OWNER_ID = p1.BPDOCUMENT_ID
    INNER JOIN CNT_CONTACT cnt
    ON ds.EDITOR_ID = cnt.CONTACT_ID
    INNER JOIN CBO_CERTIFICATE cert
    ON cert.CERTIFICATE_ID = cnt.CERTIFICATE_ID
    WHERE ds.BPFUNCTION_ID = :function_id
    AND (p1.SOLUTION_ID    = :solution_id
    OR p1.SOLUTION_ID     IS NULL)
    AND p1.STATE_ID        = :close_id
    AND p1.DESCRIPTION LIKE '%'
      || :task_description
      || '%'
    ORDER BY ds.CHANGE_TIME DESC
    ) s
  WHERE s.PERFORMED_DOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  AND rownum                    = 1
  ) AS signers_list
FROM DFA_DOCUMENT dfa_doc
INNER JOIN BO_BPDOCUMENT bo_bpdoc
ON bo_bpdoc.BPDOCUMENT_ID = dfa_doc.BPDOCUMENT_ID
INNER JOIN CBO_REFERENCE cbo_ref
ON cbo_ref.REFERENCE_ID = dfa_doc.DOCUMENTTYPE_ID
INNER JOIN BO_PROTOTYPE bp_prot
ON bp_prot.PROTOTYPE_ID = bo_bpdoc.PROTOTYPE_ID
INNER JOIN BO_ECLASS bo_eclass
ON bo_eclass.ECLASS_ID        = bp_prot.ECLASS_ID
WHERE bo_bpdoc.IDENTITYEX_ID IS NOT NULL
AND bo_eclass.PARENT_ID       = :outgoing_medo_id
AND dfa_doc.DOCUMENTTYPE_ID   = :type_id
AND dfa_doc.DELIVERY_KIND_ID  = :delivery_kind_id");

                if (_state_ids.Length > 0)
                {
                    builder.Append($@"
AND dfa_doc.STATE_ID in ({string.Join(", ", _state_ids)})");
                }

                if (ids == null || !ids.Any())
                {
                    builder.Append(@"
AND NOT EXISTS
  (SELECT 1
  FROM CBO_IDENTITYLIST p1
  INNER JOIN BO_PROTOTYPE p2
  ON p2.PROTOTYPE_ID = p1.PROTOTYPE_ID
  WHERE p1.OWNER_ID  = bo_bpdoc.IDENTITYEX_ID
  AND p2.ECLASS_ID   = :medo_external_guid_id
  )");
                }
                else
                {
                    builder.Append($@"
AND dfa_doc.BPDOCUMENT_ID IN ({string.Join(", ", ids)})");
                }

                return builder.Replace(Environment.NewLine, "\n").ToString().Trim();
            }
        }

        public async Task<IEnumerable<FullOutgoingDocumentDto>> LoadFullDocuments(IEnumerable<long> ids)
        {
            await EnsureChached();

            var result = await _repository.GetAllAsync<OutgoingFullDocumentRecordDto>(GenerateSql(), new
            {
                type_id = _type_id,
                close_id = _close_id,
                solution_id = _solution_id,
                delivery_kind_id = _delivery_kind_id,
                medo_external_guid_id = _medo_external_guid_id,
                function_id = _function_id,
                task_description = _options.Value.SignTaskFilter,
                task_coordinates_description = _options.Value.CoordinatesTaskFilter,
                outgoing_medo_id = _medo_out_docs_id,
            });

            var files = await LoadFiles(result.Where(x => x.files_list != null)
                .SelectMany(x => x.files_list.Split(';')));

            var dockets = await LoadFiles(result.Where(x => x.docket_id != null)
                .Select(x => $"{x.bpdocument_id}:{x.docket_id}:Docket"));

            var signers = LoadSigners(result.Where(x => x.signers_list != null)
                .SelectMany(x => x.signers_list.Split(';')));

            var addressee = LoadAddressee(result.Where(x => x.addressee_list != null)
                .SelectMany(x => x.addressee_list.Split(';')));

            var coordinates = LoadCoordinates(result.Where(x => x.coordinates_list != null)
                .SelectMany(x => x.coordinates_list.Split(':')));

            return result.Select(x =>
            {
                var item = new FullOutgoingDocumentDto
                {
                    Id = x.bpdocument_id,
                    IdentityEx = x.identityex_id,
                    Files = files.TryGetValue(x.bpdocument_id, out var file) ? file : Array.Empty<FileDto>(),
                    Container = new Container
                    {
                        Uid = Guid.NewGuid().ToString(),
                        Version = "1.0",
                        Document = new Document(),
                        Requisites = new ContainerRequisites
                        {
                            DocumentKind = new QualifiedValue
                            {
                                Value = _eclassMapping.TryGetValue(x.eclass_id, out var kind)
                                    ? kind
                                    : _options.Value.DefaultMedoOutgoingDocumentKind,
                            },
                            Annotation = dockets.TryGetValue(x.bpdocument_id, out var docket) ? docket[0].Text : null,
                        },
                    },
                };

                var recipients = (addressee.TryGetValue(x.bpdocument_id, out var value) ? value : default)
                    ?.Where(z => z.role_id == _dfa_role_recipient_id).ToArray() ?? Array.Empty<(long, long, string)>();

                var correspondent = (addressee.TryGetValue(x.bpdocument_id, out var value1) ? value1 : default)
                    ?.FirstOrDefault(z => z.role_id == _dfa_role_correspondent_id);

                var coordinateData = (coordinates.TryGetValue(x.bpdocument_id, out var value2) ? value2 : default)
                    ?.FirstOrDefault();

                var doc_signers = new (string signer_name, string signer_post)[]
                {
                    (string.Empty, correspondent?.name),
                };

                if (recipients.Length > 0)
                {
                    item.Receivers = _repository.GetAll<string>($@"
SELECT p1.MEDOADDRESS
  FROM MEDO_ADDRESS p1
  INNER JOIN CNT_ADDRESS p2
  ON p2.ADDRESS_ID = p1.ADDRESS_ID
  INNER JOIN CNT_CONTACT p3
  ON p3.CONTACT_ID   = p2.CONTACT_ID
  WHERE p3.CONTACT_ID in ({string.Join(", ", recipients.Select(z => z.contact_id))})").ToArray();
                }

                if (signers.TryGetValue(x.bpdocument_id, out var document_signers) && document_signers.Length > 0)
                {
                    item.Signers = document_signers.Select(z => new OutgoingDocumentSignerDto
                    {
                        Id = z.key_identifier,
                        ContactId = z.contact_id,
                        Store = z.store,
                    }).ToArray();

                    doc_signers = _repository.GetAll<(string, string)>(
                        $@"
SELECT 
  (e.LAST_NAME || ' ' || e.FIRST_NAME || ' ' || e.MIDDLE_NAME) AS Item1, 
  r.NAME AS Item2 
FROM CNT_EMPLOYEE e 
INNER JOIN CBO_REFERENCE r ON r.REFERENCE_ID = e.POSITION_ID 
WHERE e.CONTACT_ID IN {string.Join(", ", document_signers.Select(z => z.contact_id))}").ToArray();
                }
                else
                {
                    item.Signers = new[]
                    {
                        new OutgoingDocumentSignerDto
                        {
                            ContactId = correspondent?.contact_id ?? -1,
                        },
                    };
                }

                var organization = _repository.Get<(string name, string guid)>(
                    @"
SELECT m.SZI_NAME as Item1,
  m.GUID as Item2
FROM CNT_ADDRESS a
INNER JOIN CNT_CONTACT c
ON c.CONTACT_ID = a.CONTACT_ID
INNER JOIN MEDO_ADDRESS m
ON m.ADDRESS_ID          = a.ADDRESS_ID
WHERE a.BCLASS_INDICATOR = :medo_address_id
AND c.NAME               = :name",
                    new { name = _options.Value.ContactsMapping["Source"].Name, medo_address_id = _medo_address_id });

                if (organization.guid == null)
                {
                    var medo_kind = _package.FindEnumId("Medo.References", "MedoKind", "1").Result;
                    organization = _repository.GetAll<(string name, string guid)>(
                        @"
SELECT m.SZI_NAME as Item1,
  m.GUID as Item2
FROM MEDO_ADDRESS m
WHERE m.SZI_NAME = :name AND m.MEDOKIND_ID = :medo_kind",
                        new { name = _options.Value.ContactsMapping["Source"].Name, medo_kind })
                        .FirstOrDefault();
                }

                if (coordinateData.HasValue)
                {
                    item.Stamps = new StampsDataDto
                    {
                        RegistrationX = coordinateData.Value.reg_x,
                        RegistrationY = coordinateData.Value.reg_y,
                        SignatureX = coordinateData.Value.sign_x,
                        SignatureY = coordinateData.Value.sign_y,
                        SignaturePage = coordinateData.Value.sign_page,
                    };
                }

                item.AuthorGuid = organization.guid;

                item.Container.Authors.Add(new Issuer
                {
                    Organization = new Organization
                    {
                        Title = organization.name,
                    },
                    Registration = new Registration
                    {
                        Date = x.in_date,
                        Number = x.in_registration,
                        RegistrationStamp = new Stamp
                        {
                            Position = new Position
                            {
                                Page = "1",
                                TopLeft = new Coordinate(),
                                Dimension = new Dimension(),
                            },
                        },
                    },
                });

                item.Container.Authors.First().Sign.Add(new IssuerSign
                {
                    Person = new Signer
                    {
                        Name = doc_signers[0].signer_name,
                        Post = doc_signers[0].signer_post,
                    },
                    DocumentSignature = new SignatureInfo
                    {
                        SignatureStamp = new Stamp
                        {
                            Position = new Position
                            {
                                Page = "1",
                                TopLeft = new Coordinate(),
                                Dimension = new Dimension(),
                            },
                        },
                    },
                });

                var recipientsWithParent = document_signers.Length > 0
                    ? _repository.GetAll<(string parent_name, long? parent_id, long contact_id)>(
                        $@"
SELECT pc.NAME  AS Item1,
  ou.PARENT_ID  AS Item2,
  ou.CONTACT_ID AS Item3
FROM CNT_ORGUNIT ou
LEFT JOIN CNT_CONTACT pc
ON pc.CONTACT_ID    = ou.PARENT_ID
WHERE ou.CONTACT_ID IN ({string.Join(", ", document_signers.Select(z => z.contact_id))})").ToArray()
                    : Array.Empty<(string parent_name, long? parent_id, long contact_id)>();

                foreach (var (_, contact_id, name) in recipients)
                {
                    var recipient = recipientsWithParent.FirstOrDefault(z => z.contact_id == contact_id);

                    if (!(recipient.parent_id == null || string.IsNullOrEmpty(recipient.parent_name)))
                    {
                        item.Container.Addressees.Add(new ContainerAddresseesAddressee
                        {
                            Department = new QualifiedValue
                            {
                                Id = contact_id.ToString(),
                                Value = name,
                            },
                            Organization = new Organization
                            {
                                Id = recipient.parent_id.ToString(),
                                Title = recipient.parent_name,
                            },
                        });
                    }
                    else
                    {
                        item.Container.Addressees.Add(new ContainerAddresseesAddressee
                        {
                            Organization = new Organization
                            {
                                Title = name,
                            },
                        });
                    }
                }

                return item;
            });

            string GenerateSql()
            {
                var builder = new StringBuilder()
                    .Append(@"
SELECT dfa_doc.BPDOCUMENT_ID,
  dfa_doc.CONTACT_ID,
  dfa_doc.AUTHOR_ID,
  dfa_doc.INCLUDE_LIST,
  bo_bpdoc.IN_DATE,
  bo_bpdoc.IN_REGISTRATION,
  bo_bpdoc.IDENTITYEX_ID,
  bo_bpdoc.NAME,
  bo_bpdoc.DOCKET_ID,
  bo_bpdoc.ECLASS_ID,
  cbo_ref.NAME AS TYPE_NAME,
  (SELECT rtrim(xmlagg(xmlelement(E, p1.OWNER_ID
    || ':'
    || p2.ECLASS_ID
    || ':'
    || p1.IDENTITY_CODE, ';').extract('//text()')
  ORDER BY p2.ECLASS_ID).GetClobVal(), ';')
  FROM CBO_IDENTITYLIST p1
  INNER JOIN BO_PROTOTYPE p2
  ON p2.PROTOTYPE_ID = p1.PROTOTYPE_ID
  WHERE p1.OWNER_ID  = bo_bpdoc.IDENTITYEX_ID
  ) AS identity_list,
  (SELECT rtrim(xmlagg(xmlelement(E, p1.DOCUMENT_ID
    || ':'
    || p1.ROLE_ID
    || ':'
    || p1.CONTACT_ID
    || ':'
    || p2.NAME, ';').extract('//text()')
  ORDER BY p1.ROLE_ID).GetClobVal(), ';')
  FROM DFA_ADDRESSEE p1
  INNER JOIN CNT_CONTACT p2
  ON p2.CONTACT_ID     = p1.CONTACT_ID
  WHERE p1.DOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  ) AS addressee_list,
  (SELECT rtrim(xmlagg(xmlelement(E, p1.BPDOCUMENT_ID
    || ':'
    || p3.LOBSTORAGE_ID
    || ':'
    || p3.NAME, ';').extract('//text()')
  ORDER BY p1.NORD).GetClobVal(), ';')
  FROM CBO_FILE_LINK p1
  INNER JOIN CBO_FILE p2
  ON p2.FILE_ID = p1.FILE_ID
  INNER JOIN CBO_FILEVERSION p3
  ON p3.FILEVERSION_ID   = p2.CURRENTVERSION_ID
  WHERE p1.BPDOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  AND p1.DELETEDT       IS NULL
  ) AS files_list,
  (SELECT rtrim(xmlagg(xmlelement(E, s.PERFORMED_DOCUMENT_ID
    || ':'
    || s.EDITOR_ID
    || ':'
    || s.KEY_IDENTIFIER
    || ':'
    || s.STORE, ';').extract('//text()')
  ORDER BY s.EDITOR_ID).GetClobVal(), ';')
  FROM
    (SELECT ds.EDITOR_ID,
      p1.PERFORMED_DOCUMENT_ID,
      cert.KEY_IDENTIFIER,
      cert.STORE
    FROM WFA_TASK p1
    INNER JOIN DOC_STATE_HISTORY ds
    ON ds.OWNER_ID = p1.BPDOCUMENT_ID
    INNER JOIN CNT_CONTACT cnt
    ON ds.EDITOR_ID = cnt.CONTACT_ID
    INNER JOIN CBO_CERTIFICATE cert
    ON cert.CERTIFICATE_ID = cnt.CERTIFICATE_ID
    WHERE ds.BPFUNCTION_ID = :function_id
    AND (p1.SOLUTION_ID    = :solution_id
    OR p1.SOLUTION_ID     IS NULL)
    AND p1.STATE_ID        = :close_id
    AND p1.DESCRIPTION LIKE '%'
      || :task_description
      || '%'
    ORDER BY ds.CHANGE_TIME DESC
    ) s
  WHERE s.PERFORMED_DOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  AND rownum                    = 1
  ) AS signers_list,
  (SELECT rtrim(xmlagg(xmlelement(E, s.PERFORMED_DOCUMENT_ID
    || ';'
    || s.REPORT, ':').extract('//text()')).GetClobVal(), ':')
  FROM
    (SELECT p1.PERFORMED_DOCUMENT_ID,
      p1.REPORT
    FROM WFA_TASK p1
    WHERE p1.STATE_ID = :close_id
    AND p1.DESCRIPTION LIKE '%'
      || :task_coordinates_description
      || '%'
    ORDER BY p1.CREATE_DATE DESC
    ) s
  WHERE s.PERFORMED_DOCUMENT_ID = bo_bpdoc.BPDOCUMENT_ID
  AND rownum                    = 1
  ) AS coordinates_list
FROM DFA_DOCUMENT dfa_doc
INNER JOIN BO_BPDOCUMENT bo_bpdoc
ON bo_bpdoc.BPDOCUMENT_ID = dfa_doc.BPDOCUMENT_ID
INNER JOIN CBO_REFERENCE cbo_ref
ON cbo_ref.REFERENCE_ID = dfa_doc.DOCUMENTTYPE_ID
INNER JOIN BO_PROTOTYPE bp_prot
ON bp_prot.PROTOTYPE_ID = bo_bpdoc.PROTOTYPE_ID
INNER JOIN BO_ECLASS bo_eclass
ON bo_eclass.ECLASS_ID        = bp_prot.ECLASS_ID
WHERE bo_bpdoc.IDENTITYEX_ID IS NOT NULL
AND bo_eclass.PARENT_ID       = :outgoing_medo_id
AND dfa_doc.DOCUMENTTYPE_ID   = :type_id
AND dfa_doc.DELIVERY_KIND_ID  = :delivery_kind_id");

                if (_state_ids.Length > 0)
                {
                    builder.Append($@"
AND dfa_doc.STATE_ID in ({string.Join(", ", _state_ids)})");
                }

                if (ids == null || !ids.Any())
                {
                    builder.Append(@"
AND NOT EXISTS
  (SELECT 1
  FROM CBO_IDENTITYLIST p1
  INNER JOIN BO_PROTOTYPE p2
  ON p2.PROTOTYPE_ID = p1.PROTOTYPE_ID
  WHERE p1.OWNER_ID  = bo_bpdoc.IDENTITYEX_ID
  AND p2.ECLASS_ID   = :medo_external_guid_id
  )");
                }
                else
                {
                    builder.Append($@"
AND dfa_doc.BPDOCUMENT_ID IN ({string.Join(", ", ids)})");
                }

                return builder.Replace(Environment.NewLine, "\n").ToString().Trim();
            }
        }

        private async Task EnsureChached()
        {
            // Thread race is ok, it just loads ids twice.
            if (_isCached)
            {
                return;
            }

            _close_id = await _package.FindReferenceItemId("Workflow.References", "TaskState", "Close");

            _function_id = await _repository.GetAsync<int>(
                "select BPFUNCTION_ID from BO_BPFUNCTION where name = :name",
                new { Name = "WFA.ExecuteTaskCard" });

            var state_ids = _options.Value.ExportInStatuses.Length == 0
                            ? Array.Empty<long>()
                            : new long[_options.Value.ExportInStatuses.Length];

            for (int i = 0; i < state_ids.Length; i++)
            {
                state_ids[i] = await _package.FindReferenceItemId(
                    "Docflow.References",
                    "DocumentState",
                    _options.Value.ExportInStatuses[i]);
            }

            _state_ids = state_ids;

            _type_id = await _package.FindReferenceItemId("Docflow.References", "DocumentType", "OutgoingDocument");
            _solution_id = await _package.FindEnumId("Docflow.References", "SolutionKind", "APPROVE");
            _medo_address_id = await _package.FindBClassId("Medo.Contacts", "Address");
            _delivery_kind_id = await _package.FindReferenceItemId("Contacts", "DeliveryKind", "MEDO");
            _delivery_kind_delo_id = await _package.FindReferenceItemId("Contacts", "DeliveryKind", "Delo");
            _medo_external_guid_id = await _package.FindEClassId("Medo.References", "ExternalGUID");
            _external_id = await _package.FindEClassId("Common", "ExternalSystemIdentity");
            _dfa_role_recipient_id = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Recipient");
            _dfa_role_addressee_id = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Addressee");
            _dfa_role_correspondent_id = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Correspondent");
            _medo_out_docs_id = await _package.FindEClassId("Docflow.References", "MEDO_OUT_DOCS");

            if (_options.Value.EClassMapping?.Count > 0)
            {
                foreach (var item in _options.Value.EClassMapping)
                {
                    var eclassParts = item.Key.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                    if (eclassParts.Length == 2)
                    {
                        try
                        {
                            var eclassId = await _package.FindEClassId(eclassParts[0], eclassParts[1]);

                            _eclassMapping[eclassId] = item.Value;
                        }
                        catch (Exception exp)
                        {
                            _logger.Warning($"Error while loading EClassMapping {item}.");
                            _logger.Error(exp);
                        }
                    }
                    else
                    {
                        _logger.Warning($"Wrong format of EClassMapping {item}.");
                    }
                }
            }

            _isCached = true;
        }

        private async Task<Dictionary<long, FileDto[]>> LoadFiles(IEnumerable<string> data)
        {
            if (!data.Any())
            {
                return new Dictionary<long, FileDto[]>();
            }

            var parsedData = data.Select(x => x.Split(':'))
                .Where(x => x.Length == 3)
                .Select(x => (long.TryParse(x[0], out var tmp) ? tmp : -1, long.TryParse(x[1], out tmp) ? tmp : -1, x[2]))
                .Where(x => x.Item1 > 0 && x.Item2 > 0);

            var dictionary01 = parsedData.GroupBy(x => x.Item2)
                .ToDictionary(x => x.Key, x => x.First().Item3);

            var dictionary02 = (await _repository.GetAllAsync<(long, byte[], string)>(GenerateSql()))
                .ToDictionary(x => x.Item1, x => new FileDto { FileName = dictionary01[x.Item1], Data = x.Item2, Text = x.Item3 });

            return parsedData.GroupBy(x => x.Item1).ToDictionary(x => x.Key, x => x.Select(z => dictionary02[z.Item2]).ToArray());

            string GenerateSql()
            {
                var builder = new StringBuilder()
   .AppendLine($@"
SELECT a.LOBSTORAGE_ID AS Item1,
  a.X_BLOB             AS Item2,
  a.X_MEMO             AS Item3
FROM BO_LOBSTORAGE a
WHERE a.LOBSTORAGE_ID IN ({string.Join(", ", dictionary01.Keys)})");
                return builder.Replace(Environment.NewLine, "\n").ToString().Trim();
            }
        }

        private Dictionary<long, string[]> LoadSubjects(IEnumerable<string> data)
        {
            return data.Select(x => x.Split(':'))
                .Where(x => x.Length == 2)
                .Select(x => (long.TryParse(x[0], out var tmp) ? tmp : -1, x[1]))
                .Where(x => x.Item1 > 0).GroupBy(x => x.Item1)
                .ToDictionary(x => x.Key, x => x.Select(z => z.Item2).ToArray());
        }

        private Dictionary<long, (long role_id, long contact_id, string name)[]> LoadAddressee(IEnumerable<string> data)
        {
            return data.Select(x => x.Split(':'))
                .Where(x => x.Length == 4)
                .Select(x => (long.TryParse(x[0], out var tmp) ? tmp : -1, long.TryParse(x[1], out tmp) ? tmp : -1, long.TryParse(x[2], out tmp) ? tmp : -1, x[3]))
                .Where(x => x.Item1 > 0 && x.Item2 > 0).GroupBy(x => x.Item1)
                .ToDictionary(x => x.Key, x => x.Select(z => (z.Item2, z.Item3, z.Item4)).ToArray());
        }

        private Dictionary<long, (long contact_id, string key_identifier, string store)[]> LoadSigners(IEnumerable<string> data)
        {
            return data.Select(x => x.Split(':'))
                .Where(x => x.Length == 4)
                .Select(x => (long.TryParse(x[0], out var tmp) ? tmp : -1, long.TryParse(x[1], out tmp) ? tmp : -1, x[2], x[3]))
                .Where(x => x.Item1 > 0 && x.Item2 > 0).GroupBy(x => x.Item1)
                .ToDictionary(x => x.Key, x => x.Select(z => (z.Item2, z.Item3, z.Item4)).ToArray());
        }

        private Dictionary<long, (int reg_x, int reg_y, int sign_x, int sign_y, int sign_page)[]> LoadCoordinates(IEnumerable<string> data)
        {
            return data.Select(x => x.Split(';'))
                .Where(x => x.Length == 6)
                .Select(x => (long.TryParse(x[0], out var tmp) ? tmp : -1, int.TryParse(x[1], out var tmp1) ? tmp1 : -1, int.TryParse(x[2], out tmp1) ? tmp1 : -1, int.TryParse(x[3], out tmp1) ? tmp1 : -1, int.TryParse(x[4], out tmp1) ? tmp1 : -1, int.TryParse(x[5], out tmp1) ? tmp1 : -1))
                .Where(x => x.Item1 > 0 && x.Item2 > 0 && x.Item3 > 0 && x.Item4 > 0 && x.Item5 > 0 && x.Item6 > 0).GroupBy(x => x.Item1)
                .ToDictionary(x => x.Key, x => x.Select(z => (z.Item2, z.Item3, z.Item4, z.Item5, z.Item6)).ToArray());
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Story
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using Refit;

    public interface IDeloWebApi
    {
        [Get("/Delo/GetOutgoingDocuments")]
        Task<IEnumerable<DeloOutgoingDocumentDto>> GetOutgoingDocuments();

        [Get("/Delo/GetOutgoingDocumentsMeta")]
        Task<IEnumerable<MetaOutgoingDocumentDto>> GetOutgoingDocumentsMeta();

        [Post("/Delo/GetOutgoingDocumentsData")]
        Task<IEnumerable<DataOutgoingDocumentDto>> GetOutgoingDocumentsData([Body]IEnumerable<long> @params);

        [Post("/Delo/ImportDocument")]
        Task<OperationResultDto> ImportDocument([Body]DeloIncomingDocumentDto @params);

        [Post("/Delo/ExportDocument")]
        Task<OperationResultDto> ExportDocument([Body]OutgoinDocumentDto @params);
    }
}

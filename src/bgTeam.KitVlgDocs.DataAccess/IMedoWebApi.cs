﻿namespace bgTeam.KitVlgDocs.Story
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using Refit;

    public interface IMedoWebApi
    {
        [Get("/Medo/GetOutgoingDocuments")]
        Task<IEnumerable<FullOutgoingDocumentDto>> GetOutgoingDocuments();

        [Get("/Medo/GetOutgoingDocumentsMeta")]
        Task<IEnumerable<MetaOutgoingDocumentDto>> GetOutgoingDocumentsMeta();

        [Post("/Medo/GetOutgoingDocumentsData")]
        Task<IEnumerable<DataOutgoingDocumentDto>> GetOutgoingDocumentsData([Body]IEnumerable<long> @params);

        [Post("/Medo/ExportDocument")]
        Task<OperationResultDto> ExportDocument([Body]OutgoinDocumentDto @params);

        [Post("/Medo/ImportDocument")]
        Task<OperationResultDto> ImportDocument([Body]IncomingDocumentDto @params);
    }
}

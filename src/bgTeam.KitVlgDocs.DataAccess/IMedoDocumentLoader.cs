﻿namespace bgTeam.KitVlgDocs.DataAccess
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.Domain.Dto;

    public interface IMedoDocumentLoader
    {
        Task<IEnumerable<DeloOutgoingDocumentDto>> LoadDeloDocuments(IEnumerable<long> ids);

        Task<IEnumerable<MetaOutgoingDocumentDto>> LoadDeloMetaDocuments(IEnumerable<long> ids);

        Task<IEnumerable<DataOutgoingDocumentDto>> LoadDeloDataDocuments(IEnumerable<long> ids);

        Task<IEnumerable<MetaOutgoingDocumentDto>> LoadMetaDocuments(IEnumerable<long> ids);

        Task<IEnumerable<DataOutgoingDocumentDto>> LoadDataDocuments(IEnumerable<long> ids);

        Task<IEnumerable<FullOutgoingDocumentDto>> LoadFullDocuments(IEnumerable<long> ids);
    }
}

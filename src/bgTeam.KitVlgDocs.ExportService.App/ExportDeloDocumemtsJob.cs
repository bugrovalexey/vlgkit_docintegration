﻿using bgTeam.KitVlgDocs.Story;
using Quartz;
using System;
using System.Threading.Tasks;

namespace bgTeam.KitVlgDocs.ExportService.App
{
    [DisallowConcurrentExecution]
    internal class ExportDeloDocumemtsJob : IJob
    {
        private readonly IStoryBuilder _storyBuilder;

        public ExportDeloDocumemtsJob(IStoryBuilder storyBuilder)
        {
            _storyBuilder = storyBuilder;
        }

        public Task Execute(IJobExecutionContext context)
        {
            return _storyBuilder.Build(new ClientExportDeloDocumentsStoryContext())
                .ReturnAsync<ValueTuple>();
        }
    }
}

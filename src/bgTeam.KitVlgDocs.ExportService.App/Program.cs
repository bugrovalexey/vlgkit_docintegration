﻿using bgTeam.KitVlgDocs.Common;
using bgTeam.KitVlgDocs.DataAccess;
using bgTeam.KitVlgDocs.Service;
using bgTeam.KitVlgDocs.Service.Quartz;
using bgTeam.KitVlgDocs.Story;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Refit;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.KitVlgDocs.ExportService.App
{
    internal static class Program
    {
        internal static Task Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            var isService = !(Debugger.IsAttached || args.Contains("--console"));

            var builder = BuildHost(args);

            return isService
                ? builder.RunAsServiceAsync()
                : builder.RunConsoleAsync();
        }

        private static IHostBuilder BuildHost(string[] args)
        {
            return new HostBuilder()
                .UseBgTeamLogging()
                .UseKitVlgDocsLogging(AppConstants.Version)
                .UseBgTeamConfiguration(args)
                .UseBgTeamQueries<IQueryLibrary>()
                .UseBgTeamStories<IStoryLibrary>()
                .UseQuartz(null, x =>
                {
                    x.AddJob<ExportDocumemtsJob>(nameof(ExportDocumemtsJob));
                    x.AddJob<ExportDeloDocumemtsJob>(nameof(ExportDeloDocumemtsJob));
                })
                .ConfigureServices((context, x) =>
                {
                    x
                    .Configure<ExportAppOptions>(context.Configuration)
                    .AddRefitClient<IMedoWebApi>()
                        .ConfigureHttpMessageHandlerBuilder(z =>
                        {
                            z.PrimaryHandler = new HttpClientHandler()
                            {
                                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
                            };
                        })
                        .ConfigureHttpClient(c => c.BaseAddress = context.Configuration.GetValue<Uri>("ApiUri"));

                    x.AddRefitClient<IDeloWebApi>()
                        .ConfigureHttpMessageHandlerBuilder(z =>
                        {
                            z.PrimaryHandler = new HttpClientHandler()
                            {
                                AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip
                            };
                        })
                        .ConfigureHttpClient(c => c.BaseAddress = context.Configuration.GetValue<Uri>("ApiUri"));
                });
        }
    }
}

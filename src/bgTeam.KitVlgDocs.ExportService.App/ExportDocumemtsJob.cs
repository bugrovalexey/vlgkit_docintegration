﻿using bgTeam.KitVlgDocs.Story;
using Quartz;
using System;
using System.Threading.Tasks;

namespace bgTeam.KitVlgDocs.ExportService.App
{
    [DisallowConcurrentExecution]
    internal class ExportDocumemtsJob : IJob
    {
        private readonly IStoryBuilder _storyBuilder;

        public ExportDocumemtsJob(IStoryBuilder storyBuilder)
        {
            _storyBuilder = storyBuilder;
        }

        public Task Execute(IJobExecutionContext context)
        {
            return _storyBuilder.Build(new ClientExportMedoDocumentsStoryContext())
                .ReturnAsync<ValueTuple>();
        }
    }
}

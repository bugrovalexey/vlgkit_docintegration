﻿namespace bgTeam.KitVlgDocs.Service
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using bgTeam.Core;
    using bgTeam.KitVlgDocs.Service.Hosting;
    using bgTeam.KitVlgDocs.Service.Quartz;
    using bgTeam.KitVlgDocs.Service.Utilities;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;

    public static class HostBuilderExtnsions
    {
        public static IHostBuilder UseServiceBaseLifetime(this IHostBuilder hostBuilder)
        {
            return hostBuilder.ConfigureServices((hostContext, services) => services.AddSingleton<IHostLifetime, ServiceBaseLifetime>());
        }

        public static Task RunAsServiceAsync(this IHostBuilder hostBuilder, CancellationToken cancellationToken = default)
        {
            return hostBuilder.UseServiceBaseLifetime().Build().RunAsync(cancellationToken);
        }

        public static IHostBuilder UseQuartz(this IHostBuilder builder, string jobsSection, Action<IQuartzConfiguration> configure = default)
        {
            return builder
                .ConfigureServices((context, x) =>
                {
                    var configuration = string.IsNullOrWhiteSpace(jobsSection) || jobsSection.Length == 0
                        ? context.Configuration
                        : context.Configuration.GetSection(jobsSection);

                    x.AddQuartz(configuration, configure);
                });
        }

        public static IHostBuilder UseBgTeamLogging(this IHostBuilder builder)
        {
            return builder
                .ConfigureServices((context, x) =>
                {
                    x.AddSingleton<IAppLogger, AppLogger>();
                });
        }

        public static IHostBuilder UseKitVlgDocsLogging(this IHostBuilder builder, string version)
        {
            return builder.ConfigureLogging((context, x) => x.ClearProviders().AddConfiguration(context.Configuration).AddKitVlgDocsLogging(context.Configuration, version));
        }

        public static IHostBuilder UseBgTeamStories<TStoryLibrary>(this IHostBuilder builder)
        {
            return builder.ConfigureServices((context, x) => x.AddBgTeamStories<TStoryLibrary>());
        }

        public static IHostBuilder UseBgTeamQueries<TQueryLibrary>(this IHostBuilder builder)
        {
            return builder.ConfigureServices((context, x) => x.AddBgTeamQueries<TQueryLibrary>());
        }

        public static IHostBuilder UseBgTeamConnectionSetting(this IHostBuilder builder, Func<IConfiguration, string> factory)
        {
            return builder.ConfigureServices((context, x) => x.AddBgTeamConnectionSetting(factory));
        }

        public static IHostBuilder UseBgTeamConfiguration(this IHostBuilder builder, string[] args, string rootPath = null)
        {
            return builder
                .ConfigureHostConfiguration(x =>
                {
                    x.SetBasePath(rootPath ?? Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                        .AddJsonFile("hostsettings.json", optional: true)
                        .AddJsonFile("appsettings.json", optional: true)
                        .AddCommandLine(args)
                        .AddEnvironmentVariables();
                })
                .ConfigureAppConfiguration((context, x) =>
                {
                    x.SetBasePath(rootPath ?? Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                        .AddJsonFile("appsettings.json", optional: true);

                    var hostSettings = context.Configuration.Get<SettingsProxy>();

                    if (!string.IsNullOrEmpty(hostSettings.AppEnvironment))
                    {
                        x.AddJsonFile($"appsettings.{hostSettings.AppEnvironment}.json", optional: true);

                        foreach (var item in hostSettings.AppConfigsAdditional?.Split(',') ?? Array.Empty<string>())
                        {
                            var path = Path.Combine(hostSettings.AppConfigsPath ?? string.Empty, $"{item}.{hostSettings.AppEnvironment}.json");
                            x.AddJsonFile(path, optional: true);
                        }
                    }

                    x.AddCommandLine(args).AddEnvironmentVariables();
                })
                .ConfigureServices((context, x) =>
                {
                    x.AddSingleton<IAppConfiguration, AppConfiguratin>();
                });
        }
    }
}

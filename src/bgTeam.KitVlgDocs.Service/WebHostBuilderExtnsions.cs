﻿namespace bgTeam.KitVlgDocs.Service
{
    using System;
    using System.IO;
    using bgTeam.Core;
    using bgTeam.KitVlgDocs.Service.Utilities;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    public static class WebHostBuilderExtnsions
    {
        public static IWebHostBuilder UseBgTeamLogging(this IWebHostBuilder builder)
        {
            return builder
                .ConfigureServices((context, x) =>
                {
                    x.AddSingleton<IAppLogger, AppLogger>();
                });
        }

        public static IWebHostBuilder UseKitVlgDocsLogging(this IWebHostBuilder builder, string version)
        {
            return builder
                .ConfigureLogging((context, x) => x.ClearProviders().AddConfiguration(context.Configuration).AddKitVlgDocsLogging(context.Configuration, version))
                .Configure(x => x.UseMiddleware<SerilogRemoteIpLoggingMiddleware>());
        }

        public static IWebHostBuilder UseBgTeamExceptionMiddleware(this IWebHostBuilder builder)
        {
            return builder.Configure(x => x.UseMiddleware<AppExceptionMiddleware>());
        }

        public static IWebHostBuilder UseBgTeamStories<TStoryLibrary>(this IWebHostBuilder builder)
        {
            return builder.ConfigureServices((context, x) => x.AddBgTeamStories<TStoryLibrary>());
        }

        public static IWebHostBuilder UseBgTeamQueries<TQueryLibrary>(this IWebHostBuilder builder)
        {
            return builder.ConfigureServices((context, x) => x.AddBgTeamQueries<TQueryLibrary>());
        }

        public static IWebHostBuilder UseBgTeamConnectionSetting(this IWebHostBuilder builder, Func<IConfiguration, string> factory)
        {
            return builder.ConfigureServices((context, x) => x.AddBgTeamConnectionSetting(factory));
        }

        public static IWebHostBuilder UseBgTeamConfiguration(this IWebHostBuilder builder, string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("hostsettings.json", optional: true)
                .AddJsonFile("appsettings.json", optional: true)
                .AddCommandLine(args)
                .AddEnvironmentVariables()
                .Build();

            return builder
                .UseConfiguration(configuration)
                .ConfigureAppConfiguration((context, x) =>
                {
                    x.AddJsonFile("appsettings.json", optional: true);

                    var hostSettings = context.Configuration.Get<SettingsProxy>();

                    var environment = hostSettings.AppEnvironment ?? context.HostingEnvironment.EnvironmentName;

                    if (!string.IsNullOrEmpty(environment))
                    {
                        x.AddJsonFile($"appsettings.{environment}.json", optional: true);

                        foreach (var item in hostSettings.AppConfigsAdditional?.Split(',') ?? Array.Empty<string>())
                        {
                            var path = Path.Combine(hostSettings.AppConfigsPath ?? string.Empty, $"{item}.{environment}.json");
                            x.AddJsonFile(path, optional: true);
                        }
                    }

                    x.AddCommandLine(args).AddEnvironmentVariables();
                })
                .ConfigureServices((context, x) =>
                {
                    x.AddSingleton<IAppConfiguration, AppConfiguratin>();
                });
        }
    }
}

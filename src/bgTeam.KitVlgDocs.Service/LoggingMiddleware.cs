﻿namespace bgTeam.KitVlgDocs.Service
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Serilog.Context;

    public class SerilogRemoteIpLoggingMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            using (LogContext.PushProperty(LoggingBuilderExtension.PropertyName, context.Connection.RemoteIpAddress.ToString()))
            {
                await next(context);
            }
        }
    }
}

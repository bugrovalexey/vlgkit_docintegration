﻿namespace bgTeam.KitVlgDocs.Service.Quartz
{
    using global::Quartz;
    using global::Quartz.Impl;

    public static class QuartzBuilderExtension
    {
        public static IQuartzConfiguration UseSdtShedullerFactory(this IQuartzConfiguration builder)
        {
            return builder.UseShedullerFactory<StdSchedulerFactory>();
        }

        public static IQuartzConfiguration UseServiceProviderJobFactory(this IQuartzConfiguration builder)
        {
            return builder.UseJobFactory<ServiceProviderJobFactory>();
        }

        public static IQuartzConfiguration AddJob<T>(this IQuartzConfiguration builder, string identity)
            where T : IJob
        {
            return builder.AddJob<T>(identity, builder.Configuration);
        }
    }
}

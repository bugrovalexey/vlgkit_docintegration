﻿namespace bgTeam.KitVlgDocs.Service.Quartz
{
    using System;
    using global::Quartz;
    using global::Quartz.Spi;

    public class ServiceProviderJobFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public ServiceProviderJobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <inheritdoc/>
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return _serviceProvider.GetService(bundle.JobDetail.JobType) as IJob;
        }

        /// <inheritdoc/>
        public void ReturnJob(IJob job)
        {
            // Method intentionally left empty.
        }
    }
}

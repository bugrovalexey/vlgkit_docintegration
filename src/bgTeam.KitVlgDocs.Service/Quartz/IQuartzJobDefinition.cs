﻿namespace bgTeam.KitVlgDocs.Service.Quartz
{
    using System;

    public interface IQuartzJobDefinition
    {
        Type JobType { get; }

        string Identity { get; }
    }
}

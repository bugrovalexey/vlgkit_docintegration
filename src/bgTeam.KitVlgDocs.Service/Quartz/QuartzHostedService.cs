﻿namespace bgTeam.KitVlgDocs.Service.Quartz
{
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using global::Quartz;
    using global::Quartz.Spi;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;

    internal class QuartzHostedService : IHostedService
    {
        private readonly ILogger<QuartzHostedService> _logger;
        private readonly IOptionsSnapshot<JobConfiguration> _jobsOptions;

        private readonly IJobFactory _jobFactory;
        private readonly ISchedulerFactory _schedulerFactory;
        private readonly IEnumerable<IQuartzJobDefinition> _jobDefinitions;

        private IScheduler _scheduler;

        public QuartzHostedService(
            ILogger<QuartzHostedService> logger,
            IOptionsSnapshot<JobConfiguration> jobsOptions,
            IJobFactory jobFactory,
            ISchedulerFactory schedulerFactory,
            IEnumerable<IQuartzJobDefinition> jobDefinitions)
        {
            _logger = logger;
            _jobsOptions = jobsOptions;
            _jobFactory = jobFactory;
            _schedulerFactory = schedulerFactory;
            _jobDefinitions = jobDefinitions;
        }

        /// <inheritdoc/>
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"{nameof(QuartzHostedService)} starting.");

            _scheduler = await _schedulerFactory.GetScheduler(cancellationToken);

            _scheduler.JobFactory = _jobFactory;

            foreach (var item in _jobDefinitions)
            {
                await ScheduleJob(item, cancellationToken);
            }

            await _scheduler.Start(cancellationToken);
        }

        /// <inheritdoc/>
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"{nameof(QuartzHostedService)} stopping.");
            return _scheduler.Shutdown(cancellationToken);
        }

        private Task ScheduleJob(IQuartzJobDefinition jobDefinition, CancellationToken cancellationToken = default)
        {
            var configuration = _jobsOptions.Get(jobDefinition.Identity);

            if (configuration.Enabled)
            {
                var jobDetail = JobBuilder.Create(jobDefinition.JobType)
                    .WithIdentity(jobDefinition.Identity)
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .ForJob(jobDetail)
                    .WithCronSchedule(configuration.Cron)
                    .WithIdentity(jobDefinition.Identity)
                    .StartNow()
                    .Build();

                return _scheduler.ScheduleJob(jobDetail, trigger, cancellationToken);
            }
            else
            {
                return Task.CompletedTask;
            }
        }
    }
}

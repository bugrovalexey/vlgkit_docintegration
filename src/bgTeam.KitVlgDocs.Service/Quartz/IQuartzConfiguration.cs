﻿namespace bgTeam.KitVlgDocs.Service.Quartz
{
    using global::Quartz;
    using global::Quartz.Spi;
    using Microsoft.Extensions.Configuration;

    public interface IQuartzConfiguration
    {
        IConfiguration Configuration { get; }

        IQuartzConfiguration UseJobFactory<T>()
            where T : IJobFactory;

        IQuartzConfiguration UseShedullerFactory<T>()
            where T : ISchedulerFactory;

        IQuartzConfiguration AddJob<T>(string identity, IConfiguration configuration)
            where T : IJob;
    }
}

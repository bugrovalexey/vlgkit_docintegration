﻿namespace bgTeam.KitVlgDocs.Service.Quartz
{
    public class JobConfiguration
    {
        public string Cron { get; set; }

        public bool Enabled { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Service
{
    using System;
    using System.Collections.Generic;
    using bgTeam.DataAccess;
    using bgTeam.DataAccess.Impl;
    using bgTeam.Impl;
    using bgTeam.KitVlgDocs.Service.Quartz;
    using global::Quartz;
    using global::Quartz.Impl;
    using global::Quartz.Spi;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;

    public static class ServiceCollectionExtension
    {
        private class QuartzConfiguration : IQuartzConfiguration
        {
            public Type JobFactoryType { get; private set; }

            public Type SchedulerFactoryType { get; private set; }

            public Dictionary<string, (Type JobType, string Identity, IConfiguration Configuration)> Jobs { get; }

            public IConfiguration Configuration { get; }

            public QuartzConfiguration(IConfiguration configuration)
            {
                Configuration = configuration;

                Jobs = new Dictionary<string, (Type JobType, string Identity, IConfiguration Configuration)>();
            }

            public IQuartzConfiguration AddJob<T>(string identity, IConfiguration configuration)
                where T : IJob
            {
                Jobs.Add(identity, (typeof(T), identity, configuration));

                return this;
            }

            public IQuartzConfiguration UseJobFactory<T>()
                where T : IJobFactory
            {
                JobFactoryType = typeof(T);
                return this;
            }

            public IQuartzConfiguration UseShedullerFactory<T>()
                where T : ISchedulerFactory
            {
                SchedulerFactoryType = typeof(T);
                return this;
            }
        }

        private class QuartzJobDefinition : IQuartzJobDefinition
        {
            public Type JobType { get; set; }

            public string Identity { get; set; }
        }

        private class ConnectionSetting : IConnectionSetting
        {
            public string ConnectionString
            {
                get;
                set;
            }

            public ConnectionSetting(IConfiguration configuration, ConnectionSettingFactory factory)
            {
                ConnectionString = factory.Get.Invoke(configuration);
            }
        }

        private class ConnectionSettingFactory
        {
            public readonly Func<IConfiguration, string> Get;

            public ConnectionSettingFactory(Func<IConfiguration, string> get)
            {
                Get = get;
            }
        }

        public static IServiceCollection AddBgTeamStories<TStoryLibrary>(this IServiceCollection services)
        {
            services.TryAddSingleton<IStoryFactory, StoryFactory>();
            services.TryAddSingleton<IStoryBuilder, StoryBuilder>();

            return services.Scan(scan => scan
                .FromAssemblyOf<TStoryLibrary>()
                .AddClasses(classes => classes.AssignableTo(typeof(IStory<,>)))
                .AsImplementedInterfaces()
                .WithTransientLifetime());
        }

        public static IServiceCollection AddBgTeamQueries<TQueryLibrary>(this IServiceCollection services)
        {
            services.TryAddSingleton<IQueryFactory, QueryFactory>();
            services.TryAddSingleton<IQueryBuilder, QueryBuilder>();

            return services.Scan(scan => scan
                .FromAssemblyOf<TQueryLibrary>()
                .AddClasses(classes => classes.AssignableTo(typeof(IQuery<,>)))
                .AsImplementedInterfaces()
                .WithTransientLifetime());
        }

        public static IServiceCollection AddBgTeamConnectionSetting(this IServiceCollection services, Func<IConfiguration, string> factory)
        {
            if (factory == null)
            {
                throw new ArgumentNullException(nameof(factory));
            }

            return services.AddSingleton(new ConnectionSettingFactory(factory))
                 .AddSingleton<IConnectionSetting, ConnectionSetting>();
        }

        public static IServiceCollection AddQuartz(this IServiceCollection services, IConfiguration jobsConfiguration, Action<IQuartzConfiguration> configure = default)
        {
            if (jobsConfiguration == null)
            {
                throw new ArgumentNullException(nameof(jobsConfiguration));
            }

            var cfg = new QuartzConfiguration(jobsConfiguration);

            configure?.Invoke(cfg);

            services
                .AddHostedService<QuartzHostedService>()
                .AddSingleton(typeof(IJobFactory), cfg.JobFactoryType ?? typeof(ServiceProviderJobFactory))
                .AddSingleton(typeof(ISchedulerFactory), cfg.SchedulerFactoryType ?? typeof(StdSchedulerFactory));

            foreach (var item in cfg.Jobs)
            {
                var (jobType, identity, configuration) = item.Value;

                services
                    .AddTransient(jobType)
                    .AddSingleton<IQuartzJobDefinition>(new QuartzJobDefinition
                    {
                        JobType = jobType,
                        Identity = identity,
                    })
                    .Configure<JobConfiguration>(identity, configuration.GetSection(identity));
            }

            return services;
        }
    }
}

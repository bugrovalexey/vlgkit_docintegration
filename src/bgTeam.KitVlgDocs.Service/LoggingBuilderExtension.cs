﻿namespace bgTeam.KitVlgDocs.Service
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Net.NetworkInformation;
    using System.Net.Sockets;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using Serilog;
    using Serilog.Core;
    using Serilog.Events;
    using Serilog.Sinks.PostgreSQL;

    public static class LoggingBuilderExtension
    {
        internal const string PropertyName = "IPAddress";

        private const string VersionPropertyName = "AppVersion";

        private class IpEnricher : ILogEventEnricher
        {
            private LogEventProperty _cachedProperty;

            public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
            {
                if (_cachedProperty == null)
                {
                    _cachedProperty = propertyFactory.CreateProperty(PropertyName, GetLocalIPv4(NetworkInterfaceType.Ethernet));
                }

                logEvent.AddPropertyIfAbsent(_cachedProperty);
            }

            public string GetLocalIPv4(NetworkInterfaceType type)
            {
                foreach (var item in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (item.NetworkInterfaceType == type && item.OperationalStatus == OperationalStatus.Up)
                    {
                        foreach (var ip in item.GetIPProperties().UnicastAddresses)
                        {
                            if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                            {
                                return ip.Address.ToString();
                            }
                        }
                    }
                }

                return null;
            }
        }

        private class WriterBaseConverter : TypeConverter
        {
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                return sourceType == typeof(string);
            }

            public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
            {
                if (value is string str && str.Length > 0)
                {
                    var @params = str.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    var name = @params[0];
                    var args = @params[1].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(x => x.Split(new[] { ':' }, StringSplitOptions.None))
                        .ToArray();

                    var type = typeof(ColumnWriterBase).Assembly.GetType($"{typeof(ColumnWriterBase).Namespace}.{name}");

                    if (type != null)
                    {
                        foreach (var item in type.GetConstructors())
                        {
                            var ctorParams = item.GetParameters();

                            if (ctorParams.Length == args.Length)
                            {
                                var count = 0;
                                var ctorArgs = new object[ctorParams.Length];

                                for (int i = 0; i < ctorParams.Length; i++)
                                {
                                    if (args[i][0] != ctorParams[i].Name)
                                    {
                                        break;
                                    }

                                    count++;

                                    ctorArgs[i] = args[i].Length == 2
                                        ? TypeDescriptor.GetConverter(ctorParams[i].ParameterType).ConvertFromString(args[i][1])
                                        : null;
                                }

                                if (count == args.Length)
                                {
                                    return item.Invoke(ctorArgs);
                                }
                            }
                        }
                    }
                }

                return base.ConvertFrom(context, culture, value);
            }
        }

        public static ILoggingBuilder AddKitVlgDocsLogging(this ILoggingBuilder loggingBuilder, IConfiguration configuration, string version)
        {
            if (string.IsNullOrEmpty(version))
            {
                throw new ArgumentException(string.Empty, nameof(version));
            }

            TypeDescriptor.AddAttributes(typeof(ColumnWriterBase), new TypeConverterAttribute(typeof(WriterBaseConverter)));

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.With<IpEnricher>()
                .Enrich.WithProperty(VersionPropertyName, version)
                .CreateLogger();

            return loggingBuilder.AddSerilog(dispose: true);
        }
    }
}

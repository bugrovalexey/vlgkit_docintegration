﻿namespace bgTeam.KitVlgDocs.Service
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json;

    public class AppExceptionMiddleware : IMiddleware
    {
        private readonly IAppLogger _logger;

        public AppExceptionMiddleware(IAppLogger logger)
        {
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (AggregateException exp)
            {
                _logger.Error(exp);
                await HandleExceptionAsync(context, exp.Flatten().GetBaseException(), HttpStatusCode.InternalServerError);
            }
            catch (Exception exp)
            {
                _logger.Error(exp);
                await HandleExceptionAsync(context, exp, HttpStatusCode.InternalServerError);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exp, HttpStatusCode code)
        {
            var result = JsonConvert.SerializeObject(new
            {
                Code = code,
                exp.Message,
                exp.StackTrace,
            });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            return context.Response.WriteAsync(result);
        }
    }
}
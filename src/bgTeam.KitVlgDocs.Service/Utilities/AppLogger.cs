﻿namespace bgTeam.KitVlgDocs.Service.Utilities
{
    using System;
    using Microsoft.Extensions.Logging;

    public class AppLogger : IAppLogger
    {
        private readonly ILogger<AppLogger> _logger;

        public AppLogger(ILogger<AppLogger> logger)
        {
            _logger = logger;
        }

        /// <inheritdoc/>
        public void Debug(string message)
        {
            _logger.LogDebug(message);
        }

        /// <inheritdoc/>
        public void Info(string message)
        {
            _logger.LogInformation(message);
        }

        /// <inheritdoc/>
        public void Warning(string message)
        {
            _logger.LogWarning(message);
        }

        /// <inheritdoc/>
        public void Error(string message)
        {
            _logger.LogError(message);
        }

        /// <inheritdoc/>
        public void Error(Exception exp)
        {
            _logger.LogError(exp, exp.Message);
        }

        /// <inheritdoc/>
        public void Error(AggregateException exp)
        {
            _logger.LogError("Aggregate Exception ---> " + exp.Message);
            foreach (var iExp in exp.InnerExceptions)
            {
                var bExp = iExp.GetBaseException();
                _logger.LogError(bExp, bExp.Message);
            }
        }

        /// <inheritdoc/>
        public void Fatal(Exception exp)
        {
            _logger.LogCritical(exp, exp.Message);
        }

        /// <inheritdoc/>
        public void Fatal(AggregateException exp)
        {
            _logger.LogCritical("Aggregate Exception ---> " + exp.Message);
            foreach (var iExp in exp.InnerExceptions)
            {
                var bExp = iExp.GetBaseException();
                _logger.LogCritical(bExp, bExp.Message);
            }
        }
    }
}

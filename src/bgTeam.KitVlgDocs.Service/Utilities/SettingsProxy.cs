﻿namespace bgTeam.KitVlgDocs.Service.Utilities
{
    internal class SettingsProxy
    {
        public string AppEnvironment { get; set; }

        public string AppConfigsPath { get; set; }

        public string AppConfigsAdditional { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Service.Utilities
{
    using bgTeam.Core;
    using Microsoft.Extensions.Configuration;

    public class AppConfiguratin : IAppConfiguration
    {
        private readonly IConfiguration _configuration;

        public AppConfiguratin(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <inheritdoc/>
        public string this[string key]
        {
            get
            {
                return _configuration[key];
            }
        }

        /// <inheritdoc/>
        public string GetConnectionString(string name)
        {
            return _configuration.GetConnectionString(name);
        }

        /// <inheritdoc/>
        public IConfigurationSection GetSection(string name)
        {
            return _configuration.GetSection(name);
        }
    }
}

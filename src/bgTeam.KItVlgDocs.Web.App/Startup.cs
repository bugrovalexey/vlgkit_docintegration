﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Globalization;
using System.IO;

namespace bgTeam.KItVlgDocs.Web.App
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRouting().AddMvc();

            services.AddSwaggerGen(x =>
            {
                var basePath = AppContext.BaseDirectory;
                x.SwaggerDoc("v1", new OpenApiInfo { Title = "bgTeam.KItVlgDocs.Web api", Version = "v1" });
                x.IncludeXmlComments(Path.Combine(basePath, "bgTeam.KItVlgDocs.Web.xml"));
                x.IncludeXmlComments(Path.Combine(basePath, "bgTeam.KitVlgDocs.Domain.xml"));
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var customCulture = CultureInfo.DefaultThreadCurrentCulture ?? (CultureInfo)CultureInfo.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            CultureInfo.DefaultThreadCurrentCulture = customCulture;
            CultureInfo.DefaultThreadCurrentUICulture = customCulture;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/home/error");
            }

            app.UseSwagger().UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("v1/swagger.json", "bgTeam.KItVlgDocs API");
            });

            app.UseStaticFiles();

            app.UseMvc(x =>
            {
                x.MapRoute(name: "default", template: "{controller=Home}/{action=Index}");
            });
        }
    }
}

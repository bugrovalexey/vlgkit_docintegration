﻿using bgTeam.DataAccess;
using bgTeam.DataAccess.Impl.Dapper;
using bgTeam.DataAccess.Impl.Oracle;
using bgTeam.KitVlgDocs.Common;
using bgTeam.KitVlgDocs.Common.Impl;
using bgTeam.KitVlgDocs.DataAccess;
using bgTeam.KitVlgDocs.DataAccess.Impl;
using bgTeam.KitVlgDocs.Delo;
using bgTeam.KitVlgDocs.Service;
using bgTeam.KitVlgDocs.Story;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using System.Threading.Tasks;

namespace bgTeam.KItVlgDocs.Web.App
{
    internal class Program
    {
        internal static Task Main(string[] args)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            return CreateWebHostBuilder(args).Build().RunAsync();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel(options =>
                {
                    options.Limits.MaxRequestBodySize = 104857600; //100MB
                })
                .UseBgTeamLogging()
                .UseBgTeamConfiguration(args)
                .UseBgTeamQueries<IQueryLibrary>()
                .UseBgTeamStories<IStoryLibrary>()
                .UseBgTeamConnectionSetting(x => x.GetConnectionString("SYSTEMDB").Decrypt())
                .UseKitVlgDocsLogging(AppConstants.Version)
                .UseBgTeamExceptionMiddleware()
                .UseStartup<Startup>()
                .ConfigureServices((context, x) =>
                {
                    x.Configure<WebAppOptions>(context.Configuration)
                        .AddDeloEApi(context.Configuration.GetSection("EApi"))
                        .AddSingleton<IConnectionFactory, ConnectionFactoryOracle>()
                        .AddSingleton<ICrudService, CrudServiceDapper>()
                        .AddSingleton<IRepository, RepositoryDapper>()
                        .AddSingleton<IMedoPackage, MedoPackage>()
                        .AddSingleton<IEmbeddedResources, EmbeddedResources>()
                        .AddSingleton<IOutgoingDocumentStorage, OutgoingDocumentStorage>()
                        .AddSingleton<IMedoDocumentLoader, MedoDocumentLoader>();
                });
    }
}

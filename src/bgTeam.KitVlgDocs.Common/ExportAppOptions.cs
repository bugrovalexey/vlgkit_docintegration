﻿namespace bgTeam.KitVlgDocs.Common
{
    using System.Security.Cryptography.X509Certificates;

    public class ExportAppOptions
    {
        public StoreName StoreName { get; set; }

        public StoreLocation StoreLocation { get; set; }

        public X509FindType CertificateFindMode { get; set; }
    }
}

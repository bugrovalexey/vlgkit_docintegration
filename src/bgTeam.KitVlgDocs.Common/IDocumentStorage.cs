﻿namespace bgTeam.KitVlgDocs.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    public interface IDocumentStorage
    {
        Uri Root { get; }

        void Delete();

        IDocumentStorage CreateDirectory(string path);

        IEnumerable<IDocumentInfo> GetDocuments(SearchOption searchOption);

        IEnumerable<IDocumentInfo> GetDocuments(string pattern, SearchOption searchOption);

        void Move(IDocumentStorage srcStorage, string destDirectoryName, bool replace = false);

        void Move(IDocumentInfo srcDocument, string destFileName, bool replace = false);
    }
}

﻿namespace bgTeam.KitVlgDocs.Common
{
    using System.IO;

    public interface IWritableDocumentInfo : IDocumentInfo
    {
        Stream CreateWriteStream();
    }
}

﻿namespace bgTeam.KitVlgDocs.Common
{
    using bgTeam.KitVlgDocs.Common.ImageGen;
    using System.Xml.Schema;

    public interface IEmbeddedResources
    {
        ImageDefinition SignatureStamp { get; }

        ImageDefinition RegistrationStamp { get; }

        XmlSchema TransportSchema { get; }

        XmlSchema ContainerSchema { get; }
    }
}

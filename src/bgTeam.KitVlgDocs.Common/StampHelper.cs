﻿namespace bgTeam.KitVlgDocs.Common
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using iTextSharp.text.pdf;

    public static class StampHelper
    {
        public static byte[] Watermark(
            byte[] pdfBytes,
            byte[] imageBytes,
            Func<IEnumerable<(PdfContentByte, PdfDictionary, iTextSharp.text.Rectangle)>, (PdfContentByte, PdfDictionary, iTextSharp.text.Rectangle)> pageSelector = null,
            Func<iTextSharp.text.Image, iTextSharp.text.Rectangle, float, RectangleF> regionSelector = null)
        {
            using (var pdfStream = new MemoryStream(pdfBytes))
            {
                using (var imageStream = new MemoryStream(imageBytes))
                {
                    using (var outputStream = new MemoryStream())
                    {
                        Watermark(pdfStream, imageStream, outputStream, pageSelector, regionSelector);

                        return outputStream.ToArray();
                    }
                }
            }
        }

        public static byte[] Watermark(
            byte[] pdfBytes,
            IDocumentInfo imageInfo,
            Func<IEnumerable<(PdfContentByte, PdfDictionary, iTextSharp.text.Rectangle)>, (PdfContentByte, PdfDictionary, iTextSharp.text.Rectangle)> pageSelector = null,
            Func<iTextSharp.text.Image, iTextSharp.text.Rectangle, float, RectangleF> regionSelector = null)
        {
            using (var pdfStream = new MemoryStream(pdfBytes))
            {
                using (var imageStream = imageInfo.CreateReadStream())
                {
                    using (var outputStream = new MemoryStream())
                    {
                        Watermark(pdfStream, imageStream, outputStream, pageSelector, regionSelector);

                        return outputStream.ToArray();
                    }
                }
            }
        }

        public static void Watermark(
            IWritableDocumentInfo pdfInfo,
            IDocumentInfo imageInfo,
            Func<IEnumerable<(PdfContentByte, PdfDictionary, iTextSharp.text.Rectangle)>, (PdfContentByte, PdfDictionary, iTextSharp.text.Rectangle)> pageSelector = null,
            Func<iTextSharp.text.Image, iTextSharp.text.Rectangle, float, RectangleF> regionSelector = null)
        {
            using (var pdfStream = new MemoryStream())
            {
                using (var input = pdfInfo.CreateReadStream())
                {
                    input.CopyTo(pdfStream);
                }

                pdfStream.Position = 0;

                using (var imageStream = imageInfo.CreateReadStream())
                {
                    using (var outputStream = pdfInfo.CreateWriteStream())
                    {
                        outputStream.SetLength(0);

                        Watermark(pdfStream, imageStream, outputStream, pageSelector, regionSelector);
                    }
                }
            }
        }

        public static void Watermark(
            Stream pdfStream,
            Stream imageStream,
            Stream outputStream,
            Func<IEnumerable<(PdfContentByte, PdfDictionary, iTextSharp.text.Rectangle)>, (PdfContentByte, PdfDictionary, iTextSharp.text.Rectangle)> pageSelector = null,
            Func<iTextSharp.text.Image, iTextSharp.text.Rectangle, float, RectangleF> regionSelector = null)
        {
            using (var reader = new PdfReader(pdfStream))
            {
                using (var stamper = new PdfStamper(reader, outputStream))
                {
                    var pages = GetPages();
                    var image = LoadImage();

                    var page = pageSelector?.Invoke(pages) ?? pages.First();
                    var unit = page.Item2.GetAsNumber(PdfName.USERUNIT)?.FloatValue ?? 72f;

                    var region = regionSelector?.Invoke(image, page.Item3, unit) ?? new RectangleF(0f, 0f, image.Width, image.Height);

                    image.ScaleAbsoluteWidth(region.Width);
                    image.ScaleAbsoluteHeight(region.Height);
                    image.SetAbsolutePosition(region.X, region.Y);

                    page.Item1.AddImage(image);

                    IEnumerable<(PdfContentByte, PdfDictionary, iTextSharp.text.Rectangle)> GetPages()
                    {
                        for (int i = 1; i <= stamper.Reader.NumberOfPages; i++)
                        {
                            yield return (stamper.GetOverContent(i), stamper.Reader.GetPageN(i), stamper.Reader.GetPageSize(i));
                        }
                    }
                }

                iTextSharp.text.Image LoadImage()
                {
                    return iTextSharp.text.Image.GetInstance(imageStream);
                }
            }
        }
    }
}

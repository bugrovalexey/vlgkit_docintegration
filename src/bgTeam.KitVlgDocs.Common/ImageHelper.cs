﻿namespace bgTeam.KitVlgDocs.Common
{
    public static class ImageHelper
    {
        private const float MilimetresPerInch = 25.4f;

        public static float ToPixels(float millimeters, float dpi)
        {
            return millimeters / MilimetresPerInch * dpi;
        }

        public static float ToMillimeters(float pixels, float dpi)
        {
            return pixels / dpi * MilimetresPerInch;
        }
    }
}

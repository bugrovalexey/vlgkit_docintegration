﻿namespace bgTeam.KitVlgDocs.Common
{
    using System.Collections.Generic;

    public interface IIncomingDocumentStorage
    {
        void MoveToError(IDocumentStorage documentStorage, string directory = null);

        void MoveToArchive(IDocumentStorage documentStorage, string directory = null);

        void MoveToOldMedo(IDocumentStorage documentStorage, string directory = null);

        IEnumerable<IDocumentInfo> GetIncomingDocuments(string pattern);
    }
}

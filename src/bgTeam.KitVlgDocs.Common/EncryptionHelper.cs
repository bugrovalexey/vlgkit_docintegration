﻿namespace bgTeam.KitVlgDocs.Common
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using Newtonsoft.Json;

    public static class EncryptionHelper
    {
        private static readonly string Prefix = "aes:";
        private static readonly byte[] IV = Convert.FromBase64String("aLmC9gSbZQzH48YMFeAqow==");
        private static readonly byte[] Key = Convert.FromBase64String("vgBlhhO9YbydVm0l/Mz1Ug0eIogJS/24HnXlOIHzRIQ=");

        public static bool IsEncrypted(this string input)
        {
            return input.StartsWith(Prefix);
        }

        public static string Encrypt(this string input)
        {
            if (input.IsEncrypted())
            {
                return input;
            }

            using (var algorithm = Aes.Create())
            {
                algorithm.IV = IV;
                algorithm.Key = Key;

                using (var encryptor = algorithm.CreateEncryptor())
                {
                    using (var memory = new MemoryStream())
                    {
                        using (var stream = new CryptoStream(memory, encryptor, CryptoStreamMode.Write))
                        {
                            var buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new
                            {
                                // Salt -> Every Encrypt() looks different
                                Guid = Guid.NewGuid(),
                                Value = input,
                            }));

                            stream.Write(buffer, 0, buffer.Length);
                        }

                        return Prefix + Convert.ToBase64String(memory.ToArray());
                    }
                }
            }
        }

        public static string Decrypt(this string input)
        {
            if (!input.IsEncrypted())
            {
                return input;
            }

            using (var algorithm = Aes.Create())
            {
                algorithm.IV = IV;
                algorithm.Key = Key;

                using (var decryptor = algorithm.CreateDecryptor())
                {
                    var buffer = Convert.FromBase64String(input.Remove(0, Prefix.Length));
                    using (var memory = new MemoryStream(buffer))
                    {
                        using (var stream = new CryptoStream(memory, decryptor, CryptoStreamMode.Read))
                        {
                            using (var output = new MemoryStream())
                            {
                                stream.CopyTo(output);

                                return JsonConvert.DeserializeAnonymousType(Encoding.UTF8.GetString(output.ToArray()), new
                                {
                                    Guid = default(Guid),
                                    Value = default(string),
                                }).Value;
                            }
                        }
                    }
                }
            }
        }
    }
}

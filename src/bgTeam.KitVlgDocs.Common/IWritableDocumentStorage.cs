﻿namespace bgTeam.KitVlgDocs.Common
{
    public interface IWritableDocumentStorage : IDocumentStorage
    {
        IWritableDocumentInfo Create(string fileName, bool replace = false);
    }
}

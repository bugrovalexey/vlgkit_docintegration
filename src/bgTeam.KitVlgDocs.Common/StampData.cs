﻿namespace bgTeam.KitVlgDocs.Common
{
    public class StampData
    {
        public float Top { get; set; }

        public float Left { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public int Page { get; set; }
    }
}

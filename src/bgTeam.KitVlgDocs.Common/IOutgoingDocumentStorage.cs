﻿namespace bgTeam.KitVlgDocs.Common
{
    public interface IOutgoingDocumentStorage
    {
        void MoveToOutgoing(IDocumentInfo documentInfo, string directory = null);
    }
}

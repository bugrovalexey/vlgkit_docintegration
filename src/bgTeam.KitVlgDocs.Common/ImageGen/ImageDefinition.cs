﻿namespace bgTeam.KitVlgDocs.Common.ImageGen
{
    using System.Drawing;
    using Newtonsoft.Json;

    public class ImageDefinition
    {
        private static readonly JsonSerializerSettings _settings;

        static ImageDefinition()
        {
            _settings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All,
            };
            _settings.Converters.Add(new DrawingToJsonConverter());
        }

        public Size Size { get; set; }

        public string Name { get; set; }

        public Color BackgroundColor { get; set; }

        public ImageComponent[] Components { get; set; }

        public static ImageDefinition FromJson(string json)
        {
            return JsonConvert.DeserializeObject<ImageDefinition>(json, _settings);
        }

        public static string ToJson(ImageDefinition definition)
        {
            return JsonConvert.SerializeObject(definition, _settings);
        }
    }
}

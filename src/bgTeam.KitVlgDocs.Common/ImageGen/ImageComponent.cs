﻿namespace bgTeam.KitVlgDocs.Common.ImageGen
{
    using System.Drawing;

    public abstract class ImageComponent
    {
        public abstract void Draw(object parameters, Graphics graphics);
    }
}

﻿namespace bgTeam.KitVlgDocs.Common.ImageGen
{
    using System;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Drawing.Text;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Newtonsoft.Json;

    public class DrawingToJsonConverter : JsonConverter
    {
        private static readonly CultureInfo _cultureInfo;

        static DrawingToJsonConverter()
        {
            _cultureInfo = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            _cultureInfo.NumberFormat = (NumberFormatInfo)_cultureInfo.NumberFormat.Clone();
            _cultureInfo.NumberFormat.NumberDecimalSeparator = ".";
        }

        private delegate bool Parser(string s, out object result);

        private static readonly Type[] _types = new[]
        {
            typeof(Size),
            typeof(SizeF),
            typeof(Point),
            typeof(PointF),
            typeof(Rectangle),
            typeof(RectangleF),

            typeof(Font),
            typeof(Bitmap),
            typeof(StringFormat),
        };

        public override bool CanConvert(Type objectType)
            => _types.Contains(objectType);

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (objectType == typeof(Font))
            {
                var args = ((string)reader.Value)?.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                if (args?.Length == 6 && !args.Any(string.IsNullOrEmpty))
                {
                    return new Font(
                        args[0],
                        float.Parse(args[1], _cultureInfo),
                        (FontStyle)int.Parse(args[2]),
                        (GraphicsUnit)int.Parse(args[3]),
                        byte.Parse(args[4]),
                        int.Parse(args[5]) == 1);
                }

                return existingValue;
            }
            else if (objectType == typeof(Bitmap))
            {
                return new Bitmap(new MemoryStream(Convert.FromBase64String((string)reader.Value)));
            }
            else if (objectType == typeof(StringFormat))
            {
                var args = ((string)reader.Value)?.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                if (args?.Length == 6 && !args.Any(string.IsNullOrEmpty))
                {
                    return new StringFormat((StringFormatFlags)int.Parse(args[0]), int.Parse(args[1]))
                    {
                        Alignment = (StringAlignment)int.Parse(args[2]),
                        LineAlignment = (StringAlignment)int.Parse(args[3]),
                        HotkeyPrefix = (HotkeyPrefix)int.Parse(args[4]),
                        Trimming = (StringTrimming)int.Parse(args[5]),
                    };
                }

                return existingValue;
            }
            else
            {
                var parser = objectType.Name.EndsWith("F", StringComparison.OrdinalIgnoreCase)
                    ? (Parser)TryParseSingle
                    : TryParseInt32;

                var args = ((string)reader.Value)?.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                    ?.Select(x => parser.Invoke(x, out var result) ? result : null)?.ToArray();

                return (args?.Length > 0 && !args.Any(x => x == null))
                    ? Activator.CreateInstance(objectType, args)
                    : existingValue;
            }

            bool TryParseInt32(string s, out object result)
            {
                if (int.TryParse(s, out var r))
                {
                    result = r;
                    return true;
                }

                result = 0f;
                return false;
            }

            bool TryParseSingle(string s, out object result)
            {
                if (float.TryParse(s, NumberStyles.Float, _cultureInfo, out var r))
                {
                    result = r;
                    return true;
                }

                result = 0f;
                return false;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            switch (value)
            {
                case Font v:
                    writer.WriteValue($"{v.FontFamily.Name};{v.Size};{(int)v.Style};{(int)v.Unit};{v.GdiCharSet};{(v.GdiVerticalFont ? 1 : 0)}");
                    break;
                case Bitmap v:
                    using (var mem = new MemoryStream())
                    {
                        v.Save(mem, ImageFormat.Png);

                        writer.WriteValue(mem.ToArray());
                    }

                    break;
                case StringFormat v:
                    writer.WriteValue($"{(int)v.FormatFlags};{v.DigitSubstitutionLanguage};{(int)v.Alignment};{(int)v.LineAlignment};{(int)v.HotkeyPrefix};{(int)v.Trimming}");
                    break;
                case Size v:
                    writer.WriteValue($"{v.Width};{v.Height}");
                    break;
                case SizeF v:
                    writer.WriteValue($"{v.Width};{v.Height}");
                    break;
                case Point v:
                    writer.WriteValue($"{v.X};{v.Y}");
                    break;
                case PointF v:
                    writer.WriteValue($"{v.X};{v.Y}");
                    break;
                case Rectangle v:
                    writer.WriteValue($"{v.X};{v.Y};{v.Width};{v.Height}");
                    break;
                case RectangleF v:
                    writer.WriteValue($"{v.X};{v.Y};{v.Width};{v.Height}");
                    break;
            }
        }
    }
}

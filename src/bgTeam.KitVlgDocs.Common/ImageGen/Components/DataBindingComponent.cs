﻿namespace bgTeam.KitVlgDocs.Common.ImageGen.Components
{
    using System.Drawing;

    public sealed class DataBindingComponent : ImageComponent
    {
        public Font Font { get; set; }

        public Color Color { get; set; }

        public string Format { get; set; }

        public string Property { get; set; }

        public RectangleF Bounds { get; set; }

        public StringFormat Layout { get; set; }

        public override void Draw(object parameters, Graphics graphics)
        {
            var value = parameters.GetType().GetProperty(Property).GetValue(parameters);

            using (var brush = new SolidBrush(Color))
            {
                graphics.DrawString(string.Format(Format, value), Font, brush, Bounds, Layout);
            }
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Common.ImageGen.Components
{
    using System.Drawing;

    public sealed class BitmapComponent : ImageComponent
    {
        public Bitmap Image { get; set; }

        public Rectangle Bounds { get; set; }

        public override void Draw(object parameters, Graphics graphics)
            => graphics.DrawImage(Image, Bounds);
    }
}

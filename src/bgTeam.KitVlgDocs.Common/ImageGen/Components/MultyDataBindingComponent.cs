﻿namespace bgTeam.KitVlgDocs.Common.ImageGen.Components
{
    using System.Drawing;
    using System.Linq;

    public sealed class MultyDataBindingComponent : ImageComponent
    {
        public Font Font { get; set; }

        public Color Color { get; set; }

        public string Format { get; set; }

        public string[] Properties { get; set; }

        public RectangleF Bounds { get; set; }

        public StringFormat Layout { get; set; }

        public override void Draw(object parameters, Graphics graphics)
        {
            var type = parameters.GetType();
            var value = Properties.Select(x => type.GetProperty(x).GetValue(parameters)).ToArray();

            using (var brush = new SolidBrush(Color))
            {
                graphics.DrawString(string.Format(Format, value), Font, brush, Bounds, Layout);
            }
        }
    }
}

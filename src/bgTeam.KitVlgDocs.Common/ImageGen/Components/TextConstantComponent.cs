﻿namespace bgTeam.KitVlgDocs.Common.ImageGen.Components
{
    using System.Drawing;

    public sealed class TextConstantComponent : ImageComponent
    {
        public Font Font { get; set; }

        public string Text { get; set; }

        public Color Color { get; set; }

        public RectangleF Bounds { get; set; }

        public StringFormat Layout { get; set; }

        public override void Draw(object parameters, Graphics graphics)
        {
            using (var brush = new SolidBrush(Color))
            {
                graphics.DrawString(Text, Font, brush, Bounds, Layout);
            }
        }
    }
}

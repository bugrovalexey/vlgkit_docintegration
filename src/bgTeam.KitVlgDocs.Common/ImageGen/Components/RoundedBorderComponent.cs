﻿namespace bgTeam.KitVlgDocs.Common.ImageGen.Components
{
    using System.Drawing;
    using System.Drawing.Drawing2D;

    public sealed class RoundedBorderComponent : ImageComponent
    {
        public Color Color { get; set; }

        public Rectangle Bounds { get; set; }

        public int Radius { get; set; }

        public float LineWidth { get; set; }

        public override void Draw(object parameters, Graphics graphics)
        {
            using (var path = new GraphicsPath())
            {
                if (Radius == 0)
                {
                    path.AddRectangle(Bounds);
                }
                else
                {
                    var diameter = Radius * 2;
                    var size = new Size(diameter, diameter);
                    var arc = new Rectangle(Bounds.Location, size);

                    path.AddArc(arc, 180, 90);
                    arc.X = Bounds.Right - diameter;
                    path.AddArc(arc, 270, 90);
                    arc.Y = Bounds.Bottom - diameter;
                    path.AddArc(arc, 0, 90);
                    arc.X = Bounds.Left;
                    path.AddArc(arc, 90, 90);
                    path.CloseFigure();
                }

                using (var pen = new Pen(Color, LineWidth))
                {
                    graphics.DrawPath(new Pen(Color, LineWidth), path);
                }
            }
        }
    }
}

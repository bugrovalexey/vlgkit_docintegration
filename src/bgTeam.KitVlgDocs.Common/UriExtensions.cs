﻿namespace bgTeam.KitVlgDocs.Common
{
    using System;
    using System.Linq;

    public static class UriExtensions
    {
        public static Uri MakeRelative(this Uri baseUri, string relativeUri)
        {
            return new Uri(baseUri, Uri.EscapeDataString(relativeUri));
        }

        public static bool TryCreate(this Uri baseUri, string relativeUri, out Uri result)
        {
            relativeUri = string.IsNullOrWhiteSpace(relativeUri) || relativeUri.Length == 0
                ? "./"
                : Uri.EscapeDataString(relativeUri);

            if (relativeUri.Last() != '/')
            {
                relativeUri += '/';
            }

            return Uri.TryCreate(baseUri, relativeUri, out result);
        }
    }
}

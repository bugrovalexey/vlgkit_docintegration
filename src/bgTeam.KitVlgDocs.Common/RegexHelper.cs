﻿namespace bgTeam.KitVlgDocs.Common
{
    using System.Text;
    using System.Text.RegularExpressions;

    public static class RegexHelper
    {
        public static Regex ConvertPattern(string pattern)
        {
            var builder = new StringBuilder()
                .Append('^')
                .Append(Regex.Escape(pattern))
                .Replace("\\.", "[.]")
                .Replace("\\*", ".*")
                .Replace("\\?", ".")
                .Append('$');

            return new Regex(builder.ToString(), RegexOptions.IgnoreCase);
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Common
{
    using System.Security.Cryptography.X509Certificates;

    public class CertificatData
    {
        public string Key { get; set; }

        public X509FindType FindType { get; set; }

        public StoreName StoreName { get; set; }

        public StoreLocation StoreLocation { get; set; }
    }
}

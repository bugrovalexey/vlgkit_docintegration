﻿namespace bgTeam.KitVlgDocs.Common
{
    using System;
    using System.IO;

    public interface IDocumentInfo
    {
        Uri Path { get; }

        string Name { get; }

        IDocumentStorage Storage { get; }

        void Delete();

        Stream CreateReadStream();
    }
}

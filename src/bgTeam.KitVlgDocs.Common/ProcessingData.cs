﻿namespace bgTeam.KitVlgDocs.Common
{
    public class ProcessingData
    {
        public string Action { get; set; }

        public string Kind { get; set; }

        public string Folder { get; set; }

        public static implicit operator (string, string, string)(ProcessingData value)
        {
            return (value.Action, value.Kind, value.Folder);
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Common
{
    using System.Collections.Generic;

    public class WebAppOptions
    {
        public string OutgoingFolder { get; set; }

        public bool WatermarkPdf { get; set; }

        public string EnvelopeIni { get; set; }

        public string ContainerXml { get; set; }

        public string CommunicationXml { get; set; }

        public string[] ExportInStatuses { get; set; }

        public string SignTaskFilter { get; set; }

        public string CoordinatesTaskFilter { get; set; }

        public string DefaultMedoOutgoingDocumentKind { get; set; }

        public CertificatData Certificate { get; set; }

        public Dictionary<string, string> EClassMapping { get; set; }

        public Dictionary<string, string> FoldersMapping { get; set; }

        public Dictionary<string, IdNameCode<int?, string, string>> ContactsMapping { get; set; }

        public StampData SignatureStamp { get; set; }

        public StampData RegistrationStamp { get; set; }
    }
}

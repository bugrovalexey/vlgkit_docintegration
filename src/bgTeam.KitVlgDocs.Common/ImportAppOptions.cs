﻿namespace bgTeam.KitVlgDocs.Common
{
    using System.Collections.Generic;

    public class ImportAppOptions
    {
        public string ErrorFolder { get; set; }

        public string ArchiveFolder { get; set; }

        public string OldMedoFolder { get; set; }

        public string IncomingFolder { get; set; }

        public string SupportedVersion { get; set; }

        public string EnvelopeIni { get; set; }

        public string ContainerXml { get; set; }

        public string CommunicationXml { get; set; }

        public string[] CommunicationXmlAlternates { get; set; }

        public ProcessingData DefaultProcessing { get; set; }

        public Dictionary<string, Dictionary<string, ProcessingData>> Processing { get; set; }
    }
}

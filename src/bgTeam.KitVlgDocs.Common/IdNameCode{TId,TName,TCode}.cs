﻿namespace bgTeam.KitVlgDocs.Common
{
    public class IdNameCode<TId, TName, TCode>
    {
        public TId Id { get; set; }

        public TName Name { get; set; }

        public TCode Code { get; set; }

        public static implicit operator IdNameCode<TId, TName, TCode>((TId, TName, TCode) value)
        {
            return new IdNameCode<TId, TName, TCode>
            {
                Id = value.Item1,
                Name = value.Item2,
                Code = value.Item3,
            };
        }

        public static implicit operator (TId, TName, TCode)(IdNameCode<TId, TName, TCode> value)
        {
            return (value.Id, value.Name, value.Code);
        }
    }
}

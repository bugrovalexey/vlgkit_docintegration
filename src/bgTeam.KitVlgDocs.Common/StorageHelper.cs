﻿namespace bgTeam.KitVlgDocs.Common
{
    using System;
    using bgTeam.KitVlgDocs.Common.Impl;

    public static class StorageHelper
    {
        public static IDocumentStorage GetDocumentStorage(string root)
        {
            if (Uri.TryCreate(root, UriKind.Absolute, out var uri))
            {
                if (uri.Scheme == Uri.UriSchemeFtp)
                {
                    return new FtpDocumentStorage(uri);
                }
                else if (uri.Scheme == Uri.UriSchemeFile)
                {
                    return new PhysicalDocumentStorage(uri);
                }
                else if (uri.Scheme == "mem")
                {
                    return new MemoryDocumentStorage(uri);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}

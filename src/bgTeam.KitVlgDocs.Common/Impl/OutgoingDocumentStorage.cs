﻿namespace bgTeam.KitVlgDocs.Common.Impl
{
    using Microsoft.Extensions.Options;

    public class OutgoingDocumentStorage : IOutgoingDocumentStorage
    {
        private readonly IAppLogger _logger;
        private readonly IDocumentStorage _outgoingDocumentStorage;

        public OutgoingDocumentStorage(IAppLogger logger, IOptions<WebAppOptions> options)
        {
            _logger = logger;
            _outgoingDocumentStorage = StorageHelper.GetDocumentStorage(options.Value.OutgoingFolder);
        }

        public void MoveToOutgoing(IDocumentInfo documentInfo, string directory = null)
        {
            try
            {
                var storage = string.IsNullOrEmpty(directory) ?
                    _outgoingDocumentStorage :
                    _outgoingDocumentStorage.CreateDirectory(directory);

                storage.Move(documentInfo, documentInfo.Name, true);
            }
            catch
            {
                _logger.Error($"Error while moving {documentInfo.Path} to outgoing folder.");
                throw;
            }
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Common.Impl
{
    using System;
    using System.IO;
    using FluentFTP;

    public class FtpDocumentInfo : IDocumentInfo
    {
        private readonly FtpClient _ftpClient;

        public FtpDocumentInfo(Uri path, FtpClient ftpClient, FtpDocumentStorage storage)
        {
            if (path.Scheme != Uri.UriSchemeFtp || !path.IsAbsoluteUri)
            {
                throw new ArgumentException(@"Only ftp:// path allowed.", nameof(path));
            }

            Path = path;
            Storage = storage;
            _ftpClient = ftpClient;
        }

        public Uri Path { get; }

        public string Name => System.IO.Path.GetFileName(Path.AbsolutePath);

        public IDocumentStorage Storage { get; }

        public void Delete()
        {
            if (_ftpClient.FileExists(Path.AbsolutePath))
            {
                _ftpClient.DeleteFile(Path.AbsolutePath);
            }
        }

        public Stream CreateReadStream()
        {
            if (!_ftpClient.FileExists(Path.AbsolutePath))
            {
                throw new InvalidOperationException();
            }

            return _ftpClient.OpenRead(Path.AbsolutePath);
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Common.Impl
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using FluentFTP;

    public class FtpDocumentStorage : IDocumentStorage
    {
        private readonly FtpClient _ftpClient;

        public FtpDocumentStorage(Uri root)
        {
            if (root.Scheme != Uri.UriSchemeFtp || !root.IsAbsoluteUri)
            {
                throw new ArgumentException(@"Only ftp:// path allowed.", nameof(root));
            }

            if (root.AbsoluteUri.Last() != '/')
            {
                root = new Uri(root.AbsoluteUri + '/');
            }

            Root = new UriBuilder(root.Scheme, root.Host, root.Port, root.PathAndQuery).Uri;

            var userInfo = root.UserInfo.Split(':');

            _ftpClient = new FtpClient(root.Host, root.Port, new NetworkCredential(userInfo[0], userInfo[1]));

            _ftpClient.Connect();
        }

        private FtpDocumentStorage(Uri root, FtpClient client)
        {
            Root = root;

            _ftpClient = client;
        }

        public Uri Root { get; }

        public void Delete()
        {
            _ftpClient.DeleteDirectory(Root.GetComponents(UriComponents.Path, UriFormat.Unescaped));
        }

        public IEnumerable<IDocumentInfo> GetDocuments(SearchOption searchOption)
        {
            return EnumerateFiles(Root, string.Empty, searchOption);
        }

        public IEnumerable<IDocumentInfo> GetDocuments(string pattern, SearchOption searchOption)
        {
            return EnumerateFiles(Root, pattern, searchOption);
        }

        public IDocumentStorage CreateDirectory(string path)
        {
            if (Root.TryCreate(path, out var uri))
            {
                if (!_ftpClient.DirectoryExists(uri.GetComponents(UriComponents.Path, UriFormat.Unescaped)))
                {
                    _ftpClient.CreateDirectory(uri.GetComponents(UriComponents.Path, UriFormat.Unescaped));
                }

                return new FtpDocumentStorage(uri, _ftpClient);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void Move(IDocumentInfo srcDocument, string destFileName, bool replace = false)
        {
            var uri = Root.MakeRelative(destFileName);
            var path = uri.GetComponents(UriComponents.Path, UriFormat.Unescaped);
            if (srcDocument.Path == uri)
            {
                return;
            }
            else if (_ftpClient.FileExists(path))
            {
                if (replace)
                {
                    _ftpClient.DeleteFile(path);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }

            using (var srcStream = srcDocument.CreateReadStream())
            {
                using (var destStream = _ftpClient.OpenWrite(path, FtpDataType.Binary, false))
                {
                    srcStream.CopyTo(destStream);
                }

                var reply = _ftpClient.GetReply();

                if (!reply.Success)
                {
                    throw new InvalidOperationException();
                }
            }

            srcDocument.Delete();
        }

        public void Move(IDocumentStorage srcStorage, string destDirectoryName, bool replace = false)
        {
            if (Root.TryCreate(destDirectoryName, out var uri))
            {
                var path = uri.GetComponents(UriComponents.Path, UriFormat.Unescaped);

                if (srcStorage.Root == uri)
                {
                    return;
                }
                else if (_ftpClient.DirectoryExists(path))
                {
                    if (replace)
                    {
                        _ftpClient.DeleteDirectory(path);
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                }

                if (!_ftpClient.DirectoryExists(uri.GetComponents(UriComponents.Path, UriFormat.Unescaped)))
                {
                    _ftpClient.CreateDirectory(uri.GetComponents(UriComponents.Path, UriFormat.Unescaped));
                }

                foreach (var item in srcStorage.GetDocuments(SearchOption.AllDirectories))
                {
                    using (var srcStream = item.CreateReadStream())
                    {
                        using (var destStream = _ftpClient.OpenWrite(new Uri(uri, item.Name).GetComponents(UriComponents.Path, UriFormat.Unescaped), FtpDataType.Binary, false))
                        {
                            srcStream.CopyTo(destStream);
                        }

                        var reply = _ftpClient.GetReply();

                        if (!reply.Success)
                        {
                            throw new InvalidOperationException();
                        }
                    }
                }

                srcStorage.Delete();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        private IEnumerable<IDocumentInfo> EnumerateFiles(Uri root, string pattern, SearchOption searchOption)
        {
            var regex = string.IsNullOrEmpty(pattern) ? null : RegexHelper.ConvertPattern(pattern);

            foreach (var item in _ftpClient.GetListing(root.GetComponents(UriComponents.Path, UriFormat.Unescaped)))
            {
                if (item.Type == FtpFileSystemObjectType.Directory && searchOption == SearchOption.AllDirectories)
                {
                    foreach (var info in EnumerateFiles(new Uri(root, item.Name), pattern, searchOption))
                    {
                        yield return info;
                    }
                }
                else if (item.Type == FtpFileSystemObjectType.File && (regex?.IsMatch(item.Name) ?? true))
                {
                    yield return new FtpDocumentInfo(new Uri(Root, item.Name), _ftpClient, this);
                }
            }
        }
    }
}

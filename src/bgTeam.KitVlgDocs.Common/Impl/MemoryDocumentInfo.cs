﻿namespace bgTeam.KitVlgDocs.Common.Impl
{
    using System;
    using System.IO;
    using System.Threading;

    public class MemoryDocumentInfo : IWritableDocumentInfo
    {
        private const int StateNone = 0;
        private const int StateRead = 1;
        private const int StateWrite = 2;
        private const int StateFlush = 3;
        private const int StateDeleted = -1;

        private int _state;
        private readonly MemoryStream _stream;
        private readonly MemoryDocumentStorage _storage;

        public MemoryDocumentInfo(Uri path, MemoryStream stream, MemoryDocumentStorage storage)
        {
            if (path.Scheme != "mem" || !path.IsAbsoluteUri)
            {
                throw new ArgumentException(@"Only mem:// path allowed.", nameof(path));
            }

            Path = path;

            _stream = stream;
            _storage = storage;
        }

        public Uri Path { get; }

        public string Name => System.IO.Path.GetFileName(Path.LocalPath);

        public IDocumentStorage Storage => _storage;

        public void Delete()
        {
            if (Interlocked.CompareExchange(ref _state, StateDeleted, StateNone) == StateNone)
            {
                _storage.Remove(Path);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public Stream CreateReadStream()
        {
            if (Interlocked.CompareExchange(ref _state, StateRead, StateNone) == StateNone)
            {
                return new InnerMemoryStream(this);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public Stream CreateWriteStream()
        {
            if (Interlocked.CompareExchange(ref _state, StateWrite, StateNone) == StateNone)
            {
                return new InnerMemoryStream(this);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        private class InnerMemoryStream : MemoryStream
        {
            private int _state;
            private MemoryDocumentInfo _info;

            public InnerMemoryStream(MemoryDocumentInfo info)
            {
                _info = info;
                _state = info._state;
                _info._stream.WriteTo(this);
                _info._stream.Position = 0;
                Position = 0;
            }

            protected override void Dispose(bool disposing)
            {
                if (Interlocked.CompareExchange(ref _info._state, StateFlush, _state) == _state)
                {
                    _info._stream.Position = 0;
                    _info._stream.SetLength(0);
                    WriteTo(_info._stream);
                    _info._stream.Position = 0;
                    Interlocked.Exchange(ref _info._state, StateNone);
                }

                base.Dispose(disposing);
            }
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Common.Impl
{
    using System;
    using System.IO;
    using System.Xml.Schema;
    using bgTeam.KitVlgDocs.Common.ImageGen;

    public class EmbeddedResources : IEmbeddedResources
    {
        private static readonly ImageDefinition _signatureStamp = Get("def01.json", x => ImageDefinition.FromJson(x.ReadToEnd()));

        private static readonly ImageDefinition _registrationStamp = Get("def02.json", x => ImageDefinition.FromJson(x.ReadToEnd()));

        private static readonly XmlSchema _transportSchema = Get("transport.xsd", x => XmlSchema.Read(x, (sender, e) => { }));

        private static readonly XmlSchema _containerSchema = Get("container.xsd", x => XmlSchema.Read(x, (sender, e) => { }));

        /// <inheritdoc/>
        public ImageDefinition SignatureStamp => _signatureStamp;

        /// <inheritdoc/>
        public ImageDefinition RegistrationStamp => _registrationStamp;

        /// <inheritdoc/>
        public XmlSchema TransportSchema => _transportSchema;

        /// <inheritdoc/>
        public XmlSchema ContainerSchema => _containerSchema;

        private static T Get<T>(string name, Func<StreamReader, T> reader)
        {
            using (var sReader = new StreamReader(typeof(IEmbeddedResources).Assembly.GetManifestResourceStream($"{typeof(IEmbeddedResources).Namespace}.Impl.Files.{name}")))
            {
                return reader.Invoke(sReader);
            }
        }
    }
}

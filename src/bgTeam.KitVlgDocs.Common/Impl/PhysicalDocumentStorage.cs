﻿namespace bgTeam.KitVlgDocs.Common.Impl
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class PhysicalDocumentStorage : IDocumentStorage
    {
        public PhysicalDocumentStorage(Uri root)
        {
            if (!(root.IsFile && root.IsAbsoluteUri))
            {
                throw new ArgumentException(@"Only file:// root allowed.", nameof(root));
            }

            if (root.AbsoluteUri.Last() != '/')
            {
                root = new Uri(root.AbsoluteUri + '/');
            }

            Root = root;
        }

        public Uri Root { get; }

        public void Delete()
        {
            Directory.Delete(Root.LocalPath, true);
        }

        public IEnumerable<IDocumentInfo> GetDocuments(SearchOption searchOption)
        {
            return Directory.EnumerateFiles(Root.LocalPath, "*", searchOption).Select(x => new PhysicalDocumentInfo(new Uri(x), this));
        }

        public IEnumerable<IDocumentInfo> GetDocuments(string pattern, SearchOption searchOption)
        {
            return Directory.EnumerateFiles(Root.LocalPath, pattern, searchOption).Select(x => new PhysicalDocumentInfo(new Uri(x), this));
        }

        public IDocumentStorage CreateDirectory(string path)
        {
            if (Root.TryCreate(path, out var uri))
            {
                Directory.CreateDirectory(uri.LocalPath);
                return new PhysicalDocumentStorage(uri);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public void Move(IDocumentInfo srcDocument, string destFileName, bool replace = false)
        {
            var uri = Root.MakeRelative(destFileName);

            if (srcDocument.Path == uri)
            {
                return;
            }
            else if (File.Exists(uri.LocalPath))
            {
                if (replace)
                {
                    File.Delete(uri.LocalPath);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }

            if (srcDocument.Path.IsFile)
            {
                File.Move(srcDocument.Path.LocalPath, uri.LocalPath);
            }
            else
            {
                using (var srcStream = srcDocument.CreateReadStream())
                {
                    using (var destStream = File.Open(uri.LocalPath, FileMode.OpenOrCreate))
                    {
                        srcStream.CopyTo(destStream);
                    }
                }

                srcDocument.Delete();
            }
        }

        public void Move(IDocumentStorage srcStorage, string destDirectoryName, bool replace = false)
        {
            if (Root.TryCreate(destDirectoryName, out var uri))
            {
                if (srcStorage.Root == uri)
                {
                    return;
                }
                else if (Directory.Exists(uri.LocalPath))
                {
                    if (replace)
                    {
                        Directory.Delete(uri.LocalPath, true);
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                }

                Directory.CreateDirectory(uri.LocalPath);

                foreach (var item in srcStorage.GetDocuments(SearchOption.AllDirectories))
                {
                    if (item.Path.IsFile)
                    {
                        File.Move(item.Path.LocalPath, uri.MakeRelative(item.Name).LocalPath);
                    }
                    else
                    {
                        using (var srcStream = item.CreateReadStream())
                        {
                            using (var destStream = File.Open(uri.MakeRelative(item.Name).LocalPath, FileMode.OpenOrCreate))
                            {
                                srcStream.CopyTo(destStream);
                            }
                        }
                    }
                }

                srcStorage.Delete();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Common.Impl
{
    using System;
    using System.IO;

    public class PhysicalDocumentInfo : IDocumentInfo
    {
        public PhysicalDocumentInfo(Uri path, PhysicalDocumentStorage storage)
        {
            if (!(path.IsFile && path.IsAbsoluteUri))
            {
                throw new ArgumentException(@"Only file:// path allowed.", nameof(path));
            }

            Path = path;
            Storage = storage;
        }

        public Uri Path { get; }

        public string Name => System.IO.Path.GetFileName(Path.LocalPath);

        public IDocumentStorage Storage { get; }

        public void Delete()
        {
            if (File.Exists(Path.LocalPath))
            {
                File.Delete(Path.LocalPath);
            }
        }

        public Stream CreateReadStream()
        {
            if (!File.Exists(Path.LocalPath))
            {
                throw new InvalidOperationException();
            }

            return File.OpenRead(Path.LocalPath);
        }
    }
}

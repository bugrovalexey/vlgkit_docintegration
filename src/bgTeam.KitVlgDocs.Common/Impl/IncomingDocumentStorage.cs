﻿namespace bgTeam.KitVlgDocs.Common.Impl
{
    using System.Collections.Generic;
    using System.IO;
    using Microsoft.Extensions.Options;

    public class IncomingDocumentStorage : IIncomingDocumentStorage
    {
        private readonly IAppLogger _logger;
        private readonly IDocumentStorage _errorDocumentStorage;
        private readonly IDocumentStorage _archiveDocumentStorage;
        private readonly IDocumentStorage _oldMedoDocumentStorage;
        private readonly IDocumentStorage _incomingDocumentStorage;

        public IncomingDocumentStorage(IAppLogger logger, IOptions<ImportAppOptions> options)
        {
            _logger = logger;
            _errorDocumentStorage = StorageHelper.GetDocumentStorage(options.Value.ErrorFolder);
            _archiveDocumentStorage = StorageHelper.GetDocumentStorage(options.Value.ArchiveFolder);
            _oldMedoDocumentStorage = StorageHelper.GetDocumentStorage(options.Value.OldMedoFolder);
            _incomingDocumentStorage = StorageHelper.GetDocumentStorage(options.Value.IncomingFolder);
        }

        public IEnumerable<IDocumentInfo> GetIncomingDocuments(string pattern)
            => _incomingDocumentStorage.GetDocuments(pattern, SearchOption.AllDirectories);

        public void MoveToError(IDocumentStorage documentStorage, string directory = null)
        {
            try
            {
                _errorDocumentStorage.Move(documentStorage, directory, true);
            }
            catch
            {
                _logger.Error($"Error while moving {documentStorage.Root} to error folder.");
                throw;
            }
        }

        public void MoveToArchive(IDocumentStorage documentStorage, string directory = null)
        {
            try
            {
                _archiveDocumentStorage.Move(documentStorage, directory, true);
            }
            catch
            {
                _logger.Error($"Error while moving {documentStorage.Root} to archive folder.");
                throw;
            }
        }

        public void MoveToOldMedo(IDocumentStorage documentStorage, string directory = null)
        {
            try
            {
                _oldMedoDocumentStorage.Move(documentStorage, directory, true);
            }
            catch
            {
                _logger.Error($"Error while moving {documentStorage.Root} to oldmedo folder.");
                throw;
            }
        }
    }
}

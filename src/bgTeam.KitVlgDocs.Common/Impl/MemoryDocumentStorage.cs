﻿namespace bgTeam.KitVlgDocs.Common.Impl
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class MemoryDocumentStorage : IWritableDocumentStorage
    {
        private readonly ConcurrentDictionary<Uri, MemoryStream> _storage;

        public MemoryDocumentStorage(Uri root)
        {
            if (root.Scheme != "mem" || !root.IsAbsoluteUri)
            {
                throw new ArgumentException(@"Only mem:// path allowed.", nameof(root));
            }

            if (root.AbsoluteUri.Last() != '/')
            {
                root = new Uri(root.AbsoluteUri + '/');
            }

            Root = root;

            _storage = new ConcurrentDictionary<Uri, MemoryStream>();
        }

        public Uri Root { get; }

        public void Delete()
        {
            _storage.Clear();
        }

        public void Remove(Uri path)
        {
            _storage.TryRemove(path, out var stream);
        }

        public IEnumerable<IDocumentInfo> GetDocuments(SearchOption searchOption)
        {
            return EnumerateFiles(Root, string.Empty, searchOption);
        }

        public IEnumerable<IDocumentInfo> GetDocuments(string pattern, SearchOption searchOption)
        {
            return EnumerateFiles(Root, pattern, searchOption);
        }

        public void Move(IDocumentInfo srcDocument, string destFileName, bool replace = false)
        {
            var uri = Root.MakeRelative(destFileName);

            if (srcDocument.Path == uri)
            {
                return;
            }

            var existing = _storage.FirstOrDefault(x => x.Key.AbsolutePath == uri.AbsolutePath);

            if (existing.Key != null)
            {
                if (replace)
                {
                    if (_storage.TryRemove(existing.Key, out var stream))
                    {
                        stream.Close();
                    }
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }

            using (var srcStream = srcDocument.CreateReadStream())
            {
                var destStream = new MemoryStream();
                srcStream.CopyTo(destStream);
                destStream.Position = 0;
                _storage[uri] = destStream;
            }

            srcDocument.Delete();
        }

        public void Move(IDocumentStorage srcStorage, string destDirectoryName, bool replace = false)
        {
            if (Root.TryCreate(destDirectoryName, out var uri))
            {
                if (srcStorage.Root == uri)
                {
                    return;
                }

                var existing = _storage.Any(x => x.Key.AbsolutePath.StartsWith(uri.AbsolutePath));

                if (existing)
                {
                    if (replace)
                    {
                        foreach (var item in _storage.Where(x => x.Key.AbsolutePath.StartsWith(uri.AbsolutePath)))
                        {
                            if (_storage.TryRemove(item.Key, out var stream))
                            {
                                stream.Close();
                            }
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                }

                foreach (var item in srcStorage.GetDocuments(SearchOption.AllDirectories))
                {
                    using (var srcStream = item.CreateReadStream())
                    {
                        var destStream = new MemoryStream();
                        srcStream.CopyTo(destStream);
                        destStream.Position = 0;
                        _storage[uri.MakeRelative(item.Name)] = destStream;
                    }
                }

                srcStorage.Delete();
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public IDocumentStorage CreateDirectory(string path)
        {
            if (Root.TryCreate(path, out var uri))
            {
                return new MemoryDocumentStorage(uri);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        public IWritableDocumentInfo Create(string fileName, bool replace = false)
        {
            var uri = Root.MakeRelative(fileName);

            var existing = _storage.FirstOrDefault(x => x.Key.AbsolutePath == uri.AbsolutePath);

            if (existing.Key != null)
            {
                if (replace)
                {
                    Remove(existing.Key);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }

            var stream = new MemoryStream();
            _storage[uri] = stream;
            return new MemoryDocumentInfo(uri, stream, this);
        }

        private IEnumerable<IDocumentInfo> EnumerateFiles(Uri root, string pattern, SearchOption searchOption)
        {
            var regex = string.IsNullOrEmpty(pattern) ? null : RegexHelper.ConvertPattern(pattern);

            var storage = _storage.AsEnumerable();

            if (searchOption != SearchOption.AllDirectories)
            {
                storage = storage.Where(x => Path.GetDirectoryName(x.Key.AbsolutePath) == root.AbsolutePath);
            }

            if (regex != null)
            {
                storage = storage.Where(x => regex.IsMatch(Path.GetFileName(x.Key.AbsolutePath)));
            }

            return storage.Select(x => new MemoryDocumentInfo(x.Key, x.Value, this));
        }
    }
}

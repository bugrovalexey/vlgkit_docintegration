﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Linq;
    using System.Security.Cryptography.Pkcs;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.DataAccess;
    using bgTeam.KitVlgDocs.Delo;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using Newtonsoft.Json;

    public class ServerExportDeloDocumentsStory : IStory<ServerExportDeloDocumentsStoryContext, OperationResultDto>
    {
        private readonly IAppLogger _logger;
        private readonly IDeloService _deloService;
        private readonly IStoryBuilder _storyBuilder;
        private readonly IMedoDocumentLoader _documentLoader;

        public ServerExportDeloDocumentsStory(
            IAppLogger logger,
            IDeloService deloService,
            IStoryBuilder storyBuilder,
            IMedoDocumentLoader documentLoader)
        {
            _logger = logger;
            _deloService = deloService;
            _storyBuilder = storyBuilder;
            _documentLoader = documentLoader;
        }

        public OperationResultDto Execute(ServerExportDeloDocumentsStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<OperationResultDto> ExecuteAsync(ServerExportDeloDocumentsStoryContext context)
        {
            var document = (await _documentLoader.LoadDeloDocuments(new[] { context.Document.Id }))
                .FirstOrDefault();

            if (document == null)
            {
                _logger.Warning($"Документ {context.Document.Id} не найден.");
                return (false, $"Документ {context.Document.Id} не найден.");
            }

            try
            {
                _logger.Info($"Обрабатываю документ {context.Document.Id}");

                if (context.Document.Signature == null || context.Document.Signature.Length == 0)
                {
                    _logger.Warning($"Документ {context.Document.Id} не имеет подписи.");
                    return (false, $"Документ {context.Document.Id} не имеет подписи.");
                }

                if (document.Signer == null)
                {
                    _logger.Warning($"Документ {context.Document.Id} не имеет данных о подписании.");
                    return (false, $"Документ {context.Document.Id} не имеет данных о подписании.");
                }

                var documentFile = document.Files.First();

                var signedCms = new SignedCms(
                    new ContentInfo(documentFile.File.Data),
                    true);

                try
                {
                    signedCms.Decode(context.Document.Signature);

                    signedCms.CheckSignature(true);
                }
                catch (Exception exp)
                {
                    _logger.Error(exp);
                    _logger.Warning($"Документ {context.Document.Id} имеет неверную подпись.");
                    return (false, $"Документ {context.Document.Id} имеет неверную подпись.");
                }

                documentFile.Signatures = new[]
                {
                    context.Document.Signature,
                };

                _logger.Info($"Экспорт документа в систему Дело {GetDocument()}");

                var status = _deloService.ExportDocument(document);

                await _storyBuilder.Build(status)
                    .ReturnAsync<ValueTuple>();

                return status.Ins != null
                    ? (true, status.Ins.ToString())
                    : (false, $"Ошибка при обработке документа: {document.Id}.");
            }
            catch (Exception exp)
            {
                _logger.Error($"Ошибка при обработке документа: {document.Id}.");
                _logger.Error(exp);

                return (false, exp.Message);
            }

            string GetDocument() => Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(
                new
                {
                    document.Addresse,
                    document.Annotation,
                    document.ControlDate,
                    Files = document.Files.Select(x => x.File.FileName).ToArray(),
                    document.Id,
                    document.IncludeList,
                    document.RegistrationDate,
                    document.RegistrationNumber,
                    document.Signer,
                    document.Subjects,
                },
                Formatting.Indented)));
        }
    }
}

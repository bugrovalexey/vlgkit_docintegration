﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography.Pkcs;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class ClientExportMedoDocumentsStory : IStory<ClientExportMedoDocumentsStoryContext, ValueTuple>
    {
        private static int _counter;

        private readonly IAppLogger _logger;
        private readonly IOptions<ExportAppOptions> _options;
        private readonly IMedoWebApi _medoWebApi;

        public ClientExportMedoDocumentsStory(
            IAppLogger logger,
            IOptions<ExportAppOptions> options,
            IMedoWebApi medoWebApi)
        {
            _logger = logger;
            _options = options;
            _medoWebApi = medoWebApi;
        }

        public ValueTuple Execute(ClientExportMedoDocumentsStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<ValueTuple> ExecuteAsync(ClientExportMedoDocumentsStoryContext context)
        {
            var outDocs = await _medoWebApi.GetOutgoingDocumentsMeta();

            var list = new List<long>();

            foreach (var document in outDocs)
            {
                if (TryFindCertificates(document.Signers, out var _))
                {
                    list.Add(document.Id);
                }
            }

            var unprocessed = outDocs
                .Where(x => !list.Contains(x.Id))
                .Select(x => new
                {
                    x.Id,
                    Signers = string.Join(";", x.Signers.Select(z => (z.Id, z.ContactId))),
                }).ToArray();

            if (unprocessed.Any() && (Interlocked.Increment(ref _counter) & 0xF) == 0)
            {
                _logger.Warning($"Отсутствуют полностью либо частично сетификаты для {unprocessed.Length} документов {JsonConvert.SerializeObject(unprocessed)}.");
            }

            if (list.Count > 0)
            {
                _logger.Info($"Приступаю к экспорту документов, найдено - {list.Count}.");

                foreach (var document in await _medoWebApi.GetOutgoingDocumentsData(list))
                {
                    if (!TryFindCertificates(document.Signers, out var certificates))
                    {
                        continue;
                    }

                    var signedCms = new SignedCms(
                        new ContentInfo(document.Document),
                        true);

                    foreach (var certificate in certificates)
                    {
                        signedCms.ComputeSignature(new CmsSigner(certificate));
                    }

                    var (success, result) = await _medoWebApi.ExportDocument(new OutgoinDocumentDto
                    {
                        Id = document.Id,
                        Signature = signedCms.Encode(),
                    });

                    if (success)
                    {
                        _logger.Info($"Документ {document.Id}, успешно экспортирован с номером - {result}.");
                    }
                    else
                    {
                        _logger.Error(result);
                    }
                }
            }

            return default;
        }

        private bool TryFindCertificate(OutgoingDocumentSignerDto signer, out X509Certificate2 certificate)
        {
            var storeName = Enum.TryParse<StoreName>(signer.Store, true, out var name)
                ? name
                : _options.Value.StoreName;

            using (var store = new X509Store(storeName, _options.Value.StoreLocation))
            {
                store.Open(OpenFlags.ReadOnly);

                var result = store.Certificates.Find(_options.Value.CertificateFindMode, signer.Id, true);

                certificate = result.OfType<X509Certificate2>().FirstOrDefault();

                return certificate != null;
            }
        }

        private bool TryFindCertificates(OutgoingDocumentSignerDto[] signers, out IEnumerable<X509Certificate2> certificates)
        {
            var result = new List<X509Certificate2>();

            foreach (var signer in signers)
            {
                if (!TryFindCertificate(signer, out var certificate))
                {
                    certificates = null;
                    return false;
                }

                result.Add(certificate);
            }

            certificates = result;
            return true;
        }
    }
}

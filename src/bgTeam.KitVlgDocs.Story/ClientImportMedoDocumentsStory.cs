﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography.Pkcs;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Xml.Serialization;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using bgTeam.KitVlgDocs.Domain.Xml;
    using bgTeam.KitVlgDocs.Domain.Xml.Iedms;
    using Microsoft.Extensions.Options;

    public class ClientImportMedoDocumentsStory : IStory<ClientImportMedoDocumentsStoryContext, ValueTuple>
    {
        private readonly IAppLogger _logger;
        private readonly IOptions<ImportAppOptions> _options;
        private readonly IMedoWebApi _medoWebApi;
        private readonly IStoryBuilder _storyBuilder;
        private readonly IIncomingDocumentStorage _documentStorage;

        public ClientImportMedoDocumentsStory(
            IAppLogger logger,
            IOptions<ImportAppOptions> options,
            IMedoWebApi medoWebApi,
            IStoryBuilder storyBuilder,
            IIncomingDocumentStorage documentStorage)
        {
            _logger = logger;
            _options = options;
            _medoWebApi = medoWebApi;
            _storyBuilder = storyBuilder;
            _documentStorage = documentStorage;
        }

        public ValueTuple Execute(ClientImportMedoDocumentsStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<ValueTuple> ExecuteAsync(ClientImportMedoDocumentsStoryContext context)
        {
            try
            {
                var documents = (await _storyBuilder.Build(new ClientLoadMedoDocumentsForImportStoryContext())
                    .ReturnAsync<IEnumerable<MedoDocumentDto>>())
                    .ToArray();

                _logger.Info($"Приступаю к обработке входящих документов, найдено - {documents.Length}");

                foreach (var item in documents)
                {
                    try
                    {
                        if (item.Communication == null)
                        {
                            _documentStorage.MoveToError(item.Storage, item.Directory);
                            continue;
                        }

                        var process = GetProcess(item);

                        switch (process.Action)
                        {
                            case "MoveTo":
                                MoveDocument();
                                break;
                            case "Import":
                                await ImportDocument();
                                break;
                            case null:
                                _documentStorage.MoveToError(item.Storage, item.Directory);
                                continue;
                            default: throw new InvalidOperationException($"Unknow process action {process.Action}.");
                        }

                        void MoveDocument()
                        {
                            var headerGuid = item.Communication.Header.Uid;

                            var storage = StorageHelper.GetDocumentStorage(process.Folder);

                            _logger.Info($"Перемещение контейнера {item.Storage.Root} ({headerGuid}) в папку {new Uri(process.Folder).GetComponents(UriComponents.AbsoluteUri & ~UriComponents.UserInfo, UriFormat.UriEscaped)}.");

                            storage.Move(item.Storage, item.Directory, true);
                        }

                        async Task ImportDocument()
                        {
                            var headerGuid = item.Communication.Header.Uid;
                            var senderGuid = item.Communication.Header.Source.Uid;
                            var senderName = item.Communication.Header.Source.Organization;

                            if (item.Communication.Version != _options.Value.SupportedVersion)
                            {
                                _logger.Info($"Невозможно импортировать контейнер {item.Storage.Root} ({headerGuid}), версия {item.Communication.Version} не поддерживается.");
                                _documentStorage.MoveToOldMedo(item.Storage, item.Directory);
                                return;
                            }

                            _logger.Info($"Обработка контейнера {item.Storage.Root}, guid - {item.Container.Passport.Uid}");

                            var (success, result) = await _medoWebApi.ImportDocument(new IncomingDocumentDto
                            {
                                Kind = process.Kind,
                                Files = item.Container.Files.ToArray(),
                                Passport = item.Container.Passport,
                                Document = item.Container.Document,
                                HeaderGuid = headerGuid,
                                SenderName = senderName,
                                SenderGuid = senderGuid,
                                PassportXml = item.Container.PassportXml,
                                DocumentXml = ConvertXml(item.Container.Passport, process.Kind, item.Container.Files.Select(x => x.FileName)),
                                SignatureP7s = item.Container.Signatures.ToArray(),
                                MedoAddressees = item.Addressies,
                                SignatureStatus = CheckSignature(item.Container.Document.Data, item.Container.Passport, item.Container.Signatures),
                            });

                            if (success)
                            {
                                _documentStorage.MoveToArchive(item.Storage, item.Directory);
                                _logger.Info($"Контейнер {item.Storage.Root}, успешно импортирован с номером - {result}");
                            }
                            else
                            {
                                _documentStorage.MoveToError(item.Storage, item.Directory);
                                _logger.Error(result);
                            }
                        }
                    }
                    catch (Exception exp)
                    {
                        _documentStorage.MoveToError(item.Storage, item.Directory);
                        _logger.Error($"Ошибка при обработке контейнера: {item.Storage.Root}.");
                        _logger.Error(exp);
                    }
                }
            }
            catch (Exception exp)
            {
                _logger.Error(exp);
            }

            _logger.Info("Обработка входящих документов завершена.");
            return default;
        }

        private (string Action, string Kind, string Folder) GetProcess(MedoDocumentDto item)
        {
            if (_options.Value.Processing.TryGetValue(item.Communication.Header.Type.ToString(), out var processing))
            {
                switch (item.Communication.Header.Type)
                {
                    case MessageType.Документ when item.Communication?.Document != null:
                        return GetProcess(processing, item.Communication.Document.Kind?.Value);
                    case MessageType.Уведомление when item.Communication?.Notification != null:
                        return GetProcess(processing, item.Communication.Notification.Type.ToString());
                    case MessageType.Квитанция when item.Communication?.Acknowledgment != null:
                        return GetProcess(processing, item.Communication.Acknowledgment.Comment);
                    case MessageType.Транспортный_Контейнер when item.Container?.Passport != null:
                        return GetProcess(processing, item.Container.Passport.Requisites?.DocumentKind?.Value);
                    default:
                        return default;
                }
            }

            return _options.Value.DefaultProcessing;
        }

        private (string Action, string Kind, string Folder) GetProcess(Dictionary<string, ProcessingData> processing, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                foreach (var item in processing)
                {
                    if (Regex.IsMatch(value, item.Key))
                    {
                        return item.Value;
                    }
                }
            }

            return processing.TryGetValue("Default", out var @default) ? (ValueTuple<string, string, string>)@default : default;
        }

        private FileDto ConvertXml(Container passport, string kind, IEnumerable<string> fileNames)
        {
            var author = passport.Authors.First();
            var communication = new Communication
            {
                Version = "2.0",
                Header = new CommunicationHeader
                {
                    Uid = passport.Uid,
                    Type = MessageType.Документ,
                    Source = new CommunicationPartner
                    {
                        Uid = Guid.NewGuid().ToString(),
                        Organization = author.Organization.Title,
                    },
                },
                Document = new Domain.Xml.Iedms.Document
                {
                    Id = "0",
                    Uid = passport.Uid,
                    Kind = new Domain.Xml.Iedms.QualifiedValue
                    {
                        Id = "0",
                        Value = kind,
                    },
                    Num = new DocumentNumber
                    {
                        Date = author.Registration?.Date,
                        Number = author.Registration?.Number,
                    },
                    Annotation = passport.Requisites.Annotation,
                    Pages = long.TryParse(passport.Document.PagesQuantity, out var num) ? num : 0,
                    EnclosuresPages = long.TryParse(passport.Document.PagesQuantity, out num) ? num : 0,
                },
                Files = new CommunicationFiles(),
            };
            communication.Document.Addressees.AddRange(passport.Addressees.Select(x => new Addressee
            {
                Organization = new Domain.Xml.Iedms.QualifiedValue
                {
                    Id = "0",
                    Value = x.Organization.Title,
                },
            }));
            communication.Document.Correspondents.AddRange(passport.Authors.Select(x => new Correspondent
            {
                Organization = new Domain.Xml.Iedms.QualifiedValue
                {
                    Id = "0",
                    Value = x.Organization.Title,
                },
                Num = new DocumentNumber
                {
                    Date = x.Registration?.Date,
                    Number = x.Registration?.Number,
                },
            }));
            communication.Document.Signatories.AddRange(passport.Authors.SelectMany(x => x.Sign).Select(x => new Signatory
            {
                Person = new Domain.Xml.Iedms.QualifiedValue
                {
                    Id = "0",
                    Value = x.Person.Name,
                },
            }));
            communication.Files.File.Add(new AssociatedFile
            {
                LocalId = 0,
                LocalName = passport.Document.LocalName,
                Group = FileGroup.Текст_Документа,
                Description = passport.Document.Description ?? "текст документа",
                Pages = long.TryParse(passport.Document.PagesQuantity, out num) ? (long?)num : null,
            });
            var names = fileNames.Except(new[] { passport.Document.LocalName }.Union(passport.Authors.SelectMany(x => x.Sign).Select(x => x.DocumentSignature.LocalName)));
            communication.Files.File.AddRange(names.Select((x, y) => new AssociatedFile
            {
                LocalId = 1 + y,
                LocalName = x,
                Group = FileGroup.Текст_Приложения,
                Description = "текст приложения",
                Pages = 1,
            }));
            return SerializeCommunication();

            FileDto SerializeCommunication()
            {
                var ns = new XmlSerializerNamespaces();
                ns.Add("xdms", "http://www.infpres.com/IEDMS");
                ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                var serializer = new XmlSerializer(typeof(Communication));
                using (var mem = new MemoryStream())
                {
                    using (var writer = new StreamWriter(mem, Encoding.GetEncoding("windows-1251")))
                    {
                        serializer.Serialize(writer, communication, ns);
                        return new FileDto
                        {
                            Data = mem.ToArray(),
                            FileName = _options.Value.CommunicationXml,
                        };
                    }
                }
            }
        }

        private string CheckSignature(byte[] document, Container passport, IEnumerable<FileDto> signatures)
        {
            try
            {
                var signedCms = new SignedCms(new ContentInfo(document), true);

                foreach (var item in signatures)
                {
                    try
                    {
                        signedCms.Decode(item.Data);
                    }
                    catch
                    {
                        var text = Encoding.ASCII.GetString(item.Data).Replace("\r", string.Empty).Replace("\n", string.Empty);
                        var data = Convert.FromBase64String(text);
                        signedCms.Decode(data);
                    }

                    signedCms.CheckSignature(true);

                    var sign = passport.Authors.SelectMany(x => x.Sign).FirstOrDefault(x => x?.DocumentSignature?.LocalName == item.FileName);

                    if (string.IsNullOrEmpty(sign?.Person?.Name))
                    {
                        return "не действительна";
                    }

                    var personSubjectParts = sign.Person.Name.Split(' ');

                    var subjects = signedCms.Certificates.OfType<X509Certificate2>().Select(x => x.Subject).ToArray();

                    if (!subjects.Any(x => personSubjectParts.All(z => x.Contains(z))))
                    {
                        return "не действительна";
                    }
                }

                return "действительна";
            }
            catch
            {
                return "не действительна";
            }
        }
    }
}

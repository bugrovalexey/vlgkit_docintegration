﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.Delo;

    public class ClientImportDeloDocumentsStory : IStory<ClientImportDeloDocumentsStoryContext, ValueTuple>
    {
        private readonly IAppLogger _logger;
        private readonly IDeloWebApi _deloWebApi;
        private readonly IDeloService _deloService;
        private readonly IStoryBuilder _storyBuilder;

        public ClientImportDeloDocumentsStory(
            IAppLogger logger,
            IDeloWebApi deloWebApi,
            IDeloService deloService,
            IStoryBuilder storyBuilder)
        {
            _logger = logger;
            _deloWebApi = deloWebApi;
            _deloService = deloService;
            _storyBuilder = storyBuilder;
        }

        public ValueTuple Execute(ClientImportDeloDocumentsStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<ValueTuple> ExecuteAsync(ClientImportDeloDocumentsStoryContext context)
        {
            try
            {
                var documents = _deloService.LoadDocumentsForImport()
                   .ToArray();

                _logger.Info($"Приступаю к обработке входящих документов 'Дело', найдено - {documents.Length}");

                foreach (var item in documents)
                {
                    try
                    {
                        var (success, result) = await _deloWebApi.ImportDocument(item);

                        if (success)
                        {
                            _logger.Info($"Документ 'Дело' {item.Isn}, успешно импортирован с номером - {result}");

                            _deloService.SetImportState(item.Isn, result, DateTime.Now, DateTime.Now);
                        }
                        else
                        {
                            if (result?.Equals($"Документ 'Дело' с идентификатором {item.Isn} уже импортирован!", StringComparison.OrdinalIgnoreCase) ?? false)
                            {
                                _deloService.SetImportState(item.Isn, null, null, DateTime.Now);
                            }
                            else
                            {
                                _deloService.SetImportStateNote(item.Isn, false);
                            }

                            _logger.Error(result);
                        }
                    }
                    catch (Exception exp)
                    {
                        _logger.Error($"Ошибка при обработке документа 'Дело': {item.Isn}.");
                        _logger.Error(exp);
                    }
                }
            }
            catch (Exception exp)
            {
                _logger.Error(exp);
            }

            _logger.Info("Обработка входящих документов 'Дело' завершена.");
            return default;
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.DataAccess;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using Microsoft.Extensions.Options;

    public class ServerImportMedoDocumentsStory : IStory<IncomingDocumentDto, OperationResultDto>
    {
        private readonly IAppLogger _logger;
        private readonly IOptions<WebAppOptions> _options;
        private readonly IMedoPackage _package;
        private readonly ICrudService _crudService;

        public ServerImportMedoDocumentsStory(
            IAppLogger logger,
            IOptions<WebAppOptions> options,
            IMedoPackage package,
            ICrudService crudService)
        {
            _logger = logger;
            _options = options;
            _package = package;
            _crudService = crudService;
        }

        public OperationResultDto Execute(IncomingDocumentDto context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<OperationResultDto> ExecuteAsync(IncomingDocumentDto context)
        {
            if (await _package.CheckMedoDocumentUid(context.Passport.Uid))
            {
                _logger.Warning($"Документ с идентификатором {context.Passport.Uid} уже импортирован!");
                return (false, $"Документ с идентификатором {context.Passport.Uid} уже импортирован!");
            }

            var author = context.Passport.Authors.First();

            var from_contact_id = await _package.GetOrCreateMedoAddress(context.SenderName, context.SenderGuid, context.MedoAddressees.FirstOrDefault());

            var dfa_role_signer = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Signer");
            var dfa_role_sender = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Sender");
            var dfa_role_addressee = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Addressee");
            var dfa_role_medo_sender = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "MedoSender");
            var dfa_role_correspondent = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Correspondent");

            var medo_role_sender = await _package.FindEnumId("Medo.References", "AddresseeRole", "Sender");
            var medo_role_signatory = await _package.FindEnumId("Medo.References", "AddresseeRole", "Signatory");
            var medo_role_addressee = await _package.FindEnumId("Medo.References", "AddresseeRole", "Addressee");
            var medo_role_correspondent = await _package.FindEnumId("Medo.References", "AddresseeRole", "Correspondent");

            var common_bar_code = await _package.FindEClassId("Common", "BarCode");
            var medo_external_id = await _package.FindEClassId("Medo.References", "ExternalID");
            var medo_header_guid = await _package.FindEClassId("Medo.References", "HeaderGUID");
            var medo_external_guid = await _package.FindEClassId("Medo.References", "ExternalGUID");

            var now = DateTime.Now;

            var bclass_id = await _package.FindBClassId("Medo.Documents", "Document");

            var eclass_id = await _package.FindEClassId("Medo.Documents", "IncomingDocument");

            var delivery_kind_id = await _package.FindReferenceItemId("Contacts", "DeliveryKind", "MEDO-2");

            var state_id = await _package.FindReferenceItemId("Docflow.References", "DocumentState", "Create");

            var type_id = await _package.FindReferenceItemId("Docflow.References", "DocumentType", "IncomingDocument");

            var author_contact = await _package.GetContact(_options.Value.ContactsMapping["Administrator"]);

            if (author_contact.Id == null)
            {
                _logger.Warning("Не найден контакт 'Administrator'.");
                return (false, "Не найден контакт 'Administrator'.");
            }

            var to_contact = await _package.GetContact(_options.Value.ContactsMapping["Apparatus"]);

            if (to_contact.Id == null)
            {
                _logger.Warning("Не найден контакт 'Apparatus'.");
                return (false, "Не найден контакт 'Apparatus'.");
            }

            var folder_id = await _package.GetFolderByDocumentKind(to_contact.Id.Value, context.Kind) ??
                throw new InvalidOperationException($"Не удалось определить папку для типа документа '{context.Kind}'.");

            var files = new List<FileDto>(context.Files);

            try
            {
                var document = context.Document.Data;

                foreach (var item in context.Passport.Authors)
                {
                    if (!string.IsNullOrEmpty(item?.Registration?.RegistrationStamp?.LocalName))
                    {
                        var stamp = item.Registration.RegistrationStamp;

                        var stampFile = files.FirstOrDefault(x => x.FileName == stamp.LocalName);

                        document = StampHelper.Watermark(document, stampFile.Data, x => x.Skip(int.Parse(stamp.Position.Page) - 1).First(), (x, y, z) =>
                        {
                            var tw = ImageHelper.ToPixels(int.Parse(stamp.Position.Dimension.W), z);
                            var th = ImageHelper.ToPixels(int.Parse(stamp.Position.Dimension.H), z);
                            var tx = ImageHelper.ToPixels(stamp.Position.TopLeft.X, z);
                            var ty = y.Height - ImageHelper.ToPixels(stamp.Position.TopLeft.Y, z) - th;
                            return new RectangleF(tx, ty, tw, th);
                        });
                    }

                    foreach (var item1 in item.Sign)
                    {
                        if (string.IsNullOrEmpty(item1?.DocumentSignature?.SignatureStamp?.LocalName))
                        {
                            continue;
                        }

                        var stamp = item1.DocumentSignature.SignatureStamp;

                        var stampFile = files.FirstOrDefault(x => x.FileName == stamp.LocalName);

                        document = StampHelper.Watermark(document, stampFile.Data, x => x.Skip(int.Parse(stamp.Position.Page) - 1).First(), (x, y, z) =>
                        {
                            var tw = ImageHelper.ToPixels(int.Parse(stamp.Position.Dimension.W), z);
                            var th = ImageHelper.ToPixels(int.Parse(stamp.Position.Dimension.H), z);
                            var tx = ImageHelper.ToPixels(stamp.Position.TopLeft.X, z);
                            var ty = y.Height - ImageHelper.ToPixels(stamp.Position.TopLeft.Y, z) - th;
                            return new RectangleF(tx, ty, tw, th);
                        });
                    }
                }

                files.Add(new FileDto
                {
                    Data = document,
                    FileName = $"{Path.GetFileNameWithoutExtension(context.Document.FileName)}_stamp{Path.GetExtension(context.Document.FileName)}",
                });
            }
            catch (Exception exp)
            {
                _logger.Error(exp);
                return (false, "Не удалось сгенерировать документ с внедренными штампами.");
            }

            var document_id = await _package.CreateDocument(
                type_id,
                eclass_id,
                null,
                author.Registration.Number,
                author.Registration.Date,
                null,
                null,
                from_contact_id,
                null,
                null,
                null,
                null,
                context.Passport.Requisites.Annotation,
                state_id,
                files,
                now,
                null,
                null,
                to_contact.Id);

            var identityex_id = await _package.CreateIdentityEx();

            var cnt_bar_code_value = (await _package.CounterNextValue("Barcode")).ToString();

            await Task.WhenAll(
                _package.CreateIdentityList(identityex_id, medo_external_id, "0"),
                _package.CreateIdentityList(identityex_id, common_bar_code, cnt_bar_code_value),
                _package.CreateIdentityList(identityex_id, medo_header_guid, context.HeaderGuid),
                _package.CreateIdentityList(identityex_id, medo_external_guid, context.Passport.Uid));

            await _package.CreateMedoDocument(document_id, context.DocumentXml);

            // 15695 (МЭДО - Папка документов)
            await _package.AddDocumentToFolder(document_id, folder_id, now);

            await _package.CreateMedoRoleLink(
                 await _package.CreateMedoContact(document_id, from_contact_id, organization: context.SenderName, correctdt: now),
                 medo_role_sender,
                 dfa_role_medo_sender);

            await _package.CreateAddressee(document_id, from_contact_id, dfa_role_medo_sender);
            await _package.CreateAddressee(document_id, from_contact_id, dfa_role_sender, out_date: author.Registration.Date, out_registration: author.Registration.Number);

            await _package.CreateMedoRoleLink(
                 await _package.CreateMedoContact(document_id, to_contact.Id, organization: to_contact.Name, correctdt: now),
                 medo_role_addressee,
                 dfa_role_addressee);

            foreach (var sign in author.Sign)
            {
                var contact_id = await _package.CreateMedoContact(
                    document_id,
                    employee: sign.Person.Name,
                    position: sign.Person.Post,
                    note: author.Registration.Date.ToString("yyyy-MM-dd"),
                    correctdt: now);

                await _package.CreateMedoRoleLink(contact_id, medo_role_signatory, dfa_role_signer);
                await _package.CreateMedoRoleLink(contact_id, medo_role_correspondent, dfa_role_correspondent);
            }

            await _package.CreateAddressee(document_id, to_contact.Id.Value, dfa_role_addressee);

            await _package.CreateDocStateHistory(document_id, state_id, author_contact.Id.Value, now, "admin");

            await _crudService.ExecuteAsync("UPDATE BO_BPDOCUMENT SET BCLASS_INDICATOR = :bclass_id, IDENTITYEX_ID = :identityex_id, NOTE = :note WHERE BPDOCUMENT_ID = :document_id", new
            {
                document_id,
                bclass_id,
                identityex_id,
                note = await _package.GenerateDocumentNote(context),
            });

            await _crudService.ExecuteAsync("UPDATE CBO_FILEVERSION SET CONTACT_ID = :contact_id WHERE FILEVERSION_ID IN (SELECT fv.FILEVERSION_ID FROM CBO_FILEVERSION fv INNER JOIN CBO_FILE f ON f.CURRENTVERSION_ID = fv.FILEVERSION_ID INNER JOIN CBO_FILE_LINK fl ON fl.FILE_ID = f.FILE_ID WHERE fl.BPDOCUMENT_ID = :document_id)", new
            {
                document_id,
                contact_id = author_contact.Id,
            });

            await _crudService.ExecuteAsync("UPDATE DFA_DOCUMENT SET DELIVERY_KIND_ID = :delivery_kind_id, INCLUDE_LIST = :include_list, STATEDT = :statedt WHERE BPDOCUMENT_ID = :document_id", new
            {
                document_id,
                statedt = now,
                delivery_kind_id,
                include_list = context.Passport.Document.PagesQuantity,
            });

            return (true, cnt_bar_code_value);
        }
    }
}

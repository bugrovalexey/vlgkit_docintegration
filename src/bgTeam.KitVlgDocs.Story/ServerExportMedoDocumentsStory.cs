﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography.Pkcs;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Xml.Serialization;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Common.Impl;
    using bgTeam.KitVlgDocs.DataAccess;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using bgTeam.KitVlgDocs.Domain.Xml;
    using bgTeam.KitVlgDocs.Domain.Xml.Iedms;
    using bgTeam.KitVlgDocs.Story.Common;
    using iTextSharp.text.pdf;
    using Microsoft.Extensions.Options;

    public class ServerExportMedoDocumentsStory : IStory<OutgoinDocumentDto, OperationResultDto>
    {
        private readonly IAppLogger _logger;
        private readonly IMedoPackage _package;
        private readonly IStoryBuilder _storyBuilder;
        private readonly IEmbeddedResources _resources;
        private readonly IMedoDocumentLoader _documentLoader;
        private readonly IOutgoingDocumentStorage _documentStorage;
        private readonly IOptions<WebAppOptions> _options;

        public ServerExportMedoDocumentsStory(
            IAppLogger logger,
            IMedoPackage package,
            IStoryBuilder storyBuilder,
            IEmbeddedResources resources,
            IMedoDocumentLoader documentLoader,
            IOutgoingDocumentStorage documentStorage,
            IOptions<WebAppOptions> options)
        {
            _logger = logger;
            _package = package;
            _resources = resources;
            _storyBuilder = storyBuilder;
            _documentLoader = documentLoader;
            _documentStorage = documentStorage;
            _options = options;
        }

        public OperationResultDto Execute(OutgoinDocumentDto context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<OperationResultDto> ExecuteAsync(OutgoinDocumentDto context)
        {
            var document = (await _documentLoader.LoadFullDocuments(new[] { context.Id }))
                .FirstOrDefault();

            if (document == null)
            {
                _logger.Warning($"Документ {context.Id} не найден.");
                return (false, $"Документ {context.Id} не найден.");
            }

            try
            {
                _logger.Info($"Обрабатываю документ {context.Id}, guid - {document.Container.Uid}");

                if (context.Signature == null || context.Signature.Length == 0)
                {
                    _logger.Warning($"Документ {context.Id} не имеет подписи.");
                    return (false, $"Документ {context.Id} не имеет подписи.");
                }

                if (document.Signers == null || document.Signers.Length == 0)
                {
                    _logger.Warning($"Документ {context.Id} не имеет данных о подписании.");
                    return (false, $"Документ {context.Id} не имеет данных о подписании.");
                }

                var signedCms = new SignedCms(
                    new ContentInfo(document.Files.First().Data),
                    true);

                try
                {
                    signedCms.Decode(context.Signature);

                    signedCms.CheckSignature(true);
                }
                catch (Exception exp)
                {
                    _logger.Error(exp);
                    _logger.Warning($"Документ {context.Id} имеет неверную подпись.");
                    return (false, $"Документ {context.Id} имеет неверную подпись.");
                }

                var signatureCerts = signedCms.SignerInfos.Cast<System.Security.Cryptography.Pkcs.SignerInfo>().Select(x => x.Certificate);

                foreach (var item3 in document.Signers)
                {
                    var signer = signatureCerts.Select(x => x.Thumbprint).FirstOrDefault(x => x == item3.Id);

                    if (signer == null)
                    {
                        _logger.Warning($"Документ {context.Id} не имеет подписи сертификатом {item3.Id}.");
                        return (false, $"Документ {context.Id} не имеет подписи сертификатом {item3.Id}.");
                    }
                }

                var pdfFile = document.Files.First();

                if (Path.GetExtension(pdfFile.FileName) != ".pdf")
                {
                    _logger.Warning($"Документ {context.Id} имеет неверный формат ({Path.GetExtension(pdfFile.FileName)}).");
                    return (false, $"Документ {context.Id} имеет неверный формат ({Path.GetExtension(pdfFile.FileName)}).");
                }

                var storage = CreateStorage();

                var containerSignature = default(string);

                var documentInfo = FromFile(storage, pdfFile);

                var pdfDocument = LoadDocument();

                var headerUid = Guid.NewGuid().ToString();

                document.Container.Document.LocalName = documentInfo.Name;

                var author = document.Container.Authors.First();
                var sign = author.Sign.First();

                var list = new List<IDocumentInfo>();
                var include = new List<string>();

                var certificate = signatureCerts.FirstOrDefault(x => x.Thumbprint == document.Signers[0].Id);

                if (certificate == null)
                {
                    _logger.Warning($"Не удалось найти сертификат для {sign.Person.Name}.");
                    return (false, $"Не удалось найти сертификат для {sign.Person.Name}.");
                }

                // Generate Images, add stamps to document
                {
                    var firstPage = pdfDocument.GetPageN(1);

                    var rect = pdfDocument.GetPageSize(firstPage);
                    var unit = firstPage.GetAsNumber(PdfName.USERUNIT)?.FloatValue ?? 72f;

                    var registrationStamp = await _storyBuilder.Build(new ImageGenStoryContext
                    {
                        Document = pdfFile.FileName,
                        Definition = _resources.RegistrationStamp,
                        Width = _options.Value.RegistrationStamp.Width,
                        Height = _options.Value.RegistrationStamp.Height,
                        Storage = storage,
                        Parameters = new
                        {
                            author.Registration.Number,
                            author.Registration.Date,
                        },
                    }).ReturnAsync<IDocumentInfo>();

                    author.Registration.RegistrationStamp.LocalName = registrationStamp.Name;
                    author.Registration.RegistrationStamp.Position.Page = $"{_options.Value.RegistrationStamp.Page}";

                    if (document.Stamps != null)
                    {
                        author.Registration.RegistrationStamp.Position.TopLeft.X = (short)document.Stamps.RegistrationX;
                        author.Registration.RegistrationStamp.Position.TopLeft.Y = (short)document.Stamps.RegistrationY;
                    }
                    else
                    {
                        author.Registration.RegistrationStamp.Position.TopLeft.X = (short)(ImageHelper.ToMillimeters(rect.Width, unit) * _options.Value.RegistrationStamp.Left);
                        author.Registration.RegistrationStamp.Position.TopLeft.Y = (short)(ImageHelper.ToMillimeters(rect.Height, unit) * _options.Value.RegistrationStamp.Top);
                    }

                    author.Registration.RegistrationStamp.Position.Dimension.W = $"{ImageHelper.ToMillimeters(_options.Value.RegistrationStamp.Width, unit):0}";
                    author.Registration.RegistrationStamp.Position.Dimension.H = $"{ImageHelper.ToMillimeters(_options.Value.RegistrationStamp.Height, unit):0}";

                    list.Add(registrationStamp);

                    var signatureStamp = await _storyBuilder.Build(new ImageGenStoryContext
                    {
                        Document = pdfFile.FileName,
                        Definition = _resources.SignatureStamp,
                        Width = _options.Value.SignatureStamp.Width,
                        Height = _options.Value.SignatureStamp.Height,
                        Storage = storage,
                        Parameters = new
                        {
                            Serial = certificate?.SerialNumber,
                            Subject = ParseSubject(certificate),
                            certificate?.NotAfter,
                            certificate?.NotBefore,
                        },
                    }).ReturnAsync<IDocumentInfo>();

                    sign.DocumentSignature.SignatureStamp.LocalName = signatureStamp.Name;

                    if (document.Stamps != null)
                    {
                        sign.DocumentSignature.SignatureStamp.Position.Page = $"{document.Stamps.SignaturePage}";
                        sign.DocumentSignature.SignatureStamp.Position.TopLeft.X = (short)document.Stamps.SignatureX;
                        sign.DocumentSignature.SignatureStamp.Position.TopLeft.Y = (short)document.Stamps.SignatureY;
                    }
                    else
                    {
                        sign.DocumentSignature.SignatureStamp.Position.Page = $"{_options.Value.SignatureStamp.Page}";
                        sign.DocumentSignature.SignatureStamp.Position.TopLeft.X = (short)(ImageHelper.ToMillimeters(rect.Width, unit) * _options.Value.SignatureStamp.Left);
                        sign.DocumentSignature.SignatureStamp.Position.TopLeft.Y = (short)(ImageHelper.ToMillimeters(rect.Height, unit) * _options.Value.SignatureStamp.Top);
                    }

                    sign.DocumentSignature.SignatureStamp.Position.Dimension.W = $"{ImageHelper.ToMillimeters(_options.Value.SignatureStamp.Width, unit):0}";
                    sign.DocumentSignature.SignatureStamp.Position.Dimension.H = $"{ImageHelper.ToMillimeters(_options.Value.SignatureStamp.Height, unit):0}";

                    list.Add(signatureStamp);

                    document.Container.Document.PagesQuantity = $"{GetPagesCount(documentInfo)}";

                    if (_options.Value.WatermarkPdf)
                    {
                        StampHelper.Watermark(documentInfo, signatureStamp, x => x.Skip(_options.Value.SignatureStamp.Page - 1).First(), (x, y, z) =>
                        {
                            var w = ImageHelper.ToPixels(sign.DocumentSignature.SignatureStamp.Position.TopLeft.X, z);
                            var h = y.Height - ImageHelper.ToPixels(sign.DocumentSignature.SignatureStamp.Position.TopLeft.Y, z) - x.Height;
                            return new RectangleF(w, h, x.Width, x.Height);
                        });

                        StampHelper.Watermark(documentInfo, registrationStamp, x => x.Skip(_options.Value.RegistrationStamp.Page - 1).First(), (x, y, z) =>
                        {
                            var w = ImageHelper.ToPixels(author.Registration.RegistrationStamp.Position.TopLeft.X, z);
                            var h = y.Height - ImageHelper.ToPixels(author.Registration.RegistrationStamp.Position.TopLeft.Y, z) - x.Height;
                            return new RectangleF(w, h, x.Width, x.Height);
                        });
                    }
                }

                // Save signature document
                {
                    var resultInfo = storage.Create($"{Path.GetFileNameWithoutExtension(documentInfo.Name)}_{0xDEAD}.p7s");

                    using (var src = resultInfo.CreateWriteStream())
                    {
                        src.Write(context.Signature, 0, context.Signature.Length);
                    }

                    sign.DocumentSignature.LocalName = resultInfo.Name;

                    list.Add(resultInfo);
                }

                list.Add(documentInfo);

                // Rest files
                {
                    var index = 0;
                    foreach (var file in RestFiles())
                    {
                        list.Add(file);
                        document.Container.Attachments.Add(new ContainerAttachmentsAttachment
                        {
                            Order = index.ToString(),
                            LocalName = file.Name,
                            Description = "Приложение к документу",
                        });
                        index++;
                    }
                }

                // Generate passport and sign container
                {
                    var passportInfo = SavePassport(storage, document.Container);

                    if (!string.IsNullOrEmpty(_options.Value.Certificate?.Key))
                    {
                        if (TryFindCertificate(_options.Value.Certificate, out var certificate2))
                        {
                            byte[] signData = null;

                            using (var mem = new MemoryStream())
                            {
                                // passport.xml always first
                                using (var reader = passportInfo.CreateReadStream())
                                {
                                    reader.CopyTo(mem);
                                }

                                signData = list
                                    .OrderBy(x => x.Name, StringComparer.Ordinal)
                                    .Aggregate(mem, (x, y) =>
                                    {
                                        using (var reader = y.CreateReadStream())
                                        {
                                            reader.CopyTo(x);
                                        }

                                        return x;
                                    })
                                    .ToArray();
                            }

                            try
                            {
                                var signedContainerCms = new SignedCms(
                                    new ContentInfo(signData),
                                    true);

                                signedContainerCms.ComputeSignature(new CmsSigner(certificate2));

                                var resultInfo = storage.Create($"container_{0xDEAD}.p7s");
                                var signatureData = signedContainerCms.Encode();

                                using (var src = resultInfo.CreateWriteStream())
                                {
                                    src.Write(signatureData, 0, signatureData.Length);
                                }

                                list.Add(resultInfo);

                                containerSignature = resultInfo.Name;
                            }
                            catch (Exception exp)
                            {
                                _logger.Error(exp);
                                _logger.Warning($"Ошибка при подписании контейнера сертификатом организации.");
                            }
                        }
                        else
                        {
                            _logger.Warning($"Не найден сертификат организации.");
                        }
                    }
                    else
                    {
                        _logger.Warning($"Не задан сертификат организации.");
                    }

                    list.Add(passportInfo);
                }

                IEnumerable<IDocumentInfo> RestFiles()
                {
                    var dict = new Dictionary<string, int>();

                    foreach (var file in document.Files.Skip(1))
                    {
                        if (list.Any(y => y.Name.Equals(file.FileName, StringComparison.InvariantCultureIgnoreCase)))
                        {
                            var index = dict.ContainsKey(file.FileName) ? dict[file.FileName] + 1 : 1;

                            dict[file.FileName] = index;

                            file.FileName = $"{Path.GetFileNameWithoutExtension(file.FileName)} ({index}){Path.GetExtension(file.FileName)}";
                        }

                        yield return FromFile(storage, file);
                    }
                }

                // Compress and move, generate document.xml
                {
                    var resultInfo = await _storyBuilder.Build(new CompressionStoryContext
                    {
                        Document = "document",
                        Storage = storage,
                        Documents = list,
                    }).ReturnAsync<IDocumentInfo>();

                    _documentStorage.MoveToOutgoing(resultInfo, document.Container.Uid);

                    include.Add(resultInfo.Name);

                    try
                    {
                        _documentStorage.MoveToOutgoing(
                            SaveDocument(storage, new Communication
                            {
                                Version = "2.7",
                                Header = new CommunicationHeader
                                {
                                    Uid = headerUid,
                                    Created = DateTime.Now,
                                    Type = MessageType.Транспортный_Контейнер,
                                    Source = new CommunicationPartner
                                    {
                                        Uid = document.AuthorGuid,
                                        Organization = document.Container.Authors.First().Organization.Title,
                                    },
                                },
                                Container = new DocumentContainer
                                {
                                    Body = resultInfo.Name,
                                    Signature = containerSignature,
                                },
                            }), document.Container.Uid);

                        include.Add(_options.Value.CommunicationXml);
                    }
                    catch
                    {
                        _logger.Warning($"Не удалось cоздать вспомогательный файл.");
                    }
                }

                // Generate ini
                {
                    _documentStorage.MoveToOutgoing(
                        SaveEnvelop(storage, document.Receivers, $"Исходящее письмо № {author.Registration.Number} от {author.Registration.Date:dd:MM:yyyy}", include),
                        document.Container.Uid);
                }

                await UpdateIdentity(document.IdentityEx, document.Container.Uid, headerUid);

                return (true, document.Container.Uid);

                PdfReader LoadDocument()
                {
                    using (var inputCopy = new MemoryStream())
                    {
                        using (var input = documentInfo.CreateReadStream())
                        {
                            input.CopyTo(inputCopy);
                        }

                        return new PdfReader(inputCopy.ToArray());
                    }
                }
            }
            catch (Exception exp)
            {
                _logger.Error($"Ошибка при обработке документа: {document.Container.Uid}.");
                _logger.Error(exp);

                return (false, exp.Message);
            }
        }

        private void Serialize<T>(T value, Stream stream, XmlSerializerNamespaces namespaces = null)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var writer = new StreamWriter(stream, Encoding.GetEncoding("windows-1251")))
            {
                if (namespaces?.Count > 0)
                {
                    serializer.Serialize(writer, value, namespaces);
                }
                else
                {
                    serializer.Serialize(writer, value);
                }
            }
        }

        private IWritableDocumentStorage CreateStorage()
        {
            return new MemoryDocumentStorage(new Uri("mem://Out"));
        }

        private async Task UpdateIdentity(long identity_ex, string containerGuid, string headerGuid)
        {
            var medo_header_guid = await _package.FindEClassId("Medo.References", "HeaderGUID");
            var medo_external_guid = await _package.FindEClassId("Medo.References", "ExternalGUID");
            await _package.CreateIdentityList(identity_ex, medo_header_guid, headerGuid);
            await _package.CreateIdentityList(identity_ex, medo_external_guid, containerGuid);
        }

        private string ParseSubject(X509Certificate2 value)
        {
            if (value == null)
            {
                return "== INVALID ==";
            }

            var items = Regex.Split(value.Subject, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)").Select(x =>
            {
                var tmp = x.Split('=');

                return tmp.Length == 1
                    ? new KeyValuePair<string, string>(null, tmp[0].Trim())
                    : new KeyValuePair<string, string>(tmp[0].Trim(), tmp[1].Trim());
            }).ToArray();

            var sn = items.FirstOrDefault(x => string.Equals(x.Key, "SN", StringComparison.OrdinalIgnoreCase)).Value;
            var gn = items.FirstOrDefault(x =>
                string.Equals(x.Key, "GN", StringComparison.OrdinalIgnoreCase) ||
                string.Equals(x.Key, "G", StringComparison.OrdinalIgnoreCase)).Value;

            return $"{sn} {gn}";
        }

        private IWritableDocumentInfo FromFile(IWritableDocumentStorage storage, FileDto file)
        {
            var result = storage.Create(file.FileName, false);
            using (var writer = result.CreateWriteStream())
            {
                if (file.Data != null)
                {
                    writer.Write(file.Data, 0, file.Data.Length);
                }
                else if (file.Text != null)
                {
                    var data = Encoding.UTF8.GetBytes(file.Text);
                    writer.Write(data, 0, data.Length);
                }
            }

            return result;
        }

        private IWritableDocumentInfo SavePassport(IWritableDocumentStorage storage, Container container)
        {
            var info = storage.Create(_options.Value.ContainerXml);

            using (var stream = info.CreateWriteStream())
            {
                var ns = new XmlSerializerNamespaces();
                ns.Add("xdms", "http://minsvyaz.ru/container");
                Serialize(container, stream, ns);
                return info;
            }
        }

        private IWritableDocumentInfo SaveDocument(IWritableDocumentStorage storage, Communication communication)
        {
            var info = storage.Create(_options.Value.CommunicationXml);

            using (var stream = info.CreateWriteStream())
            {
                var ns = new XmlSerializerNamespaces();
                ns.Add("xdms", "http://www.infpres.com/IEDMS");
                Serialize(communication, stream, ns);
                return info;
            }
        }

        private IWritableDocumentInfo SaveEnvelop(IWritableDocumentStorage storage, string[] receivers, string subject, IEnumerable<string> files)
        {
            var info = storage.Create(_options.Value.EnvelopeIni);

            using (var stream = info.CreateWriteStream())
            {
                using (var writer = new StreamWriter(stream, Encoding.GetEncoding("windows-1251"), 8196, true))
                {
                    writer.WriteLine("[ПИСЬМО КП ПС СЗИ]");
                    writer.WriteLine("ТЕМА={0}", subject ?? string.Empty);
                    writer.WriteLine("ШИФРОВАНИЕ=0");
                    writer.WriteLine("ЭЦП=1");

                    if (receivers != null && receivers.Length > 0)
                    {
                        writer.WriteLine("[АДРЕСАТЫ]");

                        foreach (var (key, value) in receivers.Select(Selector))
                        {
                            writer.WriteLine("{0}={1}", key, value);
                        }
                    }

                    if (files != null && files.Any())
                    {
                        writer.WriteLine("[ФАЙЛЫ]");

                        foreach (var (key, value) in files.Select(Selector))
                        {
                            writer.WriteLine("{0}={1}", key, value);
                        }
                    }
                }
            }

            return info;

            (string Key, string Value) Selector(string value, int index) => (Key: index.ToString(), Value: value);
        }

        private int GetPagesCount(IWritableDocumentInfo pdfInfo)
        {
            using (var input = pdfInfo.CreateReadStream())
            {
                using (var reader = new PdfReader(input))
                {
                    return reader.NumberOfPages;
                }
            }
        }

        private bool TryFindCertificate(CertificatData certificatData, out X509Certificate2 certificate)
        {
            using (var store = new X509Store(certificatData.StoreName, certificatData.StoreLocation))
            {
                store.Open(OpenFlags.ReadOnly);

                var result = store.Certificates.Find(certificatData.FindType, certificatData.Key, true);

                certificate = result.OfType<X509Certificate2>().FirstOrDefault();

                return certificate != null;
            }
        }
    }
}

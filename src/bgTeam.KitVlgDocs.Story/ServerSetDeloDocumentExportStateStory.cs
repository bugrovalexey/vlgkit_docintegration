﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.KitVlgDocs.DataAccess;
    using bgTeam.KitVlgDocs.Domain.Dto;

    public class ServerSetDeloDocumentExportStateStory : IStory<DeloDocumentExportStatusDto, ValueTuple>
    {
        private readonly IAppLogger _logger;
        private readonly IMedoPackage _package;
        private readonly IRepository _repository;
        private readonly ICrudService _crudService;

        public ServerSetDeloDocumentExportStateStory(
            IAppLogger logger,
            IMedoPackage package,
            IRepository repository,
            ICrudService crudService)
        {
            _logger = logger;
            _package = package;
            _repository = repository;
            _crudService = crudService;
        }

        public ValueTuple Execute(DeloDocumentExportStatusDto context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<ValueTuple> ExecuteAsync(DeloDocumentExportStatusDto context)
        {
            var identityExId = await _repository.GetAsync<long?>($"SELECT bo_bpdoc.IDENTITYEX_ID from BO_BPDOCUMENT bo_bpdoc WHERE bo_bpdoc.BPDOCUMENT_ID = :DocumentId", new
            {
                DocumentId = context.Id,
            });

            if (identityExId == null)
            {
                throw new InvalidOperationException($"Не найден документ {context.Id}");
            }
            else
            {
                var external_id = await _package.FindEClassId("Common", "ExternalSystemIdentity");

                var note = await _repository.GetAsync<string>("SELECT NOTE FROM BO_BPDOCUMENT WHERE BPDOCUMENT_ID = :DocumentId", new
                {
                    DocumentId = context.Id,
                });

                await _crudService.ExecuteAsync("UPDATE BO_BPDOCUMENT SET NOTE = :Note WHERE BPDOCUMENT_ID = :DocumentId", new
                {
                    DocumentId = context.Id,
                    Note = GenerateNote(note),
                });

                if (context.Ins == null)
                {
                    await _crudService.ExecuteAsync(GenerateSql(), new { identityExId, external_id });

                    string GenerateSql()
                    {
                        var builder = new StringBuilder()
                            .AppendLine($@"
DELETE
FROM cbo_identitylist
WHERE IDENTITYLIST_ID IN
  (SELECT ilist.IDENTITYLIST_ID
  FROM cbo_identitylist ilist
  INNER JOIN bo_prototype prot
  ON prot.prototype_id = ilist.prototype_id
  WHERE ilist.owner_id = :identityExId
  AND prot.eclass_id = :external_id
  )");
                        return builder.Replace(Environment.NewLine, "\n").ToString();
                    }
                }
                else
                {
                    await _package.CreateIdentityList(identityExId.Value, external_id, context.Ins.ToString());
                }

                string GenerateNote(string oldNote)
                {
                    var state = context.Ins != null ? "выгружен" : "не выгружен";

                    if (string.IsNullOrWhiteSpace(oldNote) || oldNote.Length == 0)
                    {
                        return $"Статус экспорта: {state}";
                    }
                    else if (oldNote.Contains(";Статус экспорта:"))
                    {
                        var splited = oldNote.Split(
                            new[] { ";Статус экспорта:" },
                            StringSplitOptions.RemoveEmptyEntries);

                        return $"{splited[0]};Статус экспорта: {state}";
                    }

                    return $"{oldNote};Статус экспорта: {state}";
                }
            }

            return default;
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Story
{
    using bgTeam.KitVlgDocs.Domain.Dto;
    public class ServerExportDeloDocumentsStoryContext
    {
        public OutgoinDocumentDto Document { get; set; }
    }
}

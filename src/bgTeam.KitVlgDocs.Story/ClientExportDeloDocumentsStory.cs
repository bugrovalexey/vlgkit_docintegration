﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography.Pkcs;
    using System.Security.Cryptography.X509Certificates;
    using System.Threading;
    using System.Threading.Tasks;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class ClientExportDeloDocumentsStory : IStory<ClientExportDeloDocumentsStoryContext, ValueTuple>
    {
        private static int _counter;

        private readonly IAppLogger _logger;
        private readonly IDeloWebApi _deloWebApi;
        private readonly IOptions<ExportAppOptions> _options;

        public ClientExportDeloDocumentsStory(
            IAppLogger logger,
            IDeloWebApi deloWebApi,
            IOptions<ExportAppOptions> options)
        {
            _logger = logger;
            _deloWebApi = deloWebApi;
            _options = options;
        }

        public ValueTuple Execute(ClientExportDeloDocumentsStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<ValueTuple> ExecuteAsync(ClientExportDeloDocumentsStoryContext context)
        {
            var outDocs = await _deloWebApi.GetOutgoingDocumentsMeta();

            var list = new List<long>();

            foreach (var document in outDocs)
            {
                if (TryFindCertificates(document.Signers, out var _))
                {
                    list.Add(document.Id);
                }
            }

            var unprocessed = outDocs
                .Where(x => !list.Contains(x.Id))
                .Select(x => new
                {
                    x.Id,
                    Signers = string.Join(";", x.Signers.Select(z => (z.Id, z.ContactId))),
                }).ToArray();

            if (unprocessed.Any() && (Interlocked.Increment(ref _counter) & 0xF) == 0)
            {
                _logger.Warning($"Отсутствуют полностью либо частично сетификаты для {unprocessed.Length} документов Дело {JsonConvert.SerializeObject(unprocessed)}.");
            }

            if (list.Count > 0)
            {
                _logger.Info($"Приступаю к экспорту документов в систему 'Дело', найдено - {list.Count}.");

                foreach (var document in await _deloWebApi.GetOutgoingDocumentsData(list))
                {
                    if (!TryFindCertificates(document.Signers, out var certificates))
                    {
                        continue;
                    }

                    var signedCms = new SignedCms(
                        new ContentInfo(document.Document),
                        true);

                    foreach (var certificate in certificates)
                    {
                        signedCms.ComputeSignature(new CmsSigner(certificate));
                    }

                    var (success, result) = await _deloWebApi.ExportDocument(new OutgoinDocumentDto
                    {
                        Id = document.Id,
                        Signature = signedCms.Encode(),
                    });

                    if (success)
                    {
                        _logger.Info($"Документ {document.Id}, успешно экспортирован с номером - {result}.");
                    }
                    else
                    {
                        _logger.Error(result);
                    }
                }
            }

            return default;
        }

        private bool TryFindCertificate(OutgoingDocumentSignerDto signer, out X509Certificate2 certificate)
        {
            var storeName = Enum.TryParse<StoreName>(signer.Store, true, out var name)
                ? name
                : _options.Value.StoreName;

            using (var store = new X509Store(storeName, _options.Value.StoreLocation))
            {
                store.Open(OpenFlags.ReadOnly);

                var result = store.Certificates.Find(_options.Value.CertificateFindMode, signer.Id, true);

                certificate = result.OfType<X509Certificate2>().FirstOrDefault();

                return certificate != null;
            }
        }

        private bool TryFindCertificates(OutgoingDocumentSignerDto[] signers, out IEnumerable<X509Certificate2> certificates)
        {
            var result = new List<X509Certificate2>();

            foreach (var signer in signers)
            {
                if (!TryFindCertificate(signer, out var certificate))
                {
                    certificates = null;
                    return false;
                }

                result.Add(certificate);
            }

            certificates = result;
            return true;
        }
    }
}

﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Serialization;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using bgTeam.KitVlgDocs.Domain.Xml;
    using bgTeam.KitVlgDocs.Domain.Xml.Iedms;
    using Microsoft.Extensions.Options;

    public class ClientLoadMedoDocumentsForImportStory : IStory<ClientLoadMedoDocumentsForImportStoryContext, IEnumerable<MedoDocumentDto>>
    {
        private readonly IAppLogger _logger;
        private readonly IOptions<ImportAppOptions> _options;
        private readonly IEmbeddedResources _resources;
        private readonly IIncomingDocumentStorage _documentStorage;

        public ClientLoadMedoDocumentsForImportStory(
            IAppLogger logger,
            IOptions<ImportAppOptions> options,
            IEmbeddedResources resources,
            IIncomingDocumentStorage documentStorage)
        {
            _logger = logger;
            _options = options;
            _resources = resources;
            _documentStorage = documentStorage;
        }

        public IEnumerable<MedoDocumentDto> Execute(ClientLoadMedoDocumentsForImportStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<IEnumerable<MedoDocumentDto>> ExecuteAsync(ClientLoadMedoDocumentsForImportStoryContext context)
        {
            var result = new List<MedoDocumentDto>();

            foreach (var item in _documentStorage.GetIncomingDocuments($"{Path.GetFileNameWithoutExtension(_options.Value.EnvelopeIni)}.*"))
            {
                try
                {
                    var directory = Path.GetDirectoryName(
                        item.Path.GetComponents(UriComponents.AbsoluteUri, UriFormat.Unescaped)
                            .Replace(item.Storage.Root.GetComponents(UriComponents.AbsoluteUri, UriFormat.Unescaped), string.Empty));

                    var storage = item.Storage
                        .CreateDirectory(directory);

                    _logger.Info($"Загрузка контейнера {storage.Root}.");

                    try
                    {
                        var (files, addressies) = LoadEnvelope();

                        var (communicationHasErrors, communication) = LoadCommunication();

                        if (communicationHasErrors)
                        {
                            _logger.Warning($"В контейнере {storage.Root} не найден или не верен обязательный файл {_options.Value.CommunicationXml}, пропуск.");
                            result.Add(new MedoDocumentDto
                            {
                                Storage = storage,
                                Directory = directory,
                                Addressies = addressies,
                                Communication = communication,
                            });
                            continue;
                        }

                        if (communication.Header.Type != MessageType.Транспортный_Контейнер)
                        {
                            _logger.Warning($"Контейнер {storage.Root} имеет недопустимый тип {communication.Header.Type}, пропуск.");
                            result.Add(new MedoDocumentDto
                            {
                                Storage = storage,
                                Directory = directory,
                                Addressies = addressies,
                                Communication = communication,
                            });
                            continue;
                        }

                        var container = LoadContainer();

                        if (container.passport == null)
                        {
                            result.Add(new MedoDocumentDto
                            {
                                Storage = storage,
                                Directory = directory,
                                Addressies = addressies,
                                Communication = communication,
                            });
                            continue;
                        }

                        var medoDocument = new MedoDocumentDto
                        {
                            Storage = storage,
                            Directory = directory,
                            Addressies = addressies,
                            Communication = communication,
                            Container = new MedoContainerDto
                            {
                                Passport = container.passport,
                                Document = container.document,
                                PassportXml = container.passportXml,
                                Signatures = container.signatures,
                                Files = container.containerFiles,
                            },
                        };

                        result.Add(medoDocument);

                        (string[] files, string[] addressies) LoadEnvelope()
                        {
                            using (var stream = item.CreateReadStream())
                            {
                                using (var reader = new StreamReader(stream, Encoding.GetEncoding("windows-1251"), false, 8196, true))
                                {
                                    var ini = new IniParser.StreamIniDataParser().ReadData(reader);

                                    return (
                                        ini.Sections.Where(x => x.SectionName == "ФАЙЛЫ").SelectMany(x => x.Keys).Select(x => x.Value).ToArray(),
                                        ini.Sections.Where(x => x.SectionName == "АДРЕСАТЫ").SelectMany(x => x.Keys).Select(x => x.Value).ToArray());
                                }
                            }
                        }

                        (bool hasErrors, Communication communication) LoadCommunication()
                        {
                            var fileName = Path.GetFileName(files.FirstOrDefault(x => x.EndsWith(_options.Value.CommunicationXml)));
                            var hasErrors = false;

                            if (fileName == null)
                            {
                                foreach (var communicationName in _options.Value.CommunicationXmlAlternates)
                                {
                                    fileName = Path.GetFileName(files.FirstOrDefault(x => x.EndsWith(communicationName)));

                                    if (fileName != null)
                                    {
                                        break;
                                    }
                                }
                            }

                            if (fileName != null)
                            {
                                using (var stream = storage.GetDocuments(fileName, SearchOption.TopDirectoryOnly).First().CreateReadStream())
                                {
                                    var xml = new XmlDocument();
                                    xml.Load(stream);
                                    xml.Schemas.Add(_resources.TransportSchema);
                                    xml.Validate((sender, e) =>
                                    {
                                        hasErrors = true;
                                        _logger.Error($"Ошибка при проверки схемы файла {_options.Value.CommunicationXml} в контейнере {storage.Root}: {e.Message}.");
                                    });
                                }

                                try
                                {
                                    using (var stream = storage.GetDocuments(fileName, SearchOption.TopDirectoryOnly).First().CreateReadStream())
                                    {
                                        return (hasErrors, Deserialize<Communication>(stream));
                                    }
                                }
                                catch (Exception exp)
                                {
                                    _logger.Error(exp);
                                    return (true, null);
                                }
                            }

                            return (true, null);
                        }

                        (Container passport, FileDto passportXml, FileDto[] containerFiles, FileDto[] signatures, FileDto document) LoadContainer()
                        {
                            var fileName = communication.Container?.Body;

                            var zipDocumentInfo = storage.GetDocuments(fileName, SearchOption.TopDirectoryOnly).FirstOrDefault();

                            if (zipDocumentInfo == null)
                            {
                                _logger.Error($"В контейнере {storage.Root} не найден или не верен обязательный файл {fileName}, пропуск.");
                                return (null, null, null, null, null);
                            }

                            using (var stream = zipDocumentInfo.CreateReadStream())
                            {
                                using (var zip = new ZipArchive(stream))
                                {
                                    var passport = LoadPassport();

                                    if (passport == null)
                                    {
                                        _logger.Error($"В контейнере {storage.Root} не найден или не верен обязательный файл {_options.Value.ContainerXml}, пропуск.");
                                        return (null, null, null, null, null);
                                    }

                                    var (passportXml, restFiles, signatures, document) = EnumerateFiles();

                                    return (passport, passportXml, restFiles, signatures, document);

                                    Container LoadPassport()
                                    {
                                        var hasErrors = false;

                                        using (var passportStream = zip.Entries.First(x => x.Name == _options.Value.ContainerXml).Open())
                                        {
                                            var xml = new XmlDocument();
                                            xml.Load(passportStream);
                                            xml.Schemas.Add(_resources.ContainerSchema);
                                            xml.Validate((sender, e) =>
                                            {
                                                hasErrors = true;
                                                _logger.Error($"Ошибка при проверки схемы файла {_options.Value.ContainerXml} в контейнере {storage.Root}: {e.Message}.");
                                            });
                                        }

                                        if (!hasErrors)
                                        {
                                            using (var passportStream = zip.Entries.First(x => x.Name == _options.Value.ContainerXml).Open())
                                            {
                                                return Deserialize<Container>(passportStream);
                                            }
                                        }

                                        return null;
                                    }

                                    (FileDto passportXml, FileDto[] containerFiles, FileDto[] signatures, FileDto document) EnumerateFiles()
                                    {
                                        var allFiles = AllFiles().ToArray();

                                        var signaturesNames = passport.Authors
                                            .SelectMany(x => x.Sign)
                                            .Select(x => x.DocumentSignature.LocalName)
                                            .ToArray();

                                        return (
                                            LoadFile(_options.Value.ContainerXml),
                                            allFiles,
                                            allFiles.Where(x => signaturesNames.Contains(x.FileName)).ToArray(),
                                            allFiles.First(x => x.FileName == passport.Document.LocalName));

                                        FileDto LoadFile(string name)
                                        {
                                            var entry = zip.Entries.First(x => x.Name == name);

                                            using (var src = entry.Open())
                                            {
                                                using (var dest = new MemoryStream())
                                                {
                                                    src.CopyTo(dest);
                                                    return new FileDto
                                                    {
                                                        Data = dest.ToArray(),
                                                        FileName = name,
                                                    };
                                                }
                                            }
                                        }

                                        IEnumerable<FileDto> AllFiles()
                                        {
                                            foreach (var issuer in passport.Authors)
                                            {
                                                if (!string.IsNullOrEmpty(issuer.Registration?.RegistrationStamp?.LocalName))
                                                {
                                                    yield return LoadFile(issuer.Registration.RegistrationStamp.LocalName);
                                                }

                                                foreach (var sign in issuer.Sign)
                                                {
                                                    if (!string.IsNullOrEmpty(sign.DocumentSignature?.LocalName))
                                                    {
                                                        yield return LoadFile(sign.DocumentSignature.LocalName);
                                                    }

                                                    if (!string.IsNullOrEmpty(sign.DocumentSignature?.SignatureStamp?.LocalName))
                                                    {
                                                        yield return LoadFile(sign.DocumentSignature.SignatureStamp.LocalName);
                                                    }
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(passport.Document?.LocalName))
                                            {
                                                yield return LoadFile(passport.Document.LocalName);
                                            }

                                            if (!string.IsNullOrEmpty(passport.ContainerSignature?.LocalName))
                                            {
                                                yield return LoadFile(passport.ContainerSignature.LocalName);
                                            }

                                            foreach (var attachment in passport.Attachments)
                                            {
                                                if (!string.IsNullOrEmpty(attachment.LocalName))
                                                {
                                                    yield return LoadFile(attachment.LocalName);
                                                }

                                                foreach (var sign in attachment.Signature)
                                                {
                                                    if (!string.IsNullOrEmpty(sign.LocalName))
                                                    {
                                                        yield return LoadFile(sign.LocalName);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception exp)
                    {
                        _logger.Error(exp);
                        result.Add(new MedoDocumentDto
                        {
                            Storage = storage,
                            Directory = directory,
                        });
                    }
                }
                catch (Exception exp)
                {
                    _logger.Error(exp);
                }
            }

            return result;
        }

        private T Deserialize<T>(Stream stream)
        {
            var serializer = new XmlSerializer(typeof(T));

            return (T)serializer.Deserialize(stream);
        }
    }
}

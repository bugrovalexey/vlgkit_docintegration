﻿namespace bgTeam.KitVlgDocs.Story
{
    public class ServerSetMedoDocumentExportStateStoryContext
    {
        public long DocumentId { get; set; }

        public bool VisibleForExport { get; set; }
    }
}

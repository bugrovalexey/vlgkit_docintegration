﻿namespace bgTeam.KitVlgDocs.Story.Common
{
    using System.Collections.Generic;
    using bgTeam.KitVlgDocs.Common;

    public class CompressionStoryContext
    {
        public string Document { get; set; }

        public IWritableDocumentStorage Storage { get; set; }

        public IReadOnlyCollection<IDocumentInfo> Documents { get; set; }
    }
}

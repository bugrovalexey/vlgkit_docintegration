﻿namespace bgTeam.KitVlgDocs.Story.Common
{
    using bgTeam.KitVlgDocs.Common;

    public class StampDocumentStoryContext
    {
        public byte[] Document { get; set; }

        public StampData SignatureStamp { get; set; }

        public StampData RegistrationStamp { get; set; }
    }
}

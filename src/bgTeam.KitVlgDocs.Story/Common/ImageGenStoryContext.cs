﻿namespace bgTeam.KitVlgDocs.Story.Common
{
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Common.ImageGen;

    public class ImageGenStoryContext
    {
        public string Document { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public object Parameters { get; set; }

        public ImageDefinition Definition { get; set; }

        public IWritableDocumentStorage Storage { get; set; }
    }
}

﻿namespace bgTeam.KitVlgDocs.Story.Common
{
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.Drawing.Text;
    using System.IO;
    using System.Threading.Tasks;
    using bgTeam;
    using bgTeam.KitVlgDocs.Common;

    public class ImageGenStory : IStory<ImageGenStoryContext, IDocumentInfo>
    {
        public IDocumentInfo Execute(ImageGenStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public Task<IDocumentInfo> ExecuteAsync(ImageGenStoryContext context)
        {
            var imageName = $"{Path.GetFileNameWithoutExtension(context.Document)}_{context.Definition.Name}.png";

            using (var bmp = new Bitmap(context.Definition.Size.Width, context.Definition.Size.Height))
            {
                bmp.SetResolution(72f, 72f);
                using (var g = Graphics.FromImage(bmp))
                {
                    g.Clear(context.Definition.BackgroundColor);
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.CompositingQuality = CompositingQuality.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

                    foreach (var item in context.Definition.Components)
                    {
                        item.Draw(context.Parameters, g);
                    }
                }

                var info = context.Storage.Create(imageName, true);

                using (var mem = info.CreateWriteStream())
                {
                    var w = context.Width <= 0 ? bmp.Width : context.Width;
                    var h = context.Height <= 0 ? bmp.Height : context.Height;

                    if (w == bmp.Width && h == bmp.Height)
                    {
                        bmp.Save(mem, ImageFormat.Png);
                    }
                    else
                    {
                        using (var resized = ResizeImage(bmp, w, h))
                        {
                            resized.Save(mem, ImageFormat.Png);
                        }
                    }

                    return Task.FromResult<IDocumentInfo>(info);
                }
            }
        }

        private static Bitmap ResizeImage(Bitmap image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }
    }
}

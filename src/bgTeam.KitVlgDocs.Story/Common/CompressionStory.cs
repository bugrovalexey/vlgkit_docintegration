﻿namespace bgTeam.KitVlgDocs.Story.Common
{
    using System.IO;
    using System.IO.Compression;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam;
    using bgTeam.KitVlgDocs.Common;

    public class CompressionStory : IStory<CompressionStoryContext, IDocumentInfo>
    {
        public IDocumentInfo Execute(CompressionStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<IDocumentInfo> ExecuteAsync(CompressionStoryContext context)
        {
            var info = context.Storage.Create($"{Path.GetFileNameWithoutExtension(context.Document)}.edc.zip", true);

            using (var output = info.CreateWriteStream())
            {
                using (var archive = new ZipArchive(output, ZipArchiveMode.Update, true, Encoding.UTF8))
                {
                    foreach (var item in context.Documents)
                    {
                        var entry = archive.CreateEntry(item.Name, CompressionLevel.Optimal);

                        using (var src = item.CreateReadStream())
                        {
                            using (var dest = entry.Open())
                            {
                                await src.CopyToAsync(dest);
                            }
                        }
                    }
                }

                return info;
            }
        }
    }
}

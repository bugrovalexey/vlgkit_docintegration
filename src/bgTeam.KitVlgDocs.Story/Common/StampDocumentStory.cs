﻿namespace bgTeam.KitVlgDocs.Story.Common
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using bgTeam;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.Common.Impl;
    using iTextSharp.text.pdf;

    public class StampDocumentStory : IStory<StampDocumentStoryContext, byte[]>
    {
        private readonly IStoryBuilder _storyBuilder;
        private readonly IEmbeddedResources _resources;

        public StampDocumentStory(
            IStoryBuilder storyBuilder,
            IEmbeddedResources resources)
        {
            _storyBuilder = storyBuilder;
            _resources = resources;
        }

        public byte[] Execute(StampDocumentStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<byte[]> ExecuteAsync(StampDocumentStoryContext context)
        {
            var result = context.Document;
            var storage = CreateStorage();
            var pdfDocument = LoadDocument();

            var page = pdfDocument.GetPageN(1);
            var rect = pdfDocument.GetPageSize(page);
            var unit = page.GetAsNumber(PdfName.USERUNIT)?.FloatValue ?? 72f;

            if (context.SignatureStamp != null)
            {
                var topLeftX = (short)(ImageHelper.ToMillimeters(rect.Width, unit) * context.SignatureStamp.Left);
                var topLeftY = (short)(ImageHelper.ToMillimeters(rect.Height, unit) * context.SignatureStamp.Top);

                var stamp = await _storyBuilder.Build(new ImageGenStoryContext
                {
                    Document = "SignatureStamp",
                    Definition = _resources.SignatureStamp,
                    Width = context.SignatureStamp.Width,
                    Height = context.SignatureStamp.Height,
                    Storage = storage,
                    Parameters = new
                    {
                        Serial = Guid.NewGuid().ToString(),
                        Subject = "Test Test Test",
                        NotAfter = DateTime.Now.AddYears(1),
                        NotBefore = DateTime.Now,
                    },
                }).ReturnAsync<IDocumentInfo>();

                result = StampHelper.Watermark(result, stamp, x => x.Skip(context.SignatureStamp.Page - 1).First(), (x, y, z) =>
                {
                    var w = ImageHelper.ToPixels(topLeftX, z);
                    var h = ImageHelper.ToPixels(topLeftY, z);
                    return new RectangleF(w, h, x.Width, x.Height);
                });
            }

            if (context.RegistrationStamp != null)
            {
                var topLeftX = (short)(ImageHelper.ToMillimeters(rect.Width, unit) * context.RegistrationStamp.Left);
                var topLeftY = (short)(ImageHelper.ToMillimeters(rect.Height, unit) * context.RegistrationStamp.Top);

                var stamp = await _storyBuilder.Build(new ImageGenStoryContext
                {
                    Document = "RegistrationStamp",
                    Definition = _resources.RegistrationStamp,
                    Width = context.RegistrationStamp.Width,
                    Height = context.RegistrationStamp.Height,
                    Storage = storage,
                    Parameters = new
                    {
                        Number = "1-Test-23",
                        Date = DateTime.Now,
                    },
                }).ReturnAsync<IDocumentInfo>();

                result = StampHelper.Watermark(result, stamp, x => x.Skip(context.RegistrationStamp.Page - 1).First(), (x, y, z) =>
                {
                    var w = ImageHelper.ToPixels(topLeftX, z);
                    var h = ImageHelper.ToPixels(topLeftY, z);
                    return new RectangleF(w, h, x.Width, x.Height);
                });
            }

            return result;

            PdfReader LoadDocument()
            {
                using (var inputCopy = new MemoryStream())
                {
                    inputCopy.Write(context.Document, 0, context.Document.Length);

                    return new PdfReader(inputCopy.ToArray());
                }
            }
        }

        private static IWritableDocumentStorage CreateStorage()
        {
            return new MemoryDocumentStorage(new Uri("mem://Out"));
        }
    }
}

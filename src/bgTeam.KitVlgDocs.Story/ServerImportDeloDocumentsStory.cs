﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography.Pkcs;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.KitVlgDocs.Common;
    using bgTeam.KitVlgDocs.DataAccess;
    using bgTeam.KitVlgDocs.Domain.Dto;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;

    public class ServerImportDeloDocumentsStory : IStory<DeloIncomingDocumentDto, OperationResultDto>
    {
        private static readonly Regex _signerRegex = new Regex(@"([A-Za-zА-Яа-я]+)\s*([A-Za-zА-Яа-я]?)\.\s*([A-Za-zА-Яа-я]?)\.\s?-?\s?([A-Za-zА-Яа-я\s]*)?", RegexOptions.Compiled);

        private readonly IAppLogger _logger;
        private readonly IOptions<WebAppOptions> _options;
        private readonly IMedoPackage _package;
        private readonly ICrudService _crudService;

        public ServerImportDeloDocumentsStory(
            IAppLogger logger,
            IOptions<WebAppOptions> options,
            IMedoPackage package,
            ICrudService crudService)
        {
            _logger = logger;
            _options = options;
            _package = package;
            _crudService = crudService;
        }

        public OperationResultDto Execute(DeloIncomingDocumentDto context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<OperationResultDto> ExecuteAsync(DeloIncomingDocumentDto context)
        {
            if (await _package.CheckDeloDocumentIsn(context.Isn))
            {
                _logger.Warning($"Документ 'Дело' с идентификатором {context.Isn} уже импортирован!");
                return (false, $"Документ 'Дело' с идентификатором {context.Isn} уже импортирован!");
            }

            var from_contact = await _package.GetContact(_options.Value.ContactsMapping["DeloImport"]);

            if (from_contact.Id == null)
            {
                throw new InvalidOperationException($"Не найден контакт {from_contact} необходимий для импорта из системы 'Дело'.");
            }

            var a_bclass_id = await _package.FindBClassId("Contacts", "Address");
            var a_eclass_id = await _package.FindEClassId("Docflow.References", "PrivateFolderAddressCategory");

            var folders = await GetFolders();

            if (folders.Length == 0)
            {
                throw new InvalidOperationException($"Не удалось определить папку для импорта документа.");
            }

            var dfa_role_signer = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Signer");
            var dfa_role_sender = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Sender");
            var dfa_role_addressee = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Addressee");
            var dfa_role_correspondent = await _package.FindReferenceItemId("Docflow.References", "AddresseeRole", "Correspondent");

            var external_id = await _package.FindEClassId("Common", "ExternalSystemIdentity");
            var common_bar_code = await _package.FindEClassId("Common", "BarCode");

            var now = DateTime.Now;

            var bclass_id = await _package.FindBClassId("Docflow.Documents", "Document");
            var eclass_id = await _package.FindEClassId("Docflow.References", "ElPismo");
            var delivery_kind_id = await _package.FindReferenceItemId("Contacts", "DeliveryKind", "fromDelo");
            var state_id = await _package.FindReferenceItemId("Docflow.References", "DocumentState", "Create");
            var type_id = await _package.FindReferenceItemId("Docflow.References", "DocumentType", "IncomingDocument");
            var author_contact = await _package.GetContact(_options.Value.ContactsMapping["Administrator"]);

            if (author_contact.Id == null)
            {
                _logger.Warning("Не найден контакт 'Administrator'.");
                return (false, "Не найден контакт 'Administrator'.");
            }

            var to_contact = await _package.GetContact(_options.Value.ContactsMapping["Apparatus"]);

            if (to_contact.Id == null)
            {
                _logger.Warning("Не найден контакт 'Apparatus'.");
                return (false, "Не найден контакт 'Apparatus'.");
            }

            var files = new List<FileDto>(context.Files.SelectMany(x => new[] { x.Content }.Concat(x.Signatures)));

            _logger.Info($"Импорт документа из системы Дело {GetDocument()}");

            var document_id = await _package.CreateDocument(
                type_id,
                eclass_id,
                null,
                context.RegistrationNumber,
                context.RegistrationDate,
                null,
                null,
                from_contact.Id.Value,
                null,
                null,
                null,
                null,
                context.Annotation,
                state_id,
                files,
                now,
                null,
                null,
                to_contact.Id);

            var identityex_id = await _package.CreateIdentityEx();

            var cnt_bar_code_value = (await _package.CounterNextValue("Barcode")).ToString();

            await Task.WhenAll(
                _package.CreateIdentityList(identityex_id, external_id, context.Isn.ToString()),
                _package.CreateIdentityList(identityex_id, common_bar_code, cnt_bar_code_value));

            foreach (var item in folders)
            {
                await _package.AddDocumentToFolder(document_id, item, now);
            }

            var signers = new Dictionary<string, (int contactId, string firstName, string middleName, string lastName, string position, string keyIdentifier)>();
            var contactsSet = new HashSet<(int contactId, int roleId)>();

            await CreateAddressee(from_contact.Id.Value, dfa_role_sender, out_date: context.RegistrationDate, out_registration: context.RegistrationNumber);

            if (context.Signers?.Length > 0)
            {
                foreach (var item in context.Signers)
                {
                    var (contactId, _, _) = await _package.GetDeloContact(item, from_contact.Id.Value, a_bclass_id);

                    if (contactId != null)
                    {
                        signers[item] = await _package.GetSignerContact(contactId.Value);
                        await CreateAddressee(contactId.Value, dfa_role_signer, out_date: context.RegistrationDate, out_registration: context.RegistrationNumber);
                        await CreateAddressee(contactId.Value, dfa_role_correspondent, out_date: context.RegistrationDate, out_registration: context.RegistrationNumber);
                    }
                    else
                    {
                        _logger.Warning($"Не найден контакт '{item}' (Signer).");
                    }
                }
            }
            else
            {
                _logger.Warning($"Не заданы контакты подписавшие документ.");

                await CreateAddressee(from_contact.Id.Value, dfa_role_correspondent, out_date: context.RegistrationDate, out_registration: context.RegistrationNumber);
            }

            if (context.Subjects?.Length > 0)
            {
                var subjects = new List<long>();

                foreach (var item in context.Subjects)
                {
                    var subjectId = await _package.FindEnumByName("Docflow.References", "SubjectCategory", item);

                    if (subjectId != null)
                    {
                        subjects.Add(subjectId.Value);
                    }
                    else
                    {
                        _logger.Warning($"Не найдена тематика '{item}'.");
                    }
                }

                if (subjects.Count > 0)
                {
                    await _package.CreateClassifiers(document_id, subjects);
                }
            }

            if (context.Addressees?.Length > 0)
            {
                foreach (var item in context.Addressees)
                {
                    var name = item.Name ?? item.Department;

                    if (_options.Value.ContactsMapping.TryGetValue(name, out var mapping))
                    {
                        var (contactId, _) = await _package.GetContact(mapping);

                        if (contactId > 0)
                        {
                            if (!string.IsNullOrEmpty(item.Surname))
                            {
                                var (personContactId, _, _) = await _package.GetDeloContact(item.Surname, contactId.Value, a_bclass_id, a_eclass_id);

                                if (personContactId > 0)
                                {
                                    await CreateAddressee(personContactId.Value, dfa_role_addressee);
                                }
                                else
                                {
                                    await CreateAddressee(contactId.Value, dfa_role_addressee);
                                    _logger.Warning($"Не найден контакт '{item.Surname}' (Addressee->Person).");
                                }
                            }
                            else
                            {
                                await CreateAddressee(contactId.Value, dfa_role_addressee);
                            }
                        }
                        else
                        {
                            _logger.Warning($"Не найден контакт '{(mapping.Id, mapping.Name, mapping.Code)}' (Addressee).");
                        }
                    }
                    else
                    {
                        var (contactId, _, _) = await _package.GetDeloContact(name, to_contact.Id.Value, a_bclass_id);

                        if (contactId > 0)
                        {
                            if (!string.IsNullOrEmpty(item.Surname))
                            {
                                var (personContactId, _, _) = await _package.GetDeloContact(item.Surname, from_contact.Id.Value, a_bclass_id);

                                if (personContactId > 0)
                                {
                                    await CreateAddressee(personContactId.Value, dfa_role_addressee);
                                }
                                else
                                {
                                    await CreateAddressee(contactId.Value, dfa_role_addressee);
                                    _logger.Warning($"Не найден контакт '{item.Surname}' (Addressee->Person).");
                                }
                            }
                            else
                            {
                                await CreateAddressee(contactId.Value, dfa_role_addressee);
                            }
                        }
                        else
                        {
                            _logger.Warning($"Не найден контакт '{name}' (Addressee).");
                        }
                    }
                }
            }

            await _package.CreateDocStateHistory(document_id, state_id, author_contact.Id.Value, now, "admin");

            await _crudService.ExecuteAsync("UPDATE BO_BPDOCUMENT SET BCLASS_INDICATOR = :bclass_id, IDENTITYEX_ID = :identityex_id, NOTE = :note WHERE BPDOCUMENT_ID = :document_id", new
            {
                document_id,
                bclass_id,
                identityex_id,
                note = GenerateDocumentNote(),
            });

            await _crudService.ExecuteAsync("UPDATE CBO_FILEVERSION SET CONTACT_ID = :contact_id WHERE FILEVERSION_ID IN (SELECT fv.FILEVERSION_ID FROM CBO_FILEVERSION fv INNER JOIN CBO_FILE f ON f.CURRENTVERSION_ID = fv.FILEVERSION_ID INNER JOIN CBO_FILE_LINK fl ON fl.FILE_ID = f.FILE_ID WHERE fl.BPDOCUMENT_ID = :document_id)", new
            {
                document_id,
                contact_id = author_contact.Id,
            });

            await _crudService.ExecuteAsync("UPDATE DFA_DOCUMENT SET DELIVERY_KIND_ID = :delivery_kind_id, INCLUDE_LIST = :include_list, STATEDT = :statedt WHERE BPDOCUMENT_ID = :document_id", new
            {
                document_id,
                statedt = now,
                delivery_kind_id,
                include_list = context.IncludeList,
            });

            return (true, cnt_bar_code_value);

            async Task<int[]> GetFolders()
            {
                // Disable while testing
                //if (context.Addressees?.Length > 0)
                //{
                //    var results = new List<int>();

                //    foreach (var item in context.Addressees)
                //    {
                //        var contact = await _package.GetDeloContact(item.Surname, a_bclass_id);

                //        if (contact.ContactId != null)
                //        {
                //            var result = await _package.GetFolder(contact.AddressId.Value);

                //            if (result != null)
                //            {
                //                results.Add(result.Value);
                //            }
                //            else
                //            {
                //                _logger.Error($"Folder for contact {contact} not found.");
                //            }
                //        }
                //        else
                //        {
                //            _logger.Error($"Contact {item.Surname} not found.");
                //        }
                //    }

                //    if (results.Count > 0)
                //    {
                //        return results.ToArray();
                //    }
                //}

                var mapping = _options.Value.ContactsMapping["DeloImportFolder"];

                var folder_contact = await _package.GetContact(mapping);

                if (folder_contact.Id != null)
                {
                    var result = await _package.GetFolderByDocumentKind(folder_contact.Id.Value, "DeloDocument");

                    if (result != null)
                    {
                        return new[] { result.Value };
                    }
                }

                return Array.Empty<int>();
            }

            string GetDocument() => Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(
                new
                {
                    context.Addressees,
                    context.Annotation,
                    context.IncludeList,
                    Files = files.Select(x => x.FileName).ToArray(),
                    context.Isn,
                    context.RegistrationDate,
                    context.RegistrationNumber,
                    context.Signers,
                    context.Subjects,
                },
                Formatting.Indented)));

            string GenerateDocumentNote()
            {
                var builder = new StringBuilder(2048)
                    .AppendLine("Данные о лицах, подписавших документ: ");

                foreach (var item in context.Signers)
                {
                    builder.AppendLine(item);
                }

                foreach (var item in context.Files)
                {
                    if (item.Content?.Data?.Length > 0)
                    {
                        builder
                            .AppendFormat("Подпись документа {0}: {1}", item.Content.FileName, CheckSignature(item.Content.Data, item.Signatures, context.Signers, signers))
                            .AppendLine();
                    }
                }

                return builder.ToString();
            }

            string CheckSignature(
                byte[] document,
                FileDto[] signatures,
                string[] originalSigners,
                Dictionary<string, (int contactId, string firstName, string middleName, string lastName, string position, string keyIdentifier)> parsedSigners)
            {
                if (originalSigners?.Length > 0 && parsedSigners?.Count > 0 && originalSigners.All(parsedSigners.ContainsKey))
                {
                    try
                    {
                        var signedCms = new SignedCms(new ContentInfo(document), true);

                        foreach (var item in signatures)
                        {
                            try
                            {
                                signedCms.Decode(item.Data);
                            }
                            catch
                            {
                                var text = Encoding.ASCII.GetString(item.Data).Replace("\r", string.Empty).Replace("\n", string.Empty);
                                var data = Convert.FromBase64String(text);
                                signedCms.Decode(data);
                            }

                            var signersInfos = signedCms.SignerInfos.OfType<SignerInfo>().ToArray();

                            if (!VerifySignature(signedCms, false) && VerifySignature(signedCms, true))
                            {
                                _logger.Warning($"Неудалось проверить подпись документа 'Дело' стандартным методом.");

                                using (var chain = X509Chain.Create())
                                {
                                    chain.ChainPolicy = new X509ChainPolicy
                                    {
                                        RevocationFlag = X509RevocationFlag.EndCertificateOnly,
                                        RevocationMode = X509RevocationMode.NoCheck,
                                        VerificationFlags =
                                            X509VerificationFlags.IgnoreRootRevocationUnknown |
                                            X509VerificationFlags.IgnoreCertificateAuthorityRevocationUnknown |
                                            X509VerificationFlags.IgnoreCtlSignerRevocationUnknown |
                                            X509VerificationFlags.IgnoreEndRevocationUnknown |
                                            X509VerificationFlags.AllowUnknownCertificateAuthority,
                                    };

                                    foreach (var signerInfo in signersInfos)
                                    {
                                        if (!chain.Build(signerInfo.Certificate))
                                        {
                                            var info = string.Join(", ", chain.ChainStatus.Select(x => (x.Status, x.StatusInformation)));
                                            _logger.Warning($"Неудалось построить цепочку сертификатов для проверики подписи документа 'Дело' {info}.");
                                            return "не действительна";
                                        }

                                        if (!chain.ChainStatus.All(x => x.Status == X509ChainStatusFlags.NoError || x.Status == X509ChainStatusFlags.PartialChain))
                                        {
                                            var info = string.Join(", ", chain.ChainStatus.Select(x => (x.Status, x.StatusInformation)));
                                            _logger.Warning($"Проверка цепочки сертификатов вернула ошибку {info}.");
                                            return "не действительна";
                                        }

                                        chain.Reset();
                                    }
                                }
                            }

                            if (!VerifySigners(signersInfos))
                            {
                                return "не действительна";
                            }
                        }

                        return "действительна";
                    }
                    catch (Exception exp)
                    {
                        _logger.Warning($"Ошибка при проверке подписи документа из системы 'Дело'");
                        _logger.Error(exp);
                    }
                }

                return "не действительна";

                bool VerifySigners(SignerInfo[] signersInfos)
                {
                    if (originalSigners.Length > signersInfos.Length)
                    {
                        return false;
                    }

                    foreach (var item in originalSigners)
                    {
                        var (contactId, firstName, middleName, lastName, position, keyIdentifier) = parsedSigners[item];

                        Func<SignerInfo, bool> predicate = x => x.Certificate.Subject.Contains(item);

                        if (!string.IsNullOrEmpty(keyIdentifier))
                        {
                            predicate = x => string.Equals(x.Certificate.Thumbprint, keyIdentifier, StringComparison.OrdinalIgnoreCase);
                        }
                        else if (_signerRegex.Match(item) is Match match && match.Success)
                        {
                            predicate = x =>
                                !string.IsNullOrEmpty(firstName) && x.Certificate.Subject.IndexOf(firstName) >= 0 &&
                                !string.IsNullOrEmpty(middleName) && x.Certificate.Subject.IndexOf(middleName) >= 0 &&
                                !string.IsNullOrEmpty(lastName) && x.Certificate.Subject.IndexOf(lastName) >= 0 &&
                                !string.IsNullOrEmpty(position) && x.Certificate.Subject.IndexOf(position) >= 0;
                        }

                        if (!signersInfos.Any(predicate))
                        {
                            _logger.Warning($"Неудалось найти сертификат для {item}.");
                            return false;
                        }
                    }

                    return true;
                }

                bool VerifySignature(SignedCms cms, bool verifySignatureOnly)
                {
                    try
                    {
                        cms.CheckSignature(verifySignatureOnly);
                        return true;
                    }
                    catch (Exception exp)
                    {
                        _logger.Warning($"Ошибка при проверке подписи документа из системы 'Дело'");
                        _logger.Error(exp);
                        return false;
                    }
                }
            }

            Task CreateAddressee(int contact_id, int role_id, DateTime? out_date = null, string out_registration = null)
            {
                if (contactsSet.Add((contact_id, role_id)))
                {
                    _logger.Info($"Связка документа {document_id} с контактом {contact_id} в роли {role_id}");
                    return _package.CreateAddressee(document_id, contact_id, role_id, out_date, out_registration);
                }
                else
                {
                    _logger.Warning($"Попытка добавить к документу {document_id} дублирующийся контакт {contact_id} с ролью {role_id}");
                    return Task.CompletedTask;
                }
            }
        }
    }
}

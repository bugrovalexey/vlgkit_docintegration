﻿namespace bgTeam.KitVlgDocs.Story
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using bgTeam.DataAccess;
    using bgTeam.KitVlgDocs.DataAccess;

    public class ServerSetMedoDocumentExportStateStory : IStory<ServerSetMedoDocumentExportStateStoryContext, ValueTuple>
    {
        private readonly IAppLogger _logger;
        private readonly IMedoPackage _package;
        private readonly IRepository _repository;
        private readonly ICrudService _crudService;

        public ServerSetMedoDocumentExportStateStory(
            IAppLogger logger,
            IMedoPackage package,
            IRepository repository,
            ICrudService crudService)
        {
            _logger = logger;
            _package = package;
            _repository = repository;
            _crudService = crudService;
        }

        public ValueTuple Execute(ServerSetMedoDocumentExportStateStoryContext context)
            => ExecuteAsync(context).GetAwaiter().GetResult();

        public async Task<ValueTuple> ExecuteAsync(ServerSetMedoDocumentExportStateStoryContext context)
        {
            var identityExId = await _repository.GetAsync<long?>($"SELECT bo_bpdoc.IDENTITYEX_ID from BO_BPDOCUMENT bo_bpdoc WHERE bo_bpdoc.BPDOCUMENT_ID = :DocumentId", new
            {
                context.DocumentId,
            });

            if (identityExId == null)
            {
                throw new InvalidOperationException($"Не найден документ {context.DocumentId}");
            }
            else
            {
                var medo_header_guid = await _package.FindEClassId("Medo.References", "HeaderGUID");
                var medo_external_guid = await _package.FindEClassId("Medo.References", "ExternalGUID");

                if (context.VisibleForExport)
                {
                    await _crudService.ExecuteAsync(GenerateSql(), new { identityExId, medo_header_guid, medo_external_guid });

                    string GenerateSql()
                    {
                        var builder = new StringBuilder()
                            .AppendLine($@"
DELETE
FROM cbo_identitylist
WHERE IDENTITYLIST_ID IN
  (SELECT ilist.IDENTITYLIST_ID
  FROM cbo_identitylist ilist
  INNER JOIN bo_prototype prot
  ON prot.prototype_id = ilist.prototype_id
  WHERE ilist.owner_id = :identityExId
  AND prot.eclass_id IN (:medo_header_guid, :medo_external_guid)
  )");
                        return builder.Replace(Environment.NewLine, "\n").ToString();
                    }
                }
                else
                {
                    var medoIdentitiesIds = await _repository.GetAllAsync<long?>(
    $@"
SELECT ilist.IDENTITYLIST_ID
  FROM cbo_identitylist ilist
  INNER JOIN bo_prototype prot
  ON prot.prototype_id = ilist.prototype_id
  WHERE ilist.owner_id = :identityExId
  AND prot.eclass_id IN (:medo_header_guid, :medo_external_guid)", new
    {
        identityExId,
        medo_header_guid,
        medo_external_guid,
    });

                    if (!medoIdentitiesIds.Any())
                    {
                        await _package.CreateIdentityList(identityExId.Value, medo_header_guid, Guid.NewGuid().ToString());
                        await _package.CreateIdentityList(identityExId.Value, medo_external_guid, Guid.NewGuid().ToString());
                    }
                }
            }

            return default;
        }
    }
}

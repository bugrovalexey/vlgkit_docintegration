﻿using bgTeam.KitVlgDocs.Story;
using Quartz;
using System;
using System.Threading.Tasks;

namespace bgTeam.KitVlgDocs.Delo.ImportService.App
{
    [DisallowConcurrentExecution]
    internal class ImportDeloDocumemtsJob : IJob
    {
        private readonly IStoryBuilder _storyBuilder;

        public ImportDeloDocumemtsJob(IStoryBuilder storyBuilder)
        {
            _storyBuilder = storyBuilder;
        }

        public Task Execute(IJobExecutionContext context)
        {
            return _storyBuilder.Build(new ClientImportDeloDocumentsStoryContext())
                .ReturnAsync<ValueTuple>();
        }
    }
}

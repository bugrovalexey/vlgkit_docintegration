﻿using bgTeam.KitVlgDocs.Story;
using Quartz;
using System;
using System.Threading.Tasks;

namespace bgTeam.KitVlgDocs.ImportService.App
{
    [DisallowConcurrentExecution]
    internal class ImportDocumemtsJob : IJob
    {
        private readonly IStoryBuilder _storyBuilder;

        public ImportDocumemtsJob(IStoryBuilder storyBuilder)
        {
            _storyBuilder = storyBuilder;
        }

        public Task Execute(IJobExecutionContext context)
        {
            return _storyBuilder.Build(new ClientImportMedoDocumentsStoryContext())
                .ReturnAsync<ValueTuple>();
        }
    }
}

function DoBuild {
Param ([string]$Name, [string]$Runtime)
  $old_location = Get-Location
  Set-Location ".\src\$($Name)\"
  if (Test-Path ".\out\") { Remove-Item -LiteralPath ".\out\" -Force -Recurse; }
  Invoke-Expression "dotnet publish --self-contained --configuration Release --output out --runtime $($Runtime)"
  Set-Location $old_location;
  Copy-Item -LiteralPath ".\src\$($Name)\out\" -Destination ".\build\$($Name)\" -Recurse
  Remove-Item -LiteralPath ".\src\$($Name)\out\" -Force -Recurse
}

if (Test-Path ".\build\") { Remove-Item -LiteralPath ".\build\" -Force -Recurse; }
New-Item -ItemType Directory -Force -Path ".\build\"

DoBuild -Name "bgTeam.KItVlgDocs.Web.App" -Runtime "win-x86"
DoBuild -Name "bgTeam.KitVlgDocs.ExportService.App" -Runtime "win"
DoBuild -Name "bgTeam.KitVlgDocs.ImportService.App" -Runtime "win"
DoBuild -Name "bgTeam.KitVlgDocs.Delo.ImportService.App" -Runtime "win-x86"

If (Test-path "build.zip") { Remove-item "build.zip" -Force }
Add-Type -Assembly "System.IO.Compression.FileSystem"
[IO.Compression.ZipFile]::CreateFromDirectory(".\build\", "build.zip") 
Remove-Item -LiteralPath ".\build\" -Force -Recurse
namespace bgTeam.KitVlgDocs.Tests
{
    using System.IO;
    using System.Text;
    using System.Xml.Serialization;
    using Xunit;

    public class XmlDeserializationTest
    {
        public XmlDeserializationTest()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        [Fact]
        public void DesirialazeFromString()
        {
            var name = "RKPD_2370174_converted.pdf";
            var uid = "f302b0a7-2426-40f4-b99f-8de51659a715";

            var xml = $@"
<?xml version=""1.0"" encoding=""windows-1251""?>
<xdms:container xdms:uid=""{uid}"" xdms:version=""1.0"" xmlns:xdms=""http://minsvyaz.ru/container"">
  <xdms:test>1</xdms:test>
  <xdms:document xdms:localName=""{name}"">
    <xdms:pagesQuantity>2</xdms:pagesQuantity>
  </xdms:document>
</xdms:container>".Trim();

            var serializer = new XmlSerializer(typeof(Domain.Xml.Container));

            using (var mem = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
            {
                var doc = (Domain.Xml.Container)serializer.Deserialize(mem);

                Assert.Equal(uid, doc.Uid);
                Assert.Equal(name, doc.Document.LocalName);
            }
        }
    }
}

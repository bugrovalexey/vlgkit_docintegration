﻿using bgTeam.KitVlgDocs.Common;
using bgTeam.KitVlgDocs.Common.Impl;
using bgTeam.KitVlgDocs.Domain.Xml;
using bgTeam.KitVlgDocs.Story.Common;
using iTextSharp.text.pdf;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace bgTeam.KitVlgDocs.Tests
{
    public class UtilitiesTests
    {
        [Fact]
        public void StorageMove()
        {
            var storage1 = new PhysicalDocumentStorage(new Uri("file://E:/Test1"));
            var storage2 = new PhysicalDocumentStorage(new Uri("file://E:/Test2"));
            var directory1 = storage1.CreateDirectory("Test1");
            storage2.Move(directory1, "Test1");

            Assert.True(Directory.Exists("E:/Test2/Test1/"));
        }

        [Fact]
        public void StorageMove1()
        {
            var storage1 = new PhysicalDocumentStorage(new Uri("file://E:/Test1"));
            var storage2 = new PhysicalDocumentStorage(new Uri("file://E:/Test2"));
            var directory1 = storage1.CreateDirectory("Test1");
            storage2.Move(directory1, "#Test1", replace: true);

            Assert.True(Directory.Exists("E:/Test2/#Test1/"));
        }

        [Fact]
        public void StorageMove2()
        {
            var storage1 = new PhysicalDocumentStorage(new Uri("file://E:/Test1"));
            var storage2 = new PhysicalDocumentStorage(new Uri("file://E:/Test2"));
            var directory1 = storage1.CreateDirectory("Test1");
            File.WriteAllText(Path.Combine(directory1.Root.LocalPath, "Test.txt"), "Test123");
            var file = directory1.GetDocuments(SearchOption.AllDirectories).FirstOrDefault();

            storage2.Move(file, "#Test1.txt", replace: true);

            Assert.True(File.Exists("E:/Test2/#Test1.txt"));
        }

        [Fact]
        public void StorageMove3()
        {
            var storage1 = new PhysicalDocumentStorage(new Uri("file://E:/Test1"));
            var storage2 = new PhysicalDocumentStorage(new Uri("file://E:/Test2"));
            var directory1 = storage1.CreateDirectory("Test1");
            storage2.Move(directory1, "#Test1 1234", replace: true);

            Assert.True(Directory.Exists("E:/Test2/#Test1 1234/"));
        }

        [Fact]
        public void StorageMove4()
        {
            var storage1 = new MemoryDocumentStorage(new Uri("mem://Test1/"));
            var storage2 = new MemoryDocumentStorage(new Uri("mem://Test2/"));
            var directory1 = storage1.CreateDirectory("#Test1") as IWritableDocumentStorage;
            _ = directory1.Create("#test123.txt");
            storage2.Move(directory1, "#Test1 1234", replace: true);

            var document = storage2.GetDocuments(SearchOption.AllDirectories).FirstOrDefault();

            Assert.NotNull(document);
            Assert.Equal("/#Test1 1234/#test123.txt", document.Path.LocalPath);
        }

        [Fact]
        public void CreateDirectory()
        {
            var storage1 = new PhysicalDocumentStorage(new Uri("file://E:/Test1"));
            var directory1 = storage1.CreateDirectory("#Test1");

            Assert.True(Directory.Exists("E:/Test1/#Test1/"));
        }

        [Fact]
        public void CreateDirectory1()
        {
            var storage1 = new PhysicalDocumentStorage(new Uri("file://E:/Test1"));
            var directory1 = storage1.CreateDirectory("#Test1 Test1");

            Assert.True(Directory.Exists("E:/Test1/#Test1 Test1/"));
        }

        [Fact]
        public void CreateDirectory2()
        {
            var storage1 = new PhysicalDocumentStorage(new Uri("file://E:/Test1"));
            var directory1 = storage1.CreateDirectory("#Test1 Test1/Test1");

            Assert.True(Directory.Exists("E:/Test1/#Test1 Test1/Test1/"));
        }

        [Fact]
        public void GenerateImageTest()
        {
            const string OutName1 = nameof(GenerateImageTest) + "_stamp.png";
            const string OutName2 = nameof(GenerateImageTest) + "_number.png";

            var defenitions = new EmbeddedResources();

            var storage = new MemoryDocumentStorage(new Uri("mem://Test"));

            var context = new ImageGenStoryContext()
            {
                Width = 268,
                Height = 144,
                Storage = storage,
                Document = nameof(GenerateImageTest),
                Definition = defenitions.SignatureStamp,
                Parameters = new
                {
                    NotBefore = DateTime.Now,
                    NotAfter = DateTime.Now.AddYears(2),
                    Serial = "E358EFA489F58062F10DD7316B65649E",
                    Subject = "Иванов Иван Иванович"
                }
            };

            var story = new ImageGenStory();

            {
                var result = story.Execute(context);
                Assert.Equal(OutName1, result.Name);
                var bmp = new Bitmap(result.CreateReadStream());
                bmp.Save(OutName1);
                Assert.Equal(268, bmp.Width);
                Assert.Equal(144, bmp.Height);
            }

            context.Width = 195;
            context.Height = 16;
            context.Definition = defenitions.RegistrationStamp;
            context.Parameters = new
            {
                Number = "22-08-06/1042",
                Date = DateTime.Now,
            };

            {
                var result = story.Execute(context);
                Assert.Equal(OutName2, result.Name);
                var bmp = new Bitmap(result.CreateReadStream());
                bmp.Save(OutName2);
                Assert.Equal(195, bmp.Width);
                Assert.Equal(16, bmp.Height);
            }
        }

        [Fact]
        public void LoadImageDefinitionsFromResources()
        {
            var defs = new EmbeddedResources();

            Assert.NotNull(defs.SignatureStamp);
            Assert.NotNull(defs.RegistrationStamp);
        }

        [Fact]
        public void MemoryDocumentStorageSearchTest()
        {
            var storage = new MemoryDocumentStorage(new Uri("mem://Test"));

            var file01 = storage.Create("Test.01");
            var file02 = storage.Create("Testing.02");
            var file03 = storage.Create("Testing.12");

            var documents = storage.GetDocuments("*ing.0*", SearchOption.AllDirectories).ToArray();

            Assert.Single(documents);

            Assert.Equal("Testing.02", documents[0].Name);
            Assert.Equal(new Uri("mem://Test/Testing.02"), documents[0].Path);
        }

        [Fact]
        public void MemoryDocumentStorageReadWriteTest()
        {
            var storage = new MemoryDocumentStorage(new Uri("mem://Test"));

            var file01 = storage.Create("Test.01");
            var data01 = new byte[] { 1, 2, 3 };
            using (var stream = file01.CreateWriteStream())
            {
                stream.Write(data01, 0, data01.Length);
            }
            using (var stream = file01.CreateReadStream())
            {
                var buffer = new byte[data01.Length];
                Assert.Equal(data01.Length, stream.Length);
                stream.Read(buffer, 0, buffer.Length);
                Assert.Equal(data01, buffer);
            }
            var file02 = storage.Create("Testing.02");
            var data02 = new byte[] { 1, 2, 3, 4 };
            using (var stream = file02.CreateWriteStream())
            {
                stream.Write(data02, 0, data02.Length);
            }
            using (var stream = file02.CreateReadStream())
            {
                var buffer = new byte[data02.Length];
                Assert.Equal(data02.Length, stream.Length);
                stream.Read(buffer, 0, buffer.Length);
                Assert.Equal(data02, buffer);
            }
            var file03 = storage.Create("Testing.12");
            var data03 = new byte[] { 1, 2, 3, 4, 5 };
            using (var stream = file03.CreateWriteStream())
            {
                stream.Write(data03, 0, data03.Length);
            }
            using (var stream = file03.CreateReadStream())
            {
                var buffer = new byte[data03.Length];
                Assert.Equal(data03.Length, stream.Length);
                stream.Read(buffer, 0, buffer.Length);
                Assert.Equal(data03, buffer);
            }
        }

        [Fact]
        public async Task StampDocumentTest()
        {
            var storage = new MemoryDocumentStorage(new Uri("mem://test"));
            var resources = new EmbeddedResources();

            var signatureStampData = new StampData
            {
                Top = 0.8635f,
                Left = 0.3571f,
                Width = 256,
                Height = 142,
                Page = 1
            };

            var registrationStampData = new StampData
            {
                Top = 0.2525f,
                Left = 0.1428f,
                Width = 195,
                Height = 16,
                Page = 1
            };

            var documentInfo = storage.Create("test.pdf");

            var pdfDocument = LoadDocument();
            var firstPage = pdfDocument.GetPageN(1);

            var rect = pdfDocument.GetPageSize(firstPage);
            var unit = firstPage.GetAsNumber(PdfName.USERUNIT)?.FloatValue ?? 72f;

            var registrationStamp = await new ImageGenStory().ExecuteAsync(new ImageGenStoryContext
            {
                Document = documentInfo.Name,
                Definition = resources.RegistrationStamp,
                Width = registrationStampData.Width,
                Height = registrationStampData.Height,
                Storage = storage,
                Parameters = new
                {
                    Number = "22-08-06/1042",
                    Date = new DateTime(2019, 4, 2),
                },
            });

            var registrationStampTLX = (short)(ImageHelper.ToMillimeters(rect.Width, unit) * registrationStampData.Left);
            var registrationStampTLY = (short)(ImageHelper.ToMillimeters(rect.Height, unit) * registrationStampData.Top);

            var signatureStamp = await new ImageGenStory().ExecuteAsync(new ImageGenStoryContext
            {
                Document = documentInfo.Name,
                Definition = resources.SignatureStamp,
                Width = signatureStampData.Width,
                Height = signatureStampData.Height,
                Storage = storage,
                Parameters = new
                {
                    Serial = "640ab2bae07bedc4c163f679a746f7ab7fb5d1fa",
                    Subject = "Test Test Test",
                    NotAfter = new DateTime(2020, 1, 1),
                    NotBefore = new DateTime(2019, 1, 1),
                },
            });

            var signatureStampTLX = (short)(ImageHelper.ToMillimeters(rect.Width, unit) * signatureStampData.Left);
            var signatureStampTLY = (short)(ImageHelper.ToMillimeters(rect.Height, unit) * signatureStampData.Top);

            StampHelper.Watermark(documentInfo, signatureStamp, x => x.Skip(signatureStampData.Page - 1).First(), (x, y, z) =>
            {
                var w = ImageHelper.ToPixels(signatureStampTLX, z);
                var h = y.Height - ImageHelper.ToPixels(signatureStampTLY, z) - x.Height;
                return new RectangleF(w, h, x.Width, x.Height);
            });

            StampHelper.Watermark(documentInfo, registrationStamp, x => x.Skip(registrationStampData.Page - 1).First(), (x, y, z) =>
            {
                var w = ImageHelper.ToPixels(registrationStampTLX, z);
                var h = y.Height - ImageHelper.ToPixels(registrationStampTLY, z) - x.Height;
                return new RectangleF(w, h, x.Width, x.Height);
            });

            using (var output = File.OpenWrite(documentInfo.Name))
            {
                using (var dStream = documentInfo.CreateReadStream())
                {
                    dStream.CopyTo(output);
                }
            }

            PdfReader LoadDocument()
            {
                using (var @out = documentInfo.CreateWriteStream())
                {
                    var bytes = File.ReadAllBytes(@"F:\Soprovod_VLG.pdf");
                    @out.Write(bytes, 0, bytes.Length);
                }

                using (var inputCopy = new MemoryStream())
                {
                    using (var input = documentInfo.CreateReadStream())
                    {
                        input.CopyTo(inputCopy);
                    }

                    return new PdfReader(inputCopy.ToArray());
                }
            }
        }

        [Fact]
        public async Task StampDocumentTest1()
        {
            var document  = File.ReadAllBytes("F:/Наложение штампов/53325478/pdf_33195013.pdf");

            var regStamp = new Stamp
            {
                Position = new Position
                {
                    Page = "1",
                    TopLeft = new Coordinate
                    {
                        X = 35,
                        Y = 86,
                    },
                    Dimension = new Dimension
                    {
                        H = "3",
                        W = "66",
                    }
                }
            };

            var signStamp = new Stamp
            {
                Position = new Position
                {
                    Page = "2",
                    TopLeft = new Coordinate
                    {
                        X = 73,
                        Y = 112,
                    },
                    Dimension = new Dimension
                    {
                        H = "34",
                        W = "79",
                    }
                }
            };

            var regStampData = File.ReadAllBytes("F:/Наложение штампов/53325478/33195013_0_number.png");
            var signStampData = File.ReadAllBytes("F:/Наложение штампов/53325478/33195013_0_stamp.png");

            document = StampHelper.Watermark(document, regStampData, x => x.Skip(int.Parse(regStamp.Position.Page) - 1).First(), (x, y, z) =>
            {
                var tw = ImageHelper.ToPixels(int.Parse(regStamp.Position.Dimension.W), z);
                var th = ImageHelper.ToPixels(int.Parse(regStamp.Position.Dimension.H), z);
                var tx = ImageHelper.ToPixels(regStamp.Position.TopLeft.X, z);
                var ty = y.Height - ImageHelper.ToPixels(regStamp.Position.TopLeft.Y, z) - th;
                return new RectangleF(tx, ty, tw, th);
            });

            document = StampHelper.Watermark(document, signStampData, x => x.Skip(int.Parse(signStamp.Position.Page) - 1).First(), (x, y, z) =>
            {
                var tw = ImageHelper.ToPixels(int.Parse(signStamp.Position.Dimension.W), z);
                var th = ImageHelper.ToPixels(int.Parse(signStamp.Position.Dimension.H), z);
                var tx = ImageHelper.ToPixels(signStamp.Position.TopLeft.X, z);
                var ty = y.Height - ImageHelper.ToPixels(signStamp.Position.TopLeft.Y, z) - th;
                return new RectangleF(tx, ty, tw, th);
            });

            File.WriteAllBytes($"F:/Наложение штампов/53325478/{Guid.NewGuid()}.pdf", document);
        }
    }
}

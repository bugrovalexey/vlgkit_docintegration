﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace bgTeam.KitVlgDocs.Tests
{
    public static class Helper
    {
        public static string Unpack(string input)
        {
            var data = Convert.FromBase64String(input);
            using (var mem = new MemoryStream(data))
            {
                using (var zip = new DeflateStream(mem, CompressionMode.Decompress))
                {
                    using (var reader = new StreamReader(zip, Encoding.UTF8))
                        return reader.ReadToEnd();
                }
            }
        }
    }
}

﻿using bgTeam.KitVlgDocs.Common.ImageGen;
using bgTeam.KitVlgDocs.Common.ImageGen.Components;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace GenImage
{
    static class Program
    {
        class TestParameters01
        {
            public string Serial { get; set; }

            public string Subject { get; set; }

            public DateTime NotAfter { get; set; }

            public DateTime NotBefore { get; set; }

            public string Number { get; set; }

            public DateTime Date { get; set; }
        }
        class TestParameters02
        {
            public string Number { get; set; }

            public DateTime Date { get; set; }
        }

        static void Main(string[] args)
        {
            var definition01 = new ImageDefinition()
            {
                Name = "stamp",
                Size = new Size(368, 198),
                BackgroundColor = Color.Transparent,
                Components = new ImageComponent[]
                {
                    new RoundedBorderComponent
                    {
                        Color = Color.Black,
                        Bounds = new Rectangle(1, 1, 365, 195),
                        LineWidth = 2f,
                        Radius = 6
                    },
                    new BitmapComponent
                    {
                        Image = new Bitmap(@"sigil.png"),
                        Bounds = new Rectangle(12, 12, 77, 80)
                    },
                    new TextConstantComponent
                    {
                        Color = Color.Black,
                        Text = "ДОКУМЕНТ ПОДПИСАН",
                        Bounds = new RectangleF(100f, 26f, 260f, 18f),
                        Font = new Font("Arial", 12f, FontStyle.Bold),
                        Layout = new StringFormat()
                        {
                            Alignment = StringAlignment.Center,
                            LineAlignment = StringAlignment.Center
                        }
                    },
                    new TextConstantComponent
                    {
                        Color = Color.Black,
                        Text = "ЭЛЕКТРОННОЙ ПОДПИСЬЮ",
                        Bounds = new RectangleF(100f, 54f, 260f, 18f),
                        Font = new Font("Arial", 12f, FontStyle.Bold),
                        Layout = new StringFormat()
                        {
                            Alignment = StringAlignment.Center,
                            LineAlignment = StringAlignment.Center
                        }
                    },
                    new TextConstantComponent
                    {
                        Color = Color.Black,
                        Text = "Сертификат",
                        Bounds = new RectangleF(12f, 100f, 108f, 36f),
                        Font = new Font("Arial", 12f, FontStyle.Bold),
                        Layout = new StringFormat()
                        {
                            Alignment = StringAlignment.Near,
                            LineAlignment = StringAlignment.Near
                        }
                    },
                    new DataBindingComponent
                    {
                        Color = Color.Black,
                        Format = "{0}",
                        Bounds = new RectangleF(120f, 100f, 250f, 36f),
                        Font = new Font("Arial", 12f, FontStyle.Bold),
                        Property = nameof(TestParameters01.Serial),
                        Layout = new StringFormat()
                        {
                            Alignment = StringAlignment.Near,
                            LineAlignment = StringAlignment.Near
                        }
                    },
                    new TextConstantComponent
                    {
                        Color = Color.Black,
                        Text = "Владелец",
                        Bounds = new RectangleF(12f, 136f, 92f, 36f),
                        Font = new Font("Arial", 12f, FontStyle.Bold),
                        Layout = new StringFormat()
                        {
                            Alignment = StringAlignment.Near,
                            LineAlignment = StringAlignment.Near
                        }
                    },
                    new DataBindingComponent
                    {
                        Color = Color.Black,
                        Format = "{0}",
                        Bounds = new RectangleF(104f, 136f, 258f, 36f),
                        Font = new Font("Arial", 12f, FontStyle.Bold),
                        Property = nameof(TestParameters01.Subject),
                        Layout = new StringFormat()
                        {
                            Alignment = StringAlignment.Near,
                            LineAlignment = StringAlignment.Near
                        }
                    },
                    new MultyDataBindingComponent
                    {
                        Color = Color.Black,
                        Format = "Действителен  с  {0:dd.MM.yyyy}  по  {1:dd.MM.yyyy}",
                        Bounds = new RectangleF(12f, 174f, 354f, 18f),
                        Font = new Font("Arial", 12f, FontStyle.Bold),
                        Properties = new []
                        {
                            nameof(TestParameters01.NotBefore),
                            nameof(TestParameters01.NotAfter)
                        },
                        Layout = new StringFormat()
                        {
                            Alignment = StringAlignment.Near,
                            LineAlignment = StringAlignment.Center
                        }
                    }
                }
            };

            var definition02 = new ImageDefinition()
            {
                Name = "number",
                Size = new Size(275, 25),
                BackgroundColor = Color.Transparent,
                Components = new ImageComponent[]
                {
                    new DataBindingComponent
                    {
                        Color = Color.Black,
                        Format = "{0:dd.MM.yyyy}",
                        Bounds = new RectangleF(0f, 0f, 135f, 25f),
                        Font = new Font("Arial", 14f, FontStyle.Regular),
                        Property = nameof(TestParameters02.Date),
                        Layout = new StringFormat()
                        {
                            Alignment = StringAlignment.Center,
                            LineAlignment = StringAlignment.Center
                        }
                    },
                    new DataBindingComponent
                    {
                        Color = Color.Black,
                        Format = "{0}",
                        Bounds = new RectangleF(170f, 0f, 100f, 25f),
                        Font = new Font("Arial", 14f, FontStyle.Regular),
                        Property = nameof(TestParameters02.Number),
                        Layout = new StringFormat()
                        {
                            Alignment = StringAlignment.Center,
                            LineAlignment = StringAlignment.Center
                        }
                    }
                }
            };

            File.WriteAllText(@"E:\def01.json", ImageDefinition.ToJson(definition01));

            File.WriteAllText(@"E:\def02.json", ImageDefinition.ToJson(definition02));

            GenerateESImage(LoadCertificate(), definition02);
        }

        static string Pack(string input)
        {
            using (var mem = new MemoryStream())
            {
                using (var zip = new DeflateStream(mem, CompressionLevel.Optimal, true))
                {
                    using (var writer = new StreamWriter(zip, Encoding.UTF8))
                        writer.Write(input);
                }
                return Convert.ToBase64String(mem.ToArray());
            }
        }

        static void GenerateESImage(X509Certificate2 certificate, ImageDefinition definition)
        {
            //SN = Леньчук, G = Дмитрий Васильевич

            string GetSubject(string @default = null)
            {
                var data = certificate.Subject.Split(new[] { ", ", "=" }, StringSplitOptions.RemoveEmptyEntries);
                var index01 = Array.IndexOf(data, "SN");
                var index02 = Array.IndexOf(data, "G");
                var result = $"{(index01 > 0 ? data[index01 + 1] : string.Empty)} {(index02 > 0 ? data[index02 + 1] : string.Empty)}".Trim();

                return string.IsNullOrEmpty(result) ? @default : result;
            }

            var context = new TestParameters01
            {
                Serial = certificate.SerialNumber,
                Subject = GetSubject("Леньчук Дмитрииииииий Васильевич"),
                NotAfter = certificate.NotAfter,
                NotBefore = certificate.NotBefore,

                Number = "01-28/1675",
                Date = new DateTime(2017, 02, 10)
            };

            var bmp = new Bitmap(definition.Size.Width, definition.Size.Height);

            using (var g = Graphics.FromImage(bmp))
            {
                g.Clear(definition.BackgroundColor);
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.TextRenderingHint = TextRenderingHint.AntiAlias;
                foreach (var item in definition.Components)
                    item.Draw(context, g);
            }
            bmp.Save("E:\\test.png", System.Drawing.Imaging.ImageFormat.Png);
        }

        private static X509Certificate2 LoadCertificate()
        {
            using (var store = new X509Store(StoreLocation.CurrentUser))
            {
                store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);
                var result = store.Certificates.Find(X509FindType.FindBySubjectName, "Тест", false);
                if (result.Count > 0)
                    return result[0];
                throw new InvalidOperationException("Сертификат 'Тест' не найден, в хранилище");
            }
        }
    }
}
